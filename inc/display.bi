'#include once "fbmld.bi"
#include once "GL/gl.bi"
#include once "GL/glext.bi"
#include once "GL/glu.bi"
#include once "fbgfx.bi"
#include once "global_constants.bi"
#include once "mathfuncf.bi"
#include once "image.bi"
#include once "glconstants.bi"



namespace ndisplay
	
	type render_target_struct

		fBuffer as GLUINT
		dBuffer as GLUINT
		texture as GLUINT
		width as uinteger
		height as uinteger

		declare function create ( byref w as uinteger, byref h as uinteger, byref has_depth as integer, byref has_stencil as integer, byref is_shadow as integer = 0 ) as integer
		declare sub destroy()
		declare sub lock()
		declare sub unlock()

	end type
	
	
	type Display_Struct

		W 			as integer
		H 			as integer
		w2 		as integer
		h2 		as integer
		R_Bits  	as uinteger
		G_Bits  	as uinteger
		B_Bits  	as uinteger
		A_Bits  	as uinteger
		D_Bits  	as uinteger
		S_Bits  	as uinteger
		BPP		as integer
		MODE 		as uinteger
		GlVer 	as zstring *32
		pMatrix	as nMathf.Matrix
		biasMatrix	as nMathf.Matrix
		'lpMatrix as nMathf.Matrix
		'lvMatrix	as nMathf.Matrix
		as single FOV, Aspect, zNear, zFar
		stencil_cube as gluint
		billboard	 as gluint
		
		shadowBuffer	as render_target_struct
		
		pickBuffer		as render_target_struct
		
	end type


	


	type Light_Struct

		Ambient           as nimage.RGBAlpha
		Diffuse           as nimage.RGBAlpha
		Specular          as nimage.RGBAlpha
		Position          as nmathf.vec4f
		Direction         as nmathf.vec3f
		pMatrix   			as nmathf.matrix
		vMatrix       		as nmathf.matrix

	end type


	type font_struct
		
		dList 	as gluint
		texture	as gluint
		scale		as single
		kern		as single
		
		declare constructor()
		declare constructor( byval scale as single )
		declare destructor()
		
		'declare sub oPrint( Byval Strng As String, Byval X As Single, Byval Y As Single, Byval R As single = 1.0, Byval G As single = 1.0, Byval B As single = 1.0, byval s as single = 1.0 )
		declare sub oPrint( byval Strng As String, Byval X As Single, Byval Y As Single, Byval R As single = 1.0, Byval G As single = 1.0, Byval B As single = 1.0, byval a as single = 1.0, byval sx as single = 0.0, byval sy as single = 0.0 ) 
		declare sub load( byref fName as string )
		declare sub oBegin()
		declare sub oEnd()
		
	end type

	declare sub load_default_shaders()
	declare sub Create_Shadow_Buffer()
	declare sub enable_shadow_projection( byref light as Light_Struct, byref lookMatrix as nMathf.Matrix )
	declare sub disable_shadow_projection()
	declare function Init_GL_Window( byval W as integer, byval H as integer, byval BPP as integer, byval Num_buffers as integer, byref Num_Samples as integer, byval Fullscreen as integer ) as integer
	declare function Init_Shader( byref File_Name as string, byref Shader_Type as integer )as GlHandleARB
	declare sub Gather_Extensions()
	declare sub build_stencil_cube( byref List as gluint )
	declare sub init_light( byref index as integer, byref Light as Light_Struct, byref ambient as nimage.RGBALpha, byref diffuse as nimage.RGBAlpha, byref specular as nimage.RGBAlpha, byref position as nmathf.vec4f, byref direction as nmathf.vec3f )
	declare sub Make_Font_DList( Byref DList As GlUINT, byval Wtiles As Single, Byval Htiles As Single, Byval t_Scale As Integer )
	Declare Sub GlPrint( Byref Strng As String, Byval Texture As Gluint, Byval Text_List As GLUINT, Byval X As Single, Byval Y As Single, Byval R As Single, Byval G As Single, Byval B As single )
	declare sub Set_Ortho( Byval W As Integer, Byval H As Integer )
	declare sub Drop_Ortho()
	declare sub Begin_Shadow_Pass()
	declare sub End_Shadow_Pass( byref shadowcol as nimage.rgbAlpha = type(0f,0f,0f,0.5) )
	declare sub waitKey( byref key as uinteger ) 
	
	
end namespace
