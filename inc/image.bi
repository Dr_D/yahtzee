'#include once "fbmld.bi"
#include once "GL/gl.bi"
#include once "GL/glext.bi"
#include once "GL/glu.bi"
#include once "fbpng.bi"
#include once "global_constants.bi"
#include once "glconstants.bi"


namespace nimage

	type rgbAlpha
		
		as single R,G,B,A
		
	end type


	type texture_struct

		name 	as string*32
		img 	as GLUINT

	end type

	common shared as rgbAlpha BLACK
	common shared as rgbAlpha DGRAY
	common shared as rgbAlpha MGRAY
	common shared as rgbAlpha LGRAY

	declare function load_texture( byref filename as string, Texture() as Texture_Struct, byref force_clamp as Integer, byref searchDir As String = "res/texture/" ) as integer

end namespace
