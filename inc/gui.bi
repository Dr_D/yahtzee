#include once "fbgfx.bi"
#include once "global_constants.bi"
#include once "glconstants.bi"
#include once "image.bi"
#include once "display.bi"
#include once "mathfuncf.bi"

extern display as ndisplay.display_struct

namespace ngui
	
	
	enum guiButtonFlag
	
		guiCLOSE
		guiCOMMAND
		guiOKCANCEL
		
	end enum
	
	
	
	type label_struct
		
		posit 	as nmathf.vec2f
		size 		as nmathf.vec2f
		caption  as string*128
		wColor 	as nimage.rgbalpha
		fColor 	as nimage.rgbalpha
		font		as ndisplay.font_struct ptr
		parent	as any ptr
		
	end type
	
	
	type button_struct
		
		index		as uinteger
		visible	as uinteger = true
		posit 	as nmathf.vec2f
		size 		as nmathf.vec2f
		flag		as guiButtonFlag
		clicked 	as integer
		set_close as integer
		wColor 	as nimage.rgbalpha
		fColor 	as nimage.rgbalpha
		font		as ndisplay.font_struct ptr
		caption 	as string*128
		user_data as any ptr
		
		parent	as any ptr
		
		child_label		as integer
		
		declare sub bDraw()
		
		declare sub SetUserCallback( byval callback as any ptr )
		declare sub SetUserData( byval uData as any ptr )
		dim OnClick as function( byval as button_struct ptr ) as integer
		
	end type
	
	type text_box_struct
		
		posit 	as nmathf.vec2f
		size 		as single
		clicked 	as integer
		wColor 	as nimage.rgbalpha
		fColor 	as nimage.rgbalpha
		font		as ndisplay.font_struct ptr
		tString	as string
		pString	as string
		aString	as string
		typing 	as integer
		curspos 	as integer
		
		declare sub tDraw()
		declare sub TrimText( byval tpos as nmathf.vec2f )
		declare function ProcessKeyBoardInput( byref event as FB.EVENT ) as integer
		dim OnClick as function( byval as button_struct ptr ) as integer
		
	end type
	
	
	type window_struct
		
		index			as integer
		
		killTime		as double
		
		isTimed		as integer
		
		isAlert		as integer = false
		
		canMove		as integer = true
		
		visible		as integer = true
		
		button(any)	as button_struct
		
		tbox(any)	as text_box_struct
		
		label(any)	as label_struct
		
		parent		as any ptr
		
		closing  as integer
		do_close	as integer
		posit 	as nmathf.vec2f
		size		as nmathf.vec2f
		clicked	as integer
		wColor	as nimage.rgbalpha
		fColor	as nimage.rgbalpha
		
		is_focus	as integer
		dimmer	as single
		
		title 	as string
		font		as ndisplay.font_struct ptr
		dragging	as integer
		typing	as integer
		type_xor	as single
		type_xor_delta as single
		
		declare sub BeginDraw()
		declare sub EndDraw()
		declare sub wDraw( trimmed() as string ) 
		declare sub wPrint( byref tString as string, byref posit as nmathf.vec2f )
		
		declare function AddButton( byref posit as nmathf.vec2f, byref size as nmathf.vec2f, byref caption as string, byref wColor as nimage.rgbalpha = type(0,0,0,0), byval callback as any ptr ) as integer
		declare sub RemoveButton( byval id as integer )
		declare sub AddCloseButton()
		
		declare function AddTextBox( byref posit as nmathf.vec2f, byref size as single, byref caption as string, byref wColor as nimage.rgbalpha, byref fColor as nimage.rgbalpha ) as integer
		declare sub RemoveTextBox( byval id as integer )
		declare sub DeselectAllTboxes()
		
		declare function AddLabel( byref posit as nmathf.vec2f, byref size as nmathf.vec2f, byref caption as string, byref wColor as nimage.rgbalpha = type(0,0,0,0), byref fColor as nimage.rgbalpha = type(0,0,0,0) ) as integer
		declare sub RemoveLabel( byval id as integer )
		
	end type
	
	
		
	
	type gui_struct
		
		index			as integer
		
		wColor		as nimage.rgbalpha = type(0f,0f,.75,1f)
		fColor		as nimage.rgbalpha = type(1,1,1,1)
		
		font			as ndisplay.font_struct ptr
		
		wind(any)	as window_struct
		zOrder(any)	as integer
		
		focus			as integer
		
		declare sub SetFont( byref font as ndisplay.font_struct ptr )
		declare function AddWindow( byval posit as nmathf.vec2f, byval size as nmathf.vec2f, byval wColor as nimage.rgbalpha, byval fColor as nimage.rgbalpha ) as integer
		declare sub RemoveWindow( byval id as integer )
		
		declare sub Update( byval time_step as single )
		declare sub zSort( byval w as integer )
		declare sub update_entire_god_damn_thing()
		declare function PointInBox( byval pnt as nmathf.vec2f, byval posit as nmathf.vec2f, byval size as nmathf.vec2f ) as integer
		
		
		declare function GetWindowByTitle( byref wTitle as string ) as window_struct ptr
		
		declare constructor()
		declare destructor()
		
	end type
	
	
	declare sub DrawGuiBox( byval posit as nmathf.vec2f, byval size as nmathf.vec2f, byval fcolor as nimage.rgbalpha, byval is3d as integer, byval rev as integer, byval dimmed as single )
	declare sub DrawMenuBar( byval posit as nmathf.vec2f, byval size as nmathf.vec2f, byval sColor as nimage.rgbalpha, byval eColor as nimage.rgbalpha, byval dimmed as single )
	
	declare function OnCloseDefault( byval button as button_struct ptr ) as integer
	declare function OnClickDefault( byval button as button_struct ptr ) as integer
	
	declare function valid_char( byval ascii as integer ) as integer
	
end namespace


