'#include once "fbmld.bi"
#ifndef __hash_bi__
#define __hash_bi__

namespace nhash

	type free_func_t as sub _
	( _
	byval addr as any ptr _
	)

	type ht_node_t
		s     as zstring ptr
		id    as integer
		_data as any ptr
	end type

	type ht_t
		public:
		declare constructor _
		( _
		byval _size_ as integer _
		)
		declare destructor _
		( _
		)
		
		declare sub hkill _
		( _
		)
		
		declare function add _
		( _
		byval s     as zstring ptr, _
		byval _data as any ptr _
		) as ht_node_t ptr
		declare function lookup _
		( _
		byval s as zstring ptr _
		) as ht_node_t ptr
		declare function lookup _
		( _
		byval id as integer _
		) as ht_node_t ptr
		declare sub set_free _
		( _
		byval _free_func_ as free_func_t _
		)
		private:
		declare function hash _
		( _
		byval s as zstring ptr _
		) as uinteger
		ht        as ht_node_t ptr
		htid      as ht_node_t ptr ptr
		count     as integer
		size      as integer
		free_func as free_func_t
	end type

end namespace

#endif '__hash_bi__

