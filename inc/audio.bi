#include once "fmod.bi"

namespace nAudio

	type sample_struct

		name		as string
		wav		as FSound_Sample ptr

	end type
	
	declare function LoadSound( byref filename as string, sample() as sample_struct, byref index as integer, byref looping as integer ) as sample_struct
	
end namespace
