#include once "global_constants.bi"
#include once "fmod.bi"
#include once "fbgfx.bi"
#include once "mathfuncf.bi"
#include once "model.bi"
#include once "image.bi"
#include once "display.bi"
#include once "physics.bi"
#include once "physics_callbacks.bi"
#include once "hash.bi"
#include once "gui.bi"
#include once "game.bi"

using ndisplay
using nmodel
using nimage
using nmathf
using nphysics
using nhash
using ngui


type die_struct

	as integer			curFace

	as NewtonBody ptr	nBody

	as integer			isLocked

	as matrix			oldMatrix

	as integer 			holderID

end type


type score_node

	as integer 		locked
	
	as integer		lIndex
	
	as integer		bIndex
	
	as uinteger 	value

	as rgbAlpha		col

	as string * 8	name

end type


type score_card
	
	as string * 10	name

	as uinteger 	score

	as score_node 	upper(7)

	as score_node 	lower(8)
	
	as integer		yahtzeeCnt
	
	as integer 		hasClicked
	
	as string 		sTitle
	
	as integer 		visible
	
	as integer		isAlive
	
end type


type die_holder

	holdPos		as vec3f

	full			as integer

end type


type game_struct extends world_struct
	
	as integer 				started
	
	as integer				killGame
	
	as integer				gameOver
	
	as integer				doAdvance
	
	as integer				playerRolled
	
	as integer				hasMainMenu
	
	as font_struct			myFont	= 16
	
	as gui_struct 			myGui
	
	as score_card 			tempCard

	as score_card			plScore(any)

	as uinteger				curPlayer

	as integer				rollCount

	as integer				checkRoll

	as integer				pMode

	as die_holder			holder(4)

	as die_struct			die(4)

	declare sub roll_dice()

	declare sub hold_all_dice()

	declare sub clear_holders()
	
	declare sub clear_tempCard()

	declare sub add_die_to_holder( byref id as integer )

	declare sub remove_die_from_holder( byref id as integer )

	declare function get_free_holder() as integer

	declare function dice_stopped() as integer

	declare function get_die_face( byref die as die_struct ) as integer

	declare function create_scorecard() as integer
	
	declare sub update_scorecard()
	
	declare sub process_score()
	
	declare sub init()
	
	declare function advance() as integer
	
	declare function new_game() as integer
	
	declare function clear_game() as integer
	
	declare function create_main_menu() as integer
	
	declare function create_setup_window() as integer
	
	declare function create_exit_window() as integer
	
end type


declare function close_alert_callback( byval button as button_struct ptr ) as integer
declare function close_scorecard_callback( byval button as button_struct ptr ) as integer
declare function scorecard_score_upper_callback( byval button as button_struct ptr ) as integer
declare function scorecard_score_lower_callback( byval button as button_struct ptr ) as integer
declare function close_main_menu_callback( byval button as button_struct ptr ) as integer
declare function exit_main_callback( byval button as button_struct ptr ) as integer
declare function continue_main_callback( byval button as button_struct ptr ) as integer
declare function new_main_callback( byval button as button_struct ptr ) as integer
declare function add_player_callback( byval button as button_struct ptr ) as integer
declare function start_new_game_callback( byval button as button_struct ptr ) as integer


declare function Dice_World_Contact_Begin cdecl( byval Material as NewtonMaterial ptr, byval rBody1 as NewtonBody ptr, byval rBody2 as NewtonBody ptr ) as integer
declare function Dice_World_Contact_Process cdecl( byval Material as NewtonMaterial ptr, byval Contact as NewtonContact ptr ) as integer
declare sub Dice_World_Contact_End cdecl(byval Material as NewtonMaterial ptr)
