#inclib "lwo_load"
#include once "lwconstants.bi"
#include once "crt.bi"
''
''
'' lwo2 -- header translated with help of SWIG FB wrapper
''
'' NOTICE: This file is part of the FreeBASIC Compiler package and can't
''         be included in other distributions without authorization.
''
''
#ifndef __lwo2_bi__
#define __lwo2_bi__

type lwNode
	next as lwNode ptr
	prev as lwNode ptr
	data as any ptr
end type

type lwPlugin
	next as lwPlugin ptr
	prev as lwPlugin ptr
	ord as zstring ptr
	name as zstring ptr
	flags as integer
	data as any ptr
end type

type lwKey
	next as lwKey ptr
	prev as lwKey ptr
	value as single
	time as single
	shape as uinteger
	tension as single
	continuity as single
	bias as single
	param(0 to 4-1) as single
end type

type lwEnvelope
	next as lwEnvelope ptr
	prev as lwEnvelope ptr
	index as integer
	type as integer
	name as zstring ptr
	key as lwKey ptr
	nkeys as integer
	behavior(0 to 2-1) as integer
	cfilter as lwPlugin ptr
	ncfilters as integer
end type

#define BEH_RESET 0
#define BEH_CONSTANT 1
#define BEH_REPEAT 2
#define BEH_OSCILLATE 3
#define BEH_OFFSET 4
#define BEH_LINEAR 5

type lwEParam
	val as single
	eindex as integer
end type

type lwVParam
	val(0 to 3-1) as single
	eindex as integer
end type

type lwClipStill
	name as zstring ptr
end type

type lwClipSeq
	prefix as zstring ptr
	suffix as zstring ptr
	digits as integer
	flags as integer
	offset as integer
	start as integer
	end as integer
end type

type lwClipAnim
	name as zstring ptr
	server as zstring ptr
	data as any ptr
end type

type lwClip as st_lwClip

type lwClipXRef
	string as zstring ptr
	index as integer
	clip as lwClip ptr
end type

type lwClipCycle
	name as zstring ptr
	lo as integer
	hi as integer
end type

type st_lwClip
	next as st_lwClip ptr
	prev as st_lwClip ptr
	index as integer
	type as uinteger
	union
	still as lwClipStill
	seq as lwClipSeq
	anim as lwClipAnim
	xref as lwClipXRef
	cycle as lwClipCycle
	end union
	start_time as single
	duration as single
	frame_rate as single
	contrast as lwEParam
	brightness as lwEParam
	saturation as lwEParam
	hue as lwEParam
	gamma as lwEParam
	negative as integer
	ifilter as lwPlugin ptr
	nifilters as integer
	pfilter as lwPlugin ptr
	npfilters as integer
end type

type lwTMap
	size as lwVParam
	center as lwVParam
	rotate as lwVParam
	falloff as lwVParam
	fall_type as integer
	ref_object as zstring ptr
	coord_sys as integer
end type

type lwImageMap
	cindex as integer
	projection as integer
	vmap_name as zstring ptr
	axis as integer
	wrapw_type as integer
	wraph_type as integer
	wrapw as lwEParam
	wraph as lwEParam
	aa_strength as single
	aas_flags as integer
	pblend as integer
	stck as lwEParam
	amplitude as lwEParam
end type

#define PROJ_PLANAR 0
#define PROJ_CYLINDRICAL 1
#define PROJ_SPHERICAL 2
#define PROJ_CUBIC 3
#define PROJ_FRONT 4
#define WRAP_NONE 0
#define WRAP_EDGE 1
#define WRAP_REPEAT 2
#define WRAP_MIRROR 3

type lwProcedural
	axis as integer
	value(0 to 3-1) as single
	name as zstring ptr
	data as any ptr
end type

type lwGradKey
	next as lwGradKey ptr
	prev as lwGradKey ptr
	value as single
	_rgba(0 to 4-1) as single
end type

type lwGradient
	paramname as zstring ptr
	itemname as zstring ptr
	start as single
	end as single
	repeat as integer
	key as lwGradKey ptr
	ikey as short ptr
end type

type lwTexture
	next as lwTexture ptr
	prev as lwTexture ptr
	ord as zstring ptr
	type as uinteger
	chan as uinteger
	opacity as lwEParam
	opac_type as short
	enabled as short
	negative as short
	axis as short
	union
	imap as lwImageMap
	proc as lwProcedural
	grad as lwGradient
	end union
	tmap as lwTMap
end type

type lwTParam
	val as single
	eindex as integer
	tex as lwTexture ptr
end type

type lwCParam
	_rgb(0 to 3-1) as single
	eindex as integer
	tex as lwTexture ptr
end type

type lwGlow
	enabled as short
	type as short
	intensity as lwEParam
	size as lwEParam
end type

type lwRMap
	val as lwTParam
	options as integer
	cindex as integer
	seam_angle as single
end type

type lwLine
	enabled as short
	flags as ushort
	size as lwEParam
end type

type lwSurface
	next as lwSurface ptr
	prev as lwSurface ptr
	name as zstring ptr
	srcname as zstring ptr
	color as lwCParam
	luminosity as lwTParam
	diffuse as lwTParam
	specularity as lwTParam
	glossiness as lwTParam
	reflection as lwRMap
	transparency as lwRMap
	eta as lwTParam
	translucency as lwTParam
	bump as lwTParam
	smooth as single
	sideflags as integer
	alpha as single
	alpha_mode as integer
	color_hilite as lwEParam
	color_filter as lwEParam
	add_trans as lwEParam
	dif_sharp as lwEParam
	glow as lwEParam
	line as lwLine
	shader as lwPlugin ptr
	nshaders as integer
end type

type lwVMap
	next as lwVMap ptr
	prev as lwVMap ptr
	name as zstring ptr
	type as uinteger
	_dim as integer
	nverts as integer
	perpoly as integer
	vindex as integer ptr
	pindex as integer ptr
	val as single ptr ptr
end type

type lwVMapPt
	vmap as lwVMap ptr
	index as integer
end type

type lwPoint
	pos(0 to 2) as single
	npols as integer
	pol as integer ptr
	nvmaps as integer
	vm as lwVMapPt ptr
end type

type lwPolVert
	index as integer
	norm(0 to 3-1) as single
	nvmaps as integer
	vm as lwVMapPt ptr
end type

type lwPolygon
	surf as lwSurface ptr
	part as integer
	smoothgrp as integer
	flags as integer
	type as uinteger
	norm(0 to 2) as single
	nverts as integer
	v as lwPolVert ptr
end type

type lwPointList
	count as integer
	offset as integer
	pt as lwPoint ptr
end type

type lwPolygonList
	count as integer
	offset as integer
	vcount as integer
	voffset as integer
	pol as lwPolygon ptr
end type

type lwLayer
	next as lwLayer ptr
	prev as lwLayer ptr
	name as zstring ptr
	index as integer
	parent as integer
	flags as integer
	pivot(0 to 2) as single
	bbox(0 to 5) as single
	point as lwPointList
	polygon as lwPolygonList
	nvmaps as integer
	vmap as lwVMap ptr
end type

type lwTagList
	count as integer
	offset as integer
	tag as zstring ptr ptr
end type

type lwObject
	layer as lwLayer ptr
	env as lwEnvelope ptr
	clip as lwClip ptr
	surf as lwSurface ptr
	taglist as lwTagList
	nlayers as integer
	nenvs as integer
	nclips as integer
	nsurfs as integer
end type


declare sub lwFreeLayer cdecl alias "lwFreeLayer" (byval layer as lwLayer ptr)
declare sub lwFreeObject cdecl alias "lwFreeObject" (byval object as lwObject ptr)
declare function lwGetObject cdecl alias "lwGetObject" (byval filename as zstring ptr, byval failID as uinteger ptr, byval failpos as integer ptr) as lwObject ptr
declare sub lwFreePoints cdecl alias "lwFreePoints" (byval point as lwPointList ptr)
declare sub lwFreePolygons cdecl alias "lwFreePolygons" (byval plist as lwPolygonList ptr)
declare function lwGetPoints cdecl alias "lwGetPoints" (byval fp as FILE ptr, byval cksize as integer, byval point as lwPointList ptr) as integer
declare sub lwGetBoundingBox cdecl alias "lwGetBoundingBox" (byval point as lwPointList ptr, byval bbox as single ptr)
declare function lwAllocPolygons cdecl alias "lwAllocPolygons" (byval plist as lwPolygonList ptr, byval npols as integer, byval nverts as integer) as integer
declare function lwGetPolygons cdecl alias "lwGetPolygons" (byval fp as FILE ptr, byval cksize as integer, byval plist as lwPolygonList ptr, byval ptoffset as integer) as integer
declare sub lwGetPolyNormals cdecl alias "lwGetPolyNormals" (byval point as lwPointList ptr, byval polygon as lwPolygonList ptr)
declare function lwGetPointPolygons cdecl alias "lwGetPointPolygons" (byval point as lwPointList ptr, byval polygon as lwPolygonList ptr) as integer
declare function lwResolvePolySurfaces cdecl alias "lwResolvePolySurfaces" (byval polygon as lwPolygonList ptr, byval tlist as lwTagList ptr, byval surf as lwSurface ptr ptr, byval nsurfs as integer ptr) as integer
declare sub lwGetVertNormals cdecl alias "lwGetVertNormals" (byval point as lwPointList ptr, byval polygon as lwPolygonList ptr)
declare sub lwFreeTags cdecl alias "lwFreeTags" (byval tlist as lwTagList ptr)
declare function lwGetTags cdecl alias "lwGetTags" (byval fp as FILE ptr, byval cksize as integer, byval tlist as lwTagList ptr) as integer
declare function lwGetPolygonTags cdecl alias "lwGetPolygonTags" (byval fp as FILE ptr, byval cksize as integer, byval tlist as lwTagList ptr, byval plist as lwPolygonList ptr) as integer
declare sub lwFreeVMap cdecl alias "lwFreeVMap" (byval vmap as lwVMap ptr)
declare function lwGetVMap cdecl alias "lwGetVMap" (byval fp as FILE ptr, byval cksize as integer, byval ptoffset as integer, byval poloffset as integer, byval perpoly as integer) as lwVMap ptr
declare function lwGetPointVMaps cdecl alias "lwGetPointVMaps" (byval point as lwPointList ptr, byval vmap as lwVMap ptr) as integer
declare function lwGetPolyVMaps cdecl alias "lwGetPolyVMaps" (byval polygon as lwPolygonList ptr, byval vmap as lwVMap ptr) as integer
declare sub lwFreeClip cdecl alias "lwFreeClip" (byval clip as lwClip ptr)
declare function lwGetClip cdecl alias "lwGetClip" (byval fp as FILE ptr, byval cksize as integer) as lwClip ptr
declare function lwFindClip cdecl alias "lwFindClip" (byval list as lwClip ptr, byval index as integer) as lwClip ptr
declare sub lwFreeEnvelope cdecl alias "lwFreeEnvelope" (byval env as lwEnvelope ptr)
declare function lwGetEnvelope cdecl alias "lwGetEnvelope" (byval fp as FILE ptr, byval cksize as integer) as lwEnvelope ptr
declare function lwFindEnvelope cdecl alias "lwFindEnvelope" (byval list as lwEnvelope ptr, byval index as integer) as lwEnvelope ptr
declare function lwEvalEnvelope cdecl alias "lwEvalEnvelope" (byval env as lwEnvelope ptr, byval time as single) as single
declare sub lwFreePlugin cdecl alias "lwFreePlugin" (byval p as lwPlugin ptr)
declare sub lwFreeTexture cdecl alias "lwFreeTexture" (byval t as lwTexture ptr)
declare sub lwFreeSurface cdecl alias "lwFreeSurface" (byval surf as lwSurface ptr)
declare function lwGetTHeader cdecl alias "lwGetTHeader" (byval fp as FILE ptr, byval hsz as integer, byval tex as lwTexture ptr) as integer
declare function lwGetTMap cdecl alias "lwGetTMap" (byval fp as FILE ptr, byval tmapsz as integer, byval tmap as lwTMap ptr) as integer
declare function lwGetImageMap cdecl alias "lwGetImageMap" (byval fp as FILE ptr, byval rsz as integer, byval tex as lwTexture ptr) as integer
declare function lwGetProcedural cdecl alias "lwGetProcedural" (byval fp as FILE ptr, byval rsz as integer, byval tex as lwTexture ptr) as integer
declare function lwGetGradient cdecl alias "lwGetGradient" (byval fp as FILE ptr, byval rsz as integer, byval tex as lwTexture ptr) as integer
declare function lwGetTexture cdecl alias "lwGetTexture" (byval fp as FILE ptr, byval bloksz as integer, byval type as uinteger) as lwTexture ptr
declare function lwGetShader cdecl alias "lwGetShader" (byval fp as FILE ptr, byval bloksz as integer) as lwPlugin ptr
declare function lwGetSurface cdecl alias "lwGetSurface" (byval fp as FILE ptr, byval cksize as integer) as lwSurface ptr
declare function lwDefaultSurface cdecl alias "lwDefaultSurface" () as lwSurface ptr
declare function lwGetSurface5 cdecl alias "lwGetSurface5" (byval fp as FILE ptr, byval cksize as integer, byval obj as lwObject ptr) as lwSurface ptr
declare function lwGetPolygons5 cdecl alias "lwGetPolygons5" (byval fp as FILE ptr, byval cksize as integer, byval plist as lwPolygonList ptr, byval ptoffset as integer) as integer
declare function lwGetObject5 cdecl alias "lwGetObject5" (byval filename as zstring ptr, byval failID as uinteger ptr, byval failpos as integer ptr) as lwObject ptr
declare sub lwListFree cdecl alias "lwListFree" (byval list as any ptr, byval freeNode as sub cdecl(byval as any ptr))
declare sub lwListAdd cdecl alias "lwListAdd" (byval list as any ptr ptr, byval node as any ptr)
declare sub lwListInsert cdecl alias "lwListInsert" (byval vlist as any ptr ptr, byval vitem as any ptr, byval compare as function cdecl(byval as any ptr, byval as any ptr) as integer)
declare function dot cdecl alias "dot" (byval a as single ptr, byval b as single ptr) as single
declare sub cross cdecl alias "cross" (byval a as single ptr, byval b as single ptr, byval c as single ptr)
declare sub normalize cdecl alias "normalize" (byval v as single ptr)
declare sub set_flen cdecl alias "set_flen" (byval i as integer)
declare function get_flen cdecl alias "get_flen" () as integer
declare function getbytes cdecl alias "getbytes" (byval fp as FILE ptr, byval size as integer) as any ptr
declare sub skipbytes cdecl alias "skipbytes" (byval fp as FILE ptr, byval n as integer)
declare function getI1 cdecl alias "getI1" (byval fp as FILE ptr) as integer
declare function getI2 cdecl alias "getI2" (byval fp as FILE ptr) as short
declare function getI4 cdecl alias "getI4" (byval fp as FILE ptr) as integer
declare function getU1 cdecl alias "getU1" (byval fp as FILE ptr) as ubyte
declare function getU2 cdecl alias "getU2" (byval fp as FILE ptr) as ushort
declare function getU4 cdecl alias "getU4" (byval fp as FILE ptr) as uinteger
declare function getVX cdecl alias "getVX" (byval fp as FILE ptr) as integer
declare function getF4 cdecl alias "getF4" (byval fp as FILE ptr) as single
declare function getS0 cdecl alias "getS0" (byval fp as FILE ptr) as zstring ptr
declare function sgetI1 cdecl alias "sgetI1" (byval bp as ubyte ptr ptr) as integer
declare function sgetI2 cdecl alias "sgetI2" (byval bp as ubyte ptr ptr) as short
declare function sgetI4 cdecl alias "sgetI4" (byval bp as ubyte ptr ptr) as integer
declare function sgetU1 cdecl alias "sgetU1" (byval bp as ubyte ptr ptr) as ubyte
declare function sgetU2 cdecl alias "sgetU2" (byval bp as ubyte ptr ptr) as ushort
declare function sgetU4 cdecl alias "sgetU4" (byval bp as ubyte ptr ptr) as uinteger
declare function sgetVX cdecl alias "sgetVX" (byval bp as ubyte ptr ptr) as integer
declare function sgetF4 cdecl alias "sgetF4" (byval bp as ubyte ptr ptr) as single
declare function sgetS0 cdecl alias "sgetS0" (byval bp as ubyte ptr ptr) as zstring ptr

#endif