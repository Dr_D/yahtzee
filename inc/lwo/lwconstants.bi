#define FLEN_ERROR -1

#Define ID4 uInteger
#Define I1 Byte
#Define I2 Short
#Define I4 Integer
#Define U1 uByte
#Define U2 uShort
#Define U4 uInteger
#Define F4 Single
#Define S0 String


#define LWID_(a,b,c,d) ( ( (a) SHL(24) ) OR ( (b) SHL(16) ) OR ( (c) SHL(8) ) OR ( (d) SHL(0) ) )
#define LWID_2String(_Chunk) CHR(_Chunk SHR 24)+CHR(_Chunk SHR 16)+CHR(_Chunk SHR 8)+CHR(_Chunk)

'Chunk and sub-chunk ID's
#define ID_FORM  LWID_( ASC("F"), ASC("O"), ASC("R"), ASC("M") )
#define ID_LWO2  LWID_( ASC("L"), ASC("W"), ASC("O"), ASC("2") )
#define ID_LWOB  LWID_( ASC("L"), ASC("W"), ASC("O"), ASC("B") )

'Top level chunks
#define ID_LAYR  LWID_( ASC("L"), ASC("A"), ASC("Y"), ASC("R") )
#define ID_TAGS  LWID_( ASC("T"), ASC("A"), ASC("G"), ASC("S") )
#define ID_PNTS  LWID_( ASC("P"), ASC("N"), ASC("T"), ASC("S") )
#define ID_BBOX  LWID_( ASC("B"), ASC("B"), ASC("O"), ASC("X") )
#define ID_VMAP  LWID_( ASC("V"), ASC("M"), ASC("A"), ASC("P") )
#define ID_VMAD  LWID_( ASC("V"), ASC("M"), ASC("A"), ASC("D") )
#define ID_POLS  LWID_( ASC("P"), ASC("O"), ASC("L"), ASC("S") )
#define ID_PTAG  LWID_( ASC("P"), ASC("T"), ASC("A"), ASC("G") )
#define ID_ENVL  LWID_( ASC("E"), ASC("N"), ASC("V"), ASC("L") )
#define ID_CLIP  LWID_( ASC("C"), ASC("L"), ASC("I"), ASC("P") )
#define ID_SURF  LWID_( ASC("S"), ASC("U"), ASC("R"), ASC("F") )
#define ID_DESC  LWID_( ASC("D"), ASC("E"), ASC("S"), ASC("C") )
#define ID_TEXT  LWID_( ASC("T"), ASC("E"), ASC("X"), ASC("T") )
#define ID_ICON  LWID_( ASC("I"), ASC("C"), ASC("O"), ASC("N") )

'Polygon types
#define ID_FACE  LWID_( ASC("F"), ASC("A"), ASC("C"), ASC("E") )
#define ID_CURV  LWID_( ASC("C"), ASC("U"), ASC("R"), ASC("V") )
#define ID_PTCH  LWID_( ASC("P"), ASC("T"), ASC("C"), ASC("H") )
#define ID_MBAL  LWID_( ASC("M"), ASC("B"), ASC("A"), ASC("L") )
#define ID_BONE  LWID_( ASC("B"), ASC("O"), ASC("N"), ASC("E") )

'Polygon tags
#define ID_SURF  LWID_( ASC("S"), ASC("U"), ASC("R"), ASC("F") )
#define ID_PART  LWID_( ASC("P"), ASC("A"), ASC("R"), ASC("T") )
#define ID_SMGP  LWID_( ASC("S"), ASC("M"), ASC("G"), ASC("P") )

'envelopes
#define ID_PRE   LWID_( ASC("P"), ASC("R"), ASC("E"), ASC(" ") )
#define ID_POST  LWID_( ASC("P"), ASC("O"), ASC("S"), ASC("T") )
#define ID_KEY   LWID_( ASC("K"), ASC("E"), ASC("Y"), ASC(" ") )
#define ID_SPAN  LWID_( ASC("S"), ASC("P"), ASC("A"), ASC("N") )
#define ID_TCB   LWID_( ASC("T"), ASC("C"), ASC("B"), ASC(" ") )
#define ID_HERM  LWID_( ASC("H"), ASC("E"), ASC("R"), ASC("M") )
#define ID_BEZI  LWID_( ASC("B"), ASC("E"), ASC("Z"), ASC("I") )
#define ID_BEZ2  LWID_( ASC("B"), ASC("E"), ASC("Z"), ASC("2") )
#define ID_LINE  LWID_( ASC("L"), ASC("I"), ASC("N"), ASC("E") )
#define ID_STEP  LWID_( ASC("S"), ASC("T"), ASC("E"), ASC("P") )

'clips
#define ID_STIL  LWID_( ASC("S"), ASC("T"), ASC("I"), ASC("L") )
#define ID_ISEQ  LWID_( ASC("I"), ASC("S"), ASC("E"), ASC("Q") )
#define ID_ANIM  LWID_( ASC("A"), ASC("N"), ASC("I"), ASC("M") )
#define ID_XREF  LWID_( ASC("X"), ASC("R"), ASC("E"), ASC("F") )
#define ID_STCC  LWID_( ASC("S"), ASC("T"), ASC("C"), ASC("C") )
#define ID_TIME  LWID_( ASC("T"), ASC("I"), ASC("M"), ASC("E") )
#define ID_CONT  LWID_( ASC("C"), ASC("O"), ASC("N"), ASC("T") )
#define ID_BRIT  LWID_( ASC("B"), ASC("R"), ASC("I"), ASC("T") )
#define ID_SATR  LWID_( ASC("S"), ASC("A"), ASC("T"), ASC("R") )
#define ID_HUE   LWID_( ASC("H"), ASC("U"), ASC("E"), ASC(" ") )
#define ID_GAMM  LWID_( ASC("G"), ASC("A"), ASC("M"), ASC("M") )
#define ID_NEGA  LWID_( ASC("N"), ASC("E"), ASC("G"), ASC("A") )
#define ID_IFLT  LWID_( ASC("I"), ASC("F"), ASC("L"), ASC("T") )
#define ID_PFLT  LWID_( ASC("P"), ASC("F"), ASC("L"), ASC("T") )

'surfaces
#define ID_COLR  LWID_( ASC("C"), ASC("O"), ASC("L"), ASC("R") )
#define ID_LUMI  LWID_( ASC("L"), ASC("U"), ASC("M"), ASC("I") )
#define ID_DIFF  LWID_( ASC("D"), ASC("I"), ASC("F"), ASC("F") )
#define ID_SPEC  LWID_( ASC("S"), ASC("P"), ASC("E"), ASC("C") )
#define ID_GLOS  LWID_( ASC("G"), ASC("L"), ASC("O"), ASC("S") )
#define ID_REFL  LWID_( ASC("R"), ASC("E"), ASC("F"), ASC("L") )
#define ID_RFOP  LWID_( ASC("R"), ASC("F"), ASC("O"), ASC("P") )
#define ID_RIMG  LWID_( ASC("R"), ASC("I"), ASC("M"), ASC("G") )
#define ID_RSAN  LWID_( ASC("R"), ASC("S"), ASC("A"), ASC("N") )
#define ID_TRAN  LWID_( ASC("T"), ASC("R"), ASC("A"), ASC("N") )
#define ID_TROP  LWID_( ASC("T"), ASC("R"), ASC("O"), ASC("P") )
#define ID_TIMG  LWID_( ASC("T"), ASC("I"), ASC("M"), ASC("G") )
#define ID_RIND  LWID_( ASC("R"), ASC("I"), ASC("N"), ASC("D") )
#define ID_RFOP  LWID_( ASC("R"), ASC("F"), ASC("O"), ASC("P") )
#define ID_TRNL  LWID_( ASC("T"), ASC("R"), ASC("N"), ASC("L") )
#define ID_BUMP  LWID_( ASC("B"), ASC("U"), ASC("M"), ASC("P") )
#define ID_SMAN  LWID_( ASC("S"), ASC("M"), ASC("A"), ASC("N") )
#define ID_SIDE  LWID_( ASC("S"), ASC("I"), ASC("D"), ASC("E") )
#define ID_CLRH  LWID_( ASC("C"), ASC("L"), ASC("R"), ASC("H") )
#define ID_CLRF  LWID_( ASC("C"), ASC("L"), ASC("R"), ASC("F") )
#define ID_ADTR  LWID_( ASC("A"), ASC("D"), ASC("T"), ASC("R") )
#define ID_SHRP  LWID_( ASC("S"), ASC("H"), ASC("R"), ASC("P") )
#define ID_LINE  LWID_( ASC("L"), ASC("I"), ASC("N"), ASC("E") )
#define ID_LSIZ  LWID_( ASC("L"), ASC("S"), ASC("I"), ASC("Z") )
#define ID_ALPH  LWID_( ASC("A"), ASC("L"), ASC("P"), ASC("H") )
#define ID_AVAL  LWID_( ASC("A"), ASC("V"), ASC("A"), ASC("L") )
#define ID_GVAL  LWID_( ASC("G"), ASC("V"), ASC("A"), ASC("L") )
#define ID_BLOK  LWID_( ASC("B"), ASC("L"), ASC("O"), ASC("K") )

'texture layer
#define ID_TYPE  LWID_( ASC("T"), ASC("Y"), ASC("P"), ASC("E") )
#define ID_CHAN  LWID_( ASC("C"), ASC("H"), ASC("A"), ASC("N") )
#define ID_NAME  LWID_( ASC("N"), ASC("A"), ASC("M"), ASC("E") )
#define ID_ENAB  LWID_( ASC("E"), ASC("N"), ASC("A"), ASC("B") )
#define ID_OPAC  LWID_( ASC("O"), ASC("P"), ASC("A"), ASC("C") )
#define ID_FLAG  LWID_( ASC("F"), ASC("L"), ASC("A"), ASC("G") )
#define ID_PROJ  LWID_( ASC("P"), ASC("R"), ASC("O"), ASC("J") )
#define ID_STCK  LWID_( ASC("S"), ASC("T"), ASC("C"), ASC("K") )
#define ID_TAMP  LWID_( ASC("T"), ASC("A"), ASC("M"), ASC("P") )

'texture coordinates
#define ID_TMAP  LWID_( ASC("T"), ASC("M"), ASC("A"), ASC("P") )
#define ID_AXIS  LWID_( ASC("A"), ASC("X"), ASC("I"), ASC("S") )
#define ID_CNTR  LWID_( ASC("C"), ASC("N"), ASC("T"), ASC("R") )
#define ID_SIZE  LWID_( ASC("S"), ASC("I"), ASC("Z"), ASC("E") )
#define ID_ROTA  LWID_( ASC("R"), ASC("O"), ASC("T"), ASC("A") )
#define ID_OREF  LWID_( ASC("O"), ASC("R"), ASC("E"), ASC("F") )
#define ID_FALL  LWID_( ASC("F"), ASC("A"), ASC("L"), ASC("L") )
#define ID_CSYS  LWID_( ASC("C"), ASC("S"), ASC("Y"), ASC("S") )

'image map
#define ID_IMAP  LWID_( ASC("I"), ASC("M"), ASC("A"), ASC("P") )
#define ID_IMAG  LWID_( ASC("I"), ASC("M"), ASC("A"), ASC("G") )
#define ID_WRAP  LWID_( ASC("W"), ASC("R"), ASC("A"), ASC("P") )
#define ID_WRPW  LWID_( ASC("W"), ASC("R"), ASC("P"), ASC("W") )
#define ID_WRPH  LWID_( ASC("W"), ASC("R"), ASC("P"), ASC("H") )
#define ID_VMAP  LWID_( ASC("V"), ASC("M"), ASC("A"), ASC("P") )
#define ID_AAST  LWID_( ASC("A"), ASC("A"), ASC("S"), ASC("T") )
#define ID_PIXB  LWID_( ASC("P"), ASC("I"), ASC("X"), ASC("B") )

'procedural
#define ID_PROC  LWID_( ASC("P"), ASC("R"), ASC("O"), ASC("C") )
#define ID_COLR  LWID_( ASC("C"), ASC("O"), ASC("L"), ASC("R") )
#define ID_VALU  LWID_( ASC("V"), ASC("A"), ASC("L"), ASC("U") )
#define ID_FUNC  LWID_( ASC("F"), ASC("U"), ASC("N"), ASC("C") )
#define ID_FTPS  LWID_( ASC("F"), ASC("T"), ASC("P"), ASC("S") )
#define ID_ITPS  LWID_( ASC("I"), ASC("T"), ASC("P"), ASC("S") )
#define ID_ETPS  LWID_( ASC("E"), ASC("T"), ASC("P"), ASC("S") )

'gradient
#define ID_GRAD  LWID_( ASC("G"), ASC("R"), ASC("A"), ASC("D") )
#define ID_GRST  LWID_( ASC("G"), ASC("R"), ASC("S"), ASC("T") )
#define ID_GREN  LWID_( ASC("G"), ASC("R"), ASC("E"), ASC("N") )
#define ID_PNAM  LWID_( ASC("P"), ASC("N"), ASC("A"), ASC("M") )
#define ID_INAM  LWID_( ASC("I"), ASC("N"), ASC("A"), ASC("M") )
#define ID_GRPT  LWID_( ASC("G"), ASC("R"), ASC("P"), ASC("T") )
#define ID_FKEY  LWID_( ASC("F"), ASC("K"), ASC("E"), ASC("Y") )
#define ID_IKEY  LWID_( ASC("I"), ASC("K"), ASC("E"), ASC("Y") )

'shader
 #define ID_SHDR LWID_( ASC("S"), ASC("H"), ASC("D"), ASC("R") )
 #define ID_DATA LWID_( ASC("D"), ASC("A"), ASC("T"), ASC("A") )


'TXUV
 #define ID_TXUV LWID_( ASC("T"), ASC("X"), ASC("U"), ASC("V") )


'WEIGHT MAP[WGHT]
#define ID_WGHT LWID_( ASC("W"), ASC("G"), ASC("H"), ASC("T") )
#define ID_MORF LWID_( ASC("M"), ASC("O"), ASC("R"), ASC("F") )
#define ID_SPOT LWID_( ASC("S"), ASC("P"), ASC("O"), ASC("T") )
