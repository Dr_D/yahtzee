'vector/matrix classes by Dr_D

'#inclib "mathf"

#include once "crt/string.bi"




namespace nmathf
	
const false = 0, true = not false	
	

const as single pi = 3.1415926, pi2 = pi*2f, pi_half = pi/2f, pi_4th = pi/4f, pi_8th = pi/8f, pi_180 = pi/180f, inv_pi_180 = 180f/pi, pi_360 = pi/360f
const as single rad2Deg = 57.2957795130823
const as integer Sphere_Intersects = 1, Sphere_Front = 2, Sphere_Behind = 3
const as integer MAT_COPY_SIZE = 16 * sizeof(single)


type matrix_ as matrix


type vec2f

	declare constructor ( byval x as single, byval y as single )
	declare constructor ( byval x as double, byval y as double )
	declare constructor ( byref v2d as vec2f )
	declare constructor ( )

	x as single
	y as single

	'declare operator =(byref rhs as vec2f)
	declare operator cast() as string

	declare function dot ( byref v as vec2f ) as single
	declare function magnitude() as single
	declare sub normalize()
	declare function UnitVec() as vec2f
	declare function cross( byref v as vec2f ) as vec2f
	declare function cross_analog( byref v as vec2f ) as single
	declare function distance( byref v as vec2f ) as single
	declare function AngleBetween( byref v as vec2f ) as single

end type


type vec3f

	declare constructor ( byval x as single, byval y as single, byval z as single )
	declare constructor ( byref v3d as vec3f )
	declare constructor ( )

	x as single
	y as single
	z as single

	declare operator Cast() as string
	declare operator Cast() as vec2f
	declare operator *= ( byref rhs as matrix_ )
	declare operator *= ( byref s as single )

	declare operator +=( byref rhs as vec3f )

	declare function dot ( byref v as vec3f ) as single
	declare function magnitude() as single
	declare sub normalize()
	declare function UnitVec() as vec3f
	declare function cross( byref v as vec3f ) as vec3f
	declare function distance( byref v as vec3f ) as single
	declare function AngleBetween( byref v as vec3f ) as single

end type


type vec4f

	declare constructor ( byval x as single, byval y as single, byval z as single, byval w as double )
	declare constructor ( byref v4d as vec4f )
	declare constructor ( )

	x as single
	y as single
	z as single
	w as single

	declare operator Cast() as string
	declare operator Cast() as vec3f
	declare operator *= ( byref rhs as matrix_ )
	declare operator *= ( byref s as single )
	'declare operator *= ( byref s as double )
	
	declare operator let ( byref rhs as vec3f )
	
	declare function dot ( byref v as vec4f ) as single
	declare function magnitude() as single
	declare sub normalize()
	declare function UnitVec() as vec4f
	declare function cross( byref v as vec4f ) as vec4f
	declare function distance ( byref v as vec4f ) as single

end type


declare operator = (byref lhs as vec2f, byref rhs as vec2f ) as integer
declare operator = (byref lhs as vec3f, byref rhs as vec3f ) as integer

declare operator + ( byref lhs as vec2f, byref rhs as single ) as vec2f
declare operator + ( byref lhs as vec2f, byref rhs as vec2f ) as vec2f



declare operator + ( byref lhs as vec3f, byref rhs as single ) as vec3f
declare operator + ( byref lhs as vec3f, byref rhs as vec3f ) as vec3f

declare operator + ( byref lhs as vec3f, byref rhs as double ) as vec4f
declare operator + ( byref lhs as vec4f, byref rhs as vec4f ) as vec4f
declare operator + ( byref lhs as vec4f, byref rhs as single ) as vec4f



declare operator - ( byref lhs as vec2f, byref rhs as vec2f ) as vec2f

declare operator - ( byref lhs as vec3f ) as vec3f
declare operator - ( byref lhs as vec3f, byref rhs as vec3f ) as vec3f
declare operator - ( byref lhs as vec3f, byref rhs as vec4f ) as vec3f
declare operator - ( byref lhs as vec3f, byref rhs as single ) as vec3f

declare operator - ( byref lhs as vec4f, byref rhs as vec4f ) as vec4f
declare operator - ( byref lhs as vec4f ) as vec4f

declare operator * ( byref lhs as vec2f, byref rhs as vec2f ) as vec2f
declare operator * ( byref lhs as vec2f, byref rhs as single ) as vec2f
declare operator * ( byref lhs as vec2f, byref rhs as double ) as vec2f

declare operator * ( byref lhs as vec3f, byref rhs as vec3f ) as vec3f
declare operator * ( byref lhs as vec3f, byref rhs as vec4f ) as vec3f
declare operator * ( byref lhs as vec3f, byref rhs as double ) as vec3f
declare operator * ( byref lhs as vec3f, byref rhs as single ) as vec3f

declare operator * ( byref lhs as vec4f, byref rhs as vec4f ) as vec4f
declare operator * ( byref lhs as vec4f, byref rhs as single ) as vec4f
'declare operator * ( byref lhs as vec4f, byref rhs as double ) as vec4f


declare operator / ( byref lhs as vec2f, byref rhs as vec2f ) as vec2f
declare operator / ( byref lhs as vec2f, byref rhs as integer  ) as vec2f
declare operator / ( byref lhs as vec2f, byref rhs as single   ) as vec2f
declare operator / ( byref lhs as vec2f, byref rhs as double   ) as vec2f


declare operator / ( byref lhs as vec3f, byref rhs as vec3f ) as vec3f
declare operator / ( byref lhs as vec3f, byref rhs as single ) as vec3f
declare operator / ( byref lhs as vec3f, byref rhs as double ) as vec3f


declare operator / ( byref lhs as vec4f, byref rhs as vec4f ) as vec4f
declare operator / ( byref lhs as vec4f, byref rhs as single ) as vec4f
declare operator / ( byref lhs as vec4f, byref rhs as double ) as vec4f



type vmat
	
	r as vec3f
	u as vec3f
	f as vec3f
	p as vec3f
	a as vec3f
	
	declare constructor()
	declare constructor ( byref p as vec3f, byref f as vec3f, byref r as vec3f, byref u as vec3f )
	
	declare sub rotate( byval x as single, byval y as single, byval z as single )
	
end type



type matrix

	public:

	declare constructor( byref c as single = 0)
	declare constructor( byref r as vec3f, byref u as vec3f, byref f as vec3f, byref p as vec3f )
	declare constructor( byref x as matrix )
	declare sub LoadIdentity()
	declare sub LookAt( byref v1 as vec3f, byref v2 as vec3f, byref vup as vec3f )
	declare sub PointAt( byref v1 as vec3f, byref v2 as vec3f )
	declare sub ZeroUp( byref tP as vec3f )
	declare function Inverse() as matrix
	declare function PlanarProjection( byref lightpos as vec4f, byref plane as vec4f ) as matrix
	declare sub InfiniteProjection( byref fov as single, byref aspect as single, byref znear as single )
	declare sub Perspective( byval fov as single, byval aspect as single, byval zNear as single, byval zFar as single )
	declare sub AxisAngle( byref v as vec3f, byref angle as single )
	declare sub Translate( byref v as vec2f )
	declare sub Translate( byref v as vec3f )
	declare sub Translate( byref x as single, byref y as single, byref z as single )
	declare sub Translate( byref x as integer, byref y as integer, byref z as integer )
	declare sub Rotate( byref v as vec3f )
	declare sub Rotate( byref anglex as single, byref angley as single, byref anglez as single )
	declare sub Rotate( byref anglex as integer, byref angley as integer, byref anglez as integer )
	declare sub Scale( byref scalar as single )
	declare sub Scale( byref scalarx as single, byref scalary as single, byref scalarz as single )
	declare sub Gram_Schmidt( byref d as vec3f )

	declare property right( byref v as vec3f )
	declare property Up( byref v as vec3f )
	declare property Forward( byref v as vec3f )
	declare property Position( byref v as vec3f )

	declare property right( byref v as vec4f )
	declare property Up( byref v as vec4f )
	declare property Forward( byref v as vec4f )
	declare property Position( byref v as vec4f )

	declare property right() as vec4f
	declare property Up() as vec4f
	declare property Forward() as vec4f
	declare property Position() as vec4f
	declare property GetArrayData() as single ptr
	
	
	'declare operator = ( byref mat as matrix )
	declare operator *= ( byref mat as matrix )
	declare operator cast() as string
	declare operator cast() as single ptr
	declare operator cast() as vmat

	'private:
	m(15) as single = any

end type

declare operator * ( byref lhs as matrix, byref rhs as matrix ) as matrix
declare operator * ( byref lhs as vec2f, byref rhs as matrix ) as vec2f
declare operator * ( byref lhs as vec3f, byref rhs as matrix ) as vec4f
declare operator * ( byref lhs as vec4f, byref rhs as matrix ) as vec4f


declare function inv_sqr( byval dist as single ) as single
declare function cotan( byref num as single ) as single
declare function poly_normal( byref V as vec3f ptr ) as vec3f
declare function plane_distance( byref tNormal as vec3f, byref tPoint as vec3f) as single
declare function closestpointonline( byref Va as vec3f, byref Vb as vec3f, byref vPoint as vec3f ) as vec3f
declare function intersected_plane( byref V as vec3f ptr, byref vLine as vec3f ptr, byref vNormal as vec3f, byref OriginDist as single) as integer
declare function intersection_point( byref vNormal as vec3f, byref vLine as vec3f ptr, byref tDistance as single ) as vec3f
declare function point_inside_triangle3d( byref vPoint as vec3f, byref V as vec3f ptr ) as integer
declare function sameside( byref p1 as vec3f, byref p2 as vec3f, byref a as vec3f, byref b as vec3f ) as integer
declare function line_triangle_intersection( byref V as vec3f ptr, byref vLine as vec3f ptr, byref New_Intersection as vec3f ) as integer
declare sub get_collision_offset(byref vNormal as vec3f, byref Radius as single, byref Dist as single, byref vOffSet as vec3f)
declare function classify_sphere(byref vCenter as vec3f, byref VNormal as vec3f, byref vPoint as vec3f, byref Radius as single, byref Dist as single ) as integer
declare function edge_sphere_collision( byref vCenter as vec3f, byref V as vec3f ptr, byref Radius as single ) as integer
declare function point_inside_triangle2d (  byref x as integer, byref y as integer, byref p1 as vec2f, byref p2 as vec2f, byref p3 as vec2f ) as integer
declare function AABB overload( byref a1 as vec2f, byref a2 as vec2f, byref b1 as vec2f, byref b2 as vec2f) as integer
declare function AABB overload( byref a1 as vec3f, byref a2 as vec3f, byref b1 as vec3f, byref b2 as vec3f) as integer
declare function Spline_Point( Points() as vec3f, byval p as single ) as vec3f
	

end namespace
