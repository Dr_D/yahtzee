
#include once "mathfuncf.bi"
#include once "gl/gl.bi"

#ifndef false
const as integer false = 0
const as integer true = not false
#endif

namespace nFrustum

	'' This will allow us to create an object to keep track of our frustum
	type Tfrustum

		public:
		'' Call this every time the camera moves to update the frustum
		declare sub CalculateFrustum()

		'' This takes a 3D point and returns TRUE if it's inside of the frustum
		declare function PointInFrustum( byval x as single, byval y as single, byval z as single ) as integer

		'' This takes a 3D point and a radius and returns TRUE if the sphere is inside of the frustum
		declare function SphereInFrustum( byval x as single, byval y as single, byval z as single , byval  radius  as single) as integer

		'' This takes the center and half the length of the cube.
		'declare function CubeInFrustum( byval x as single, byval y as single, byval z as single, byval size as single) as integer
		declare function CubeInFrustum( byref centroid as nmathf.vec3f, byval size as nmathf.vec3f ) as integer


		private:
		'' This holds the A B C and D values for each side of our frustum.
		m_Frustum(6,4) as single

	end type

end namespace

