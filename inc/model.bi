'#include once "fbmld.bi"
#include once "global_constants.bi"
#include once "lwo/lwconstants.bi"
#include once "lwo/load_lwo.bi"
#include once "display.bi"
#include once "image.bi"
#include once "mathfuncf.bi"
#include once "hash.bi"
#include once "crt.bi"

Const as single Shadow_Infinity = 1000.0

namespace nmodel
	
	
	type shadow_struct
		
		volume(any)			as nmathf.vec4f
		vCnt					as integer
		
		volume_Cap(any)  	as nmathf.vec4f
		cCnt					as integer
		
		volume_vbo				as gluint
		
		volume_cap_vbo			as gluint
		
	end type
	

	type surface_struct
		
		surfName			As string
		start_ID       as uinteger
		end_ID         as uinteger
		shade_Mode     as uinteger
		double_Sided   as integer
		is_Tex			as byte
		tex1				as gluint
		tex2		     	as gluint
		tex3		     	as gluint
		tex4		     	as gluint
		texb			 	as gluint
		is_Multi		 	as byte
		is_Bump			as byte
		rgbalpha       as nimage.Rgbalpha
		ambient        as nimage.Rgbalpha
		diffuse        as nimage.Rgbalpha
		specular       as nimage.Rgbalpha
		emission       as nimage.Rgbalpha
		shininess      as single
		dList          as gluint
		shader			as glHandleARB
		group			 	as integer
		
		
		vertex_index(any)	as uinteger
		
		has_shader		 as integer
		dim shader_callback as sub( byref surface as surface_struct, texture() as nimage.texture_struct )
		
	end type
	

	type triangle_struct
		
		point_id(0 to 2) 	as uinteger
		t1coord(0 to 2)	as nmathf.vec2f
		t2coord(0 to 2)	as nmathf.vec2f
		t3coord(0 to 2)	as nmathf.vec2f
		t4coord(0 to 2)	as nmathf.vec2f
		bcoord(0 to 2)	 	as nmathf.vec2f
		vnormals(0 to 2) 	as nmathf.vec3f
		con(0 to 2)      	as integer
		visible          	as integer
		invisible        	as integer
		normal           	as nmathf.vec3f
		midpoint				as nmathf.vec3f
		plane            	as nmathf.vec4f
		group            	as integer
		string_id        	as string
		flagged				as integer
		
	end type
	
	
	type bone_struct
		
		isRoot				as integer
		m						as nmathf.matrix
		weight(any)			as single
		id						as string * 16
		position(1)			as nmathf.vec3f
		pindex(1)			as uinteger
		
		child(any)			as bone_struct ptr
		
		parent				as bone_struct ptr
		
	end type
		
	'type bone_container
			
	'	count 	as uinteger
	'	child		as bone_struct ptr
			
	'end type
		
	
	type morph_struct
		
		vec(any)			as nmathf.vec3f
		index(any)		as uinteger
		id					as string
		
	end type
	
	
	type box_struct
	
		centroid			as nmathf.vec3f
		size				as nmathf.vec3f
		
		vertices(7) 	as nmathf.vec3f
		triangles(11) 	as triangle_struct
		
	end type
	
	
	type model_struct
		
		vertices(any)		as nmathf.vec3f
		morphs(any)			as morph_struct
		tvertices(any)		as nmathf.vec3f
		triangles(any)		as triangle_struct
		surfaces(any)		as surface_struct
		
		'VBO rendering structure...
		vnormals(any)		as nmathf.vec3f
		uv1(any)				as nmathf.vec2f
		uv2(any)				as nmathf.vec2f
		uv3(any)				as nmathf.vec2f
		uv4(any)				as nmathf.vec2f
		uvb(any)				as nmathf.vec2f
		
		
		mBone(any)			as bone_struct
		
		'maxRoot				as uinteger
		Bone(any)			as bone_struct ptr
		
		pivot					as nmathf.vec3f
		dlist 				as gluint
		mName 				as string
		shadow_caster		as integer
		static_shadows 	as integer
		volume_created		as integer
		
		bbox				as box_struct
		
		shadow			as shadow_struct
		
		'will generate a vertex buffer object for the model's vertices
		vbo						as gluint
		
		'will generate a list of indices into the vbo(vertex) array for each surface
		vbo_index(any)			as gluint
		
		'will generate a list of indices for vertex normals
		normal_vbo				as gluint
		
		'vbo for texture cooords for texture unit 1
		uv1_vbo					as gluint
		
		'vbo for texture cooords for texture unit 2
		uv2_vbo					as gluint
		
		'vbo for texture cooords for texture unit 3
		uv3_vbo					as gluint
		
		'vbo for texture cooords for texture unit 4
		uv4_vbo					as gluint
		
		'vbo for texture cooords for bump channel
		uvb_vbo					as gluint
		
	end type
	

	type group_struct
		
		gName	as string
		group	as integer
		id1		as integer
		id2		as integer
		id3		as integer
		id4		as integer
		bump_id as integer
		
	end type
	
	
	declare function tri_box( byref tri as nmathf.vec3f ptr ) as box_struct
	declare function lwobject_2model3d( byref model as model_struct ptr, byval Object as lwObject ptr, texture() as nimage.texture_struct, byref force_clamp as integer ) as integer
	declare sub material_sort( byref model as model_struct ptr )
	declare sub bone_sort( byref model as model_struct ptr, bone() as bone_struct )
	declare function material_sort_callback cdecl ( byval elm1 as any ptr, byval elm2 as any ptr ) as integer
	declare sub bone_add_child( byref tBone as bone_struct ptr, bone() as bone_struct )
	
	declare sub RenderDepthPass( byref model as model_struct ptr, byref tMatrix as nmathf.matrix )
	declare sub render_model_tex_only( byref Model as Model_struct ptr, byref matrix as nMathf.Matrix, byref isDepthPass as integer )
	declare sub render_model_vbo( byref model as model_struct ptr, byref tMatrix as nmathf.matrix )
	
	declare sub unload_model3d( tModel() as Model_struct, byref max_models as integer, byref killid as integer )
	declare function load_model3d( byref filename as string, model() as model_struct, texture() as nimage.texture_struct, byref force_clamp as integer, byref static_shadows as integer ) as integer	

	declare sub center_model( byref model as model_struct ptr )
	declare sub Calc_Normals( byref model as model_struct ptr )
	declare sub Calc_vNormals( byref model as model_struct ptr )
	declare sub Calc_Planes( byref model as model_struct ptr )
	declare sub Set_Poly_Neighbors( byref model as model_struct ptr )
	declare function find_surface_center( byref model as model_struct ptr, byref surfname as string ) as nmathf.vec3f
	declare sub resort_model_surfaces( byref model as model_struct ptr )
	declare function patch_discontinuous_uvs( byref model as model_struct ptr ) as integer
	declare function GetModelByName( byref tString as string, model() as model_struct ) as model_struct ptr
		
end namespace
