#include once "GL/gl.bi"
#include once "GL/glext.bi"
#include once "GL/glu.bi"
#include once "global_constants.bi"


'for framebuffer objects
common shared _framebuffer_                    as integer
common shared glGenFramebuffersEXT             as PFNglGenFramebuffersEXTPROC
common shared glDeleteFramebuffersEXT          as PFNglDeleteFramebuffersEXTPROC
common shared glBindFramebufferEXT             as PFNglBindFramebufferEXTPROC
common shared glFramebufferTexture2DEXT        as PFNglFramebufferTexture2DEXTPROC
common shared glFramebufferRenderbufferEXT     as PFNglFramebufferRenderbufferEXTPROC
common shared glGenRenderbuffersEXT            as PFNglGenRenderbuffersEXTPROC
common shared glBindRenderbufferEXT            as PFNglBindRenderbufferEXTPROC
common shared glRenderbufferStorageEXT         as PFNglRenderbufferStorageEXTPROC
common shared glDeleteRenderbuffersEXT         as PFNglDeleteRenderbuffersEXTPROC
common shared glCheckFramebufferStatusEXT      as PFNglCheckFramebufferStatusEXTPROC

'for multitexture (oldschool)
common shared _multitexture_                    as integer
common shared maxTexelUnits                     as Gluint
common shared glMultiTexCoord2fARB              as PFNglMultiTexCoord2fARBPROC
common shared glMultiTexCoord2fvARB             as PFNglMultiTexCoord2fvARBPROC
common shared glActiveTextureARB                as PFNGlActiveTextureARBPROC
common shared glClientActiveTextureARB          as PFNglClientActiveTextureARBPROC
common shared glGenerateMipmapEXT               as PFNglGenerateMipmapEXTPROC


common shared _vbo_										as integer
common shared glDeleteBuffers							as PFNGLDELETEBUFFERSPROC
common shared glGenBuffers	 							as PFNGLGENBUFFERSPROC
common shared glIsBuffer 								as PFNGLISBUFFERPROC
common shared glBufferDaTa 							as PFNGLBUFFERDATAPROC
common shared glBufferSubData 						as PFNGLBUFFERSUBDATAPROC
common shared glGetBufferSubData						as PFNGLGETBUFFERSUBDATAPROC 
common shared glMapBuffer 								as PFNGLMAPBUFFERPROC
common shared glUnMapBuffer 							as PFNGLUNMAPBUFFERPROC
common shared glGetBufferParameterIV				as PFNGLGETBUFFERPARAMETERIVPROC
common shared glGetBufferPointerV 					as PFNGLGETBUFFERPOINTERVPROC
common shared glBindBuffer 							as PFNGLBINDBUFFERPROC



'for shaders
common shared _shader100_                      as integer
common shared glCreateShaderObjectARB          as PFNglCreateShaderObjectARBPROC
common shared glShaderSourceARB                as PFNglShaderSourceARBPROC
common shared glGetShaderSourceARB             as PFNglGetShaderSourceARBPROC
common shared glCompileShaderARB               as PFNglCompileShaderARBPROC
common shared glDeleteObjectARB                as PFNglDeleteObjectARBPROC
common shared glCreateProgramObjectARB         as PFNglCreateProgramObjectARBPROC
common shared glAttachObjectARB                as PFNglAttachObjectARBPROC
common shared glUseProgramObjectARB            as PFNglUseProgramObjectARBPROC
common shared glLinkProgramARB                 as PFNglLinkProgramARBPROC
common shared glValidateProgramARB             as PFNglValidateProgramARBPROC
common shared glGetObjectParameterivARB        as PFNglGetObjectParameterivARBPROC
common shared glGetInfoLogARB                  as PFNglGetInfoLogARBPROC
common shared glGetUniformLocationARB          as PFNglGetUniformLocationARBPROC
common shared glUniform1iARB                   as PFNglUniform1iARBPROC
common shared glUniform1fARB                   as PFNglUniform1fARBPROC
common shared glUniform2fvARB                  as PFNglUniform2fvARBPROC
common shared glUniform3fvARB                  as PFNglUniform3fvARBPROC


common shared glActiveStencilFaceEXT           as PFNglActiveStencilFaceEXTPROC
common shared glBlendFuncSeparate 				  as PFNGLBLENDFUNCSEPARATEPROC




common shared as GlHandleARB Shader_Bump,  Shader_MultiBump, shader_toon, Shader_PCF, Shader_Shadow
common shared as GLINT DecalLoc, NormalLoc, alphaTex, ShaderDepthLoc, texLoc1, texLoc2, texLoc3, tType, passNum
common shared as GLINT shadowTexLoc
common shared as glboolean enable_texture
common shared as GLINT colormap1, colormap2, alphamap, normalmap, myText
common shared _anisotropic_ as integer
common shared _shadowmap_ as integer
common shared _stencil2_ as integer

common shared as integer use_shaders, use_multitex, use_shadows, use_anisotropic, use_vbo_volumes
