#include once "Newton.bi"
#include once "global_constants.bi"
'#include once "fbmld.bi"
#include once "display.bi"
#include once "image.bi"
#include once "model.bi"
#include once "mathfuncf.bi"
#include once "physics_callbacks.bi"
#include once "hash.bi"
#include once "audio.bi"
#include once "frustum.bi"

namespace nphysics
	
	
	enum camera_type
		cFIRST_PERSON
		cCHASE
		cCHOPPER
		cCINEMATIC
	end enum
	
	enum control_type
		cDEMO
		cKEYBOARD
		cGAMEPAD
		cMOUSE
	end enum
	
	enum contact_type
		cNONE
		cROAD
		cGRASS
		cDIRT
		cGRAVEL
		cWATER
		cSNOW
		cMUD
	end enum
	
	enum entity_type
		eCHARACTER
		eCAMERA
		eCONVEX_HULL
		eCOLLISION_TREE
		eVEHICLE
	end enum
	
	enum joint_type
		jBALL_SOCKET
		jHINGE
		jSLIDER
		jCORKSCREW
		jUNIVERSAL
		jUP
		jCUSTOM
	end enum
	
	enum fake_joint_type
		fSUPENSION
		fCHARACTER
		fENGINE
	end enum
	
	enum tire_type
		TIRE_FR = 1
		TIRE_FL
		TIRE_RL
		TIRE_RR
		TIRE_ODD
	end enum
	
	
	enum PARTICLE_PROPERTIES
		PARTICLE_SMOKE = 1
		PARTICLE_GRAVEL
		PARTICLE_DUST
		PARTICLE_SNOW
		PARTICLE_RAIN
		PARTICLE_LEAF
		PARTICLE_SAND
		PARTICLE_CRASH
	
		WEATHER_CLEAR
		WEATHER_SNOW
		WEATHER_RAIN
		WEATHER_WIND
		WEATHER_SAND
	end enum
	
	
	
	
	type particle_struct

		position			as nmathf.vec3f
		position_delta	as nmathf.vec3f
		position_theta	as nmathf.vec3f

		omega				as nmathf.vec3f
		omega_delta		as nmathf.vec3f
		omega_theta		as nmathf.vec3f

		scale				as nmathf.vec3f
		scale_delta		as nmathf.vec3f
		scale_theta		as nmathf.vec3f

		colour			as nmathf.vec4f
		colour_delta	as nmathf.vec4f
		colour_theta	as nmathf.vec4f
		
		dist				as single
		active			as integer

		end_time			as single
		tex_id			as integer

		prop				as PARTICLE_PROPERTIES

	end type


	type emitter_struct
		
		curWeather			  as particle_properties
		
		particle(any)			as particle_struct

		terrain_fx_counter	as integer
		max_terrain_fx			as integer
		weather_fx_counter	as integer
		max_weather_fx			as integer

		declare sub ProcessParticles( byval timeStep as single, byval curTime as single, byref campos as nmathf.vec3f )
		
		'declare constructor()
		'declare destructor()
		
		declare sub CreateParticles( byref amt as integer )
		
	end type
	
	
	type material_struct
		
		name			as string
		reference	as integer
		
	end type
	
	
	
	type joint_struct
		
		jType			    as joint_type
		nJoint          as NewtonJoint ptr
		dof             as integer
		body            as NewtonBody ptr
		parent_body     as NewtonBody ptr
		mass            as single
		local_matrix0   as nmathf.matrix
		local_matrix1   as nmathf.matrix
		tMatrix         as nmathf.matrix
		mod_matrix      as nmathf.matrix
		collision       as NewtonCollision ptr
		pin             as nmathf.vec3f
		pivot           as nmathf.vec3f
		omega           as nmathf.vec3f
		max_omega       as nmathf.vec3f
		collision_state as integer
		do_destruction  as integer
		destroyed       as integer
		min_friction    as single
		max_friction    as single
		angle           as single
		has_limits      as integer
		min_angle       as single
		max_angle       as single
		model		       as nmodel.model_struct ptr
		id              as uinteger
		break_force     as uinteger
		
	end type

	
	'fake joints are not included in the physics engine
	'they're basically for creating a visual instance(can be animated) between two things
	'if we want to add a spinning windmill, on top of a static pole... this is what we would use
	'still need work, but this is the struct for it
	'right now, the vehicle suspension(rods) animation uses it.
	
	type fake_joint_struct
		
		ftpye				as fake_joint_type
		
		'for newton body targets
		parent		as NewtonBody ptr
		child			as newtonbody ptr
		
		'for an index into the vehicle tire array
		targetID		as integer
		
		'"origin" will always be inherited from the parent transformation matrix, when rendered
		'offset from the origin
		offset1		as nmathf.matrix
		'offset from target
		offset2		as nmathf.matrix
		
		'translation 1... if there is any extra translation for offset1, add it with this
		translate1	as nmathf.vec3f
		
		'translation 2... if there is any extra translation for offset2, add it with this
		translate2	as nmathf.vec3f
		
		'the rendering transformation matrix
		tMatrix 			as nmathf.matrix
		
		'ptr to the model to render when this fake joint is called
		tModel			as nmodel.model_struct ptr
		
		culled			as integer
		
		shadow_culled 	as integer

	end type
	

	type transmission_struct
		
		rpm							as single
		torque_curve(3)			as single
		gear_ratio(3)				as single
		gear							as integer
		differential_ratio		as single
		efficiency					as single
		reverse						as integer
		
	end type
	
	
	type Tire_Struct
		
		reference			as integer
		
		index 				as integer
		tModel 				as nmodel.model_struct ptr
		tBody					as NewtonBody ptr
		local_position 	as nmathf.vec3f
		tMatrix				as nmathf.matrix
		rotMatrix			as nmathf.matrix
		mass					as single
		radius				as single
		model_id				as integer
		width					as single
		latgrip				as single
		latgripco			as single
		longgrip				as single
		longgripco			as single
		steer_mode			as integer
		prop					as tire_type
		contact_surf		as contact_type
		
		is_sliding 			as integer
		on_ground			as integer
		add_particle		as integer
		particle				as particle_struct
		
		tName 				as string

		culled				as integer
		
		shadow_culled		as integer
		
		roadSound			as integer
		grassSound			as integer
		gravelSound			as integer
		
	end type
	
	'static entities are basically just a placeholder so we know what model to render
	'they get added to the collision tree, which is what newton uses for collisions & stuff
	'we need to keep the models and matrices for them though... or else we would have nothing to render
	'so it works like this:
	'You call world_struct.TreeBegin()
	'Then, you can load a model, move it, rotate it, scale it, etc, etc...
	'Next, you call world_struct.TreeAddStaticEntity( model, tMatrix ), where it will increment world_struct.max_static_entities,
	'and add a new instance to the world_struct.static_entity ptr
	'Finally, you call world_struct.TreeEnd()
	'then whenever we render the world, we just loop through the static_entities, draw them and then do it again for the shadows
	'one reason i do it this way is to make culling easier, but it will also make the editor much more versatile
	type static_entity_struct
		
		'the actual model we're using for this fucker
		tModel					as nmodel.Model_struct ptr
		'transformation matrix for object... we need to know the rotation, scaling, translation where it was in the collision tree
		tMatrix					as nmathf.Matrix
		'inverse matrix for shadow volumes... since this is a static object we can store the inverse matrix and shadow volume on constant memory
		iMatrix					as nmathf.Matrix
		'just rotation, no translation
		rotMatrix				as nmathf.Matrix
		'for frustum culling
		culled					as integer
		'for shadow volume culling
		shadow_culled 			as integer
		'method for rendering shadow false = zPass, true = zFail
		shadow_mode				as integer
		
		'volume(any)			as nmathf.vec4f
		'vCnt					as integer
		'
		'volume_Cap(any)  	as nmathf.vec4f
		'cCnt					as integer
		'
		'volume_vbo				as gluint
		'
		'volume_cap_vbo			as gluint
		
		static_shadows			as integer
		
	end type
	
	
	'entity_struct is the object container for physics entities
	type entity_struct EXTENDS static_entity_struct
		
		eType						as entity_type 'enum from above
		eName						as string      'the name assigned by the user
		'tModel					as nmodel.Model_struct ptr 'a ptr to the model for rendering and geometry in the case of a convex hull
		radius              	as single'radius of the object
		id                  	as integer'id... unused for now
		material            	as material_struct'the material of the entity, used in fine tuning collisions with other objects and the collision tree
		mass                	as single'mass of the object
		'newt						as newt_entity_struct'this struct holds the newtonBody and newtonCollision ptrs... i tried this as a test because my oop is failing :\
		body                	as NewtonBody ptr
		collision           	as NewtonCollision ptr
				
		'tMatrix					as nmathf.matrix'this is the main transformation matrix, it gets set in physics_callbacks.bi/generic_physics_transform()
		bMatrix					as nmathf.matrix'same as above
		initMatrix				as nmathf.matrix
		
		last_position			as nmathf.vec3f
		reset_timer				as single
		
		angle               	as nmathf.vec3f
		velocity            	as nmathf.vec3f
		
		external_torque     	as nmathf.vec3f
		
		omega						as nmathf.vec3f
		external_omega      	as nmathf.vec3f
		
		force						as nmathf.vec3f
		external_force      	as nmathf.vec3f
		
		contact_surf			as contact_type
		
		morf_delta			as single
		morf					as single
		morf_targ			as integer
		cur_action			as integer
		on_ground			as integer
		contact_string		as string
		in_water				as integer
		foul					as integer
		foul_pos				as nmathf.vec3f
		last_good_pos		as nmathf.vec3f
		hit_goal				as integer
		win					as integer
		goal_time			as single
		win_time				as single
		reset_velocities	as integer
		shots					as integer
		
		tempTarg				as nmathf.vec3f
		
		particle				as particle_struct
		
		play_celebrate		as integer
		'declare sub rb_SetMatrix( byref tMatrix as nmathf.matrix )
		
		fJoint(any) as fake_joint_struct
		
		declare sub AddFakeJoint()
		
		declare sub SetMaterial( byref material as material_struct )
		
	end type
	
	
	type vehicle_struct EXTENDS entity_struct
		
		tire(any)				as tire_struct
		aModel					as nmodel.model_struct ptr
		
		suspension(any)		as fake_joint_struct
		
		refernece				as integer
		
		nJoint					as NewtonJoint ptr
		
		trcnt						as integer

		transmission			as transmission_struct
		
		engine_torque			as single
		
		brake						as integer
		critical_brake			as integer
		at_goal					as integer
		throttle					as single
		nitro						as single
		wind_resistance		as single
		center_of_mass 		as nmathf.vec3f

		rear_tire_omegaMag	as single
		suspension_length		as single
		steer_angle				as single
		steer_speed				as single
		steer_spring			as single
		front_tire				as integer
		rear_tire				as integer
		
		velocity_target		as single

		test_string				as string
		iPoint					as nmathf.vec3f
		current_node			as integer
		current_lane			as single
		
		tName						as string
		
		declare sub AddTire( byref nWorld as NewtonWorld ptr, byref model as nModel.Model_Struct ptr, byref position as nMathf.vec3f, byref tMass as single, byref canSteer as integer, byref prop as tire_type )
		declare sub AddShock( byref model as nModel.model_struct ptr, byref offset1 as nmathf.matrix, byref trans1 as nMathf.vec3f, byref offset2 as nmathf.matrix, byref trans2 as nMathf.vec3f, byref tireID as integer ) 
		declare sub ProcessSuspension()
		
		in_crash					as integer
		is_airborn				as integer
		is_sliding				as integer
		
		engineSound				as integer
		crashSound				as integer
		celebrateSound			as integer
		
	end type
	
	
	type camera_struct EXTENDS entity_struct
		
		oldcType					as camera_type
		cType						as camera_type
		cTarg						as nmathf.vec3f
		cLook						as nmathf.vec3f
		cUp						as nmathf.vec3f
		lookMatrix				as nmathf.matrix
		targID					as integer
		targDist					as single
		
		frustum					as nFrustum.Tfrustum
		
		vMat 						as nmathf.vMat
		
		'mainModelview			as nmathf.matrix
		
	end type
	
	
	type world_struct
		
		wWorld					as NewtonWorld ptr
		wCollision				as NewtonCollision ptr
		wBody						as NewtonBody ptr
		wMatrix 					as nmathf.Matrix
		
		material(any)			as material_struct
		
		first_run 				as integer
		last_time				as double
		deltaTime 				as double
		frame_steps				as double
		frequency				as double
		sim_speed 				as double
		accumulative_step		as double
		paused 					as integer
		
		
		light						as ndisplay.light_struct
		
		sample(any)          as nAudio.sample_struct	
		
		texture(any)			as nImage.texture_struct
		
		static_entity(any)	as static_entity_struct
		
		entity(any)				as entity_struct		
		
		model(any)				as nModel.model_struct
		
		joint(any)      		as joint_struct
		
		fJoint(any)				as fake_joint_struct
		
		vehicle(any)			as vehicle_struct
		
		emitter					as emitter_struct
		
		camera					as camera_struct
		
		entity_reset_time		as single
		
		control_mode			as control_type
		
		
		
		'game play__________________
		
		'functions for vehicles
		declare sub ProcessDrivers()
		
		
		'functions for audio management..
		declare function AddSound( byref index as integer, byref filename as string, byref looping as integer ) as nAudio.sample_struct
		declare function FindSoundByName( byref sName as string ) as FSound_Sample ptr
		
		'functions for material management...
		declare sub AddMaterial( byref tName as string, byref soft as single, byref elastic as single, byref collide as integer, byref staticFriction as single, kineticFriction as single )
		declare function FindMaterialByName( byref mName as string ) as material_struct
		
		'functions for vehicle management...
		declare sub AddVehicle( byref iMatrix as nmathf.matrix, byref model as nModel.model_struct ptr, byref mass as single )
		
		'functions for joint management...
		declare sub AddRollingFrictionJoint( byref eId as integer )
		declare sub AddRollingFrictionJointToVehicle( byref vId as integer )
		declare sub RemoveJoint( byref jId as integer )
		
		
		'functions for fake joint management...
		declare sub AddFakeJoint( byref parent as any ptr, byref tOrigin as nmathf.vec3f ptr, byref translation as nMathf.vec3f, byref rotationMatrix as any ptr, byref oTarget as nmathf.vec3f ptr, byref aModel as nmodel.model_struct ptr )
		declare sub AddFakeJoint()      
		declare sub ProcessFakeJoints()
		
		'functions for entity management...
		declare sub AddCharacterEntity( byref iMatrix as nmathf.matrix, byref mass as single, byref eSize as nmathf.vec3f, byref eName as string, byref model as nmodel.model_struct ptr )
		declare sub AddGenericEntity( byref iMatrix as nmathf.matrix, byref mass as single, byref eSize as nmathf.vec3f, byref eName as string, byref model as nModel.model_struct ptr )
		declare sub AddCamera( byref eSize as nmathf.vec3f, byref position as nmathf.vec3f, byref lookat as nmathf.vec3f, byref eName as string )
		declare function FindEntityByType( byref eType as entity_type )as entity_struct ptr
		declare function FindEntityByName( byref eName as string ) as entity_struct ptr
		
		'functions for model management...
		declare function LoadModel( byref filename as string, byref force_clamp as integer, byref static_shadows as integer ) as integer
		
		'functions for texture management...
		declare function LoadTexture( byref filename as string, byref force_clamp as Integer, byref searchDir As String = "res/texture/" ) as integer
		declare function FindTextureByName( byref tName as string ) as integer
		
		'functions for Newton world management...
		declare sub Create( byref frame_steps as single, byref sim_speed as single )
		declare sub Destroy()
		declare sub Update()
		
		'functions for collision tree management...
		declare sub TreeBegin()
		declare sub TreeAddStaticEntity( byref model as nmodel.model_struct ptr, byref tMatrix as nmathf.matrix )
		declare sub TreeEnd()
		
		declare sub render_scene()
		
		declare sub render_scene( byref lookMatrix as nMathf.Matrix )
		
	end type
	
	'not used yet...
	type ray_struct
		
		param               as single
		intersection_point  as nmathf.vec3f
		vec                 as nmathf.vmat
		normal              as nmathf.vec3f
		nBody		           as NewtonBody ptr
		object_id           as integer
		collision_id        as integer
		designation         as string
		
	end type
	 
	
	declare function particle_sort cdecl ( byval elm1 as any ptr, byval elm2 as any ptr ) as integer
	
	declare function character_raycast_world( byval nWorld as NewtonWorld ptr, byref mControl as nMathf.vMat ) as ray_struct
	
	declare function RayPrefilterCallback cdecl(byval as NewtonBody ptr, byval as NewtonCollision ptr, byval as any ptr) as uinteger
	
	'type NewtonWorldRayPrefilterCallback as function cdecl(byval as NewtonBody ptr, byval as NewtonCollision ptr, byval as any ptr) as uinteger
	
end namespace
