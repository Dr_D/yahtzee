#include once "Newton.bi"
#include once "global_constants.bi"
'#include once "fbmld.bi"
#include once "model.bi"
#include once "mathfuncf.bi"
#include once "physics.bi"
#include once "hash.bi"


namespace nPhysics
	
	declare function character_ray_filter cdecl(byval nBody as NewtonBody ptr, byval Normal as dFloat ptr, byval Collision_ID as integer, byval User_Data as any ptr, byval Intersect_Param as dFloat) as dFloat
	
	declare sub entity_Leave_World cdecl(byval rBody as NewtonBody ptr)

	declare sub character_controller cdecl( byval rBody as NewtonBody ptr )
	
	declare sub camera_controller cdecl( byval rBody as NewtonBody ptr )
	declare sub camera_physics_transform cdecl( byval rBody as NewtonBody ptr, byval mMatrix as dFloat ptr )

	declare sub generic_physics_transform cdecl( byval rBody as NewtonBody ptr, byval mMatrix as dFloat ptr )
	declare sub generic_force_torque cdecl( byval rBody as NewtonBody ptr )

	declare sub world_physics_transform cdecl( byval rBody as NewtonBody ptr, byval mMatrix as dFloat ptr )

	declare function Character_World_Contact_Begin cdecl( byval Material as NewtonMaterial ptr, byval rBody1 as NewtonBody ptr, byval rBody2 as NewtonBody ptr ) as integer
	declare function Character_World_Contact_Process cdecl( byval Material as NewtonMaterial ptr, byval Contact as NewtonContact ptr ) as integer
	declare sub character_World_Contact_End cdecl(byval Material as NewtonMaterial ptr)

	declare sub rolling_friction_callback cdecl( byval rjoint as NewtonJoint ptr )
	
	declare sub Tire_Callback cdecl( byval vJoint as NewtonJoint ptr )
	
	declare sub vehicle_physics_transform cdecl( byval rBody as NewtonBody ptr, byval mMatrix as dFloat ptr )
	
	declare sub vehicle_force_torque cdecl( byval rBody as NewtonBody ptr )
	
	declare function Vehicle_World_Contact_Begin cdecl( byval Material as NewtonMaterial ptr, byval rBody1 as NewtonBody ptr, byval rBody2 as NewtonBody ptr ) as integer
	declare function Vehicle_World_Contact_Process cdecl( byval Material as NewtonMaterial ptr, byval Contact as NewtonContact ptr ) as integer
	declare sub Vehicle_World_Contact_End cdecl(byval Material as NewtonMaterial ptr)
	
	declare function Vehicle_Vehicle_Contact_Begin cdecl( byval Material as NewtonMaterial ptr, byval rBody1 as NewtonBody ptr, byval rBody2 as NewtonBody ptr ) as integer
	declare function Vehicle_Vehicle_Contact_Process cdecl( byval Material as NewtonMaterial ptr, byval Contact as NewtonContact ptr ) as integer
	declare sub Vehicle_Vehicle_Contact_End cdecl(byval Material as NewtonMaterial ptr)
	
	declare function Camera_World_Contact_Begin cdecl( byval Material as NewtonMaterial ptr, byval rBody1 as NewtonBody ptr, byval rBody2 as NewtonBody ptr ) as integer
	declare function Camera_World_Contact_Process cdecl( byval Material as NewtonMaterial ptr, byval Contact as NewtonContact ptr ) as integer
	declare sub Camera_World_Contact_End cdecl(byval Material as NewtonMaterial ptr)
	
end namespace
