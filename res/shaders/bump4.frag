varying vec3 lVec;
varying vec3 eVec;
varying vec2 texCoord;
uniform sampler2D colorMap;
uniform sampler2D normalMap;

void main (void)
{
    
	float dSqr = dot(lVec, lVec);
	
	float att = clamp( 0.9998 * sqrt(dSqr), 0.0, 1.0);
	
	vec3 tlVec = lVec * inversesqrt(dSqr);

	vec3 vVec = normalize(eVec);
	
	vec4 base = vec4(texture2D(colorMap, texCoord).rgb, 1.0);
	
	vec3 bump = normalize( texture2D(normalMap, texCoord).rgb * 2.0 - 1.0);

	vec4 vAmbient = gl_LightSource[1].ambient * gl_FrontMaterial.ambient;

	float diffuse = max( dot(tlVec, bump), 0.0 );
	
	vec4 vDiffuse = gl_LightSource[1].diffuse * gl_FrontMaterial.diffuse * diffuse;	

	float specular = pow(clamp(dot(reflect(-tlVec, bump), vVec), 0.0, 1.0), gl_FrontMaterial.shininess );
	
	vec4 vSpecular = gl_LightSource[1].specular * gl_FrontMaterial.specular * specular;
	
	gl_FragColor = ( vAmbient*base + vDiffuse*base + vSpecular) * att;
	gl_FragColor.a = gl_Color.a;
}
