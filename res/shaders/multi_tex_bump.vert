varying vec3 lVec; 
varying vec3 eVec;
varying vec2 texCoord0;
varying vec2 texCoord1;
varying vec2 texCoord2;
varying vec2 texCoord3;

void main(void)
{
    vec3 vTangent;
	
	vec3 c1 = cross(gl_Normal, vec3(0.0, 0.0, 1.0));
	
	vec3 c2 = cross(gl_Normal, vec3(0.0, 1.0, 0.0));
	
	if(length(c1)>length(c2))
	{
		vTangent = c1;	
	}
	else
	{
		vTangent = c2;	
	}
	
	vTangent = normalize(vTangent);

	gl_Position = ftransform();
	
	texCoord0 = gl_MultiTexCoord0.xy;
	
	texCoord1 = gl_MultiTexCoord1.xy;
	
	texCoord2 = gl_MultiTexCoord2.xy;
	
	texCoord3 = gl_MultiTexCoord3.xy;
	
	vec3 n = normalize(gl_NormalMatrix * gl_Normal);
	
	vec3 t = normalize(gl_NormalMatrix * vTangent);
	
	vec3 b = cross(n, t);
	
	vec3 vVertex = vec3(gl_ModelViewMatrix * gl_Vertex);
	
	vec3 tVec = gl_LightSource[0].position.xyz - vVertex;
	
	lVec = vec3( dot(tVec, t), dot(tVec, b), dot(tVec, n) );
	
	tVec = vVertex * -1.0;
	
	eVec = vec3( dot(tVec, t), dot(tVec, b), dot(tVec, n) );
}
