#version 120
#extension GL_ARB_shading_language_include : require
 
uniform sampler2DShadow shadowMap;
 
uniform sampler2D tex1;

uniform sampler2D tex2;

uniform sampler2D tex3;

uniform int tType;

uniform int passNum;
 
varying vec4 projCoord;
 
varying vec2 texCoord1;

varying vec2 texCoord2;

varying vec2 texCoord3;
 
varying vec4 tlColor;

varying vec3 lVec; 

varying vec3 eVec;

vec4 shadowColor;

 
vec4 shColor( vec3 sUV, sampler2DShadow sMap )
{
 
	const float transparency = 0.3;
 
	const float mapSz = 1.0 / 4096.0;
 	
 	vec4 shadowColor = shadow2D(sMap, sUV);
	 
	 shadowColor += shadow2D(sMap, sUV.xyz + vec3( mapSz,  mapSz, 0));
	 shadowColor += shadow2D(sMap, sUV.xyz + vec3( mapSz, -mapSz, 0));
	 shadowColor += shadow2D(sMap, sUV.xyz + vec3( mapSz,  	  0, 0));
	 shadowColor += shadow2D(sMap, sUV.xyz + vec3(-mapSz,  mapSz, 0));
	 shadowColor += shadow2D(sMap, sUV.xyz + vec3(-mapSz, -mapSz, 0));
	 shadowColor += shadow2D(sMap, sUV.xyz + vec3(-mapSz,  	  0, 0));
	 shadowColor += shadow2D(sMap, sUV.xyz + vec3(        0,  mapSz, 0));
	 shadowColor += shadow2D(sMap, sUV.xyz + vec3(        0, -mapSz, 0));
	 
	 shadowColor = shadowColor / 9.0;
	 
	 shadowColor += transparency;
	 
	 shadowColor = clamp(shadowColor, 0.0, 1.0);
 
 	return shadowColor;
 
}
 
 
void main ()
{
	vec3 shadowUV = projCoord.xyz / projCoord.q;
	
	vec4 actualColor = texture2D(tex1, texCoord1); 
	 
	vec4 color1 =  actualColor * tlColor;
	
	vec4 color2 = texture2D(tex2, texCoord2) * tlColor;
	
	vec4 color3 = texture2D(tex3, texCoord3);
	
	
	if (passNum==0) 
	{
		if(actualColor.a>0.0) 
		{
			gl_FragColor = color1;
		}
		else discard;
	}
	
	else
	{
		shadowColor = vec4(shColor( shadowUV, shadowMap ).rgb, 1.0);
	
		 if(shadowUV.x >= 0.0 && shadowUV.y >= 0.0 && shadowUV.x <= 1.0 && shadowUV.y <= 1.0 )
		 {
		 
		 	if (tType == 0 )
		 	{ 
				if(actualColor.a>0.0) 
				{
					gl_FragColor = color1 * shadowColor;
				}
				else discard;
			}
			
			else if (tType == 1)
			{
				if(actualColor.a>0.0)
				{
					gl_FragColor = ((color2*color3.r) + (color1*(1.0-color3.r))) * shadowColor;
				}
				else discard;
			}
			
			else if (tType == 3)
			{
			
				float dSqr = dot(lVec, lVec);
		
				float att = clamp( 0.9998 * sqrt(dSqr), 0.0, 1.0);
		
				vec3 tlVec = lVec * inversesqrt(dSqr);
	
				vec3 vVec = normalize(eVec);
		
				vec4 base = vec4(texture2D(tex1, texCoord1).rgb, 1.0);
		
				vec3 bump = normalize( texture2D(tex2, texCoord1).rgb * 2.0 - 1.0);
	
				vec4 vAmbient = gl_LightSource[1].ambient * gl_FrontMaterial.ambient;
	
				float diffuse = max( dot(tlVec, bump), 0.0 );
		
				vec4 vDiffuse = gl_LightSource[1].diffuse * gl_FrontMaterial.diffuse * diffuse;	
	
				float specular = pow(clamp(dot(reflect(-tlVec, bump), vVec), 0.0, 1.0), gl_FrontMaterial.shininess );
		
				vec4 vSpecular = gl_LightSource[1].specular * gl_FrontMaterial.specular * specular;
				
				if(actualColor.a>0.0) 
				{
					gl_FragColor = (( vAmbient*base + vDiffuse*base + vSpecular) * att) * shadowColor;
				}
				else discard;
				
			}
			
		 }
			else
		 {
			if(actualColor.a>0.0) 
			{
				gl_FragColor = vec4(0.0);
			}
			else discard;
		 }
	 
	 }
}
