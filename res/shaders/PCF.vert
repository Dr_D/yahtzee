varying vec4 projCoord;

varying vec2 texCoord1;

varying vec2 texCoord2;

varying vec2 texCoord3;

varying vec4 tlColor;

varying vec3 lVec; 

varying vec3 eVec;


void main()
{

	vec3 normal = normalize(gl_NormalMatrix * gl_Normal);
	
	vec4 realPos = gl_ModelViewMatrix * gl_Vertex;
	
	vec3 lightDir = normalize(vec3(gl_LightSource[1].position - realPos));
	
	
	
	//This section for normal mapping...
	vec3 vVertex = realPos.xyz;
	
	vec3 vTangent;
	
	vec3 c1 = cross(gl_Normal, vec3(0.0, 0.0, 1.0)); 
	
	vec3 c2 = cross(gl_Normal, vec3(0.0, 1.0, 0.0)); 
	
	if( length(c1) > length(c2) )
	{
		vTangent = c1;
	}
	else
	{
		vTangent = c2;
	}
	
	vec3 tVec = gl_LightSource[1].position.xyz - vVertex;
	
	vTangent = normalize(vTangent);

	vec3 t = normalize(gl_NormalMatrix * vTangent);
	
	vec3 b = cross(normal, t);
	
	lVec = vec3( dot(tVec, t), dot(tVec, b), dot(tVec, normal) );

	tVec = vVertex * -1.0;
	
	eVec = vec3( dot(tVec, t), dot(tVec, b), dot(tVec, normal) );
	
	//Previous section for normal mapping...
	
	

	float shine = gl_FrontMaterial.shininess;

	
	
	projCoord = gl_TextureMatrix[0] * realPos;
	
	gl_FrontColor = gl_Color;
	
	texCoord1 = gl_MultiTexCoord1.xy;
	
	texCoord2 = gl_MultiTexCoord2.xy;
	
	texCoord3 = gl_MultiTexCoord3.xy;
	
	float lightIntensity = max(dot(normal, normalize(lightDir)), 0.0);
	
	vec4 diffuseLight = lightIntensity * gl_LightSource[1].diffuse;
	
	vec3 halfVector = normalize(gl_LightSource[1].halfVector.xyz);
	
	vec4 specularLight = pow(max(dot(normal, normalize(halfVector)), 0.0), shine) * gl_LightSource[1].specular;
	
	tlColor = gl_FrontColor * (gl_FrontMaterial.emission + (gl_FrontMaterial.diffuse*diffuseLight) + (gl_FrontMaterial.specular*specularLight) + gl_LightSource[1].ambient);
	
	gl_Position = ftransform();
	
}
