
varying vec3 lVec; 
varying vec3 eVec;
varying vec2 texCoord;

void main(void)
{
   vec3 vTangent;
	
	vec3 c1 = cross(gl_Normal, vec3(0.0, 0.0, 1.0)); 
	
	vec3 c2 = cross(gl_Normal, vec3(0.0, 1.0, 0.0)); 
	
	if( length(c1) > length(c2) )
	{
		vTangent = c1;
	}
	else
	{
		vTangent = c2;
	}
	
	vTangent = normalize(vTangent);
	
	gl_Position = ftransform();
	
	texCoord = gl_MultiTexCoord1.xy;
	
	vec3 n = normalize(gl_NormalMatrix * gl_Normal);
	
	vec3 t = normalize(gl_NormalMatrix * vTangent);
	
	vec3 b = cross(n, t);
	
	vec3 vVertex = vec3(gl_ModelViewMatrix * gl_Vertex);
	
	vec3 tVec = gl_LightSource[1].position.xyz - vVertex;

	lVec = vec3( dot(tVec, t), dot(tVec, b), dot(tVec, n) );

	tVec = vVertex * -1.0;
	
	eVec = vec3( dot(tVec, t), dot(tVec, b), dot(tVec, n) );
	
	gl_FrontColor = gl_Color;
}
