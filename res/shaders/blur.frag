varying vec2 texCoord;

uniform sampler2D texture;

uniform float blurSize;

//const int blurSize = 5;

const float PI = 3.14159265;

void main() {  
  
  vec2 p = texCoord;
	
	vec4 tCol;
	float cnt;
	
	for (int y=-blurSize; y<blurSize+1; y++)
	{
		for (int x=-blurSize; x<blurSize+1; x++)
		{
			vec2 tCoord = vec2(x/640.0, y/480.0);
				
			tCol += texture2D(texture, p+tCoord);
				
			cnt+=1.0;
			
		}
	}
	
	tCol/=cnt;
	
  gl_FragColor = vec4(tCol.rgb,1.0);
}