//Generic Vertex Shader

varying vec2 texCoord;

void main(void)
{

	gl_Position = ftransform();
	texCoord = gl_MultiTexCoord1.xy;
	

}
