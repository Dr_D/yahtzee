//fragement shader for skipping 0 alpha pixels in depth writes
//

uniform sampler2D tex1;

varying vec2 texCoord;

void main ()
{
	
	vec4 tCol = texture2D(tex1, texCoord);
	
	if (tCol.a == 0.0)
	{
		discard;
	}
	else gl_FragDepth = gl_FragCoord.z;
	
}

