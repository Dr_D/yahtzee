---

## Requirements

This is all that you'll need to compile the program.
Everything else is self-contained.

1. FreeBASIC compiler version 1.07.1 or later
2. FBEdit gui editor for FreeBASIC.

---
