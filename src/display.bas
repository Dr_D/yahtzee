#include once "display.bi"

extern Display as ndisplay.Display_Struct

namespace ndisplay

'this first function is a hack that will eventually be replaced by callbacks for each shader...
'that seems to be the most robust method i've come up with
sub load_default_shaders()

	dim as gluint Shader_Compile_Success

	if use_shaders and _shader100_ then

		dim as GlHandleARB Vertex_Bump, Fragment_Bump

		Vertex_Bump   = init_shader( "res/shaders/PCF.vert", GL_VERTEX_SHADER_ARB )
		Fragment_Bump = init_shader( "res/shaders/PCF.frag", GL_FRAGMENT_SHADER_ARB )

		Shader_PCF = GlCreateProgramObjectARB()
		glAttachObjectARB( Shader_PCF, Vertex_Bump )
		glAttachObjectARB( Shader_PCF, Fragment_Bump )
		glLinkProgramARB( Shader_PCF )

		GlValidateProgramARB( Shader_PCF )
		glGetObjectParameterivARB( Shader_PCF, GL_OBJECT_VALIDATE_STATUS_ARB, @Shader_Compile_Success )
		if Shader_Compile_Success = 0 then
			#ifdef _debugout
			print #debugout, "GLSL program(PCF) failed to compile."
			#endif
		else
			#ifdef _debugout
			print #debugout, "GLSL program(PCF) compiled successfully."
			#endif
		end if

		ShaderDepthLoc       = glGetUniformLocationARB( Shader_PCF, strptr("shadowMap") )
		texLoc1 					= glGetUniformLocationARB( Shader_PCF, strptr("tex1") )
		texLoc2 					= glGetUniformLocationARB( Shader_PCF, strptr("tex2") )
		texLoc3 					= glGetUniformLocationARB( Shader_PCF, strptr("tex3") )
		tType 					= glGetUniformLocationARB( Shader_PCF, strptr("tType") )
		passNum 					= glGetUniformLocationARB( Shader_PCF, strptr("passNum") )

		glUseProgramObjectARB( Shader_PCF )
		glUniform1iARB(ShaderDepthLoc, 0 )
		glUniform1iARB(texLoc1, 1 )
		glUniform1iARB(texLoc2, 2 )
		glUniform1iARB(texLoc3, 3 )
		glUniform1iARB(passNum, 0 )

		glUseProgramObjectARB( 0 )

		glDeleteObjectArb( Vertex_Bump )
		glDeleteObjectArb( Fragment_Bump )





		Vertex_Bump   = init_shader( "res/shaders/shadowMapping.vert", GL_VERTEX_SHADER_ARB )
		Fragment_Bump = init_shader( "res/shaders/shadowMapping.frag", GL_FRAGMENT_SHADER_ARB )

		Shader_Shadow = GlCreateProgramObjectARB()
		glAttachObjectARB( Shader_Shadow, Vertex_Bump )
		glAttachObjectARB( Shader_Shadow, Fragment_Bump )
		glLinkProgramARB( Shader_Shadow )

		GlValidateProgramARB( Shader_Shadow )
		glGetObjectParameterivARB( Shader_Shadow, GL_OBJECT_VALIDATE_STATUS_ARB, @Shader_Compile_Success )
		if Shader_Compile_Success = 0 then
			#ifdef _debugout
			print #debugout, "GLSL program(depth-write) failed to compile."
			#endif
		else
			#ifdef _debugout
			print #debugout, "GLSL program(dpeth-write) compiled successfully."
			#endif
		end if

		shadowTexLoc 					 = glGetUniformLocationARB( Shader_Shadow, strptr("tex1") )

		glUseProgramObjectARB( Shader_Shadow )
		glUniform1iARB(shadowTexLoc, 1 )
		glUseProgramObjectARB( 0 )

		glDeleteObjectArb( Vertex_Bump )
		glDeleteObjectArb( Fragment_Bump )




		#if 0
		dim shared as glInt blurLoc
		dim shared as glHandleARB Shader_Blur

		'temporary blur test...
		Vertex_Bump   = init_shader( "res/shaders/shadowMapping.vert", GL_VERTEX_SHADER_ARB )
		Fragment_Bump = init_shader( "res/shaders/blur.frag", GL_FRAGMENT_SHADER_ARB )

		Shader_Blur = GlCreateProgramObjectARB()
		glAttachObjectARB( Shader_Blur, Vertex_Bump )
		glAttachObjectARB( Shader_Blur, Fragment_Bump )
		glLinkProgramARB( Shader_Blur )

		GlValidateProgramARB( Shader_Blur )
		glGetObjectParameterivARB( Shader_Blur, GL_OBJECT_VALIDATE_STATUS_ARB, @Shader_Compile_Success )
		if Shader_Compile_Success = 0 then
			#ifdef _debugout
			print #debugout, "GLSL program(Blur) failed to compile."
			#endif
		else
			#ifdef _debugout
			print #debugout, "GLSL program(Blur) compiled successfully."
			#endif
		end if

		dim as uinteger bTexLoc 					 = glGetUniformLocationARB( Shader_Blur, strptr("texture") )
		dim as uinteger blurLoc 					 = glGetUniformLocationARB( Shader_Blur, strptr("blurSize") )

		glUseProgramObjectARB( Shader_Blur )
		glUniform1iARB(bTexLoc, 1 )
		glUniform1fARB(blurLoc, 5 )
		glUseProgramObjectARB( 0 )

		glDeleteObjectArb( Vertex_Bump )
		glDeleteObjectArb( Fragment_Bump )
		#endif


	end if

end sub


sub waitKey( byref key as uinteger )
	
	do
		
		screencontrol FB.POLL_EVENTS 
		
	loop while multikey(key)
	
		
end sub


	sub render_target_struct.lock()

		if glBindFramebufferEXT then

			glBindFramebufferEXT ( GL_FRAMEBUFFER_EXT, this.fBuffer )

			glViewport( 0, 0, this.Width, this.Height )

		end if

	end sub


	sub render_target_struct.unlock()

		if glBindFramebufferEXT then

			glBindFramebufferEXT ( GL_FRAMEBUFFER_EXT, 0 )

			glViewport( 0, 0, display.W, display.H )

		end if

	end sub


	function render_target_struct.create( byref w as uinteger, byref h as uinteger, byref has_depth as integer, byref has_stencil as integer, byref is_shadow as integer = 0 ) as integer

		dim as uinteger status = 0

		if glBindFramebufferEXT then

			with this

				if not is_shadow then
					'create a texture to attach to the framebuffer
					glGenTextures (1, @.texture)
					glBindTexture (GL_TEXTURE_2D, .texture)
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
					glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA, W, H, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0)
					'glGenerateMipmapEXT(GL_TEXTURE_2D)

					'create framebuffer object and attach texture
					glGenFramebuffersEXT (1, @.fBuffer)
					glBindFramebufferEXT (GL_FRAMEBUFFER_EXT, .fBuffer)

					glFramebufferTexture2DEXT (GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, .texture, 0)
					glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_RENDERBUFFER_EXT, .texture)

					if has_depth <> 0 then

						glGenRenderbuffersEXT(1, @.dBuffer)
						glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, .dBuffer)
						glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, w, h)
						glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, .dBuffer)

						'after a bit of research, it seems this next bit of code wont even work without gl 3.0 headers/support.
						'elseif has_stencil <> 0 then
						'GL_UNSIGNED_INT_24_8_EXT 'this is the correct extension, seems our header is becoming dated.
						'GL_UNSIGNED_INT_24_8_NV 'our header only supplies this, and i don't feel like swigging it.

						'glGenRenderbuffersEXT( 1, @.dBuffer )
						'glBindRenderbufferEXT( GL_RENDERBUFFER_EXT, .dBuffer )
						'glRenderbufferStorageEXT( GL_RENDERBUFFER_EXT, GL_UNSIGNED_INT_24_8_EXT, w, h )

						'glFramebufferRenderbufferEXT( GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, .dBuffer )
						'glFramebufferRenderbufferEXT( GL_FRAMEBUFFER_EXT, GL_STENCIL_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, .dBuffer )

					end if

				else

					glActiveTextureARB(GL_TEXTURE0)
					glGenTextures (1, @.texture)
					glBindTexture (GL_TEXTURE_2D, .texture)
					glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, w, h, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0)
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
					glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE_ARB, GL_INTENSITY)
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE_ARB, GL_COMPARE_R_TO_TEXTURE)
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC_ARB, GL_LEQUAL)
					'glGenerateMipmapEXT(GL_TEXTURE_2D)

					'create framebuffer object and attach texture
					glGenFramebuffersEXT (1, @.fBuffer)
					glBindFramebufferEXT (GL_FRAMEBUFFER_EXT, .fBuffer)
					glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, .texture, 0)
					glDrawBuffer(GL_NONE)
					glReadBuffer(GL_NONE)
					glBindFramebufferEXT (GL_FRAMEBUFFER_EXT, 0)

				end if

				status = glCheckFramebufferStatusEXT( GL_FRAMEBUFFER_EXT )

				if status <> GL_FRAMEBUFFER_COMPLETE_EXT then
					return 0
				end if

				this.width  = W
				this.height = H
				glBindFramebufferEXT ( GL_FRAMEBUFFER_EXT, 0 )

				return 1


			end with

		else
			return 0
		end if

	end function


	sub render_target_struct.destroy()

		with this

			if .fBuffer then
				glDeleteFramebuffersEXT( 1, @.fBuffer)
				glDeleteTextures( 1, @.texture )
			end if

			if .dBuffer then
				glDeleteRenderbuffersEXT(1, @.dBuffer)
			end if

		end with

	end sub

	
	sub enable_shadow_projection( byref light as Light_Struct, byref lookMatrix as nMathf.Matrix )

	dim as nMathf.matrix textureMatrix

	textureMatrix = display.biasMatrix * light.pMatrix * light.vMatrix * lookMatrix.Inverse()

	glActiveTextureARB( GL_TEXTURE0 )
	glEnable( GL_TEXTURE_2D )
	glBindTexture(GL_TEXTURE_2D, display.shadowBuffer.texture)

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE_ARB, GL_COMPARE_R_TO_TEXTURE)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC_ARB, GL_LEQUAL)
	glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE_ARB, GL_INTENSITY)

	glMatrixMode(GL_TEXTURE)
	glLoadMatrixf(textureMatrix)
	glMatrixMode( GL_MODELVIEW )

end sub


sub disable_shadow_projection()

	glDisable(GL_LIGHTING)
	glDisable(GL_ALPHA_TEST)

	glActiveTextureARB( GL_TEXTURE0 )
	glDisable( GL_TEXTURE_2D )

	glMatrixMode(GL_TEXTURE)
	glLoadIdentity()

	glDisable(GL_BLEND)

end sub


	sub Begin_Shadow_Pass()
		glShadeModel( GL_FLAT )
		glDisable( GL_TEXTURE_2D )
		glClear( GL_STENCIL_BUFFER_BIT )
		glDisable(GL_LIGHTING)
		glDepthMask(GL_FALSE)
		glDepthFunc(GL_LEQUAL)

		glEnable(GL_STENCIL_TEST)
		glColorMask(0, 0, 0, 0)
		glStencilFunc(GL_ALWAYS, 1, &hffffffff)
	end sub

	sub End_Shadow_Pass( byref shadowcol as nimage.rgbAlpha = type(0f,0f,0f,0.5) )

		'glColor4f(shadowcol.r, shadowcol.g, shadowcol.b, shadowcol.a)
		'glCallList( display.Stencil_Cube )
		glDisable(GL_STENCIL_TEST)
		glDepthFunc(GL_LESS)
		glDisable(GL_BLEND)
		glDepthMask(1)

	end sub


	Sub Create_Shadow_Buffer()
		#if 0
		'If glfwExtensionSupported( "GL_ARB_depth_texture" ) = 0 Then
		'    Print "Error: ARB depthtexture extension not supported!"
		'    Print "Shadowing will not work."
		'    Beep
		'    Sleep 2000
		'    Use_Shadows = False
		'    Exit Sub
		'End If
		'
		'If glfwExtensionSupported( "GL_ARB_shadow" ) = 0 Then
		'    Print "Error: ARB Shadow extension not supported!"
		'    Print "Shadowing will not work."
		'    Beep
		'    Sleep 2000
		'    Use_Shadows = False
		'    Exit Sub
		'End If

		display.shadowSize = 512
		'Use_Shadows= True
		glGenTextures(1, @display.shadowTex )
		glBindTexture(GL_TEXTURE_2D, display.shadowTex )
		glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, display.shadowSize, display.shadowSize, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP)
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE_ARB, GL_COMPARE_R_TO_TEXTURE)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC_ARB, GL_LEQUAL)
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FAIL_VALUE_ARB, .5)
		'glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE_ARB, GL_INTENSITY)
		'glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE_ARB, GL_COMPARE_R_TO_TEXTURE)
		'glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC_ARB, GL_LEQUAL)
		'glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FAIL_VALUE_ARB, .5)

		'Can't do anything like this without a PCF shader,
		'which would limit the amount of people that can try it even more :(
		'glGenerateMipmapEXT( GL_TEXTURE_2D )
		'
		'If glfwExtensionSupported( ("GL_EXT_texture_filter_anisotropic")) Then
		'   Dim As GlFloat Largest_Texture_Filter
		'    glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, @Largest_Texture_Filter)
		'    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, Largest_Texture_Filter)
		'End If


		'exit sub



		glGenFramebuffersEXT (1, @display.Frame_Buffer)
		glGenRenderbuffersEXT(1, @display.Depth_rb)
		glBindFramebufferEXT (GL_FRAMEBUFFER, display.Frame_Buffer)
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, display.shadowTex, 0)

		'glGenRenderbuffersEXT(1, @.dBuffer)
		'glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, .dBuffer)
		'glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, w, h)
		'glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, .dBuffer)

		glDrawBuffer(GL_NONE)
		glReadBuffer(GL_NONE)
		glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, display.Depth_Rb)
		glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, display.ShadowSize, display.ShadowSize)
		glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, display.Depth_Rb)

		dim as integer status = glCheckFramebufferStatusEXT( GL_FRAMEBUFFER_EXT )

		if status <> GL_FRAMEBUFFER_COMPLETE_EXT then

			print #debugout, "BAD MOJO!!!"

		else

			print #debugout, "Shadow buffer created"

		end if

		glBindFramebufferEXT (GL_FRAMEBUFFER_EXT, 0)

		#endif

	End Sub


	sub set_ortho( byval W as integer, byval H as integer )

		glMatrixMode( Gl_PROJECTION )
		glPushMatrix()
		glLoadIdentity()
		glMatrixMode( Gl_MODELVIEW )
		glPushMatrix()
		glLoadIdentity()
		glOrtho( 0, W, 0, H, -1, 1 )

	end sub


	sub drop_ortho()

		glMatrixMode( Gl_PROJECTION )
		glPopMatrix()
		glMatrixMode( Gl_MODELVIEW )
		glPopMatrix()

	end sub


	sub init_light( byref index as integer, byref light as Light_Struct, byref ambient as nimage.RGBALpha, byref diffuse as nimage.RGBAlpha, byref specular as nimage.RGBAlpha, byref position as nmathf.vec4f, byref direction as nmathf.vec3f )

		light.ambient = ambient
		light.diffuse = diffuse
		light.specular = specular
		light.position = position
		light.direction = direction

		glEnable( GL_LIGHT0 + index )
		glEnable( GL_LIGHTING )

		'slightly better specular reflections without using a shader...
		glLightModeli(GL_LIGHT_MODEL_COLOR_CONTROL, GL_SEPARATE_SPECULAR_COLOR)

		glLightfv( GL_LIGHT0 + index, GL_AMBIENT, @light.ambient.r )
		glLightfv( GL_LIGHT0 + index, GL_DIFFUSE, @light.diffuse.r )
		glLightfv( GL_LIGHT0 + index, GL_SPECULAR, @light.specular.r )
		glLightfv( GL_LIGHT0 + index, GL_POSITION, @light.position.x )

		glMatrixMode(GL_MODELVIEW)

		glLoadIdentity()
		gluPerspective(60, 1, 2, 200)
		glGetFloatv(GL_MODELVIEW_MATRIX, light.pMatrix )

		glLoadIdentity()
		gluLookAt(	light.Position.x, light.Position.y, light.Position.z, 0, 0, 0, 0, 1, 0)
		glGetFloatv(GL_MODELVIEW_MATRIX, light.vMatrix )

	end sub


	sub make_font_dlist( byref DList as GlUINT,  byval Wtiles as single, byval Htiles as single, byval t_Scale as integer )

		DList = glgenlists(256)
		dim as integer i
		dim as double Tx, Ty, XStepp = 1/Wtiles, YStepp = 1/Htiles
		glMatrixMode( GL_MODELVIEW )
		glLoadidentity()
		glPushmatrix()
		i=0
		for TY as integer = Htiles-1 to 0 step -1
			for TX as integer = 0 to Wtiles-1
				glNewList DList+i, GL_COMPILE
				glBegin( GL_QUADS )

				glTexCoord2d( Tx*XStepp, (Ty+1)*YStepp )
				glVertex2i( 0, t_Scale )

				glTexCoord2d( Tx*XStepp, Ty*YStepp )
				glVertex2i( 0, 0 )

				glTexCoord2d( (Tx+1)*XStepp, Ty*YStepp )
				glVertex2i( t_Scale, 0 )

				glTexCoord2d( (Tx+1)*XStepp, (Ty+1)*YStepp )
				glVertex2i( t_Scale, t_Scale )

				glEnd()

				glTranslatef( t_Scale, 0, 0 )
				glEndList()
				i+=1
			next
		next
		glPopMatrix()

	end sub


	sub glprint( byref Strng as string, byval Texture as Gluint, byval Text_List as GLUINT, byval X as single, byval Y as single, byval R as single, byval G as single, byval B as single )

		dim as integer i, Length = len(Strng)
		'glEnable( GL_TEXTURE_2D )
		'glBindTexture( GL_TEXTURE_2D, Texture )
		'glEnable( GL_BLEND )
		'glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA )
		'glPolygonMode( GL_FRONT, GL_FILL )
		'glMatrixMode( GL_PROJECTION )
		'glPushMatrix()
		'glLoadIdentity()
		'glMatrixMode( GL_MODELVIEW )
		'glPushMatrix()
		'glLoadIdentity()
		'glOrtho( 0,Display.W, 0, Display.H, -1, 1 )
		'glDisable( GL_LIGHTING )
		'glDisable( GL_DEPTH_TEST )

		glTranslatef( X, Y, 0 )
		glColor3f( R,G,B )
		glListbase( Text_List )
		glCallLists( Length, GL_UNSIGNED_BYTE, strptr(Strng) )

		'glMatrixMode( GL_PROJECTION )
		'glPopMatrix()
		'glmatrixmode( GL_MODELVIEW )
		'glPopMatrix()
		'glenable( GL_LIGHTING )
		'glEnable( GL_DEPTH_TEST )
		'glDisable( GL_BLEND )

	end sub


	function init_gl_window( byval W as integer, byval H as integer, byval BPP as integer, byval Num_buffers as integer, byref Num_Samples as integer, byval Fullscreen as integer ) as integer

		dim Flags as integer = FB.GFX_OPENGL

		if FullScreen <> 0 then
			Flags or = FB.GFX_FULLSCREEN
		end if

		screencontrol FB.SET_GL_DEPTH_BITS, 24
		screencontrol FB.SET_GL_COLOR_BITS, BPP
		screencontrol FB.SET_GL_STENCIL_BITS, 8

		if Num_Samples>0 then
			screencontrol FB.SET_GL_NUM_SAMPLES, Num_Samples
			Flags or = FB.GFX_MULTISAMPLE
		end if

		screenres W, H, BPP, Num_Buffers, Flags or FB.GFX_HIGH_PRIORITY

		Display.glVer = *glGetString( GL_VERSION )

		glviewport( 0, 0, W, H )
		Display.W = W
		Display.H = H
		Display.W2 = W\2
		Display.H2 = H\2
		Display.BPP = BPP
		Display.FOV = 60
		Display.Aspect = W/H
		Display.znear = .1
		Display.zfar = 400

		'display.pMatrix.InfiniteProjection( display.fov, display.aspect, display.znear )
		display.pMatrix.perspective( display.fov, display.aspect, display.znear, display.zFar )
		glMatrixMode( GL_PROJECTION )
		glLoadIdentity()
		glLoadMatrixf( display.pMatrix )
		glMatrixMode( GL_MODELVIEW )
		glLoadIdentity()

		glClearColor( 0, 0, 0, 0 )
		glClearDepth( 1.0 )
		glEnable( GL_DEPTH_TEST )
		glDepthFunc( GL_LEQUAL )
		glEnable( GL_COLOR_MATERIAL )
		glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE)

		glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST )
		glPolygonmode( GL_FRONT, GL_FILL )
		glEnable( GL_CULL_FACE )
		glCullFace(GL_FRONT)
		glFrontFace( GL_CCW )
		glShadeModel( GL_SMOOTH )
		'glEnable(GL_NORMALIZE)

		gather_extensions()
		
		display.shadowBuffer.Create( 4096, 4096, 0, 0, true )
		
		display.pickBuffer.Create(display.W, display.h, 1, 0) 
		
		display.biasMatrix.m(0) = .5
		display.biasMatrix.m(1) = 0
		display.biasMatrix.m(2) = 0
		display.biasMatrix.m(3) = 0

		display.biasMatrix.m(4) = 0
		display.biasMatrix.m(5) = .5
		display.biasMatrix.m(6) = 0
		display.biasMatrix.m(7) = 0

		display.biasMatrix.m(8) =  0
		display.biasMatrix.m(9) =  0
		display.biasMatrix.m(10) = .5
		display.biasMatrix.m(11) = 0

		display.biasMatrix.m(12) = .5
		display.biasMatrix.m(13) = .5
		display.biasMatrix.m(14) = .5
		display.biasMatrix.m(15) = 1


		'glEnable( GL_VERTEX_ARRAY ) 'enabled for the duration of the game...
		build_stencil_cube( display.stencil_cube )

		display.billboard = glGenLists(1)

		glNewList( display.billboard, GL_COMPILE )

		glBegin( GL_QUADS )
		glVertex3f( -1, 1, 0 )
		glTexCoord2f(0,1)

		glVertex3f( -1, -1, 0 )
		glTexCoord2f(0,0)

		glVertex3f( 1, -1, 0 )
		glTexCoord2f(1,0)

		glVertex3f( 1, 1, 0 )
		glTexCoord2f(1,1)
		glEnd()

		glEndList()

		return 1

	end function


	sub gather_extensions()

		dim extensions as string
		screencontrol FB.GET_GL_EXTENSIONS, extensions

		dim as integer i, j

		do
			i+=1
			j=i
			dim as string tstring
			dim as string char
			do
				char = mid(extensions,j,1)
				tstring+=char
				j=j+1
				if char = " " then
					exit do
				end if
			loop while j<len(extensions)

			i = j-1
			print #debugout, tstring

		loop while i<len(extensions)


		if (instr(extensions, "GL_EXT_texture_filter_anisotropic") <> 0) then
			_anisotropic_ = 1
		end if

		if (instr(extensions, "GL_ARB_depth_texture") <> 0 ) then

			#ifdef _debugout
			print #debugout, "GL_ARB_depth_texture is supported"
			#endif

		end if


		if (instr(extensions, "GL_ARB_shadow") <> 0 ) then

			#ifdef _debugout
			print #debugout, "GL_ARB_shadow is supported"
			#endif

		end If


		if (instr(extensions, "GL_EXT_framebuffer_object") <> 0) then

			#ifdef _debugout
			print #debugout, "GL_EXT_framebuffer_object is supported"
			#endif

			_framebuffer_ = 1
			glGenFramebuffersEXT            = screenglproc("glGenFramebuffersEXT")
			glDeleteFramebuffersEXT         = screenglproc("glDeleteFramebuffersEXT")
			glBindFramebufferEXT            = screenglproc("glBindFramebufferEXT")
			glFramebufferTexture2DEXT       = screenglproc("glFramebufferTexture2DEXT")
			glFramebufferRenderbufferEXT    = screenglproc("glFramebufferRenderbufferEXT")
			glGenRenderbuffersEXT           = screenglproc("glGenRenderbuffersEXT")
			glBindRenderbufferEXT           = screenglproc("glBindRenderbufferEXT")
			glRenderbufferStorageEXT        = screenglproc("glRenderbufferStorageEXT")
			glDeleteRenderbuffersEXT		  = screenglproc("glDeleteRenderbuffersEXT")
			glCheckFramebufferStatusEXT	  = screenglproc("glCheckFramebufferStatusEXT")
			glGenerateMipmapEXT          	  = screenglproc("glGenerateMipmapEXT")

			glBlendFuncSeparate				  = screenglproc("glBlendFuncSeparate")

		else
			#ifdef _debugout
			print #debugout, "GL_EXT_framebuffer_object is NOT supported"
			#endif
		end if


		'VBO... vertex buffer objects... (store geometry on GPU)
		if (instr(extensions, "GL_ARB_vertex_buffer_object") <> 0) then

			#ifdef _debugout
			print #debugout, "GL_ARB_vertex_buffer_object supported"
			#endif

			_vbo_ = true

			glDeleteBuffers						= screenglproc("glDeleteBuffers")
			glGenBuffers	 						= screenglproc("glGenBuffers")
			glIsBuffer 								= screenglproc("glIsBuffer")
			glBufferData 							= screenglproc("glBufferData")
			glBufferSubData 						= screenglproc("glBufferSubData")
			glGetBufferSubData 					= screenglproc("glGetBufferSubData")
			glMapBuffer 							= screenglproc("glMapBuffer")
			glUnMapBuffer 							= screenglproc("glUnMapBuffer")
			glGetBufferParameteriv 				= screenglproc("glGetBufferParameteriv")
			glGetBufferPointerv 					= screenglproc("glGetBufferPointerv")
			glBindBuffer							= screenglproc("glBindBuffer")

		else
			#ifdef _debugout
			print #debugout, "GL_ARB_vertex_buffer_object not supported."
			#endif
		end if



		if (instr(extensions, "GL_ARB_multitexture") <> 0) then

			#ifdef _debugout
			print #debugout, "GL_ARB_multitexture is supported"
			#endif

			_multitexture_ = 1
			glMultiTexCoord2fARB            = screenglproc("glMultiTexCoord2fARB")
			glMultiTexCoord2fvARB           = screenglproc("glMultiTexCoord2fvARB")
			glActiveTextureARB              = screenglproc("glActiveTextureARB")
			glClientActiveTextureARB        = screenglproc("glClientActiveTextureARB")
			glgetintegerv( GL_MAX_TEXTURE_UNITS_ARB, @maxTexelUnits )
			#ifdef _debugout
			print #debugout, maxTexelUnits & " Texture units supported"
			#endif
			if maxTexelUnits<3 then
				_multitexture_ = 0
				#ifdef _debugout
				print #debugout, "Insufficient texture units for multitexture support"
				#endif
			end if

		else
			#ifdef _debugout
			print #debugout, "GL_ARB_multitexture is NOT supported"
			#endif
		end if


		if (instr(extensions, "GL_ARB_shading_language_100") <> 0) then
			#ifdef _debugout
			print #debugout, "GL_ARB_shading_language_100 is supported"
			#endif
			if (instr(extensions, "GL_ARB_vertex_program") <> 0) then
				#ifdef _debugout
				print #debugout, "GL_ARB_vertex_program is supported"
				#endif
				if (instr(extensions, "GL_ARB_fragment_program") <> 0) then
					#ifdef _debugout
					print #debugout, "GL_ARB_fragment_program is supported"
					#endif
					_shader100_ = 1
					glCreateShaderObjectARB     = screenglproc("glCreateShaderObjectARB")
					glShaderSourceARB           = screenglproc("glShaderSourceARB")
					glGetShaderSourceARB        = screenglproc("glGetShaderSourceARB")
					glCompileShaderARB          = screenglproc("glCompileShaderARB")
					glDeleteObjectARB           = screenglproc("glDeleteObjectARB")
					glCreateProgramObjectARB    = screenglproc("glCreateProgramObjectARB")
					glAttachObjectARB           = screenglproc("glAttachObjectARB")
					glUseProgramObjectARB       = screenglproc("glUseProgramObjectARB")
					glLinkProgramARB            = screenglproc("glLinkProgramARB")
					glValidateProgramARB        = screenglproc("glValidateProgramARB")
					glGetInfoLogARB             = screenglproc("glGetInfoLogARB")
					glGetObjectParameterivARB   = screenglproc("glGetObjectParameterivARB")
					glGetUniformLocationARB     = screenglproc("glGetUniformLocationARB")
					glUniform1iARB              = screenglproc("glUniform1iARB")
					glUniform1fARB              = screenglproc("glUniform1fARB")
					glUniform2fvARB             = screenglproc("glUniform2fvARB")
					glUniform3fvARB             = screenglproc("glUniform3fvARB")
				else
					#ifdef _debugout
					print #debugout, "GL_ARB_fragment_program is NOT supported"
					#endif
				end if
			else
				#ifdef _debugout
				print #debugout, "GL_ARB_vertex_program is NOT supported"
				#endif
			end if
		else
			#ifdef _debugout
			print #debugout, "GL_ARB_shading_language_100 is NOT supported"
			#endif
		end if

		glActiveStencilFaceEXT = screenglproc("glActiveStencilFaceEXT")
		if glActiveStencilFaceEXT = 0 then
			_stencil2_ = 0
			#ifdef _debugout
			print #debugout, "glActiveStencilFaceEXT is NOT supported"
			#endif
		else
			#ifdef _debugout
			print #debugout, "glActiveStencilFaceEXT is supported"
			#endif
			_stencil2_ = 1
		end if

	end sub


	function init_shader( byref File_Name as string, byref Shader_Type as integer )as GlHandleARB

		dim as integer i
		dim as integer Line_Cnt
		dim as string Shader_Text, tString
		dim as Gluint Shader_Compile_Success
		dim as GlHandleARB Shader
		dim as uinteger FileNum = freefile

		open File_Name for binary as #FileNum
		do while not eof(FileNum)
			line input #FileNum, tString
			Shader_Text += tString + chr( 13, 10 )
		loop
		close #FileNum

		dim as GLcharARB ptr table(0) => { strptr( Shader_Text ) }
		Shader = glCreateShaderObjectARB( Shader_Type )
		glShaderSourceARB( Shader, 1, @table(0), 0 )
		glCompileShaderARB( Shader )

		glGetObjectParameterivARB( Shader, GL_OBJECT_COMPILE_STATUS_ARB, @Shader_Compile_Success )
		if Shader_Compile_Success = 0 then
			dim as Gluint infologsize
			glGetObjectParameterivARB( Shader, GL_OBJECT_INFO_LOG_LENGTH_ARB, @infoLogSize)
			dim as GlByte infolog(InfoLogSize)
			glGetInfoLogARB( Shader, InfoLogSize, 0, @infoLog(0))
			tString=""
			for i = 0 to InfoLogSize-1
				tString+=chr(InfoLog(i))
			next
			#ifdef _debugout
			print #debugout, "Shader Infolog error message:"
			print #debugout, tString
			#endif
			return 0
		else
			return Shader
		end if

	end function

	sub build_stencil_cube( byref List as gluint )

		List = glGenLists(1)
		glNewList( List, GL_COMPILE )
		'glFrontFace(GL_CCW)
		glColorMask(1, 1, 1, 1)
		'glColor4f(0.0, 0.0, 0.0, 0.5)
		glEnable(GL_BLEND)
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
		glStencilFunc( GL_NOTEQUAL, 0, &hffffffff )
		glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP)
		'glpushmatrix()
		glloadidentity()
		glbegin(GL_TRIANGLE_STRIP)
		glVertex3f(-1, 1, -1)
		glVertex3f(-1,-1, -1)
		glVertex3f( 1, 1, -1)
		glVertex3f( 1,-1, -1)
		glend()
		'glpopmatrix()

		glDisable(GL_BLEND)
		glDepthFunc(GL_LEQUAL)
		glDepthMask(GL_TRUE)
		glEnable(GL_LIGHTING)
		glDisable(GL_STENCIL_TEST)
		glShadeModel(GL_SMOOTH)
		glEndList()

	end sub

	constructor font_struct()

	end constructor

	constructor font_struct( byval scale as single )
	
	dim as integer fNum = freefile
	
	if scale = 0 then
		scale = 16
	end if

	this.scale = scale
	this.kern = .6'75

	'glgentextures( 1, @this.texture )

	this.dList = glGenLists(256)

	dim as integer i = 0
	dim as single Tx, Ty, stepp = 1/16
	glMatrixMode( GL_MODELVIEW )
	glLoadidentity()
	glPushmatrix()
	
	dim as string fRead(255)
	
	open "res/data/Font Metrics.ini" for input as #fNum
	
	line input #fNum, fRead(0)
	
	for v as integer = 0 to 255
	
		line input #fNum, fRead(v)
		
	next
	close #fNum
	
	for TY as integer = 15 to 0 step -1
		for TX as integer = 0 to 15
			
			dim as string temp = fRead(i)
			
			dim as integer vSpot = instr(temp,"=")+1
			
			dim as single pOut = val(mid(temp,vSpot))/100f
			
			glNewList( this.dList+i, GL_COMPILE )
			glBegin( GL_QUADS )

			glTexCoord2d( Tx*Stepp, (Ty+1)*Stepp )
			glVertex2f( 0, 1.25 )

			glTexCoord2d( Tx*Stepp, Ty*Stepp )
			glVertex2f( 0, 0 )

			glTexCoord2d( (Tx+1)*Stepp, Ty*Stepp )
			glVertex2f( 1, 0 )

			glTexCoord2d( (Tx+1)*Stepp, (Ty+1)*Stepp )
			glVertex2f( 1, 1.25 )

			glEnd()


			'glTranslatef( this.kern, 0, 0 )
			glTranslatef(.675,0,0)
			glEndList()
			i+=1
		next
	next
	
	
	
	glPopMatrix()

	'this saves a mul in multiple places later... possibly thousands
	'if you need the actual value of kern back... lol :p
	this.kern = this.scale*this.kern

	#ifdef _debugout
	print #debugout, "font constructor called"
	#endif

	end constructor

	destructor font_struct()

	glDeleteLists( this.dList, 256 )
	glDeleteTextures( 1, @this.texture )

	#ifdef _debugout
	print #debugout, "font destructor called"
	#endif

	end destructor


	sub font_struct.oBegin()

		glEnable( GL_TEXTURE_2D )
		glBindTexture( GL_TEXTURE_2D, this.texture )
		glEnable( GL_BLEND )
		glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA )
		glPolygonMode( GL_FRONT, GL_FILL )
		glMatrixMode( GL_PROJECTION )
		glPushMatrix()
		glLoadIdentity()
		glMatrixMode( GL_MODELVIEW )
		glPushMatrix()
		glLoadIdentity()
		glOrtho( 0,Display.W, 0, Display.H, -1, 1 )
		glDisable( GL_LIGHTING )
		glDisable( GL_DEPTH_TEST )
		glCullFace(GL_BACK)

	end sub

	sub font_struct.oEnd()

		glMatrixMode( GL_PROJECTION )
		glPopMatrix()
		glmatrixmode( GL_MODELVIEW )
		glPopMatrix()
		glenable( GL_LIGHTING )
		glEnable( GL_DEPTH_TEST )
		glDisable( GL_BLEND )

	end sub

	sub font_struct.oPrint( byval Strng As String, Byval X As Single, Byval Y As Single, Byval R As single = 1.0, Byval G As single = 1.0, Byval B As single = 1.0, byval a as single = 1.0, byval sx as single = 0.0, byval sy as single = 0.0 )

		if sx = 0 then
			sx = this.scale
		end if

		if sy = 0 then
			sy = this.scale
		end if
		
		glPushMatrix()
		glTranslatef( X, Y, 0 )
		glScalef(sx,sy,1)
		glColor4f( R,G,B,A )
		glListbase( this.dList )
		glCallLists( len(strng), GL_UNSIGNED_BYTE, strptr(Strng) )
		glPopMatrix()

	end sub

	sub font_struct.load( byref fileName as string )

		dim as string tstring
		dim as integer lastslash

		for i as integer = 1 to len(filename)
			if mid(filename,i,1) = "/" then
				lastslash=i
			end if
		next

		tstring = mid(filename, lastslash + 1 )


		glGenTextures( 1, @this.texture )
		glBindTexture( GL_TEXTURE_2D, this.texture )
		glEnable( GL_TEXTURE_2D )

		dim SprWidth as integer
		dim SprHeight as Integer
		dim as any ptr temp


		if temp=0 Then
			png_dimensions( "res/texture/" & tstring, SprWidth, SprHeight )
			temp = png_load( "res/texture/" & tstring, PNG_TARGET_OPENGL )
		end if

		glteximage2d( GL_TEXTURE_2D, 0, GL_RGBA, SprWidth, SprHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, temp )

		'glubuild2dmipmaps( GL_TEXTURE_2D, GL_RGBA, SprWidth, SprHeight, GL_RGBA, GL_UNSIGNED_BYTE, temp )

		'if force_clamp then
		gltexparameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE )
		gltexparameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE )
		'else
		'	gltexparameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT )
		'	gltexparameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT )
		'end if

		gltexparameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR )
		gltexparameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR )
		gltexenvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)


		'if use_anisotropic <> 0 then
		'	dim as glFloat Largest_Texture_Filter
		'	glgetfloatv( GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, @Largest_Texture_Filter )
		'	gltexparameterf( GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, Largest_Texture_Filter )
		'end if

		png_destroy( temp )
		
		print #debugout, "font loaded, " & filename

	end sub

end namespace
