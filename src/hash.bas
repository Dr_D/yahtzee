#include once "crt.bi"
#include once "hash.bi"
#include once "global_constants.bi"

'yetifoot

extern h_table	as nhash.ht_t ptr

namespace nhash


	constructor ht_t _
	( _
	byval _size_ as integer _
	)

	ht = callocate( _size_ * sizeof( ht_node_t ) )
	htid = callocate( _size_ * sizeof( ht_node_t ptr ) )
	count = 0
	size = _size_
	free_func = null
	
	#ifdef _debugout
		print #debugout, "hash constructor called"
	#endif
	end constructor

	destructor ht_t _
	( _
	)

	for i as integer = 0 to size - 1
		if ht[i].s then
			deallocate( ht[i].s )
			if ht[i]._data then
				if free_func then
					free_func( ht[i]._data )
				else
					free( ht[i]._data )
				end if
			end if
		end if
	next i

	deallocate( ht )
	deallocate( htid )
	
	#ifdef _debugout
		print #debugout, "hash destructor called"
	#endif
	
	end destructor
	
	
	sub ht_t.hkill _
	( _
	)

	for i as integer = 0 to size - 1
		if ht[i].s then
			deallocate( ht[i].s )
			if ht[i]._data then
				if free_func then
					free_func( ht[i]._data )
				else
					free( ht[i]._data )
				end if
			end if
		end if
	next i

	deallocate( ht )
	deallocate( htid )
	
	#ifdef _debugout
		print #debugout, "hash removed manually"
	#endif
	
	end sub
	
	
	function ht_t.add _
		( _
		byval s     as zstring ptr, _
		byval _data as any ptr _
		) as ht_node_t ptr

		dim as uinteger p = any

		function = NULL

		if s then
			if lookup( s ) = NULL then
				p = hash( s ) mod size
				while ht[p].s <> NULL
					p = (p + 1) mod size
				wend
				ht[p].s = callocate( strlen( s ) + 1 )
				*ht[p].s = *s
				ht[p].id = count
				ht[p]._data = _data
				htid[count] = @ht[p]
				count += 1
			end if
		end if

	end function

	function ht_t.lookup _
		( _
		byval s as zstring ptr _
		) as ht_node_t ptr

		dim as uinteger p = any

		function = NULL

		if s then
			p = hash( s ) mod size
			while ht[p].s <> NULL
				if *ht[p].s = *s then
					function = @ht[p]
					exit while
				end if
				p = (p + 1) mod size
			wend
		end if

	end function

	function ht_t.lookup _
		( _
		byval id as integer _
		) as ht_node_t ptr

		function = htid[id]

	end function

	sub ht_t.set_free _
		( _
		byval _free_func_ as free_func_t _
		)

		free_func = _free_func_

	end sub

	function ht_t.hash _
		( _
		byval s as zstring ptr _
		) as uinteger

		dim as uinteger result = 0
		dim as integer  s_len = strlen( s )

		for i as integer = 0 to s_len - 1
			result = s[i] + (result shl 5) - result
		next i

		function = result

	end function

end namespace
