#include once "physics.bi"

extern Display as ndisplay.Display_Struct
extern h_table	as nhash.ht_t ptr

namespace nphysics

	'this file need restructuring more than anything else
	'there are some problems with the structure, obviously
	'this is physics.bas, which should intuitively handle only physics, but it doesn't...
	'its basically the game handler module... since it contains the world structure
	
	sub world_struct.render_scene( byref lookMatrix as nMathf.Matrix )


	glDisable( GL_BLEND )

	display.shadowBuffer.Lock()

	glClear( GL_DEPTH_BUFFER_BIT or GL_COLOR_BUFFER_BIT )

	glMatrixMode( GL_PROJECTION )
	glLoadMatrixf( this.light.pMatrix )

	glMatrixMode( GL_MODELVIEW )
	glLoadMatrixf( this.light.vMatrix )

	glDisable(GL_CULL_FACE)
	'glCullFace(GL_FRONT)
	glShadeModel(GL_FLAT)
	glColorMask(0, 0, 0, 0)

	glEnable( GL_POLYGON_OFFSET_FILL )
	glPolygonOffset( 2, 4 )
	glUseProgramObjectArb( Shader_Shadow )

	with this

		if ubound(.static_entity)>-1 then

			for se as integer = 0 to ubound(.static_entity)

				with .static_entity(se)

					if not .culled then

						nmodel.renderdepthpass( .tModel, .tMatrix )

					end if

				end with

			next

		end if


		if ubound(.entity)>-1 then

			for e as integer = 0 to ubound(.entity)

				with .entity(e)

					'if .eType<>eCAMERA then

					if not .culled then

						nmodel.renderdepthpass( .tModel, .tMatrix )

					end if

					'end if

				end with

			next

		end if

	end with

	glUseProgramObjectArb( 0 )

	glDisable( GL_POLYGON_OFFSET_FILL )

	display.shadowBuffer.Unlock()


	glEnable( GL_CULL_FACE )
	glCullFace(GL_BACK)
	glShadeModel(GL_SMOOTH)
	glColorMask(1, 1, 1, 1)
	glEnable(GL_COLOR_MATERIAL )


	'2nd pass
	'drawBuffer.Lock()

	glClear( GL_DEPTH_BUFFER_BIT or GL_COLOR_BUFFER_BIT )

	glViewport(0, 0, Display.W, Display.H)

	glMatrixMode( GL_TEXTURE)
	glLoadIdentity()
	glMatrixMode( GL_PROJECTION )
	glLoadMatrixf( display.pMatrix )

	glMatrixMode( GL_MODELVIEW )
	glLoadMatrixf(lookMatrix)


	glLightfv( GL_LIGHT1, GL_POSITION, @this.light.position.x )
	glLightfv( GL_LIGHT1, GL_AMBIENT, @nImage.DGRAY.r )
	glLightfv( GL_LIGHT1, GL_DIFFUSE, @nImage.DGRAY.r )
	glLightfv( GL_LIGHT1, GL_SPECULAR, @nImage.BLACK.r )
	glEnable(GL_LIGHTING)
	glEnable(GL_LIGHT1)

	glColor4f(.2,.2,.2,1)


	glUseProgramObjectARB( Shader_PCF )
	glUniform1iARB(tType, 0)

	with this

		if ubound(.static_entity)>-1 then

			for se as integer = 0 to ubound(.static_entity)

				with .static_entity(se)

					if not .culled then

						nmodel.render_model_vbo( .tModel, .tMatrix )

					end if

				end with

			next

		end if


		if ubound(.entity)>-1 then

			for e as integer = 0 to ubound(.entity)

				with .entity(e)

					'if .eType<>eCAMERA then

					if not .culled then

						nmodel.render_model_vbo( .tModel, .tMatrix )

					end if

					'end if

				end with

			next

		end if


	end with

	glUseProgramObjectARB( 0 )



	'pass 3
	ndisplay.enable_shadow_projection( this.light, lookMatrix )

	glLightfv( GL_LIGHT1, GL_AMBIENT, @this.light.ambient.r )
	glLightfv( GL_LIGHT1, GL_DIFFUSE, @this.light.diffuse.r )
	glLightfv( GL_LIGHT1, GL_SPECULAR, @this.light.specular.r )
	glLightModeli(GL_LIGHT_MODEL_COLOR_CONTROL, GL_SEPARATE_SPECULAR_COLOR)

	glMatrixMode( GL_PROJECTION )
	glLoadMatrixf( display.pMatrix )

	glMatrixMode( GL_MODELVIEW )
	glLoadMatrixf(lookMatrix)

	glColor4f(1,1,1,1)
	glUseProgramObjectARB( Shader_PCF )
	glUniform1iARB(passNum, 1 )


	with this

		if ubound(.static_entity)>-1 then

			for se as integer = 0 to ubound(.static_entity)

				with .static_entity(se)

					if not .culled then

						nmodel.render_model_vbo( .tModel, .tMatrix )

					end if

				end with

			next

		end if


		if ubound(.entity)>-1 then

			for e as integer = 0 to ubound(.entity)

				with .entity(e)

					'if .eType<>eCAMERA then

					if not .culled then

						nmodel.render_model_vbo( .tModel, .tMatrix )

					end if

					'end if

				end with

			next

		end if


	end with

	glUniform1iARB(passNum, 0 )

	glUseProgramObjectARB( 0 )

	ndisplay.disable_shadow_projection()

	'drawBuffer.Unlock()


	'glDisable(GL_LIGHTING)
	'glDisable(GL_LIGHT1)
	'glColor3f(1,1,1)
	'
	'glDisable(GL_CULL_FACE)
	'
	'Set_Ortho(display.w, display.h)
	'
	'	glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)
	'
	'	glActiveTextureARB( GL_TEXTURE1 )
	'	glEnable(GL_TEXTURE_2D)
	'	glBindTexture( GL_TEXTURE_2D, drawBuffer.texture )
	'
	'	glColor3f(1,1,1)
	'
	'	glUseProgramObjectARB( Shader_Blur )
	'
	'	glUniform1fARB(blurLoc, 10+9.9*sin(timer*10))
	'
	'	glBegin(GL_QUADS)
	'
	'		glMultiTexCoord2fARB(GL_TEXTURE1,0,0)
	'		glVertex3f(0,0,0)
	'
	'		glMultiTexCoord2fARB(GL_TEXTURE1,1,0)
	'		glVertex3f(display.W,0,0)
	'
	'		glMultiTexCoord2fARB(GL_TEXTURE1,1,1)
	'		glVertex3f(display.W,display.H,0)
	'
	'		glMultiTexCoord2fARB(GL_TEXTURE1,0,1)
	'		glVertex3f(0,display.H,0)
	'
	'	glEnd()
	'
	'	glUseProgramObjectARB(0)
	'
	'Drop_Ortho()

	end sub
	
	function RayPrefilterCallback cdecl(byval nBody as NewtonBody ptr, byval collision as NewtonCollision ptr, byval aPtr as any ptr) as uinteger
		
		dim as dFloat Mass, Mx, My, Mz
		dim as Ray_Struct ptr Ray
		dim as Entity_Struct ptr Entity

		NewtonBodyGetMassMatrix( nBody, @Mass, @Mx, @My, @Mz)
		
		if mass>0 then
			
			entity = NewtonBodyGetUserData( nBody )
			
			print #debugout, entity->eName 
			
			return 1
			
			
		else
			
			'not sure how to get the surface here...
			'the plan was to skip anything with the tag "invisible"
			
		end if
		
		return 0
		
	end function
	
	
	function character_raycast_world( byval nWorld as NewtonWorld ptr, byref mControl as nMathf.vMat ) as Ray_Struct
		
		using nMathf
		
		dim As GLdouble modelview(15)
		glGetDoublev( GL_MODELVIEW_MATRIX, @modelview(0) )
		dim As GLdouble projection(15)
		glGetDoublev( GL_PROJECTION_MATRIX, @projection(0) )
		dim As GLint viewport(3)
		glGetIntegerv(GL_VIEWPORT, @viewport(0))
		dim As Single WinX, WinY, WinZ
		dim as integer mouseX, mouseY
		getmouse( mouseX, mouseY )
		WinX = MouseX
		WinY = MouseY
		WinY = viewport(3) - WinY
		'glReadPixels(winX, winY, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, @winZ)
		'if winZ=1 Then winZ = .995
		'setting z to 1 to cast an infinited ray...
		winZ = 1

		'dim As UBYTE Col(3)
		'glReadPixels(winX, winY, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, @Col(0))

		dim As GLdouble posX, posY, posZ
		gluUnProject( winX, winY, winZ, @modelview(0), @projection(0), @viewport(0), @posX, @posY, @posZ )


		dim as Vec3f Ray_Begin = mControl.p
		dim as vec3f Ray_End = vec3f(posX, posY, posZ)' mControl.p + mControl.f * 25f

		dim Ray_Data as Ray_Struct
		Ray_Data.Designation = "No objects in range."

		NewtonWorldRaycast( nWorld, @Ray_Begin.x, @Ray_End.x, @character_ray_filter(), @Ray_Data, 0 )

		Ray_Data.InterSection_Point = Ray_Begin + ((Ray_End - Ray_Begin) * Ray_Data.Param)

		if Ray_Data.nBody then

			dim as entity_struct ptr Hit_Object

			Hit_Object = NewtonBodyGetUserData( Ray_Data.nBody )

			Ray_Data.Designation = Hit_Object->eName

			dim as vec3f Mag_Vector
			Mag_Vector = Ray_Data.InterSection_Point - Hit_Object->tMatrix.position
			Mag_Vector.Normalize

			dim as single Mag = Hit_Object->Mass

			dim as Vec3f Force_Vector
			Force_Vector = -Ray_Data.Normal*Mag

			'NewtonWorldUnfreezeBody( nWorld, Ray_Data.nBody )
			'NewtonAddBodyImpulse( Ray_Data.nBody, @Force_Vector.x, @Ray_Data.InterSection_Point.x )
			
			dim as dFloat Mass, Ixx, Iyy, Izz
			NewtonBodyGetMassMatrix( Ray_Data.nBody, @Mass, @Ixx, @Iyy, @Izz )
			
			if mass = 0 then Ray_Data.nBody = 0
			
		else
			
			Ray_Data.Designation = *h_table->lookup( Ray_Data.Collision_ID )->s
			
			
		end if

		return Ray_Data

	end function
	
		
	sub world_struct.ProcessDrivers()
		
		#if 0
		
		using nmathf

		with this

			'autonomous vehicles... not a ton of AI in here.
			for v as integer = 0 to ubound(.vehicle)

				'this first section resets and aligns the vehicle to their last checkpoint, if it gets stuck somehow
				'it works on a timer, which you can adjust with "world_struct.entity_reset_time"
				if .vehicle(v).last_position.distance(.vehicle(v).tMatrix.Position)<.005 then

					if this.last_time>.vehicle(v).reset_timer then

						dim as vec3f firstBonePos = .model(0).bone(.vehicle(v).current_node).posit(1)
						dim as vec3f secBonePos = .model(0).bone(.vehicle(v).current_node).posit(0)
						dim as vec3f direction = secBonePos - firstBonePos
						direction.normalize
						secBonePos.y+=3
						dim as nmathf.matrix idMatrix
						idMatrix.LoadIdentity()
						idMatrix.PointAt( secBonePos+direction, firstBonePos+direction )
						idMatrix.Rotate(0f,-270f,0f)

						NewtonBodySetMatrix( .vehicle(v).Body, idMatrix )

						.vehicle(v).reset_timer = this.last_time+this.entity_reset_time
					end if

				else

					.vehicle(v).reset_timer = this.last_time+this.entity_reset_time

				end if

				'node calculation...
				'basically checkpoints
				'if a car doesn't get within the radius of the checkpoint, it must go back

				dim as integer hit_node
				dim as vec3f tPos = .vehicle(v).tMatrix.position
				dim as integer b = .vehicle(v).current_node
				dim as vec3f bPos = .model(0).bone(b).posit(0)

				if tPos.distance(.Vehicle(v).tempTarg)<=25f then

					hit_node = true

					bPos = .model(0).bone((b+1) mod ubound(.model(0).bone) ).posit(0)
					dim as vec3f direction = .vehicle(v).tempTarg - bPos

					direction.normalize

					dim as vec3f tUp = vec3f(0f,1f,0f)
					dim as vec3f thisLane = direction.Cross(tUp)

					bPos+=(thisLane*.vehicle(v).current_lane)
					bPos.y = .model(0).bone((b+1) mod ubound(.model(0).bone) ).posit(0).y

					.Vehicle(v).tempTarg = bPos
					.Vehicle(v).current_node = (.Vehicle(v).current_node+1) mod ubound(.model(0).bone)

				end if


				dim as vec3f npPos = .vehicle(v).tMatrix.position
				dim as vec3f nbPos = (.Vehicle(v).tempTarg+.model(0).bone(b).posit(1))/2f

				dim as vec3f tForward = .vehicle(v).tMatrix.right
				dim as single targetAngle = -atan2( npPos.z-nbPos.z, npPos.x-nbPos.x )
				dim as single bodyAngle = -atan2( npPos.z-(npPos.z+tForward.z), npPos.x-(npPos.x+tForward.x) )
				dim as single steerTarget = sin(bodyAngle-targetAngle)

				
				dim as single curveAngle
				dim as integer cnt, lookAhead = 4

				do

					b=(b+1) mod (ubound(.model(0).bone)+1)
					dim as integer b2 = (b+1) mod (ubound(.model(0).bone)+1)

					dim as vec3f v1 = .model(0).bone(b).posit(0)
					dim as vec3f v2 = .model(0).bone(b).posit(1)

					dim as vec3f v3 = .model(0).bone(b2).posit(0)
					dim as vec3f v4 = .model(0).bone(b2).posit(1)
					
					
					dim as single ang1 = -atan2( v2.z-v1.z, v2.x-v1.x)
					dim as single ang2 = -atan2( v4.z-v3.z, v4.x-v3.x)
					
					curveAngle+= (ang2-ang1)

					cnt+=1

				loop until cnt = lookAhead
				
				.vehicle(v).velocity_target = cos(curveAngle)*50f
				
				if .vehicle(v).contact_surf = cGRAVEL then
					.vehicle(v).velocity_target*=.5
				end if
				

				if v = 0 and this.control_mode <> cDEMO then
					
					'this is nothing, since the human player will control all driving
					
				else
					
					dim as single tVel = vehicle(v).velocity.magnitude

					if tVel < .vehicle(v).velocity_target then
						
						.vehicle(v).throttle = 100
						.vehicle(v).brake = false
						
					else
						
						.vehicle(v).throttle = 0
						.vehicle(v).brake = true
						
					end if
					
					

					
					if tVel <=1f then tVel = 1f
					if tVel >=pi then tVel = pi
					.vehicle(v).steer_angle = ( SteerTarget - .vehicle(v).steer_angle ) / tVel

					if .vehicle(v).steer_angle < -pi_8th then

						.vehicle(v).steer_angle = -pi_8th

					elseif vehicle(v).steer_angle > pi_8th then
						
						.vehicle(v).steer_angle = pi_8th

					end if


					'change lanes... not random for good AI... but it is random now. lol
					if int(rnd*50) = 13 and hit_node then

						if .vehicle(v).current_lane = 0 then

							if int(rnd*2) = 0 then

								.vehicle(v).current_lane+=1

							else

								.vehicle(v).current_lane-=1

							end if

						elseif .vehicle(v).current_lane<0 then

							.vehicle(v).current_lane+=1

						elseif .vehicle(v).current_lane>0 then

							.vehicle(v).current_lane-=1

						end if

						if .vehicle(v).current_lane<-1 then .vehicle(v).current_lane=-1
						if .vehicle(v).current_lane>1 then .vehicle(v).current_lane=1

						.vehicle(v).current_lane*=8f

					end if

				end if

			next

		end with
		
		#endif
		
	end sub


	function world_struct.AddSound( byref index as integer, byref filename as string, byref looping as integer ) as nAudio.sample_struct

		nAudio.LoadSound( filename, this.sample(), index, looping )

		return nAudio.sample_struct

	end function


	function world_struct.FindSoundByName( byref sName as string ) as FSound_Sample ptr

		for s as integer = 0 to ubound(sample)
			if ltrim(rtrim(sample(s).name)) = sName then
				return sample(s).wav
			end if
		next

		return 0

	end function

	sub world_struct.AddFakeJoint()

		redim preserve fJoint(ubound(fJoint)+1)

		print #debugout, "fake joint added, #" & ubound(fJoint)

	end sub


	sub world_struct.AddFakeJoint( byref parent as any ptr, byref tOrigin as nmathf.vec3f ptr, byref translation as nMathf.vec3f, byref rotationMatrix as any ptr, byref oTarget as nmathf.vec3f ptr, byref aModel as nmodel.model_struct ptr )

		redim preserve fJoint(ubound(fJoint)+1)
		'memset( @this.fake_joint( ubound(this.fake_joint) ), 0, sizeof(fake_joint_struct) )

		'with fJoint(ubound(fJoint))

		'	.origin = tOrigin
		'	.rotMatrix = rotationMatrix
		'	.oMatrix1.LoadIdentity()
		'	.oMatrix1.Translate(translation)

		'	.target = oTarget
		'	.oMatrix2.LoadIdentity()
		'	.tModel = aModel

		'end with


	end sub


	sub world_struct.ProcessFakeJoints()

		'with this

		'	for j as integer = 0 to ubound(.fake_joint)

		'		dim as nmathf.vec3f p1
		'		dim as nmathf.vec3f p2 = *cast( nmathf.vec3f ptr, .fake_joint(j).target )
		'		dim as nmathf.matrix rotMatrix = *cast( nmathf.matrix ptr, .fake_joint(j).rotMatrix )
		'
		'
		'		dim as nmathf.matrix temp1 = .fake_joint(j).oMatrix1

		'		rotMatrix.Translate(temp1.Position)

		'		p1=rotMatrix.Position

		'		.fake_joint(j).tMatrix.PointAt( p1, p2 )

		'	next

		'end with

	end sub



	function world_struct.LoadModel( byref filename as string, byref force_clamp as integer, byref static_shadows as integer ) as integer

		nmodel.load_model3d( filename, Model(), Texture(), 0, static_shadows )

		Model(ubound(model)).static_shadows = static_shadows

		return 0

	end function


	sub world_struct.AddVehicle( byref iMatrix as nmathf.matrix, byref model as nModel.model_struct ptr, byref mass as single )

		redim preserve this.vehicle( ubound(this.vehicle)+1 )
		memset( @this.vehicle( ubound(this.vehicle) ), 0, sizeof(vehicle_struct) )

		dim as nMathf.Matrix tMatrix
		tMatrix.LoadIdentity

		with this.vehicle( ubound(this.vehicle) )

			.tModel = model

			.Collision = NewtonCreateConvexHull( this.wWorld, ubound(.tModel->vertices)+1, @.tModel->vertices(0).x, sizeof(nmathf.vec3f), 0)
			NewtonConvexCollisionSetUserID( .collision, eVEHICLE )

			.body = NewtonCreateBody(this.wWorld, .collision)

			.ID = eVEHICLE

			var up = nmathf.vec3f(0,1,0)
			.nJoint = NewtonConstraintCreateVehicle( this.wWorld, @up.x, .body )

			dim as nmathf.vec3f low  = nmathf.vec3f(2^32-1,2^32-1,2^32-1)
			dim as nmathf.vec3f high = nmathf.vec3f(-(2^32-1), -(2^32-1), -(2^32-1) )

			'get the center of mass directly from the model dimensions
			with *.tModel

				for v as integer = 0 to ubound(.vertices)

					if .vertices(v).x < low.x then
						low.x = .vertices(v).x
					end if

					if .vertices(v).y < low.y then
						low.y = .vertices(v).y
					end if

					if .vertices(v).z < low.z then
						low.z = .vertices(v).z
					end if

					if .vertices(v).x > high.x then
						high.x = .vertices(v).x
					end if

					if .vertices(v).y > high.y then
						high.y = .vertices(v).y
					end if

					if .vertices(v).z > high.z then
						high.z = .vertices(v).z
					end if

				next

			end with

			'modify the center of mass to better suit a vehicle
			dim as single yCenter = ((high.y - low.y) / 2f) - ((high.y - low.y) / 7f)
			dim as nmathf.vec3f center = nmathf.vec3f( 0.0, -yCenter, 0.0 )
			NewtonBodySetCentreOfMass( .Body, @center.x )

			NewtonJointSetUserData( .nJoint, cast( vehicle_struct ptr, @this.vehicle(ubound(this.vehicle))) )

			dim as nmathf.vec3f Inertia, Origin
			NewtonConvexCollisionCalculateInertialMatrix( .Collision, @Inertia.x, @Origin.x )

			dim as single Ixx = Mass * Inertia.X
			dim as single Iyy = Mass * Inertia.Y
			dim as single Izz = Mass * Inertia.Z
			NewtonBodySetMassMatrix ( .Body, Mass, Ixx, Iyy, Izz )

			.Mass = Mass

			.tMatrix.loadIdentity()
			.bMatrix.loadIdentity()

			var omega = nmathf.vec3f(.00001, .00001, .00001)

			NewtonBodySetAngularDamping( .Body, @omega.x )
			NewtonBodySetLinearDamping( .Body, .00001 )

			'NewtonBodyCoriolisForcesMode( .Body, 1 )
			NewtonBodySetJointRecursiveCollision( .Body, 1 )
			NewtonBodySetAutoFreeze( .Body, 0 )
			NewtonWorldUnfreezeBody ( this.wWorld, .Body )
			NewtonBodySetContinuousCollisionMode( .Body, 1 )

			NewtonBodySetMatrix( .Body, iMatrix )
			.initMatrix = iMatrix

			NewtonBodySetForceAndTorqueCallback( .Body, @vehicle_force_torque )
			NewtonBodySetTransformCallback( .Body, @vehicle_physics_transform )
			NewtonBodySetUserData( .Body, cast( vehicle_struct ptr, @this.vehicle(ubound(this.vehicle))) )
			NewtonReleaseCollision( this.wWorld, .Collision )

		end with


		for v as integer = 0 to ubound(this.vehicle)

			with this.vehicle(v)

				NewtonBodySetUserData( .Body, cast( vehicle_struct ptr, @this.vehicle(v)) )
				NewtonJointSetUserData( .nJoint, cast( vehicle_struct ptr, @this.vehicle(v)) )

			end with

		next

	end sub

	sub vehicle_struct.AddShock( byref model as nModel.model_struct ptr, byref offset1 as nmathf.matrix, byref trans1 as nMathf.vec3f, byref offset2 as nmathf.matrix, byref trans2 as nMathf.vec3f, byref tireID as integer )

		redim preserve this.suspension(ubound(this.suspension)+1)

		with this.suspension(ubound(this.suspension))

			.tModel = model
			.targetID = tireID
			.translate1 = trans1
			.translate2 = trans2
			memcpy( .offset1, offset1, nmathf.MAT_COPY_SIZE )
			memcpy( .offset2, offset2, nmathf.MAT_COPY_SIZE )

		end with

	end sub


	sub vehicle_struct.ProcessSuspension()

		if ubound(suspension)>-1 then

			for s as integer = 0 to ubound(suspension)

				with suspension(s)

					dim as nMathf.matrix origMatrix = this.tMatrix*.offset1
					dim as nMathf.matrix targMatrix = this.tire(.targetID).tMatrix*.offset2

					.tMatrix.PointAt( origMatrix.position, targMatrix.position)

					.tMatrix.translate( .translate1 )

				end with

			next

		end if

	end sub


	sub vehicle_struct.AddTire( byref nWorld as NewtonWorld ptr, byref model as nModel.Model_Struct ptr, byref position as nMathf.vec3f, byref tMass as single, byref canSteer as integer, byref prop as tire_type )

		redim preserve this.tire(ubound(this.tire)+1)
		memset( @this.tire( ubound(this.tire) ), 0, sizeof(tire_struct) )

		dim as nmathf.matrix lMatrix
		lMatrix.LoadIdentity()
		lMatrix.Position = position
		var up = nMathf.vec3f (0,0,1)

		with this.tire(ubound(this.tire))

			.tName = "Tire " & ubound(this.tire)

			.tModel = Model
			.mass = tMass
			.steer_mode = canSteer

			'original values
			'.latgrip   = 0.02659997
			'.latgripco = 0.02469997
			'.longgrip = .08
			'.longgripco = .0075

			if canSteer then

				'front tires, arbitrary, but close...

				.latgrip   = 0.7659997
				.latgripco = 0.7469997
				.longgrip = .75
				.longgripco = .95

			else

				'rear tires, arbitrary, but close...

				.latgrip   = 1.8659997
				.latgripco = 1.8469997
				.longgrip = .75
				.longgripco = .95

			end if

			.prop = prop

			'standard suspension type...

			dim as single susLength
			dim as single susSpring
			dim as single susShock

			if canSteer then
				susLength = 1.5
				susSpring = (200.0 * GRAVITY) / susLength
				susShock = 2.0 * sqr( susSpring )
			else
				susLength = 1.65
				susSpring = (200.0 * GRAVITY) / susLength
				susShock = 2.0 * sqr( susSpring )
			end if


			dim as nmathf.vec3f low  = nmathf.vec3f(2^32-1,2^32-1,2^32-1)
			dim as nmathf.vec3f high = nmathf.vec3f(-(2^32-1), -(2^32-1), -(2^32-1) )

			'get the tire size directly from the model dimensions
			with *Model

				for v as integer = 0 to ubound(.vertices)

					if .vertices(v).x < low.x then
						low.x = .vertices(v).x
					end if

					if .vertices(v).y < low.y then
						low.y = .vertices(v).y
					end if

					if .vertices(v).z < low.z then
						low.z = .vertices(v).z
					end if

					if .vertices(v).x > high.x then
						high.x = .vertices(v).x
					end if

					if .vertices(v).y > high.y then
						high.y = .vertices(v).y
					end if

					if .vertices(v).z > high.z then
						high.z = .vertices(v).z
					end if

				next

			end with

			dim as single wide = (high.z-low.z)
			dim as single radius = (high.x-low.x)/2.0

			'NewtonVehicleAddTire( nJoint, lMatrix, @up.x, tMass, wide, radius, susShock, susSpring, susLength, @this.tire(ubound(this.tire)), prop )
			NewtonVehicleAddTire( nJoint, lMatrix, @up.x, tMass, wide, radius, susShock, susSpring, susLength, 0, prop )
			NewtonVehicleSetTireCallBack( nJoint, @Tire_Callback() )

		end with




		print #debugout, "tire added " & ubound(this.tire)

	end sub



	sub world_struct.AddRollingFrictionJoint( byref eId as integer )

		'adds a rolling friction joint to the world and tie it to entity(eId)
		'useful for balls to relieve the "feeling" of rolling through a viscous fluid

		redim preserve this.joint( ubound(this.joint)+1 )
		memset( @this.joint( ubound(this.joint) ), 0, sizeof(joint_struct) )

		dim as integer jId = ubound(this.joint)

		this.joint(jId).nJoint = NewtonConstraintCreateUserJoint( this.wWorld, 1, @Rolling_Friction_Callback(), this.entity(eId).body, 0 )
		this.joint(jId).parent_body = this.entity(eId).body
		NewtonJointSetUserData( this.joint(jId).nJoint, cast( entity_struct ptr, @entity(eId) ) )
		var angdamp = nmathf.vec3f(.05,.05,.05)
		NewtonBodySetAngularDamping( this.entity(eId).body, @angdamp.x )
		NewtonBodySetLinearDamping( this.entity(eId).body, .05 )
		NewtonBodyCoriolisForcesMode( this.entity(eId).body, 1 )

	end sub


	sub world_struct.AddRollingFrictionJointToVehicle( byref vId as integer )

		'adds a rolling friction joint to the world and tie it to vehicle(vId)
		'useful for balls to relieve the "feeling" of rolling through a viscous fluid

		redim preserve this.joint( ubound(this.joint)+1 )
		memset( @this.joint( ubound(this.joint) ), 0, sizeof(joint_struct) )

		dim as integer jId = ubound(this.joint)

		this.joint(jId).nJoint = NewtonConstraintCreateUserJoint( this.wWorld, 1, @Rolling_Friction_Callback(), this.vehicle(vId).body, 0 )
		this.joint(jId).parent_body = this.Vehicle(vId).body
		NewtonJointSetUserData( this.joint(jId).nJoint, cast( entity_struct ptr, @this.vehicle(vId) ) )
		var angdamp = nmathf.vec3f(.0001,.0001,.0001)
		NewtonBodySetAngularDamping( this.vehicle(vId).body, @angdamp.x )
		NewtonBodySetLinearDamping( this.vehicle(vId).body, .0001 )
		NewtonBodyCoriolisForcesMode( this.vehicle(vId).body, 1 )

	end sub




	sub world_struct.RemoveJoint( byref jId as integer )

		'if this.max_joints > 0 then

		'	swap this.joint[this.max_joints-1], this.joint[jId]

		'	this.max_joints-=1

		'	if this.max_joints > 0 then

		'		this.joint = reallocate( this.joint, this.max_joints*sizeof(joint_struct) )
		'
		'	else

		'		deallocate( this.joint )

		'	end if

		'end if

	end sub


	sub world_struct.AddCharacterEntity( byref iMatrix as nmathf.matrix, byref mass as single, byref eSize as nmathf.vec3f, byref eName as string, byref model as nmodel.model_struct ptr )


		redim preserve entity(ubound(entity)+1)
		memset( @this.entity( ubound(this.entity) ), 0, sizeof(entity_struct) )

		with entity(ubound(entity))

			.tModel = model
			.eType = eCharacter
			.eName = eName
			.tMatrix.LoadIdentity()
			.bMatrix.LoadIdentity()


			.Collision = NewtonCreateSphere(this.wWorld, eSize.x, eSize.y, eSize.z, 0 )
			.Body = NewtonCreateBody(this.wWorld, .Collision )

			dim as single Ixx = 0.4f * mass * esize.y * esize.y
			dim as single Iyy = Ixx
			dim as single Izz = Ixx
			NewtonBodySetMassMatrix (.Body, mass, Ixx, Iyy, Izz)

			.Mass = Mass

			.radius = esize.y

			NewtonBodySetForceAndTorqueCallback( .Body, @character_controller() )
			NewtonBodySetTransformCallback( .Body, @generic_physics_transform() )
			NewtonBodySetUserData( .Body, @entity(ubound(entity)) )

			NewtonBodySetAutoFreeze( .Body, 0 )
			NewtonWorldUnfreezeBody (this.wWorld, .Body)
			NewtonReleaseCollision( this.wWorld, .Collision )

			NewtonBodySetContinuousCollisionMode( .Body, 1 )

			'.tMatrix.Translate(0,10+int(rnd*15),0)
			.tMatrix = iMatrix
			NewtonBodySetMatrix( .Body, .tMatrix )

			if ubound(entity) = 0 then
				this.AddRollingFrictionJoint( ubound(entity) )
			end if

			'this.AddRollingFrictionJoint( this.max_entities - 1 )
			'char.max_joints = 1
			'char.joint = callocate( char.max_joints*sizeof(joint_struct))
			'char.joint[0].nJoint = NewtonConstraintCreateUserJoint( nWorld, 1, @Rolling_Friction_Callback(), char.body, 0 )
			'NewtonJointSetUserData( char.joint[0].nJoint, @char )


		end with


		for e as integer = 0 to ubound(entity)

			NewtonBodySetUserData( entity(e).Body, cast(entity_struct ptr, @entity(e)) )

			if ubound(joint)>-1 then

				for j as integer = 0 to ubound(joint)

					if joint(j).parent_body = entity(e).body then

						NewtonJointSetUserData( joint(j).nJoint, cast(entity_struct ptr, @entity(e)) )

					end if
				next

			end if

		next



		#ifdef _debugout
		print #debugout, entity(ubound(entity)).eName & " added."
		'print #debugout, "max entities = " & ubound(entity)+1 & " " & @entity(ubound(entity))

		for e as integer = 0 to ubound(entity)
			'print #debugout, e & " " & @entity(ubound(entity))
		next

		#endif

	end sub



	sub world_struct.AddGenericEntity( byref iMatrix as nmathf.matrix, byref mass as single, byref eSize as nmathf.vec3f, byref eName as string, byref model as nmodel.model_struct ptr )

		redim preserve entity(ubound(entity)+1)
		memset( @this.entity( ubound(this.entity) ), 0, sizeof(entity_struct) )

		with entity(ubound(entity))

			.tModel = model
			.eType = eCharacter
			.eName = eName
			.tMatrix.LoadIdentity()
			.bMatrix.LoadIdentity()

			'.Collision = NewtonCreateSphere(this.wWorld, eSize.x, eSize.y, eSize.z, 0 )
			.Collision = NewtonCreateConvexHull( this.wWorld, ubound(.tModel->vertices)+1, @.tModel->vertices(0).x, sizeof(nmathf.vec3f), 0)
			NewtonConvexCollisionSetUserID( .collision, eCONVEX_HULL )


			.Body = NewtonCreateBody(this.wWorld, .Collision )

			'dim as single Ixx = 0.4f * mass * esize.y * esize.y
			'dim as single Iyy = Ixx
			'dim as single Izz = Ixx
			'NewtonBodySetMassMatrix (.Body, mass, Ixx, Iyy, Izz)

			dim as nmathf.vec3f Inertia, Origin
			NewtonConvexCollisionCalculateInertialMatrix( .Collision, @Inertia.x, @Origin.x )

			dim as single Ixx = Mass * Inertia.X
			dim as single Iyy = Mass * Inertia.Y
			dim as single Izz = Mass * Inertia.Z
			NewtonBodySetMassMatrix ( .Body, Mass, Ixx, Iyy, Izz )

			.Mass = Mass

			.radius = esize.y

			NewtonBodySetForceAndTorqueCallback( .Body, @generic_force_torque() )
			NewtonBodySetTransformCallback( .Body, @generic_physics_transform() )
			NewtonBodySetUserData( .Body, @entity(ubound(entity)) )

			'NewtonBodySetAutoFreeze( .Body, 0 )
			'NewtonWorldUnfreezeBody (this.wWorld, .Body)
			NewtonReleaseCollision( this.wWorld, .Collision )

			'NewtonBodySetContinuousCollisionMode( .Body, 1 )

			'.tMatrix.Translate(0,10+int(rnd*15),0)
			.tMatrix = iMatrix
			NewtonBodySetMatrix( .Body, .tMatrix )

			'if ubound(entity) = 0 then
			'	this.AddRollingFrictionJoint( ubound(entity) )
			'end if

			'this.AddRollingFrictionJoint( this.max_entities - 1 )
			'char.max_joints = 1
			'char.joint = callocate( char.max_joints*sizeof(joint_struct))
			'char.joint[0].nJoint = NewtonConstraintCreateUserJoint( nWorld, 1, @Rolling_Friction_Callback(), char.body, 0 )
			'NewtonJointSetUserData( char.joint[0].nJoint, @char )


		end with


		for e as integer = 0 to ubound(entity)

			NewtonBodySetUserData( entity(e).Body, cast(entity_struct ptr, @entity(e)) )

			if ubound(joint)>-1 then

				for j as integer = 0 to ubound(joint)

					if joint(j).parent_body = entity(e).body then

						NewtonJointSetUserData( joint(j).nJoint, cast(entity_struct ptr, @entity(e)) )

					end if
				next

			end if

		next

		#ifdef _debugout
		print #debugout, entity(ubound(entity)).eName & " added."
		'print #debugout, "max entities = " & ubound(entity)+1 & " " & @entity(ubound(entity))

		for e as integer = 0 to ubound(entity)
			'print #debugout, e & " " & @entity(ubound(entity))
		next

		#endif

	end sub



	sub world_struct.AddCamera( byref eSize as nmathf.vec3f, byref position as nmathf.vec3f, byref lookat as nmathf.vec3f, byref eName as string )

		with this.camera

			.tMatrix.LoadIdentity()
			.tMatrix.Position = position
			.cLook = lookat
			.eType = eCamera
			.eName = eName

			.Collision = NewtonCreateSphere(this.wWorld, eSize.x, eSize.y, eSize.z, 0 )
			.Body = NewtonCreateBody(this.wWorld, .Collision )
			NewtonBodySetMassMatrix( .Body, 1f, 1f, 1f, 1f )
			.Mass = 1f

			NewtonBodySetForceAndTorqueCallback( .Body, @camera_controller() )
			NewtonBodySetTransformCallback( .Body, @camera_physics_transform() )
			NewtonBodySetUserData( .Body, @this.camera )

			NewtonBodySetAutoFreeze( .Body, 0 )
			NewtonWorldUnfreezeBody ( this.wWorld, .Body )
			NewtonReleaseCollision( this.wWorld, .Collision )

			'NewtonBodySetContinuousCollisionMode( .Body, 1 )

			NewtonBodySetMatrix( .Body, .tMatrix )

		end with

		#ifdef _debugout
		print #debugout, "Camera, " & ename & " added."
		#endif

	end sub



	sub entity_struct.SetMaterial( byref tMaterial as material_struct )

		this.material = tMaterial
		NewtonBodySetMaterialGroupID( this.Body, this.material.reference )

	end sub

	function world_struct.FindEntityByType( byref eType as entity_type ) as entity_struct ptr

		for e as integer = 0 to ubound(entity)
			if entity(e).eType = eType then
				return @entity(e)
			end if
		next

		return 0

	end function

	function world_struct.FindEntityByName( byref eName as string ) as entity_struct ptr
		'this function was meant to return a pointer for use in the scripting engine and level editor

		for e as integer = 0 to ubound(entity)
			if ltrim(rtrim(entity(e).eName)) = eName then
				'print #debugout, entity(e).eName & "  body ptr  = " & entity(e).newt.Body & " from world_struct.FindEntityByName()"
				return @entity(e)
			end if
		next

		return 0

	end function


	sub world_struct.Create( byref frame_steps as single, byref sim_speed as single )

		if this.wWorld<>0 then
			newtonDestroy( this.wWorld )
		end if

		this.frame_steps = frame_steps
		this.frequency = 1f/frame_steps
		this.sim_speed = sim_speed
		this.wWorld = NewtonCreate( 0, 0 )
		this.entity_reset_time = 3f

		#ifdef __fb_linux__
		NewtonSetPlatformArchitecture(wWorld, 0)
		#endif
		NewtonSetMinimumFrameRate( wWorld, this.frame_steps )
		NewtonSetSolverModel( wWorld, 3 )
		NewtonSetFrictionModel( wWorld, 1 )


		emitter.CreateParticles( 200 )


		FSOUND_SetMinHardwareChannels( 32 )

		if( FSOUND_Init(44100, 0, 32) = FALSE ) then
			#ifdef _debugout
			print #debugout, "Can't initialize FMOD"
			#endif
		end if

		FSOUND_3D_SetRolloffFactor( 1 )


		#ifdef _debugout
		print #debugout, "frame steps " & this.frame_steps
		print #debugout, "frequency " & this.frequency
		print #debugout, "sim speed " & this.sim_speed
		#endif

		this.last_time = timer

	end sub

	sub world_struct.Destroy()

		for i as integer = 0 to ubound(this.texture)
			glDeleteTextures( 1, @this.texture(i).img )
		next

		for m as integer = 0 to ubound(model)

			glDeleteLists( model(m).dList, 1)

			for s as integer = 0 to ubound(model(m).surfaces)

				glDeleteLists( model(m).surfaces(s).dList, 1)

			next

		next

		erase this.model
		erase this.entity
		erase this.static_entity
		erase this.texture
		erase this.joint
		erase this.fJoint
		erase this.vehicle

		newtonDestroy( wWorld )
		FSound_Close()

	end sub

	sub world_struct.Update()

		using nMathf

		if first_run = 0 then
			first_run = 1
			last_time = timer
		end if

		deltaTime =  timer-last_time

		if not paused then

			accumulative_step += deltaTime*sim_speed

			while accumulative_step >= frequency

				NewtonUpdate( wWorld, frequency)

				accumulative_step -= frequency

			wend


			ProcessFakeJoints()
			emitter.ProcessParticles( deltaTime, last_time, this.camera.tMatrix.position)

			if ubound(vehicle)>-1 then

				for v as integer = 0 to ubound(vehicle)

					with vehicle(v)

						.ProcessSuspension()

						if ubound(.tire)>-1 then
							for t as integer = 0 to ubound(.tire)

								with .tire(t)

									if .add_particle then

										for pt as integer = 0 to ubound(emitter.particle)

											if emitter.particle(pt).active = false then

												if emitter.terrain_fx_counter < emitter.max_terrain_fx then

													.add_particle = 0

													with emitter.particle(pt)
														select case as const vehicle(v).tire(t).particle.prop

															case PARTICLE_SMOKE,	PARTICLE_DUST,	PARTICLE_GRAVEL, PARTICLE_SNOW,	PARTICLE_RAIN,	PARTICLE_LEAF,	PARTICLE_SAND

																emitter.terrain_fx_counter+=1
																.end_time = last_time+.75
																.active = true
																.position = vehicle(v).tire(t).particle.position
																.position_delta = vec3f(0,3.8,0)
																.prop = vehicle(v).tire(t).particle.prop
																.colour = vec4f(1,1,1,.6)
																.colour_delta = vec4f(0f,0f,0f, -1.2/(.end_time-last_time))
																.scale = vec3f(.5+rnd*.5, .5+rnd*.5, .5+rnd*.5)
																.scale_delta = vec3f(.2+rnd*.5,.2+rnd*.5, .2+rnd*.5)
																.omega = vec3f(0f, 0f, rnd*360f)
																.omega_delta = vec3f(0f, 0f, 0f)

															case PARTICLE_CRASH

																emitter.terrain_fx_counter+=1
																.end_time = last_time+2
																.active = true
																.position = vehicle(v).tire(t).particle.position
																.prop = vehicle(v).tire(t).particle.prop
																.colour = vec4f(1,1,0,1)
																.colour_delta = vec4f(0,0,0,-2.5)
																.scale = vec3f(.5+rnd*.5, .5+rnd*.5, .5+rnd*.5)
																.scale_delta = vec3f(1+rnd*3, 1+rnd*3, 1+rnd*3)
																.omega = vec3f(0, 0, rnd*360)
																.omega_delta = vec3f(0, 0, -30+rnd*60)


														end select
													end with

													exit for

												end if

											end if

										next

									end if
								end with

							next
						end if

					end with

				next

			end if

		end if

		last_time = timer

	end sub

	sub world_struct.TreeBegin()

		wCollision = NewtonCreateTreeCollision( wWorld, 0 )
		NewtonTreeCollisionBeginBuild( wCollision )
		wBody = NewtonCreateBody( wWorld, wCollision )

	end sub

	sub world_struct.TreeAddStaticEntity( byref model as nmodel.model_struct ptr, byref tMatrix as nmathf.matrix )

		redim preserve static_entity(ubound(static_entity)+1)
		memset( @this.static_entity( ubound(this.static_entity) ), 0, sizeof(static_entity_struct) )

		with static_entity(ubound(static_entity))
			.tMatrix = tMatrix
			.iMatrix = tMatrix.Inverse()
			.tModel = model
		end with

		for t as integer = 0 to ubound(model->triangles)'model->max_triangles-1
			dim as nmathf.vec3f nPoint(2)
			npoint(0) = Model->tVertices(Model->Triangles(t).Point_ID(0))*tMatrix
			nPoint(1) = Model->tVertices(Model->Triangles(t).Point_ID(1))*tMatrix
			nPoint(2) = Model->tVertices(Model->Triangles(t).Point_ID(2))*tMatrix
			dim as integer unique_id = h_table->lookup( Model->Triangles(t).string_id )->id

			NewtonTreeCollisionAddFace( wCollision, 3, @nPoint(0).x, sizeof( nmathf.vec3f ), unique_id )
		next

		#ifdef _debugout
		print #debugout, "max_static_entities " & ubound(static_entity)+1
		#endif

	end sub

	sub world_struct.TreeEnd()

		NewtonTreeCollisionEndBuild( wCollision, 0 )

		dim as nmathf.Matrix iMatrix
		iMatrix.LoadIdentity()

		dim as single boxp0(2), boxp1(2)
		NewtonCollisionCalculateAABB ( wCollision, iMatrix, @boxP0(0), @boxP1(0) )

		boxP0(0) -= 100.0f
		boxP0(1) -= 100.0f
		boxP0(2) -= 100.0f
		boxP1(0) += 100.0f
		boxP1(1) += 100.0f
		boxP1(2) += 100.0f

		NewtonSetWorldSize ( wWorld, @boxP0(0), @boxP1(0) )
		NewtonReleaseCollision ( wWorld, wCollision )

		NewtonBodySetTransformCallback( wBody, @world_physics_transform() )

		NewtonBodySetUserData( wBody, @this )
		NewtonWorldSetUserData( wWorld, @this )
		NewtonBodySetMatrix ( wBody, iMatrix )


		redim preserve material(0)
		material(0).name = "Default"
		material(0).reference = NewtonMaterialGetDefaultGroupID ( this.wWorld )
		NewtonMaterialSetDefaultSoftness  ( this.wWorld, material(0).reference, material(0).reference, .75)
		NewtonMaterialSetDefaultElasticity( this.wWorld, material(0).reference, material(0).reference, .5)
		NewtonMaterialSetDefaultCollidable( this.wWorld, material(0).reference, material(0).reference, 1)
		NewtonMaterialSetDefaultFriction  ( this.wWorld, material(0).reference, material(0).reference, 1, .9)

		redim preserve material(ubound(material)+1)

		dim as integer m = ubound(material)

		material(m).name = "World"
		material(m).reference = NewtonMaterialCreateGroupID ( this.wWorld )
		NewtonMaterialSetDefaultSoftness  ( this.wWorld, material(m).reference, material(m).reference, .75)
		NewtonMaterialSetDefaultElasticity( this.wWorld, material(m).reference, material(m).reference, .5)
		NewtonMaterialSetDefaultCollidable( this.wWorld, material(m).reference, material(m).reference, 1)
		NewtonMaterialSetDefaultFriction  ( this.wWorld, material(m).reference, material(m).reference, 1, .9)
		NewtonBodySetMaterialGroupID( this.wBody, material(m).reference )

	end sub


	function world_struct.LoadTexture( byref filename as string, byref force_clamp as Integer, byref searchDir As String = "res/texture/" ) as integer

		return nimage.load_texture( filename, this.texture(), force_clamp, searchDir )

	end function

	function world_struct.FindTextureByName( byref tName as string ) as integer

		for t as integer = 0 to ubound(texture)
			if ltrim(rtrim(texture(t).name)) = tName then
				print #debugout, "texture " & tName & ", found!"
				return t
			end if
		next

	end function

	sub world_struct.AddMaterial(byref tName as string, byref soft as single, byref elastic as single, byref collide as integer, byref staticFriction as single, kineticFriction as single )

		redim preserve material(ubound(material)+1)

		dim as integer m = ubound(material)

		material(m).name = tName
		material(m).reference = NewtonMaterialCreateGroupID ( this.wWorld )
		NewtonMaterialSetDefaultSoftness  ( this.wWorld, material(m).reference, material(m).reference, soft)
		NewtonMaterialSetDefaultElasticity( this.wWorld, material(m).reference, material(m).reference, elastic)
		NewtonMaterialSetDefaultCollidable( this.wWorld, material(m).reference, material(m).reference, collide)
		NewtonMaterialSetDefaultFriction  ( this.wWorld, material(m).reference, material(m).reference, staticFriction, kineticFriction)

		print #debugout, "Material " & material(m).name & ", added."
		print #debugout, "REFERENCE, " & material(0).reference
	end sub

	function world_struct.FindMaterialByName( byref mName as string ) as material_struct

		if ubound(material)>-1 then

			for m as integer = 0 to ubound(material)
				if ltrim(rtrim(material(m).name)) = mName then
					print #debugout, "material " & mName & ", found!"
					return material(m)
				end if
			next

			print #debugout, "Material " & mName & ", not found!"
			return material(0)

		end if

	end function


	sub emitter_struct.CreateParticles( byref amt as integer )

		redim this.particle(amt)
		this.max_terrain_fx = ubound(this.particle)/2
		this.max_weather_fx = ubound(this.particle)/2

	end sub

	function particle_sort cdecl ( byval elm1 as any ptr, byval elm2 as any ptr ) as integer

		function = sgn( (cptr(particle_struct ptr, elm2)->dist) - (cptr(particle_struct ptr, elm1)->dist) )

	end function

	sub emitter_struct.ProcessParticles( byval timeStep as single, byval curTime as single, byref campos as nmathf.vec3f )

		'this need work... will take care of that later as it isn't that important with so few particles.
		with this

			for p as integer = 0 to ubound(.particle)
				with particle(p)

					if curTime > .end_time then

						if .active then

							.active = false

							select case as const .prop

								case PARTICLE_SMOKE, PARTICLE_DUST, PARTICLE_SNOW, PARTICLE_GRAVEL
									this.terrain_fx_counter-=1

								case WEATHER_RAIN, WEATHER_SNOW
									this.weather_fx_counter-=1

							end select

						end if


					end if

					if .active then

						.position+=.position_delta * timeStep
						.colour+=.colour_delta * timeStep
						.scale+=.scale_delta * timeStep
						.omega+=.omega_delta * timeStep
						.dist = int(.position.distance(campos)*1000f)

					end if

				end with
			next

			qsort( @.particle(0), ubound(.particle)+1, sizeof( particle_struct ), cast( any ptr, @particle_sort()) )

		end with

	end sub


	sub world_struct.render_scene()

		'if use_shadows then
		'	prep_1st_pass( light )

		'	for t as integer = 0 to ubound(static_entity)
		'		render_1st_pass( static_entity(t).tModel, static_entity(t).tMatrix, texture() )
		'	next

		'	for p as integer = 0 to ubound(entity)
		'		with entity(p)

		'			for o as integer = 0 to .max_objects - 1
		'				if not .object[o].culled then
		'					render_1st_pass( .object[o].tModel, .object[o].tMatrix, texture )
		'				end if
		'			next

		'			for v as integer = 0 to .max_vehicles-1

		'				if not .vehicle[v].culled then
		'					render_1st_pass( .vehicle[v].tModel, .vehicle[v].tMatrix, texture )
		'				end if

		'				for t as integer = 0 to .vehicle[v].max_tires-1
		'					if not .vehicle[v].tire[t].culled then
		'						render_1st_pass( .vehicle[v].tire[t].tModel, .vehicle[v].tire[t].tMatrix, texture )
		'					end if
		'				next

		'			next

		'			for fj as integer = 0 to .max_fake_joints-1

		'				if not .fake_joint[fj].culled then
		'					render_1st_pass( .fake_joint[fj].tModel, .fake_joint[fj].tMatrix, texture )
		'				end if

		'			next

		'		end with
		'	next


		'	prep_2nd_pass()

		'	for t as integer = 0 to world.max_tree_objects-1
		'		render_2nd_pass( world.tree_object[t].tModel, world.tree_object[t].tMatrix, display.light[0] )
		'	next

		'	for p as integer = 0 to entity.max_entities-1
		'		with entity.dat[p]

		'			for o as integer = 0 to .max_objects-1
		'				if not .object[o].culled then
		'					render_2nd_pass( .object[o].tModel, .object[o].tMatrix, display.light[0] )
		'				end if
		'			next

		'			for v as integer = 0 to .max_vehicles-1

		'				if not .vehicle[v].culled then
		'					render_2nd_pass( .vehicle[v].tModel, .vehicle[v].tMatrix, display.light[0] )
		'				end if

		'				for t as integer = 0 to .vehicle[v].max_tires-1

		'					if not .vehicle[v].tire[t].culled then
		'						render_2nd_pass( .vehicle[v].tire[t].tModel, .vehicle[v].tire[t].tMatrix, display.light[0] )
		'					end if

		'				next

		'			next

		'			for fj as integer = 0 to .max_fake_joints-1
		'				if not .fake_joint[fj].culled then
		'					render_2nd_pass( .fake_joint[fj].tModel, .fake_joint[fj].tMatrix, display.light[0] )
		'				end if
		'			next

		'		end with
		'	next


		'	prep_3rd_pass( display.light[0] )
		'end if

		'for t as integer = 0 to world.max_tree_objects-1
		'	render_3Rd_pass( world.tree_object[t].tModel, world.tree_object[t].tMatrix, texture )
		'next

		'for p as integer = 0 to entity.max_entities-1
		'	with entity.dat[p]

		'		for o as integer = 0 to .max_objects-1
		'			if not .object[o].culled then
		'				render_3rd_pass( .object[o].tModel, .object[o].tMatrix, texture )
		'			end if
		'		next

		'		for v as integer = 0 to .max_vehicles-1

		'			if not .vehicle[v].culled then
		'				render_3rd_pass( .vehicle[v].tModel, .vehicle[v].tMatrix, texture )
		'			end if

		'			for t as integer = 0 to .vehicle[v].max_tires-1

		'				if not .vehicle[v].tire[t].culled then
		'					render_3rd_pass( .vehicle[v].tire[t].tModel, .vehicle[v].tire[t].tMatrix, texture )
		'				end if

		'			next

		'		next

		'		for fj as integer = 0 to .max_fake_joints-1

		'			if not .fake_joint[fj].culled then
		'				render_3rd_pass( .fake_joint[fj].tModel, .fake_joint[fj].tMatrix, texture )
		'			end if

		'		next

		'	end with
		'next


		'if use_shadows	then
		'	glCallList( display.Stencil_Cube )
		'end if

	end sub


	sub render_2nd_pass( byref entity as static_entity_struct, byref light as ndisplay.light_struct )

		'if entity.model->shadow_caster then

		'	dim as nmathf.matrix imat = tMatrix.Inverse()
		'	dim as nmathf.vec4f tLp = light.position * imat

		'	glPushMatrix()
		'	glMultMatrixf( tMatrix )

		'	if entity.static_shadows then
		'
		'		if model->volume_created = false then
		'
		'			model->volume_created = true
		'
		'			calc_shadow_volume_wCaps( model, tLp )

		'			'if _vbo_ then

		'			'	glBindBuffer( GL_ARRAY_BUFFER_ARB, model->volume_vbo )
		'			'	glBufferData( GL_ARRAY_BUFFER_ARB, model->vCnt*sizeof(nmathf.vec4f), model->volume, GL_STATIC_DRAW_ARB )

		'			'	glBindBuffer( GL_ARRAY_BUFFER_ARB, model->volume_cap_vbo )
		'			'	glBufferData( GL_ARRAY_BUFFER_ARB, model->cCnt*sizeof(nmathf.vec4f), model->volume_Cap, GL_STATIC_DRAW_ARB )

		'			'end if

		'		end if

		'	else

		'		calc_shadow_volume_wCaps( model, tLp )

		'		if _vbo_ and use_vbo_volumes then

		'			glBindBuffer( GL_ARRAY_BUFFER_ARB, model->volume_vbo )
		'			glBufferSubData( GL_ARRAY_BUFFER_ARB,  0,  model->vCnt * sizeof(nmathf.vec4f), @model->volume(0).x )

		'			glBindBuffer( GL_ARRAY_BUFFER_ARB, model->volume_cap_vbo )
		'			glBufferSubData( GL_ARRAY_BUFFER_ARB,  0,  model->cCnt * sizeof(nmathf.vec4f), @model->volume_Cap(0).x )

		'		end if

		'	end if

		'	do_shadow_pass_wCaps( model, tLp )
		'
		'	glPopMatrix()

		'end if

	end sub


end namespace
