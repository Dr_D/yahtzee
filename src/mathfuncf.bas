#include once "mathfuncf.bi"

'Dr_D

namespace nmathf

	'vector2___________________________________________________________________________________
	constructor vec2f ( byval x as single, byval y as single )

	this.x = x
	this.y = y

	end constructor


	constructor vec2f ( byval x as double, byval y as double )

	this.x = x
	this.y = y

	end constructor


	constructor vec2f ( byref v2d as vec2f )

	this.x = v2d.x
	this.y = v2d.y

	end constructor


	constructor vec2f ( )

	this.x = 0.0
	this.y = 0.0

	end constructor


	'operator =( byref lhs as vec2f, byref rhs as vec2f ) as integer
	'
	'	return (lhs.x = lhs.x and rhs.y = rhs.y)
	'
	'end operator

	operator = (byref lhs as vec2f, byref rhs as vec2f ) as integer

		if lhs.x = rhs.x and lhs.y = rhs.y then
			return true
		end if

		return false

	end operator


	function vec2f.dot ( byref v as vec2f ) as single

		return  this.x * v.x + this.y * v.y

	end function


	function vec2f.magnitude( ) as single

		dim Mag as single = any
		mag = sqr( this.x * this.x + this.y * this.y )
		if mag = 0 then mag = 1
		return mag

	end function


	sub vec2f.normalize()

		this = this / this.magnitude()

	end sub


	function vec2f.UnitVec() as vec2f

		return this / this.magnitude()

	end function


	function vec2f.cross( byref v as vec2f ) as vec2f
		'technically, there is no cross product for a vec2, so we call this perpendicular product the cross product as to stay in harmony with vec3

		dim as vec2f crvec, rvec
		crvec = v - this
		dim as single vLen = crvec.magnitude
		rvec.x = ( crvec.y)  / vLen
		rvec.y = (-crvec.x)  / vLen
		return rvec

	end function


	function vec2f.cross_analog( byref v as vec2f ) as single

		return this.x*v.y-this.y*v.x

	end function


	function vec2f.distance( byref v as vec2f ) as single

		dim as single dx = (v.x - this.x)
		dim as single dy = (v.y - this.y)

		return sqr( dx*dx + dy*dy )

	end function


	function vec2f.AngleBetween( byref v as vec2f ) as single

		return acos( this.dot(v) / (this.magnitude * v.magnitude) )

	end function


	operator vec2f.cast() as string

		return "x: " & this.x & ", y: " & this.y

	end operator


	operator + ( byref lhs as vec2f, byref rhs as single ) as vec2f

		return type<vec2f>( lhs.x + rhs, lhs.y + rhs )

	end operator


	operator + ( byref lhs as vec2f, byref rhs as vec2f ) as vec2f

		return type<vec2f>( lhs.x + rhs.x, lhs.y + rhs.y )

	end operator

	operator - ( byref lhs as vec2f, byref rhs as vec2f ) as vec2f

		return type<vec2f>( lhs.x - rhs.x, lhs.y - rhs.y )

	end operator

	operator * ( byref lhs as vec2f, byref rhs as vec2f ) as vec2f

		return type<vec2f>( lhs.x * rhs.x, lhs.y * rhs.y )

	end operator


	operator * ( byref lhs as vec2f, byref rhs as single ) as vec2f

		return type<vec2f>( lhs.x * rhs, lhs.y * rhs )

	end operator


	operator * ( byref lhs as vec2f, byref rhs as double ) as vec2f

		return type<vec2f>( lhs.x * rhs, lhs.y * rhs )

	end operator


	operator / ( byref lhs as vec2f, byref rhs as integer ) as vec2f

		return type<vec2f>( lhs.x / rhs, lhs.y / rhs )

	end operator


	operator / ( byref lhs as vec2f, byref rhs as vec2f ) as vec2f

		return type<vec2f>( lhs.x / rhs.x, lhs.y / rhs.y )

	end operator


	operator / ( byref lhs as vec2f, byref rhs as single ) as vec2f

		return type<vec2f>(lhs.x / rhs, lhs.y / rhs)

	end operator

	operator / ( byref lhs as vec2f, byref rhs as double ) as vec2f

		return type<vec2f>(lhs.x / rhs, lhs.y / rhs)

	end operator



	'vector3______________________________________________________________________________

	operator = (byref lhs as vec3f, byref rhs as vec3f ) as integer

		if abs(lhs.x - rhs.x) < 0.000001f then

			if abs(lhs.y - rhs.y) < 0.000001f then

				if abs(lhs.z - rhs.z) < 0.000001f then

					return true

				end if

			end if

		end if

		return false

	end operator

	constructor vec3f ( byval tx as single, byval ty as single, byval tz as single )

	x = tx
	y = ty
	z = tz

	end constructor


	constructor vec3f ( byref v3d as vec3f )

	this.x = v3d.x
	this.y = v3d.y
	this.z = v3d.z

	end constructor


	constructor vec3f ( )

	this.x = 0.0
	this.y = 0.0
	this.z = 0.0

	end constructor


	function vec3f.dot ( byref v As vec3f ) as single

		Return  this.x * v.x + this.y * v.y + this.z * v.z

	end function


	function vec3f.magnitude( ) As single

		Dim Mag As single = any
		mag = Sqr( this.x*this.x + this.y*this.y + this.z*this.z )
		If mag = 0 Then mag = 1
		return mag

	end function


	sub vec3f.normalize()

		this = this / this.magnitude()

	end sub


	function vec3f.UnitVec() as vec3f

		return this / this.magnitude()

	end function


	function vec3f.cross( byref v as vec3f ) as vec3f

		return Type<vec3f>((this.y * v.z) - (v.y * this.z), (this.z * v.x) - (v.z * this.x), (this.x * v.y) - (v.x * this.y))

	end function


	function vec3f.distance( byref v as vec3f ) as single

		dim as single dx = (v.x - this.x)
		dim as single dy = (v.y - this.y)
		dim as single dz = (v.z - this.z)

		return Sqr( dx*dx + dy*dy + dz*dz )

	end function


	function vec3f.AngleBetween( byref v As vec3f ) As single

		return acos( this.dot(v) / (this.magnitude * v.magnitude) )

	end function


	operator vec3f.cast() as string

		return "x: " & this.x & ", y: " & this.y & ", z: " & this.z

	end operator


	operator vec3f.cast() as vec2f

		return type<vec2f>(this.x, this.y)

	end operator


	operator + ( Byref lhs As vec3f, Byref rhs As Single ) As vec3f

		Return Type<vec3f>( lhs.x + rhs, lhs.y + rhs,  lhs.z + rhs )

	End operator


	operator + ( Byref lhs As vec3f, Byref rhs As vec3f ) As vec3f

		Return Type<vec3f>( lhs.x + rhs.x, lhs.y + rhs.y,  lhs.z + rhs.z )

	End operator


	operator - ( Byref lhs As vec3f ) As vec3f

		Return Type<vec3f>(-lhs.x, -lhs.y, -lhs.z )

	End operator


	operator - ( Byref lhs As vec3f, Byref rhs As vec3f ) As vec3f

		Return Type<vec3f>(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z )

	End operator


	operator - ( Byref lhs As vec3f, Byref rhs As vec4f ) As vec3f

		Return Type<vec3f>(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z )

	End operator


	operator - ( Byref lhs As vec3f, Byref rhs As Single ) As vec3f

		Return Type<vec3f>(lhs.x - rhs, lhs.y - rhs, lhs.z - rhs )

	End operator


	operator * ( Byref lhs As vec3f, Byref rhs As vec3f ) As vec3f

		Return Type<vec3f>(lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z )

	End operator


	operator * ( Byref lhs As vec3f, Byref rhs As vec4f ) As vec3f

		Return Type<vec3f>(lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z )

	End operator


	operator * ( Byref lhs As vec3f, Byref rhs As Double ) As vec3f

		Return Type<vec3f>(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs )

	End operator


	operator * ( Byref lhs As vec3f, Byref rhs As Single ) As vec3f

		Return Type<vec3f>(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs )

	End operator


	operator vec3f.*= ( byref rhs as matrix_ )

		this = this * rhs

	end operator


	operator vec3f.*= ( Byref s As single )

		this.x *= s
		this.y *= s
		this.z *= s

	End operator


	operator vec3f.+=( byref rhs As vec3f )

		this.x += rhs.x
		this.y += rhs.y
		this.z += rhs.z

	end operator


	operator / ( Byref lhs As vec3f, Byref rhs As vec3f ) As vec3f

		Return Type<vec3f>(lhs.x / rhs.x, lhs.y / rhs.y, lhs.z / rhs.z )

	End operator


	operator / ( Byref lhs As vec3f, Byref rhs As Single ) As vec3f

		Return Type<vec3f>(lhs.x / rhs, lhs.y / rhs, lhs.z / rhs )

	End operator


	operator / ( Byref lhs As vec3f, Byref rhs As Double ) As vec3f

		Return Type<vec3f>(lhs.x / rhs, lhs.y / rhs, lhs.z / rhs )

	End operator


	'vector4____________________________________________________________________________
	constructor vec4f ( byval x as single, byval y as single, byval z as single, byval w as double )

	this.x = x
	this.y = y
	this.z = z
	this.w = w

	end constructor


	constructor vec4f ( byref v4d as vec4f )

	this.x = v4d.x
	this.y = v4d.y
	this.z = v4d.z
	this.w = v4d.w

	end constructor


	constructor vec4f ( )

	this.x = 0.0
	this.y = 0.0
	this.z = 0.0
	this.w = 0.0

	end constructor


	function vec4f.dot ( byref v as vec4f ) as single

		return  this.x * v.x + this.y * v.y + this.z * v.z + this.w * v.w

	end function


	function vec4f.magnitude( ) as single

		dim Mag as single = any
		mag = sqr( this.x*this.x + this.y*this.y + this.z*this.z + this.w*this.w )
		if mag = 0 then mag = 1
		return mag

	end function


	sub vec4f.normalize()

		this = this / this.magnitude()

	end sub


	function vec4f.UnitVec() as vec4f

		return this / this.magnitude()

	end function


	function vec4f.cross( byref v as vec4f ) as vec4f

		return type<vec4f>((this.y * v.z) - (v.y * this.z), (this.z * v.x) - (v.z * this.x), (this.x * v.y) - (v.x * this.y), this.w)

	end function


	function vec4f.distance( byref v as vec4f ) as single

		dim as single dx = (v.x - this.x)
		dim as single dy = (v.y - this.y)
		dim as single dz = (v.z - this.z)
		dim as single dw = (v.w - this.w)

		return Sqr( dx*dx + dy*dy + dz*dz + dw*dw )

	end function


	operator vec4f.cast() as string

		return "x: " & this.x & ", y: " & this.y & ", z: " & this.z & ", w: " & this.w

	end operator


	operator vec4f.cast() as vec3f

		return type<vec3f>(this.x, this.y, this.z)

	end operator


	operator + ( byref lhs as vec3f, byref rhs as double ) as vec4f

		return type<vec4f>( lhs.x, lhs.y, lhs.z, rhs )

	end operator


	operator + ( byref lhs as vec4f, byref rhs as vec4f ) as vec4f

		return type<vec4f>( lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w )

	end operator


	operator + ( byref lhs as vec4f, byref rhs as single ) as vec4f

		return type<vec4f>( lhs.x + rhs, lhs.y + rhs, lhs.z + rhs, lhs.w + rhs )

	end operator


	operator - ( byref lhs as vec4f, byref rhs as vec4f ) as vec4f

		return type<vec4f>( lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z, lhs.w - rhs.w )

	end operator


	operator - ( Byref lhs As vec4f ) As vec4f

		Return Type<vec4f>(-lhs.x, -lhs.y, -lhs.z, -lhs.w )

	End operator


	operator * ( byref lhs as vec4f, byref rhs as vec4f ) as vec4f

		return type<vec4f>( lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z, lhs.w * rhs.w )

	end operator


	operator * ( byref lhs as vec4f, byref rhs as single ) as vec4f

		return type<vec4f>(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs, lhs.w * rhs )

	end operator

	'operator * ( byref lhs as vec4f, byref rhs as double ) as vec4f
	'
	'	return type<vec4f>(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs, lhs.w * rhs )
	'
	'end operator


	operator vec4f.*= ( byref rhs as matrix )

		this = this * rhs

	end operator

	operator vec4f.*= ( Byref s As single )

		this.x *= s
		this.y *= s
		this.z *= s
		this.w *= s

	End operator

	'operator vec4f.*= ( Byref s As double )
	'
	'    this.x *= s
	'    this.y *= s
	'    this.z *= s
	'    this.w *= s
	'
	'End operator



	operator / ( byref lhs as vec4f, byref rhs as integer ) as vec4f

		return type<vec4f>(lhs.x / rhs, lhs.y / rhs, lhs.z / rhs, lhs.w / rhs )

	end operator


	operator / ( byref lhs as vec4f, byref rhs as vec4f ) as vec4f

		return type<vec4f>( lhs.x / rhs.x, lhs.y / rhs.y, lhs.z / rhs.z, lhs.w / rhs.w )

	end operator


	operator / ( byref lhs as vec4f, byref rhs as single ) as vec4f

		return type<vec4f>(lhs.x / rhs, lhs.y / rhs, lhs.z / rhs, lhs.w / rhs )

	end operator


	operator / ( byref lhs as vec4f, byref rhs as double ) as vec4f

		return type<vec4f>(lhs.x / rhs, lhs.y / rhs, lhs.z / rhs, lhs.w / rhs )

	end operator


	'matrix________________________________________________________________________________
	constructor matrix ( byref c as single = 0 )

	for i as integer = 0 to 15
		this.m(i) = c
	next

	end constructor


	constructor matrix ( byref r as vec3f, byref u as vec3f, byref f as vec3f, byref p as vec3f )

	this.m(0)  = r.x
	this.m(1)  = r.y
	this.m(2)  = r.z
	this.m(3)  = 0

	this.m(4)  = u.x
	this.m(5)  = u.y
	this.m(6)  = u.z
	this.m(7)  = 0

	this.m(8)  = f.x
	this.m(9)  = f.y
	this.m(10) = f.z
	this.m(11) = 0

	this.m(12) = p.x
	this.m(13) = p.y
	this.m(14) = p.z
	this.m(15) = 1

	end constructor


	constructor matrix ( byref x as matrix )
	memcpy( @this.m(0), @x.m(0), MAT_COPY_SIZE )
	end constructor


	sub matrix.LoadIdentity()
		m(0 )  = 1
		m(1 )  = 0
		m(2 )  = 0
		m(3 )  = 0

		m(4 )  = 0
		m(5 )  = 1
		m(6 )  = 0
		m(7 )  = 0

		m(8 )  = 0
		m(9 )  = 0
		m(10)  = 1
		m(11)  = 0

		m(12)  = 0
		m(13)  = 0
		m(14)  = 0
		m(15)  = 1
	end sub


	operator matrix.*= ( byref mat as matrix )

		dim as single tmp_m(15) = any

		with mat
			tmp_m(0)  = (.m(0)  * this.m(0))+(.m(1)  * this.m(4))+(.m(2)  * this.m(8)) +(.m(3)  * this.m(12))
			tmp_m(1)  = (.m(0)  * this.m(1))+(.m(1)  * this.m(5))+(.m(2)  * this.m(9)) +(.m(3)  * this.m(13))
			tmp_m(2)  = (.m(0)  * this.m(2))+(.m(1)  * this.m(6))+(.m(2)  * this.m(10))+(.m(3)  * this.m(14))
			tmp_m(3)  = (.m(0)  * this.m(3))+(.m(1)  * this.m(7))+(.m(2)  * this.m(11))+(.m(3)  * this.m(15))
			tmp_m(4)  = (.m(4)  * this.m(0))+(.m(5)  * this.m(4))+(.m(6)  * this.m(8)) +(.m(7)  * this.m(12))
			tmp_m(5)  = (.m(4)  * this.m(1))+(.m(5)  * this.m(5))+(.m(6)  * this.m(9)) +(.m(7)  * this.m(13))
			tmp_m(6)  = (.m(4)  * this.m(2))+(.m(5)  * this.m(6))+(.m(6)  * this.m(10))+(.m(7)  * this.m(14))
			tmp_m(7)  = (.m(4)  * this.m(3))+(.m(5)  * this.m(7))+(.m(6)  * this.m(11))+(.m(7)  * this.m(15))
			tmp_m(8)  = (.m(8)  * this.m(0))+(.m(9)  * this.m(4))+(.m(10) * this.m(8)) +(.m(11) * this.m(12))
			tmp_m(9)  = (.m(8)  * this.m(1))+(.m(9)  * this.m(5))+(.m(10) * this.m(9)) +(.m(11) * this.m(13))
			tmp_m(10) = (.m(8)  * this.m(2))+(.m(9)  * this.m(6))+(.m(10) * this.m(10))+(.m(11) * this.m(14))
			tmp_m(11) = (.m(8)  * this.m(3))+(.m(9)  * this.m(7))+(.m(10) * this.m(11))+(.m(11) * this.m(15))
			tmp_m(12) = (.m(12) * this.m(0))+(.m(13) * this.m(4))+(.m(14) * this.m(8)) +(.m(15) * this.m(12))
			tmp_m(13) = (.m(12) * this.m(1))+(.m(13) * this.m(5))+(.m(14) * this.m(9)) +(.m(15) * this.m(13))
			tmp_m(14) = (.m(12) * this.m(2))+(.m(13) * this.m(6))+(.m(14) * this.m(10))+(.m(15) * this.m(14))
			tmp_m(15) = (.m(12) * this.m(3))+(.m(13) * this.m(7))+(.m(14) * this.m(11))+(.m(15) * this.m(15))
		end with

		memcpy(@this.m(0), @tmp_m(0), MAT_COPY_SIZE)

	end operator


	operator *( byref lhs as matrix, byref rhs as matrix ) as matrix

		var tmp = lhs
		tmp *= rhs
		return tmp

	end operator


	operator matrix.cast() as string

		dim as string mstring
		for i as integer = 0 to 15
			if i<10 then
				mstring += trim("(" & i & " ) " & this.m(i)) & chr(13,10)
			else
				mstring += trim("(" & i & ") " & this.m(i)) & chr(13,10)
			end if
		next
		return mstring

	end operator


	operator matrix.cast() as single ptr
		return @this.m(0)
	end operator


	operator * ( byref lhs as vec2f, byref rhs as matrix ) as vec2f

		return type<vec2f>( lhs.x*rhs.getarraydata[0] + lhs.y*rhs.getarraydata[4] + rhs.getarraydata[12], _
		lhs.x*rhs.getarraydata[1] + lhs.y*rhs.getarraydata[5] + rhs.getarraydata[13] )

	end operator


	operator * ( byref lhs as vec3f, byref rhs as matrix ) as vec4f

		return type<vec4f>( lhs.x*rhs.getarraydata[0] + lhs.y*rhs.getarraydata[4] + lhs.z*rhs.getarraydata[8]  + rhs.getarraydata[12], _
		lhs.x*rhs.getarraydata[1] + lhs.Y*rhs.getarraydata[5] + lhs.Z*rhs.getarraydata[9]  + rhs.getarraydata[13], _
		lhs.x*rhs.getarraydata[2] + lhs.Y*rhs.getarraydata[6] + lhs.Z*rhs.getarraydata[10] + rhs.getarraydata[14], _
		lhs.x*rhs.getarraydata[3] + lhs.Y*rhs.getarraydata[7] + lhs.Z*rhs.getarraydata[11] + rhs.getarraydata[15] )

	end operator


	operator * ( byref lhs as vec4f, byref rhs as matrix ) as vec4f

		return type<vec4f>( lhs.x*rhs.getarraydata[0] + lhs.y*rhs.getarraydata[4] + lhs.z*rhs.getarraydata[8]  + rhs.getarraydata[12], _
		lhs.x*rhs.getarraydata[1] + lhs.Y*rhs.getarraydata[5] + lhs.Z*rhs.getarraydata[9]  + rhs.getarraydata[13], _
		lhs.x*rhs.getarraydata[2] + lhs.Y*rhs.getarraydata[6] + lhs.Z*rhs.getarraydata[10] + rhs.getarraydata[14], _
		lhs.x*rhs.getarraydata[3] + lhs.Y*rhs.getarraydata[7] + lhs.Z*rhs.getarraydata[11] + rhs.getarraydata[15] )

	end operator


	sub matrix.lookat( byref v1 as vec3f, byref v2 as vec3f, byref vup as vec3f )

		dim as vec3f d = v1-v2
		d.normalize
		dim as vec3f r = d.cross(vup)
		dim as vec3f u = r.cross(d)
		r.normalize
		u.normalize

		this.m(0)  = -r.x
		this.m(1)  = u.x
		this.m(2)  = d.x
		this.m(3)  = 0.0

		this.m(4)  = -r.y
		this.m(5)  = u.y
		this.m(6)  = d.y
		this.m(7)  = 0.0

		this.m(8)  = -r.z
		this.m(9)  = u.z
		this.m(10) = d.z
		this.m(11) = 0

		this.m(12)  = 0.0
		this.m(13)  = 0.0
		this.m(14)  = 0.0
		this.m(15)  = 1.0

		dim as matrix matrix2
		matrix2.LoadIdentity
		matrix2.Position =-v1
		this*= matrix2

	end sub


	sub matrix.PointAt( byref v1 as vec3f, byref v2 as vec3f )

		dim as vec3f d = v1-v2
		d.normalize
		dim as vec3f u = vec3f(0,1,0)

		if abs(d.y)>.9999 then
			u = vec3f( 0, 0, 1 )
		end if

		dim as vec3f r = -d.cross(u)
		r.normalize
		u = -r.cross(d)
		u.normalize

		this.m(0)  = r.x
		this.m(1)  = r.y
		this.m(2)  = r.z
		this.m(3)  = 0.0

		this.m(4)  = u.x
		this.m(5)  = u.y
		this.m(6)  = u.z
		this.m(7)  = 0.0

		this.m(8)  = d.x
		this.m(9)  = d.y
		this.m(10) = d.z
		this.m(11) = 0.0

		this.m(12)  = v1.x
		this.m(13)  = v1.y
		this.m(14)  = v1.z
		this.m(15)  = 1.0

	end sub


	sub matrix.ZeroUp( byref tP as vec3f )
		
		dim as vec3f tU, tR, tF

		if abs(this.up.y) > .9 then

			tU = this.up
			tR = vec3f(1,0,0)
			tF = tU.Cross(tR)

		elseif abs(this.forward.y) > .9 then

			tF = this.forward
			tR = vec3f(0,0,1)
			tU = tF.Cross(tR)

		elseif abs(this.right.y) > .9 then

			tR = this.right
			tF = vec3f(1,0,0)
			tU = tR.Cross(tF)

		end if

		this.up = tU
		this.right = tR
		this.forward = tF
		this.position = tP

	end sub

	function matrix.Inverse() as matrix

		dim as matrix inverted
		inverted.m(0)  = this.m(0)
		inverted.m(1)  = this.m(4)
		inverted.m(2)  = this.m(8)
		inverted.m(4)  = this.m(1)
		inverted.m(5)  = this.m(5)
		inverted.m(6)  = this.m(9)
		inverted.m(8)  = this.m(2)
		inverted.m(9)  = this.m(6)
		inverted.m(10) = this.m(10)
		inverted.m(3)  = 0.0f
		inverted.m(7)  = 0.0f
		inverted.m(11) = 0.0f
		inverted.m(15) = 1.0f

		inverted.m(12) = -(this.m(12) * this.m(0)) - (this.m(13) * this.m(1)) - (this.m(14) * this.m(2))
		inverted.m(13) = -(this.m(12) * this.m(4)) - (this.m(13) * this.m(5)) - (this.m(14) * this.m(6))
		inverted.m(14) = -(this.m(12) * this.m(8)) - (this.m(13) * this.m(9)) - (this.m(14) * this.m(10))
		return inverted

	end function


	function matrix.PlanarProjection( byref lightpos as vec4f, byref plane as vec4f ) as matrix

		with plane
			var xx = .x * lightpos.x
			var yy = .y * lightpos.y
			var zz = .z * lightpos.z
			var ww = .w * lightpos.w
			var dot = xx + yy + zz + ww

			var projected_matrix = matrix()

			projected_matrix.m( 0) = dot - xx
			projected_matrix.m( 1) = -lightpos.y * .x
			projected_matrix.m( 2) = -lightpos.z * .x
			projected_matrix.m( 3) = -lightpos.w * .x

			projected_matrix.m( 4) = -lightpos.x * .y
			projected_matrix.m( 5) = dot - yy
			projected_matrix.m( 6) = -lightpos.z * .y
			projected_matrix.m( 7) = -lightpos.w * .y

			projected_matrix.m( 8) = -lightpos.x * .z
			projected_matrix.m( 9) = -lightpos.y * .z
			projected_matrix.m(10) = dot - zz
			projected_matrix.m(11) = -lightpos.w * .z

			projected_matrix.m(12) = -lightpos.x * .w
			projected_matrix.m(13) = -lightpos.y * .w
			projected_matrix.m(14) = -lightpos.z * .w
			projected_matrix.m(15) = dot - ww

			return projected_matrix
		end with


	end function


	sub matrix.InfiniteProjection( byref fov as single, byref aspect as single, byref znear as single )

		this.m(4) = 0
		this.m(8) = 0
		this.m(12) = 0
		this.m(1) = 0
		this.m(9) = 0
		this.m(13) = 0
		this.m(2) = 0
		this.m(6) = 0
		this.m(3) = 0
		this.m(7) = 0
		this.m(15) = 0

		'this.m(0) = ( 1 / Tan(fov) ) / aspect
		'this.m(5) = ( 1 / Tan(fov) )
		'this.m(14) = -2 * znear
		'this.m(10) = -1
		'this.m(11) = -1

		this.m(0) = ( 1 / Tan(fov) ) / aspect
		this.m(5) = ( 1 / Tan(fov) )
		this.m(14) = -2 * znear
		this.m(10) = -1+(0.6/50)
		this.m(11) = -1

		this.m(14) = ((0.6/50)-2)*0.6

	end sub


	sub matrix.Perspective( byval fov as single, byval aspect as single, byval zNear as single, byval zFar as single )

		dim as single ymax, xmax
		dim as single temp, temp2, temp3, temp4
		ymax = znear * tan(fov * pi / 360.0)
		xmax = ymax * aspect

		dim as single lft = -xmax
		dim as single rght = xmax
		dim as single bottom = -ymax
		dim as single top = ymax

		temp = 2.0 * znear
		temp2 = rght - lft
		temp3 = top - bottom
		temp4 = zfar - znear
		this.m(0) = temp / temp2
		this.m(1) = 0.0
		this.m(2) = 0.0
		this.m(3) = 0.0
		this.m(4) = 0.0
		this.m(5) = temp / temp3
		this.m(6) = 0.0
		this.m(7) = 0.0
		this.m(8) = (rght + lft) / temp2
		this.m(9) = (top + bottom) / temp3
		this.m(10) = (-zfar - znear) / temp4
		this.m(11) = -1.0
		this.m(12) = 0.0
		this.m(13) = 0.0
		this.m(14) = (-temp * zfar) / temp4
		this.m(15) = 0.0

	end sub


	sub matrix.AxisAngle( byref v as vec3f, byref angle as single )

		dim as single tc = cos(angle*pi_180)
		dim as single ts = sin(angle*pi_180)
		dim as single tt = 1.0 - tc
		v.normalize
		this.m(0)  = tc + v.x*v.x*tt
		this.m(5)  = tc + v.y*v.y*tt
		this.m(10) = tc + v.z*v.z*tt
		dim as single tmp1 = v.x*v.y*tt
		dim as single tmp2 = v.z*ts
		this.m(4) = tmp1 + tmp2
		this.m(1) = tmp1 - tmp2
		tmp1 = v.x*v.z*tt
		tmp2 = v.y*ts
		this.m(8) = tmp1 - tmp2
		this.m(2) = tmp1 + tmp2
		tmp1 = v.y*v.z*tt
		tmp2 = v.x*ts
		this.m(9) = tmp1 + tmp2
		this.m(6) = tmp1 - tmp2
		this.m(15) = 1.0

	end sub


	sub matrix.Translate( byref v as vec2f )

		var t = matrix()
		t.LoadIdentity
		t.Position = type<vec3f>(v.x,v.y,t.Position.z)
		this *= t

	end sub


	sub matrix.Translate( byref v as vec3f )

		var t = matrix()
		t.LoadIdentity
		t.Position = type<vec3f>(v.x,v.y,v.z)
		this *= t

	end sub


	sub matrix.Translate( byref x as single, byref y as single, byref z as single )

		var t = matrix()
		t.LoadIdentity
		t.Position = type<vec3f>(x,y,z)
		this *= t

	end sub


	sub matrix.Translate( byref x as Integer, byref y as Integer, byref z as Integer )

		var t = matrix()
		t.LoadIdentity
		t.Position = type<vec3f>(x,y,z)
		this *= t

	end sub


	sub matrix.Rotate( byref anglex as single, byref angley as single, byref anglez as single )

		dim as single tcos = any
		dim as single tsin = any
		var tm = matrix()

		with tm

			if anglex <> 0 then
				tm.LoadIdentity
				tcos = cos( anglex * pi_180 )
				tsin = sin( anglex * pi_180 )
				.m( 5)= tcos
				.m( 6)= tsin
				.m( 9)=-tsin
				.m(10)= tcos
				this *= tm
			end if

			if angley <> 0 then
				tm.LoadIdentity
				tcos = cos( angley * pi_180 )
				tsin = sin( angley * pi_180 )
				.m( 0)= tcos
				.m( 2)=-tsin
				.m( 8)= tsin
				.m(10)= tcos
				this *= tm
			end if

			if anglez <> 0 then
				tm.LoadIdentity
				tcos = cos( anglez * pi_180 )
				tsin = sin( anglez * pi_180 )
				.m(0) = tcos
				.m(1) = tsin
				.m(4) =-tsin
				.m(5) = tcos
				this *= tm
			end if

		end with

	end sub


	sub matrix.Rotate( byref anglex as integer, byref angley as integer, byref anglez as integer )

		dim as single tcos = any
		dim as single tsin = any
		var tm = matrix()

		if anglex <> 0 then
			tm.LoadIdentity
			tcos = cos( anglex * pi_180 )
			tsin = sin( anglex * pi_180 )
			tm.m( 5)= tcos
			tm.m( 6)= tsin
			tm.m( 9)=-tsin
			tm.m(10)= tcos
			this *= tm
		end if

		if angley <> 0 then
			tm.LoadIdentity
			tcos = cos( angley * pi_180 )
			tsin = sin( angley * pi_180 )
			tm.m( 0)= tcos
			tm.m( 2)=-tsin
			tm.m( 8)= tsin
			tm.m(10)= tcos
			this *= tm
		end if

		if anglez <> 0 then
			tm.LoadIdentity
			tcos = cos( anglez * pi_180 )
			tsin = sin( anglez * pi_180 )
			tm.m(0) = tcos
			tm.m(1) = tsin
			tm.m(4) =-tsin
			tm.m(5) = tcos
			this *= tm
		end if
	end sub


	sub matrix.Rotate( byref v as vec3f )

		Rotate v.x, v.y, v.z

	end sub


	sub matrix.Scale( byref scalar as single )

		dim as matrix temp
		temp.LoadIdentity
		temp.m(0)  = scalar
		temp.m(5)  = scalar
		temp.m(10) = scalar
		this*=temp

	end sub


	sub matrix.Scale( byref scalarx as single, byref scalary as single, byref scalarz as single )

		dim as matrix temp
		temp.LoadIdentity
		temp.m(0)  = scalarx
		temp.m(5)  = scalary
		temp.m(10) = scalarz
		this*=temp

	end sub


	sub matrix.Gram_Schmidt( byref d as vec3f )

		dim as vec3f r
		dim as vec3f u
		dim as vec3f f = d

		f *= ( 1.0 / sqr(f.dot(f)) )

		if ABS(f.z) >.577 then
			r = f * type<vec3f>(-f.y, f.z, 0.0)
		else
			r = f * type<vec3f>(-f.y, f.x, 0.0)
		end if
		r *= ( 1.0 / sqr(r.dot(r)) )
		u = r * f

		this.LoadIdentity
		this.Forward = f
		this.Up = u
		this.Right = r
		this.Position = type<vec3f>(0,0,0)

	end sub


	operator matrix.cast() as vmat

		return type<vmat>( this.right, this.up, this.forward, this.position )

	end operator


	property matrix.Right( byref v  as vec3f )
	m(0) = v.x
	m(1) = v.y
	m(2) = v.z
	end property


	property matrix.Up( byref v  as vec3f )
	m(4) = v.x
	m(5) = v.y
	m(6) = v.z
	end property


	property matrix.Forward( byref v  as vec3f )
	m(8)  = v.x
	m(9)  = v.y
	m(10) = v.z
	end property


	property matrix.Position( byref v  as vec3f )
	m(12) = v.x
	m(13) = v.y
	m(14) = v.z
	end property


	property matrix.Right( byref v  as vec4f )
	m(0) = v.x
	m(1) = v.y
	m(2) = v.z
	m(3) = v.w
	end property


	property matrix.Up( byref v  as vec4f )
	m(4) = v.x
	m(5) = v.y
	m(6) = v.z
	m(7) = v.w
	end property


	property matrix.Forward( byref v  as vec4f )
	m(8)  = v.x
	m(9)  = v.y
	m(10) = v.z
	m(11) = v.w
	end property


	property matrix.Position( byref v  as vec4f )
	m(12) = v.x
	m(13) = v.y
	m(14) = v.z
	m(15) = v.w
	end property


	property matrix.Right() as vec4f
	return type<vec4f>( m(0), m(1), m(2), m(3) )
	end property


	property matrix.Up() as vec4f
	return type<vec4f>( m(4), m(5), m(6), m(7) )
	end property


	property matrix.Forward() as vec4f
	return type<vec4f>( m(8), m(9), m(10), m(11) )
	end property


	property matrix.Position() as vec4f
	return type<vec4f>( m(12), m(13), m(14), m(15) )
	end property


	property matrix.GetArrayData() as single ptr
	return @this.m(0)
	end property


	constructor vmat()

	this.r = vec3f(1,0,0)
	this.u = vec3f(0,1,0)
	this.f = vec3f(0,0,1)
	this.p = vec3f(0,0,0)

	end constructor


	constructor vmat( byref p as vec3f, byref f as vec3f, byref r as vec3f, byref u as vec3f )

	this.r = r
	this.u = u
	this.f = f
	this.p = p

	end constructor


	sub vmat.rotate( byval x as single, byval y as single, byval z as single )

		dim as vmat v = this
		dim as single tCos = cos(x)
		dim as single tSin = sin(x)

		v.f = v.f * tCos + v.u * tSin
		v.f.normalize
		v.u = v.f.cross(v.r)
		v.u*=-1

		tCos = cos(y)
		tSin = sin(y)
		v.f = v.f * tCos - v.r * tSin
		v.f.normalize
		v.r = v.f.cross(v.u)

		tCos = cos(z)
		tSin = sin(z)
		v.r = v.r * tCos + v.u * tSin
		v.r.normalize
		v.u = v.f.cross(v.r)
		v.u*=-1

		this = v

	end sub


	function inv_sqr( byval dist as single ) as single

		static as single xhalf
		static as integer ti

		xhalf = 0.5f*dist
		ti = *cast( integer ptr, @dist )
		ti = &h5f3759df - (ti shr 1)
		dist = *cast( single ptr, @ti )

		return dist*(1.5f - xhalf*dist*dist)

	end function


	function cotan( byref num as single ) as single

		return 1f / tan(num)

	end function


	function poly_normal( byref V as vec3f ptr ) as vec3f

		dim as vec3f V1 = V[1] - V[0]
		dim as vec3f V2 = V[2] - V[0]
		dim as vec3f tNorm

		tNorm = v1.Cross(v2)
		tNorm.Normalize

		return tNorm

	end function


	function plane_distance( byref tNormal as vec3f, byref tPoint as vec3f) as single

		plane_distance = -((tNormal.x * tPoint.x) + (tNormal.y * tPoint.y) + (tNormal.z * tPoint.z))

	end function

	function closestpointonline( byref Va as vec3f, byref Vb as vec3f, byref vPoint as vec3f ) as vec3f

		dim vVector1 as vec3f, vVector2 as vec3f, vVector3 as vec3f
		dim d as double, t as double

		vVector1 = VPoint - Va
		vVector2 = Vb - Va

		vVector2.Normalize

		d = va.distance(vb)
		t = vVector2.Dot(vVector1)

		if t<=0 then
			return Va
		end if

		if t>=d then
			return Vb
		end if

		vVector3 = vVector2 * t

		return Va + vVector3

	end function


	function intersected_plane( byref V as vec3f ptr, byref vLine as vec3f ptr, byref vNormal as vec3f, byref OriginDist as single) as integer

		dim Dist1 as single, Dist2 as single

		vNormal = poly_normal( @V[0] )
		OriginDist = plane_distance(VNormal, V[0] )

		Dist1 = ((vNormal.x * vLine[0].x) + (vNormal.y * vLine[0].y) + (vNormal.z * vLine[0].z)) + OriginDist
		Dist2 = ((vNormal.x * vLine[1].x) + (vNormal.y * vLine[1].y) + (vNormal.z * vLine[1].z)) + OriginDist

		if Dist1*Dist2>=0 then
			intersected_plane = 0
		elseif Dist1*Dist2<0 then
			intersected_plane = -1
		end if

	end function


	function intersection_point( byref vNormal as vec3f, byref vLine as vec3f ptr, byref tDistance as single ) as vec3f

		dim as vec3f vReturn
		dim vLineDir as vec3f
		dim Numerator as double, Denominator as double, Dist as double

		vLineDir = VLine[1] - VLine[0]
		vLineDir.Normalize

		Numerator = -((vNormal.X*VLine[0].X) + (vNormal.Y*vLine[0].Y) + ((vNormal.Z*vLine[0].Z) + tDistance))
		Denominator = vNormal.Dot(vLineDir)

		if Denominator = 0 then
			vReturn = VLine[0]
		else
			Dist = Numerator / Denominator
			vReturn = vLine[0] + (vLineDir * Dist)
		end if

		return vReturn

	end function


	function point_inside_triangle3d( byref vPoint as vec3f, byref V as vec3f ptr ) as integer

		if sameside(vPoint, V[0], V[1], V[2]) and sameside(vPoint, V[1], V[0], V[2]) _
		and sameside(vPoint, V[2], V[0], V[1]) then return -1

	end function


	function sameside( byref p1 as vec3f, byref p2 as vec3f, byref a as vec3f, byref b as vec3f ) as integer

		dim as vec3f cp1, cp2, tVector1, tVector2

		tVector1 = b-a
		tVector2 = p1-a
		cp1 = tVector1.Cross( tVector2 )
		tVector2 = p2-a
		cp2 = tVector1.Cross( tVector2 )

		function = cp1.Dot(cp2)>=0

	end function


	function line_triangle_intersection( byref V as vec3f ptr, byref vLine as vec3f ptr, byref New_Intersection as vec3f ) as integer

		dim vNormal as vec3f, OriginDistance as single

		if intersected_plane( V, VLine, vNormal, OriginDistance) then
			New_Intersection = intersection_point( vNormal, vLine, OriginDistance )
			if point_inside_triangle3d( New_Intersection, V ) then
				return true
			end if
		end if

		return false

	end function


	sub get_collision_offset(byref vNormal as vec3f, byref Radius as single, byref Dist as single, byref vOffSet as vec3f)

		dim distanceOver as single

		distanceOver = Radius - Dist

		vOffset = vNormal * distanceOver

	end sub


	function classify_sphere(byref vCenter as vec3f, byref VNormal as vec3f, byref vPoint as vec3f, byref Radius as single, byref Dist as single ) as integer

		dim D as single

		D = plane_distance (vNormal, vPoint)
		Dist = (vNormal.x*VCenter.x  + vNormal.y*vCenter.y + vNormal.z*vCenter.z + D)

		if Dist < Radius then
			return SPHERE_INTERSECTS
		elseif Dist >= Radius then
			return SPHERE_FRONT
		else
			return SPHERE_BEHIND
		end if

	end function


	function edge_sphere_collision( byref vCenter as vec3f, byref V as vec3f ptr, byref Radius as single ) as integer

		dim vPoint as vec3f, Dist as single
		dim i as integer, iAnd1 as integer

		for i = 0 to 2
			iAnd1 = (i + 1) mod 3
			vPoint = closestpointonline( V[i], V[iAnd1], vCenter )
			Dist = vPoint.Distance( vCenter )
			if Dist < Radius then
				return -1
			end if
		next

	end function


	function point_inside_triangle2d (  byref x as integer, byref y as integer, byref p1 as vec2f, byref p2 as vec2f, byref p3 as vec2f ) as integer

		dim as integer b0 = (p2.x - p1.X) * (p3.Y - p1.Y) - (p3.X - p1.X) * (p2.Y - p1.Y)

		if b0 = 0 then
			exit function
		end if

		dim as single b1 = (((p2.x - x) * (p3.Y - y)) - ((p3.X - x) * (p2.Y - y))) / b0
		dim as single b2 = (((p3.x - x) * (p1.Y - y)) - ((p1.X - x) * (p3.Y - y))) / b0
		dim as single b3 = (((p1.x - x) * (p2.Y - y)) - ((p2.X - x) * (p1.Y - y))) / b0

		if b1 > 0 and b2 > 0 and b3 > 0 then
			return 1
		elseif b1 > 0 and b2 > 0 and b3 = 0 then
			return 2
		elseif b1 > 0 and b2 = 0 and b3 > 0 then
			return 3
		elseif b1 = 0 and b2 > 0 and b3 > 0 then
			return 4
		end if

	end function



	function AABB overload( byref a1 as vec2f, byref a2 as vec2f, byref b1 as vec2f, byref b2 as vec2f) as integer

		if b2.x>=a1.x and b1.x<=a2.x then
			if b2.y>=a1.y and b1.y<=a2.y then
				return true
			end if
		end if

		return false

	end function


	function AABB overload( byref a1 as vec3f, byref a2 as vec3f, byref b1 as vec3f, byref b2 as vec3f) as integer

		if b2.x>=a1.x and b1.x<=a2.x then
			if b2.y>=a1.y and b1.y<=a2.y then
				if b2.z>=a1.z and b1.z<=a2.z then
					return true
				end if
			end if
		end if

		return false

	end function



	Function Spline_Point( Points() as vec3f, byval p as single ) as vec3f

		dim as vec3f rPoint
		dim as vec3f a,b,c

		c.x = 3 * (Points(1).X - Points(0).X)
		b.x = 3 * (Points(3).X - Points(1).X) - c.x
		a.x = Points(2).X - Points(0).X - c.x - b.x

		c.y = 3 * (Points(1).Y - Points(0).Y)
		b.y = 3 * (Points(3).Y - Points(1).Y) - c.y
		a.y = Points(2).Y - Points(0).Y - c.y - b.y

		c.z = 3 * (Points(1).Z - Points(0).Z)
		b.z = 3 * (Points(3).Z - Points(1).Z) - c.z
		a.z = Points(2).Z - Points(0).Z - c.z - b.z

		rPoint.X = a.x * p ^ 3 + b.x * p ^ 2 + c.x * p + Points(0).X
		rPoint.Y = a.y * p ^ 3 + b.y * p ^ 2 + c.y * p + Points(0).Y
		rPoint.Z = a.z * p ^ 3 + b.z * p ^ 2 + c.z * p + Points(0).Z

		return rPoint

	end function



end namespace
