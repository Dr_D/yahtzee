#include once "game.bi"

using ndisplay
using nmodel
using nimage
using nmathf
using nphysics
using nhash
using ngui


extern Display as ndisplay.Display_Struct
extern h_table	as nhash.ht_t ptr
dim as display_struct display
dim as ht_t ptr h_table


randomize timer


declare function main( byval argc as integer, byval argv as zstring ptr ptr ) as integer


end main( __FB_ARGC__, __FB_ARGV__ )


function main( byval argc as integer, byval argv as zstring ptr ptr ) as integer

	debugout = freefile
	open cons for output as #debugout

	h_table = new ht_t( 64 )

	init_gl_window( 1024, 768, 32, 0, 16, 0 )

	dim as game_struct game

	game.init()

	use_shaders = true
	use_multitex = true
	use_anisotropic = true
	load_default_shaders()

	game.myFont.oBegin()
	dim as string tString = "Loading, please Wait..."
	game.myFont.oPrint( tString, display.w2-len(tString)*7, display.h2+10, 1,1,1, 20,20 )
	tString = "Please read the instructions before playing!"
	game.myFont.oPrint( tString, display.w2-len(tString)*7, 15, 1,1,1, 20,20 )
	game.myFont.oEnd()
	flip

	init_light( 1, game.light, type(.5,.5,.5,1.0), type(.8,.8,.8,1.0), type(.7,.7,.7,1.0), vec4f(-35, 150, -35, 1), vec3f(0f,1f,0f) )

	game.Create( 225, 3.5f )

	game.LoadModel( "res/model/playboard.lwo", 0, 1 )
	game.LoadModel( "res/model/die.lwo", 0, 1 )

	dim as matrix idMatrix
	idMatrix.LoadIdentity()

	game.TreeBegin()

	game.TreeAddStaticEntity( @game.Model(0), idMatrix )

	game.TreeEnd()

	game.AddMaterial( "Dice", .75, .75, 1, 1.5, 1.4 )

	for i as integer = 0 to 4

		dim as string temp = "Holder" + str(i+1)

		dim as vec3f tpos = find_surface_center( @game.Model(0), temp )

		game.AddGenericEntity( idMatrix, 50, vec3f(.5,.5,.5), str(i) + " Die", GetModelByName( "die.lwo", game.Model() ) )

		game.holder(i).holdPos = tPos+vec3f(0,.5,0)
		game.holder(i).full = false

	next


	for e as integer = 0 to ubound(game.entity)
		game.entity(e).SetMaterial(game.FindMaterialByName( "Dice" ) )
		NewtonMaterialSetCollisionCallback( game.wWorld, game.entity(e).material.reference, game.FindMaterialByName("World").reference, @game.entity(e), @Dice_World_Contact_Begin(), @Dice_World_Contact_Process(), @Dice_World_Contact_End() )
	next


	for i as integer = 0 to 4

		dim as vec3f tOmega = vec3f(.001, .001, .001)
		game.die(i).nBody = game.Entity(i).body
		NewtonBodySetLinearDamping( game.die(i).nBody, .001)
		NewtonBodySetAngularDamping( game.die(i).nBody, @tOmega.x)
	next

	game.hold_all_dice()

	
	dim as matrix lookMatrix
	dim as vMat mControl
	mControl.p = vec3f(0,15,10)
	mControl.f = (vec3f(0,0,1.5)-mControl.p).UnitVec

	dim as integer tMx, tMy, mouseClip, mouseMode = true

	setmouse(display.w2,display.h2)

	game.update()
	

	do


		dim as integer button
		getmouse( tMx, tMy,, button )
		mouseClip = false

		if tMx<0 then mouseClip = true

		if multikey(FB .SC_TAB) then

			mouseMode = not mouseMode

			if mouseMode then

				setmouse(, , 1)

			else

				tMx = display.w2
				tmY = display.h2

				setmouse(display.w2, display.h2, false)

			end if

			waitKey(FB.SC_TAB)

		end if

		if mouseMode = 0 then

			if multikey(FB.SC_W) then
				mControl.p+=mControl.f * game.deltaTime*25f
			end if

			if multikey(FB.SC_S) then
				mControl.p-=mControl.f * game.deltaTime*25f
			end if

			if multikey(FB.SC_D) then
				mControl.p+=mControl.r * game.deltaTime*25f
			end if

			if multikey(FB.SC_A) then
				mControl.p-=mControl.r * game.deltaTime*25f
			end if

			mControl.Rotate( (display.h2-tMy)/500f, (display.w2-tMx)/500f, -mControl.r.y )

			setmouse(display.w2, display.h2, false)

		end if


		if multikey(FB.SC_ESCAPE) then

			if not game.hasMainMenu then

				game.create_main_menu()

			end if

		end if


		lookMatrix.LookAt( mControl.p, mControl.p + mControl.f*5f, mControl.u )


		if game.started then

			if multikey(FB.SC_V) then

				if game.rollCount<3 then

					dim as string tString = game.plScore(game.curPlayer).name + "'s Scorecard"

					dim as window_struct ptr myWindow = game.myGui.GetWindowByTitle( tString )

					if myWindow <> 0 then

						myWindow->visible = not myWindow->visible

						myWindow->is_focus = myWindow->visible

						game.plScore(game.curPlayer).visible = myWindow->visible

					end if

				end if

				waitKey(FB.SC_V)

			end if

			if not game.plScore(game.curPlayer).visible then

				select case as const game.pMode

					case 0

						if (button and FB.BUTTON_RIGHT) then

							if not mouseClip then

								if game.dice_stopped() then

									if game.rollCount = 0 then

										game.clear_holders()

									end if

									if not game.checkRoll then

										game.playerRolled = true

										game.checkRoll = true

										game.roll_dice()

									end if

								end if

							end if

						end if


						if (button and FB.BUTTON_LEFT) then

							if game.dice_stopped() and game.rollCount >0 then

								dim as Ray_Struct rayData = character_raycast_world( game.wWorld, mControl )

								if rayData.nBody then

									dim as entity_struct ptr Hit_Object

									Hit_Object = NewtonBodyGetUserData( rayData.nBody )

									'I named the dice to have their index as the first character of their name
									'so all you need to do is check the name,
									'and it will give you the array index...
									dim as integer thisDie = val(rayData.designation)

									if game.die(thisDie).isLocked then

										game.remove_die_from_holder( thisDie )

									else

										game.add_die_to_holder( thisDie )

									end if

								end if

							end if

						end if


						if game.checkRoll then

							if game.dice_stopped() then

								game.checkRoll = false

								game.process_score()

								game.rollCount+=1

								if game.rollCount>2 then

									game.pMode = 1

								end if

							end if

						end if


					case 1


						if game.dice_stopped() then

							game.hold_all_dice()

							game.process_score()

							game.pMode = 2

						end if


					case 2

						dim as string tString = game.plScore(game.curPlayer).name + "'s Scorecard"

						dim as window_struct ptr myWindow = game.myGui.GetWindowByTitle( tString )

						myWindow->visible = true

						myWindow->is_Focus = true

						game.pMode = 3

					case 3

						'since the scorecard gui is open at this point,
						'pMode will increment to 4 in the close_window() callback
						'after the player decides how to use their score

					case 4

						dim as string tString = game.plScore(game.curPlayer).name + "'s Scorecard"

						dim as window_struct ptr myWindow = game.myGui.GetWindowByTitle( tString )

						myWindow->visible = false

						game.rollCount = 0

						game.pMode = 0

				end select


			end if

		end if


		game.advance()

		game.render_scene( lookMatrix )

		game.myGui.Update(game.deltaTime)


		if game.started then

			if not game.hasMainMenu then

				if game.pMode = 0 then

					if game.dice_stopped() then

						if not game.playerRolled then

							if not game.plScore(game.curPlayer).visible then

								game.myFont.oBegin()
								dim as single col = .5+.5*sin(game.last_time*5)
								dim as string tString = "Your turn, " & game.plScore(game.curPlayer).name & "!"
								game.myFont.oPrint( tString, display.w2-len(tString)*21, display.h2, col,col,col,col, 60, 75 )
								game.myFont.oEnd()

							end if

						end if

					end if

				end if

			end if

		end if
		
		if inkey = chr(255) & "k" then game.killGame = true
		
		sleep( 3, 1 )

		flip

	loop until game.killGame


	game.Destroy()


	h_table->hkill()


	return 0


end function
