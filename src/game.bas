#include once "game.bi"

extern Display as ndisplay.Display_Struct
extern h_table	as nhash.ht_t ptr

using ndisplay
using nmodel
using nimage
using nmathf
using nphysics
using nhash
using ngui

sub game_struct.init()


	dim as integer fNum = freefile

	dim as string temp

	open "res/data/scorecard.dat" for input as #fNum

	for i as integer = 0 to ubound(tempCard.upper)

		line input #fNum, temp

		tempCard.upper(i).name = temp

	next

	for i as integer = 0 to ubound(tempCard.lower)

		line input #fNum, temp

		tempCard.lower(i).name = temp

	next

	close #fNum

	myFont.Load( "res/texture/font.png" )
	myGui.SetFont( @myFont )



	create_main_menu()

	print #debugout, " "
	print #debugout, "All windows " & ubound(myGui.wind)
	print #debugout, " "

end sub


function game_struct.advance() as integer

	update()

	fSOUND_Update()

	if started then

		if doAdvance then

			playerRolled = false

			doAdvance = false

			rollCount = 0

			tempCard.hasClicked = false


			dim as integer pCount = ubound(plScore)

			do

				pCount-=1

				curPlayer+=1

				if curPlayer>ubound(plScore) then

					curPlayer = 0

				end if

				if plScore(curPlayer).isAlive then

					exit do

				end if

			loop while pCount > 0


			'seems strange to set gameOver true here,
			'but it's set false in the next loop, if the conditions are right
			gameOver = true

			for p as integer = 0 to ubound(plScore)

				if plScore(p).isAlive then

					gameOver = false

				end if

			next

			clear_tempCard()

			if gameOver then

				started = false

				if not hasMainMenu then

					create_main_menu()

				end if

			end if

		end if

	end if

	return false

end function


function game_struct.create_main_menu() as integer
	
	
	dim as window_struct ptr cheatOpen = myGui.GetWindowByTitle("NO CHEATING!!!")
	
	if cheatOpen<>0 then
		
		cheatopen->do_close = true
		
	end if
	

	dim as vec2f size  = vec2f(500, 100)
	dim as vec2f posit = vec2f(display.w2-size.x/2, display.h2-size.y/2)

	dim as integer thisWindow = myGui.AddWindow( posit, size, type( 0.59, 0.41, 0.3), type(0f,0f,0f,1f) )

	with myGui.wind(thisWindow)

		.title = "Main Menu"

		.isAlert = true

		dim as integer thisButton = .AddButton( (size/2)-vec2f(200,(size.y/2)-6), vec2f(96,32), "Continue",, 0)
		.button(thisButton).SetUserData(@myGui.wind(thisWindow).index)
		.button(thisButton).SetUserCallback( @continue_main_callback() )
		.button(thisButton).SetUserData(@this)

		thisButton = .AddButton( (size/2)-vec2f(48,(size.y/2)-6), vec2f(96,32), "New Game",, 0)
		.button(thisButton).SetUserData(@myGui.wind(thisWindow).index)
		.button(thisButton).SetUserCallback( @new_main_callback() )
		.button(thisButton).SetUserData(@this)

		thisButton = .AddButton( (size/2)-vec2f(-96,(size.y/2)-6), vec2f(96,32), "Quit",, 0)
		.button(thisButton).SetUserData(@myGui.wind(thisWindow).index)
		.button(thisButton).SetUserCallback( @exit_main_callback() )
		.button(thisButton).SetUserData(@this)

	end with

	hasMainMenu = true

	return true

end function


function add_player_callback( byval button as button_struct ptr ) as integer
	
	dim as game_struct ptr myGame = cast( game_struct ptr, button->user_data )
	
	dim as window_struct ptr createExists = myGame->myGui.GetWindowByTitle("Setup A New Game")
	
	if createExists = 0 then
		
		return false
		
	end if
	
	dim as integer pCount = ubound(myGame->plScore)+1
	
	redim preserve myGame->plScore(pCount)
	
	myGame->plScore(pCount).name = createExists->tBox(0).tString
	
	myGame->plScore(pCount).isAlive = true
	
	createExists->AddLabel( vec2f(10, (createExists->size.y-130)-pCount*25), vec2f(275,20), "Player " & pCount+1 & " : " & myGame->plScore(pCount).name )
	
	print #debugout, myGame->plScore(pCount).name
	
	return true
	
end function


function game_struct.create_setup_window() as integer
	
	started = false
	
	dim as window_struct ptr createExists = myGui.GetWindowByTitle("Setup A New Game")
	
	if createExists <> 0 then
		
		clear_game()
		
		return false
		
	end if
		
	dim as vec2f size  = vec2f(325, 500)
	dim as vec2f posit = vec2f(display.w2-size.x/2, display.h2-size.y/2)
	
	dim as integer thisWindow = myGui.AddWindow( posit, size, type( 0.59, 0.41, 0.3), type(0f,0f,0f,1f) )
	
	with myGui.wind(thisWindow)
		
		.title = "Setup A New Game"
		
		dim as integer thisButton = .AddButton( (size/2)-vec2f(64,(size.y/2)-6), vec2f(128,32), "START GAME!",, 0)
		.button(thisButton).SetUserData(@this)
		.button(thisButton).SetUserCallback( @start_new_game_callback() )
		
		thisButton = .AddButton( vec2f(170, size.y-75), vec2f(140,32), "Add Player",, 0)
		.button(thisButton).SetUserData(@this)
		.button(thisButton).SetUserCallback( @add_player_callback() )
		
		dim as integer thistBox = .AddTextBox( vec2f(10, size.y-70), 150, "Name", type(1,1,1,1), type(0,0,0,1) )
		
		
	end with
	
	return true
	
end function


function start_new_game_callback( byval button as button_struct ptr ) as integer
	
	dim as game_struct ptr myGame = cast( game_struct ptr, button->user_data )
	
	dim as window_struct ptr createExists = myGame->myGui.GetWindowByTitle("Setup A New Game")
	
	if ubound(myGame->plScore)<0 then
		
		return false
		
	end if
	
	createExists->closing = true
	
	myGame->hasMainMenu = false
	
	myGame->new_game()
	
	return true
	
end function


function exit_main_callback( byval button as button_struct ptr ) as integer

	dim as game_struct ptr myGame = cast(game_struct ptr, button->user_data )

	myGame->killGame = true

	return true

end function


function continue_main_callback( byval button as button_struct ptr ) as integer

	dim as game_struct ptr myGame = cast(game_struct ptr, button->user_data )

	dim as window_struct ptr myWin = myGame->myGui.GetWindowByTitle("Main Menu")

	myWin->closing = true

	print #debugout, "Continue the game!"

	myGame->hasMainMenu = false

	return true

end function


function new_main_callback( byval button as button_struct ptr ) as integer

	dim as game_struct ptr myGame = cast(game_struct ptr, button->user_data )

	dim as window_struct ptr myWin = myGame->myGui.GetWindowByTitle("Main Menu")
	
	if myWin<>0 then
	
		myWin->closing = true
		
	end if
	
	myGame->hasMainMenu = false
	
	myGame->clear_game()
		
	myGame->create_setup_window()

	return true

end function


function game_struct.clear_game() as integer
	
	if ubound(plScore)>-1 then

		for p as integer = 0 to ubound(plScore)

			dim as string tString = plScore(p).name + "'s Scorecard"

			dim as window_struct ptr myWindow = myGui.GetWindowByTitle( tString )

			myWindow->do_close = true
			
			myWindow->title = "Junk " & p
			
		next
		
		erase plScore
		
	end if
	
	return true
	
end function


function game_struct.new_game() as integer
	
	create_scorecard()

	started = true
	
	curPlayer = 0
	
	gameOver = false
	
	pMode = 0

	rollCount = 0
	
	playerRolled = false
	
	hasMainMenu = false
	
	return true

end function


sub game_struct.clear_tempCard()


	for l as integer = 0 to ubound(tempCard.lower)

		tempCard.lower(l).value = 0

	next


	for u as integer = 0 to ubound(tempCard.upper)

		tempCard.upper(u).value = 0

	next


end sub


function close_alert_callback( byval button as button_struct ptr ) as integer

	dim as window_struct ptr parent = cast(window_struct ptr, button->parent )

	parent->closing = true

	return true

end function


function close_scorecard_callback( byval button as button_struct ptr ) as integer

	dim as game_struct ptr myGame = cast(game_struct ptr, button->user_data )


	if myGame->rollCount<3 then

		dim as window_struct ptr parent

		parent = cast(window_struct ptr, button->parent )

		parent->visible = false

		myGame->plScore(myGame->curPlayer).visible = false

		if myGame->tempCard.hasClicked then

			myGame->doAdvance = true

		end if

		return false

	end if


	if not myGame->tempCard.hasClicked then

		dim as integer tx, ty
		getmouse(tx,ty)

		dim as vec2f size  = vec2f(300, 200)
		dim as vec2f posit = vec2f(tx-size.x/2, display.h-ty-size.y/2)

		dim as integer wIndex = myGame->myGui.AddWindow( posit, size, type( 0.59, 0.41, 0.3), type(0,0,0,1) )

		with myGame->myGui.wind(wIndex)

			.AddButton( (size/2)-vec2f(32,(size.y/2)-3), vec2f(64,32), "OK",, @close_alert_callback())

			dim as vec2f lSize = vec2f(size.x-30, 20)

			.AddLabel( vec2f((size.x/2)-lSize.x/2, (size.y/2)-lSize.y/2), lSize, "YOU MUST SCORE ALL DICE!", type(0.2, 0.2, 0.0), type(1,1,0,1) )

			.title = "NO CHEATING!!!"

			.isAlert = true

		end with

		myGame->plScore(myGame->curPlayer).visible = true

		return false

	else

		dim as window_struct ptr parent

		parent = cast(window_struct ptr, button->parent )

		parent->visible = false

		myGame->pMode = 4

		myGame->plScore(myGame->curPlayer).visible = false

		myGame->doAdvance = true

		return true

	end if

end function


function scorecard_score_upper_callback( byval button as button_struct ptr ) as integer

	dim as game_struct ptr myGame = cast(game_struct ptr, button->user_data )

	dim as window_struct ptr parent = cast(window_struct ptr, button->parent )

	dim as integer curPlayer = myGame->curPlayer

	if myGame->tempCard.hasClicked then return false

	if not myGame->playerRolled then return false

	if not myGame->plScore(curPlayer).upper(button->index).locked then

		myGame->tempCard.hasClicked = true

		myGame->plScore(curPlayer).upper(button->index).locked = true

		myGame->plScore(curPlayer).upper(button->index).value = myGame->tempCard.upper(button->index).value

		parent->label(button->child_label).caption = str(myGame->tempCard.upper(button->index).value)

		button->visible = false

		myGame->clear_tempCard()

	end if

	myGame->update_scorecard()

	return true

end function


function scorecard_score_lower_callback( byval button as button_struct ptr ) as integer

	dim as game_struct ptr myGame = cast(game_struct ptr, button->user_data )

	dim as window_struct ptr parent = cast(window_struct ptr, button->parent )

	dim as integer curPlayer = myGame->curPlayer

	if myGame->tempCard.hasClicked then return false

	if not myGame->playerRolled then return false

	if not myGame->plScore(curPlayer).lower(button->index).locked then

		dim as integer scoreValue = myGame->tempCard.lower(button->index).value

		myGame->tempCard.hasClicked = true

		if scoreValue = 50 then

			if val(parent->label(button->child_label).caption) = 50 then

				myGame->plScore(curPlayer).yahtzeeCnt+=1

			else

				myGame->plScore(curPlayer).lower(button->index).value = scoreValue

				parent->label(button->child_label).caption = str(scoreValue)

			end if

		else


			myGame->plScore(curPlayer).lower(button->index).locked = true

			button->visible = false

			if val(parent->label(button->child_label).caption) <> 50 then

				myGame->plScore(curPlayer).lower(button->index).value = scoreValue

				parent->label(button->child_label).caption = str(scoreValue)

			end if

		end if

		myGame->clear_tempCard()

	end if

	myGame->update_scorecard()

	return true

end function


function game_struct.create_scorecard() as integer

	dim as vec2f size  = vec2f(500, 500)
	dim as vec2f posit = vec2f(display.w2-size.x/2, display.h2-size.y/2)

	for p as integer = 0 to ubound(plScore)

		dim as integer wIndex = myGui.AddWindow( posit, size, type( 0.59, 0.41, 0.3), type(0f,0f,0f,1f) )

		with myGui.wind(wIndex)

			.visible = false

			.title = plScore(p).name + "'s Scorecard"

			.AddButton( (size/2)-vec2f(48,(size.y/2)-6), vec2f(96,32), "Continue",, 0)
			.AddLabel( vec2f(2,size.y-50), vec2f(size.x-4,20), "Choose how to score.", type( 0.59, 0.41, 0.3) )

			.button(0).SetUserData(@this)
			.button(0).SetUserCallback( @close_scorecard_callback() )


			for l as integer = 0 to ubound(plScore(p).upper)

				dim as string temp = tempCard.upper(l).name

				if l<6 then

					.AddLabel( vec2f(10,(.size.y-90)-l*40), vec2f(80,20), temp, type( 0.59, 0.41, 0.3) )

					tempCard.upper(l).bIndex = .AddButton( vec2f(100,(.size.y-96)-l*40), vec2f(64,32), str(tempCard.upper(l).value) + " " + chr(26),, @scorecard_score_upper_callback() )

					.button(ubound(.button)).SetUserData(@this)
					.button(ubound(.button)).index = l

					.button(ubound(.button)).child_label = .AddLabel( vec2f(174,(.size.y-90)-l*40), vec2f(40,20), str(plScore(p).upper(l).value), type( 0.59, 0.41, 0.3) )

					plScore(p).upper(l).lIndex = .button(ubound(.button)).child_label

				else

					.AddLabel( vec2f(10,(.size.y-90)-l*40), vec2f(80,20), temp, type(0.4, 0.19, 0.0 ), type(1,1,1) )

					tempCard.upper(l).lIndex = .AddLabel( vec2f(174,(.size.y-90)-l*40), vec2f(40,20), str(plScore(p).upper(l).value), type(0.4, 0.19, 0.0 ), type(1,1,1)  )

					plScore(p).upper(l).lIndex = tempCard.upper(l).lIndex

				end if

			next


			for l as integer = 0 to ubound(plScore(p).lower)

				dim as string temp = tempCard.lower(l).name

				if l<7 then

					.AddLabel( vec2f(10+255,(.size.y-90)-l*40), vec2f(95,20), temp, type( 0.59, 0.41, 0.3) )

					tempCard.lower(l).bIndex = .AddButton( vec2f(115+255,(.size.y-96)-l*40), vec2f(64,32), str(plScore(p).lower(l).value) + " " + chr(26),, @scorecard_score_lower_callback())

					.button(ubound(.button)).SetUserData(@this)
					.button(ubound(.button)).index = l

					.button(ubound(.button)).child_label = .AddLabel( vec2f(189+255,(.size.y-90)-l*40), vec2f(40,20), str(plScore(p).lower(l).value), type( 0.59, 0.41, 0.3) )

				else

					.AddLabel( vec2f(10+255,(.size.y-90)-l*40), vec2f(95,20), temp, type(0.4, 0.19, 0.0), type(1,1,1)  )

					tempCard.lower(l).lIndex = .AddLabel( vec2f(189+255,(.size.y-90)-l*40), vec2f(40,20), str(plScore(p).lower(l).value), type(0.4, 0.19, 0.0), type(1,1,1)  )

					plScore(p).lower(l).lIndex = tempCard.lower(l).lIndex

				end if
			next

		end with

	next

	return 1

end function


sub game_struct.process_score()

	dim as integer isTriple = false
	dim as integer isQuad = false
	dim as integer isYahtzee = false
	dim as integer isFullHouse = false
	dim as integer isSmallStraight = false
	dim as integer isLargeStraight = false
	dim as integer fullSum

	dim as integer faceCnt(1 to 6)


	print #debugout, " "
	print #debugout, "Results of current roll..."

	dim as string temp

	for d as integer = 0 to ubound(die)

		die(d).curFace = get_die_face( die(d) )

		temp += str(die(d).curFace) + ", "
	next

	print #debugout, temp


	for f as integer = 1 to 6

		for d as integer = 0 to ubound(die)

			if die(d).curFace = f then

				faceCnt(f)+=1

				fullSum+=die(d).curFace

			end if

		next

	next


	'small/large straight check
	for f1 as integer = 1 to 3

		dim as integer conCount

		if faceCnt(f1) > 0 then

			for f2 as integer = f1+1 to ubound(faceCnt)

				if faceCnt(f2) > 0 then

					conCount+=1

				else

					exit for

				end if

			next

			if conCount>2 then

				isSMallStraight = true

			end if

			if conCount>3 then

				isLargeStraight = true

			end if

		end if

	next


	for f as integer = 1 to 6

		'triple
		if faceCnt(f) >= 3 then

			isTriple = true

		end if

		'quad
		if faceCnt(f) >= 4 then

			isQuad = true

		end if

		'yahtzee...
		if faceCnt(f) = 5 then

			isYahtzee = true

		end if


		'full house...
		if faceCnt(f) = 3 then

			for f1 as integer = 1 to 6

				if f1<>f then

					if faceCnt(f1) = 2 then

						isFullHouse = true

					end if

				end if

			next

		end if

	next


	'reset the temporary scorecard
	for l as integer = 0 to 5

		tempCard.upper(l).value = 0

	next

	for l as integer = 0 to 6

		tempCard.lower(l).value = 0

	next


	dim as integer total

	for d as integer = 0 to ubound(die)

		total+=die(d).curFace

	next


	tempCard.lower(6).value = total


	for f as integer = 1 to 6
		tempCard.upper(f-1).value = faceCnt(f)*f
	next



	if isTriple then

		print #debugout, "Triple!"

		for f as integer = 1 to 6
			if faceCnt(f) >= 3 then

				tempCard.lower(0).value = total

				exit for

			end if
		next

	end if


	if isQuad then

		print #debugout, "Quad!"

		for f as integer = 1 to 6
			if faceCnt(f) >= 4 then

				tempCard.lower(1).value = total

				exit for

			end if
		next

	end if


	if isYahtzee then

		print #debugout, "Yahtzee!"

		'if plScore(curPlayer).yahtzeeCnt
		tempCard.lower(5).value = 50

	end if

	if isFullHouse then

		print #debugout, "Full House!"

		tempCard.lower(2).value = 25

	end if

	if isSmallStraight then

		print #debugout, "Small Straight!"

		tempCard.lower(3).value = 30

	end if

	if isLargeStraight then

		print #debugout, "Large Straight!"

		tempCard.lower(4).value = 40

	end if

	tempCard.hasClicked = false

	update_scorecard()

end sub


sub game_struct.update_scorecard()

	dim as string tString = plScore(curPlayer).name + "'s Scorecard"

	dim as window_struct ptr myWindow = myGui.GetWindowByTitle( tString )

	if myWindow = 0 then

		print #debugout, "Valid window for " + tString + " not found."

		exit sub

	end if

	dim as integer upScore

	dim as integer stillAlive = false

	for l as integer = 0 to ubound(tempCard.upper)

		if l<6 then

			if not plScore(curPlayer).upper(l).locked then

				myWindow->button(tempCard.upper(l).bIndex).caption = str(tempCard.upper(l).value) + " " + chr(26)

				stillAlive = true

			else

				myWindow->button(tempCard.upper(l).bIndex).caption = "Used"

			end if

			upScore+=plScore(curPlayer).upper(l).value

		end if

	next

	if upScore>62 then

		myWindow->label(tempCard.upper(6).lIndex).caption = "35"

		upScore+=35

	end if

	myWindow->label(tempCard.upper(7).lIndex).caption = str(upScore)


	dim as integer lowScore

	for l as integer = 0 to ubound(tempCard.lower)

		if l<7 then

			if not plScore(curPlayer).lower(l).locked then

				myWindow->button(tempCard.lower(l).bIndex).caption = str(tempCard.lower(l).value) + " " + chr(26)

				stillAlive = true

			else

				myWindow->button(tempCard.lower(l).bIndex).caption = "Used"

			end if

			lowScore+=plScore(curPlayer).lower(l).value

		end if

	next

	dim as integer yatScr = plScore(curPlayer).yahtzeeCnt*150

	lowScore+=yatScr

	myWindow->label(tempCard.lower(7).lIndex).caption = str(yatScr)
	myWindow->label(tempCard.lower(8).lIndex).caption = str(lowScore)

	plScore(curPlayer).isAlive = stillAlive

	if not stillAlive then

		print #debugout, "The game is over!"

	end if

end sub


function game_struct.get_die_face( byref die as die_struct ) as integer

	static as matrix tMatrix

	NewtonBodyGetMatrix( die.nBody, tMatrix )

	if tMatrix.up.y > .9 then return 1
	if tMatrix.right.y < -.9 then return 2
	if tMatrix.forward.y > .9 then return 3
	if tMatrix.right.y > .9 then return 4
	if tMatrix.forward.y < -.9 then return 5
	if tMatrix.up.y < -.9 then return 6

end function


function game_struct.dice_stopped() as integer

	for d as integer = 0 to ubound(die)

		if NewtonBodyGetSleepingState( die(d).nBody ) = 1 then

			if not die(d).isLocked then

				return false

			end if

		end if

	next

	return true

end function


sub game_struct.roll_dice()

	static as matrix idMatrix

	for i as integer = 0 to ubound(die)

		if not die(i).isLocked then

			idMatrix.loadIdentity()
			idMatrix.Translate( -3+i*1.5, 6f, 10f)
			idMatrix.Rotate(rnd*360,rnd*360,rnd*360)

			dim as vec3f fVector = vec3f((i-2f)*-5f, -10f*rnd*2f, -20f)

			dim as vec3f posit = idMatrix.position

			dim as vec3f rOmega = vec3f(-5f+rnd*10f, -1f+rnd*2f, -5f+rnd*10f)

			NewtonWorldUnfreezeBody( wWorld, die(i).nBody )
			NewtonBodySetMatrix( die(i).nBody, idMatrix )
			NewtonBodySetOmega( die(i).nBody, @rOmega.x )
			NewtonAddBodyImpulse( die(i).nBody, @fVector.x, @posit.x )

		end if

	next

end sub

function game_struct.get_free_holder() as integer

	for i as integer = 0 to ubound(holder)

		if holder(i).full = false then

			return i

		end if

	next

	return 0

end function


sub game_struct.hold_all_dice()

	for d as integer = 0 to ubound(die)

		if not die(d).isLocked then

			add_die_to_holder(d)

		end if

	next

end sub


sub game_struct.clear_holders()

	for d as integer = 0 to ubound(die)

		die(d).isLocked = false

	next

	for h as integer = 0 to ubound(holder)

		holder(h).full = false

	next

end sub


sub game_struct.add_die_to_holder( byref id as integer )

	die(id).isLocked = true

	dim as matrix nMatrix

	NewtonBodyGetMatrix( die(id).nBody, nMatrix )

	die(id).oldMatrix = nMatrix

	dim as integer holderId = get_free_holder()

	nMatrix.ZeroUp( holder(holderID).holdPos )

	holder(holderID).full = true

	die(id).holderId = holderId

	die(id).curFace = get_die_face( die(id) )

	NewtonWorldUnfreezeBody( wWorld, die(id).nBody )
	NewtonBodySetMatrix( die(id).nBody, nMatrix )

end sub


sub game_struct.remove_die_from_holder( byref id as integer )

	die(id).isLocked = false

	NewtonWorldUnfreezeBody( wWorld, die(id).nBody )
	NewtonBodySetMatrix( die(id).nBody, die(id).oldMatrix )

	holder(die(id).holderId).full = false

end sub


function Dice_World_Contact_Begin cdecl( byval Material as NewtonMaterial ptr, byval rBody1 as NewtonBody ptr, byval rBody2 as NewtonBody ptr ) as integer

	return 1

end function


function Dice_World_Contact_Process cdecl( byval Material as NewtonMaterial ptr, byval Contact as NewtonContact ptr ) as integer

	static as integer first_run
	static as uinteger SURF_SIDES
	static as uinteger SURF_BOTTOM
	static as uinteger SURF_TABLE

	'pseudo-compile the hash lookup so that we're not searching strings at runtime...
	if not first_run then

		dim as string temp

		temp = "Board_Insides"
		SURF_SIDES = h_table->lookup( strptr(temp) )->id

		temp = "Board_Bottom"
		SURF_BOTTOM = h_table->lookup( strptr(temp) )->id

		temp = "Table"
		SURF_BOTTOM = h_table->lookup( strptr(temp) )->id

		first_run = true

	end if

	dim as integer Contact_Surface
	dim as dFloat ConTang1, ConTang2, Mag, NormSpeed1, NormSpeed2
	dim as nmathf.vec3f contact_position, contact_normal

	dim as entity_Struct ptr die = NewtonMaterialGetMaterialPairUserData( Material )
	contact_surface = NewtonMaterialGetContactFaceAttribute( Material )

	NewtonMaterialGetContactPositionAndNormal( Material, @contact_position.x, @contact_normal.x )


	'die->contact_string = *h_table->lookup( contact_surface )->s
	'dim as integer contact_ID = h_table->lookup( contact_surface )->id

	'print #debugout, contact_surface, contact_ID

	select case contact_surface

		case SURF_SIDES

			NewtonMaterialSetContactStaticFrictionCoef( material, 0.01, 0 )
			NewtonMaterialSetContactStaticFrictionCoef( material, 0.05, 1 )
			NewtonMaterialSetContactKineticFrictionCoef( material, 0.01, 0 )
			NewtonMaterialSetContactKineticFrictionCoef( material, 0.05, 1 )

			NewtonMaterialSetContactSoftness( material, .15 )
			NewtonMaterialSetContactElasticity( material, .5 )

		case SURF_BOTTOM

			NewtonMaterialSetContactStaticFrictionCoef( material, 1.95, 0 )
			NewtonMaterialSetContactStaticFrictionCoef( material, 1.91, 1 )
			NewtonMaterialSetContactKineticFrictionCoef( material, 0.95, 0 )
			NewtonMaterialSetContactKineticFrictionCoef( material, 0.9, 1 )
			NewtonMaterialSetContactSoftness( material, .001 )
			NewtonMaterialSetContactElasticity( material, .05 )

		case SURF_TABLE

			NewtonMaterialSetContactStaticFrictionCoef( material, 1.95, 0 )
			NewtonMaterialSetContactStaticFrictionCoef( material, 1.9, 1 )
			NewtonMaterialSetContactKineticFrictionCoef( material, 1.95, 0 )
			NewtonMaterialSetContactKineticFrictionCoef( material, 1.9, 1 )
			NewtonMaterialSetContactSoftness( material, 50 )
			NewtonMaterialSetContactElasticity( material, .0001 )


	end select




	return 1

end function

sub Dice_World_Contact_End cdecl(byval Material as NewtonMaterial ptr)

end sub