#include once "audio.bi"
'#include once "physics.bi"

namespace nAudio

	function LoadSound( byref filename as string, sample() as sample_struct, byref index as integer, byref looping as integer ) as sample_struct

		dim as string tstring
		dim as integer lastslash

		for i as integer = 1 to len(filename)
			if mid(filename,i,1) = "/" then
				lastslash=i
			end if
		next

		tstring = mid(filename, lastslash + 1 )

		if ubound(sample)>-1 then
			for s as integer = 0 to ubound( sample )
				if sample(s).name = tstring then
					return sample(s)
				end if
			next
		end if

		redim preserve sample(ubound(sample)+1)

		'dim as sample_struct tSample

		dim as integer flags = FSOUND_HW3D

		if looping<>0 then
			flags or = FSOUND_LOOP_NORMAL
		end if

		'tSample.name = tString
		'tSample.wav = FSOUND_Sample_Load( index, filename,  flags, 0, 0 )
		sample(ubound(sample)).name = tString 
		sample(ubound(sample)).wav = FSOUND_Sample_Load( index, filename,  flags, 0, 0 )
		
		'FSOUND_Sample_Load( FSOUND_FREE, "res/audio/sfx/harley.wav",  FSOUND_HW3D or FSOUND_LOOP_NORMAL, 0, 0)
		
		return sample(ubound(sample))

	end function

end namespace
