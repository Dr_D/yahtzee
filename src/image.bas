#include once "image.bi"

nImage.BLACK = type( 0, 0, 0, 1 )
nImage.DGRAY = type( .25, .25, .25, 1 )
nImage.MGRAY  = type( .5, .5, .5, 1 )
nImage.LGRAY  = type ( .75, .75, .75, 1 )

namespace nimage
		

	function load_texture( byref filename as string, Texture() as Texture_Struct, byref force_clamp as Integer, byref searchDir As String = "res/texture/" ) as integer

		dim as string tstring
		dim as integer lastslash

		for i as integer = 1 to len(filename)
			if mid(filename,i,1) = "/" then
				lastslash=i
			end if
		next

		tstring = mid(filename, lastslash + 1 )

		if ubound(texture)>-1 then
			for i as integer = 1 to ubound( Texture )
				if Texture(i).name = tstring then
					return i
				end if
			next
		end if
		
		redim preserve Texture(ubound(texture)+1)
		
		Texture(ubound(texture)).name = tstring
		glgentextures( 1, @Texture(ubound(texture)).img )
		glenable( GL_TEXTURE_2D )
		glbindtexture( GL_TEXTURE_2D, Texture(ubound(texture)).img )

		dim SprWidth as integer
		dim SprHeight as Integer
		dim as any ptr temp

		if len(searchDir)>0 Then
			png_dimensions( searchDir & tstring, SprWidth, SprHeight )
			temp = png_load( searchDir & tstring, PNG_TARGET_OPENGL )
		end if

		glteximage2d( GL_TEXTURE_2D, 0, GL_RGBA, SprWidth, SprHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, temp )

		glubuild2dmipmaps( GL_TEXTURE_2D, GL_RGBA, SprWidth, SprHeight, GL_RGBA, GL_UNSIGNED_BYTE, temp )

		if force_clamp then
			gltexparameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE )
			gltexparameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE )
		else
			gltexparameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT )
			gltexparameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT )
		end if

		gltexparameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR )
		gltexparameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR )
		gltexenvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)


		if use_anisotropic <> 0 then
			dim as glFloat Largest_Texture_Filter
			glgetfloatv( GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, @Largest_Texture_Filter )
			gltexparameterf( GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, Largest_Texture_Filter )
		end if

		png_destroy( temp )
		

		return ubound(texture)

	end function

end namespace 'nimage
