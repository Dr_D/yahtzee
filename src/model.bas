#include once "model.bi"

extern h_table	as nhash.ht_t ptr

using nMathf

namespace nmodel


	function tri_box( byref tri as nmathf.vec3f ptr ) as box_struct

		dim as box_struct bbox

		var low  = vec3f(2^32-1,2^32-1,2^32-1)
		var high = vec3f(-(2^32-1), -(2^32-1), -(2^32-1) )

		for v as integer = 0 to 2

			if tri[v].x < low.x then
				low.x = tri[v].x
			end if

			if tri[v].y < low.y then
				low.y = tri[v].y
			end if

			if tri[v].z < low.z then
				low.z = tri[v].z
			end if

			if tri[v].x > high.x then
				high.x = tri[v].x
			end if

			if tri[v].y > high.y then
				high.y = tri[v].y
			end if

			if tri[v].z > high.z then
				high.z = tri[v].z
			end if

		next

		bbox.centroid.x = (high.x+low.x) / 2.0
		bbox.centroid.y = (high.y+low.y) / 2.0
		bbox.centroid.z = (high.z+low.z) / 2.0

		bbox.size.x = (high.x-low.x) / 2.0
		bbox.size.y = (high.y-low.y) / 2.0
		bbox.size.z = (high.z-low.z) / 2.0

		if bbox.size.x = 0 then
			bbox.size.x = 1
		end if

		if bbox.size.y = 0 then
			bbox.size.y = 1
		end if

		if bbox.size.z = 0 then
			bbox.size.z = 1
		end if

		return bbox

	end function


	function GetModelByName( byref tString as string, model() as model_struct ) as model_struct ptr

		for m as integer = 0 to ubound(model)
			if ltrim(rtrim(model(m).mName)) = tString then
				return @model(m)
			end if
		next

		return 0

	end function

	sub center_model( byref model as model_struct ptr )

		dim as nmathf.vec3f centroid

		for v as integer = 0 to ubound(model->vertices)
			centroid+=model->vertices(v)
		next

		centroid/=csng(ubound(model->vertices))

		for v as integer = 0 to ubound(model->vertices)
			model->vertices(v)-=centroid
		next

	end sub

	sub resort_model_surfaces( byref model as model_struct ptr )

		for s1 as integer = 0 to ubound(model->surfaces)
			for s2 as integer = s1 to ubound(model->surfaces)
				if s1<>s2 then
					if Model->surfaces(s1).RGBAlpha.a <= Model->surfaces(s2).RGBAlpha.a then
						swap Model->surfaces(s1), Model->surfaces(s2)
					end if
				end if
			next
		next

		for s1 as integer = 0 to ubound(model->surfaces)
			#ifdef _debugout
			print #debugout, Model->surfaces(s1).surfName & " " & Model->surfaces(s1).RGBAlpha.a
			#endif
		next

	end sub


	function find_surface_center( byref model as model_struct ptr, byref surfname as string ) as nmathf.vec3f

		dim as single hits
		dim as nmathf.vec3f center

		for s as integer = 0 to ubound(model->surfaces)

			if ltrim(rtrim(model->surfaces(s).surfName)) = surfname then

				for t as integer = Model->surfaces(s).Start_ID to Model->surfaces(s).End_ID

					for p as integer = 0 to 2
						center+=Model->vertices(Model->triangles(t).point_id(p))
						hits+=1
					next

				next

			end if

		next

		center/=hits

		return center

	end function


	function material_sort_callback cdecl ( byval elm1 as any ptr, byval elm2 as any ptr ) as integer

		return sgn( (cptr(triangle_struct ptr, elm1)->Group) - (cptr(triangle_struct ptr, elm2)->Group) )

	end function


	sub material_sort( byref model as model_struct ptr )

		qsort( @model->triangles(0), ubound(model->triangles)+1, sizeof(triangle_struct), cast( any ptr, @material_sort_callback()) )

		if ubound(model->surfaces) = 0 then
			model->surfaces(0).start_ID = 0
			model->surfaces(0).end_ID = ubound(model->triangles)
			model->surfaces(0).group = model->triangles(0).group
			exit sub
		end if


		dim as integer groupCur, groupCnt = 0
		model->surfaces(0).start_ID = 0
		model->Surfaces(0).group = model->triangles(0).group
		groupCur = model->triangles(0).group


		for i as integer = 0 to ubound(model->triangles)

			if model->triangles(i).group <> groupCur then

				model->surfaces(groupCnt).end_ID = i - 1
				model->surfaces(groupCnt).group = model->triangles(i-1).Group
				groupCnt += 1

				if groupCnt<=ubound(model->surfaces) then

					model->surfaces(groupCnt).start_ID = i
					model->surfaces(groupCnt).group = model->triangles(i).group
					groupCur = model->triangles(i).group

				end if

			end if

		next

		model->surfaces(ubound(model->surfaces)).end_ID = ubound(model->triangles)
		model->surfaces(ubound(model->surfaces)).group = model->triangles(ubound(model->triangles)).group

	end sub


	function lwobject_2model3d( byref model as model_struct ptr, byval lwObject as lwObject ptr, texture() as nimage.texture_struct, byref force_clamp as integer ) as integer

		dim as lwLayer ptr layer = @lwObject->layer[0]

		dim as integer bone_count, tri_count

		'dim as bone_struct bone(any)

		dim as lwPoint ptr pt
		dim as lwPolygon ptr pol
		dim as lwVMap ptr vmap
		dim as byte ptr tag
		dim as integer n

		dim as nmathf.matrix rmatrix
		rmatrix.LoadIdentity()
		rmatrix.Scale(1f,1f,-1f)

		redim Model->vertices(layer->point.count)
		redim Model->tvertices(layer->point.count)
		redim Model->vnormals(layer->point.count)

		#ifdef _debugout
			print #debugout, " "
			print #debugout, "max vertices = " & ubound(model->vertices)
		#endif


		'store the object's tag list in an array
		print #debugout, " "
		print #debugout, "Object taglist... "
		dim as string taglist()
		dim as integer tcnt
		
		for c as integer = 0 to lwObject->taglist.count - 1
			
			redim preserve taglist(tcnt)
			
			taglist(tcnt) = *lwObject->taglist.tag[c]
			
			print #debugout, taglist(tcnt)
			 
			tcnt+=1
			
		next

		
		dim as string clip_names()
		dim as lwClip ptr clipNode = lwObject->clip
		
		print #debugout, " "
		print #debugout, "Object clip nodes... "
		
		do while clipNode
			
			dim as integer nClips = ubound(clip_names) + 1 
			
			redim preserve clip_names(nClips)
			
			clip_names(nClips) = *clipnode->still.name
			
			print #debugout, clip_names(nClips)
			
			clipNode = clipNode->next
			
		loop
		

		dim as integer max_triangles = 0
		dim as integer max_bones = 0

		for i as integer = 0 to layer->polygon.count - 1

			dim as lwPolygon ptr pol = @layer->polygon.pol[ i ]

			select case pol->type

				case ID_FACE
					max_triangles += 1

				case ID_BONE
					max_bones+=1

			end select

		next

		redim Model->triangles(max_triangles-1)

		redim Model->shadow.Volume( max_triangles*5)

		redim Model->shadow.Volume_Cap( max_triangles*5)

		#ifdef _debugout
		print #debugout, "max triangles = " & max_triangles
		print #debugout, "max bones = " & max_bones
		#endif

		if max_bones>0 then

			redim model->mBone(max_bones-1)

			for b as integer = 0 to ubound(model->mBone)

				'model->bone(b) = callocate( sizeof(bone_struct) )

				with model->mBone(b)
					redim .weight(ubound(model->vertices))
				end with

			next

		end if

		dim as lwSurface ptr surf = lwObject->surf
		dim as integer max_surfaces = 0


		do while surf
			if *surf->name <> "Bone" then
				max_surfaces += 1
			end if
			surf = surf->next
		loop

		dim as surface_struct allsurfs( max_surfaces-1 )
		dim as group_struct group( max_surfaces-1 )

		redim model->surfaces(max_surfaces-1)

		#ifdef _debugout
			print #debugout, " "
			print #debugout, "max surfaces = " & max_surfaces
		#endif

		surf = lwObject->surf
		dim as integer gcnt
		do while surf
			if *surf->name <> "Bone" then
				group(gcnt).gName = *surf->name
				group(gcnt).group = gcnt
				group(gcnt).id1 = nimage.load_texture( clip_names(surf->color.tex->imap.cindex-1), Texture(), force_clamp )

				allsurfs(gcnt).RGBAlpha.R = surf->color._rgb(0)
				allsurfs(gcnt).RGBAlpha.G = surf->color._rgb(1)
				allsurfs(gcnt).RGBAlpha.B = surf->color._rgb(2)
				allsurfs(gcnt).RGBAlpha.A = 1.0-surf->Transparency.val.val

				allsurfs(gcnt).Ambient = type(.5,.5,.5,1.0)

				allsurfs(gcnt).Diffuse.R = surf->Diffuse.val
				allsurfs(gcnt).Diffuse.G = surf->Diffuse.val
				allsurfs(gcnt).Diffuse.B = surf->Diffuse.val
				allsurfs(gcnt).Diffuse.A = surf->Diffuse.val

				allsurfs(gcnt).Specular.R = surf->Specularity.val
				allsurfs(gcnt).Specular.G = surf->Specularity.val
				allsurfs(gcnt).Specular.B = surf->Specularity.val
				allsurfs(gcnt).Specular.A = surf->Specularity.val

				allsurfs(gcnt).shininess = surf->Glossiness.val*128

				if surf->bump.tex > 0 then
					group(gcnt).bump_id = nimage.load_texture( clip_names(surf->bump.tex->imap.cindex-1), Texture(), force_clamp )
				end if

				dim as integer gcntlr
				dim as lwTexture ptr thistex = surf->color.tex
				do while thistex

					select case as const gcntlr
						case 1
							group(gcnt).id2 = nimage.load_texture( clip_names(thistex->imap.cindex-1), Texture(), force_clamp )
						case 2
							group(gcnt).id3 = nimage.load_texture( clip_names(thistex->imap.cindex-1), Texture(), force_clamp )
						case 3
							group(gcnt).id4 = nimage.load_texture( clip_names(thistex->imap.cindex-1), Texture(), force_clamp )
					end select

					gcntlr+=1
					thistex = thistex->next
				loop

				gcnt+=1

			end if
			surf = surf->next
		loop

		'initialize the bones and see how many morph targets there are
		bone_count = 0
		for i as integer = 0 to layer->polygon.count - 1

			pol = @layer->polygon.pol[ i ]

			select case pol->type
				case ID_FACE
				case ID_BONE

					if ubound(model->mBone)>-1 then
						with model->mBone(bone_count)

							.m.LoadIdentity
							.position(0) = nmathf.vec3f(layer->point.pt[pol->v[0].index].pos(0), layer->point.pt[pol->v[0].index].pos(1), layer->point.pt[pol->v[0].index].pos(2))*rMatrix
							.position(1) = nmathf.vec3f(layer->point.pt[pol->v[1].index].pos(0), layer->point.pt[pol->v[1].index].pos(1), layer->point.pt[pol->v[1].index].pos(2))*rMatrix
							.m.Translate( .position(0) )
							.pindex(0) = pol->v[0].index
							.pindex(1) = pol->v[1].index

							.id = taglist(pol->part)

						end with

						#ifdef _debugout
						print #debugout, model->mBone(bone_count).id
						#endif

						bone_count += 1
					end if

			end select

		next
		
		
		dim as lwPointList ptr mPoint = @layer->point
		
		print #debugout, " "
		print #debugout, "Check for bones..."
		
		for p as integer = 0  to mPoint->count-1
			
			dim as lwPoint ptr pt = @mPoint->pt[p]
			
			if pt->pol then
				
				dim as lwPolygon ptr pol = @layer->polygon.pol[*pt->pol]
				
				'print #debugout, LWID_2STRING(layer->polygon.pol[*pt->pol].type)
				
				
				if pol->type = ID_BONE then
					
					'print #debugout, *pol->surf->srcname
					
					print #debugout, pol->part, pt->nPols
					
				end if
				
			end if
			
		next
		
		'=====================================================================================================================
		print #debugout, " "
		print #debugout, "VMAP COUNT " & layer->nVmaps

		dim as lwVMap ptr mvMap = layer->vMap

		do while mvMap

			print #debugout, LWID_2STRING( mvMap->type ) & " " & *mvMap->name & " vertex count =" & mvMap->nVerts

			if mvMap->type = ID_MORF then

				dim as integer curMorf = ubound(model->morphs) + 1
				redim preserve model->morphs( curMorf )
				model->morphs( curMorf ).id = *mvMap->name

				with model->morphs(curMorf)

					redim .vec(mvMap->nVerts)
					redim .index(mvMap->nVerts)

					for v as integer = 0 to mvMap->nVerts-1
						
						.vec(v) = vec3f(mvMap->val[ v ][ 0 ], mvMap->val[ v ][ 1 ], mvMap->val[ v ][ 2 ])
						
						.index(v) = mvMap->vIndex[v]

					next

				end with

			end if
			
			mvMap = mvMap->next
			
		loop
		

		tri_count = 0
		'the main loop
		for i as integer = 0 to layer->polygon.count - 1
			pol = @layer->polygon.pol[i]

			select case pol->type
				case ID_FACE
					for k as integer = 0 to pol->nverts - 1

						n = pol->v[ k ].index
						pt = @layer->point.pt[ n ]

						if n>ubound(model->vertices) then 'tri_count is checked at the end of the loop... before it loops back to here
							#ifdef _debugout
							print #debugout, "VERTEX INDEX EXCEEDED!!!"
							#endif
						end if
						Model->triangles(tri_count).point_ID(k) = n
						Model->triangles(tri_count).vNormals(k) = nmathf.vec3f(pol->v[k].norm(0), pol->v[k].norm(1), pol->v[k].norm(2)) *rMatrix
						Model->triangles(tri_count).vNormals(k).Normalize
						Model->vertices(n) = nmathf.vec3f( layer->point.pt[n].pos(0), layer->point.pt[n].pos(1), layer->point.pt[n].pos(2) )*rMatrix


						' vmaps for this vertex
						for j as integer = 0 to pt->nvmaps - 1
							vmap = pt->vm[ j ].vmap
							if ( vmap->perpoly ) then continue for
							n = pt->vm[ j ].index

							if vmap->type = ID_TXUV then

								if *pol->surf->color.tex->imap.vmap_name = *vmap->name then
									Model->triangles(tri_count).t1coord(k) = nmathf.vec2f( vmap->val[ n ][ 0 ], vmap->val[ n ][ 1 ] )
								end if
								dim as integer gcntlr
								dim as lwTexture ptr thistex = pol->surf->color.tex
								do while thistex

									if *thistex->imap.vmap_name = *vmap->name then
										select case as const gcntlr
											case 1
												Model->triangles(tri_count).t2coord(k) = nmathf.vec2f( vmap->val[ n ][ 0 ], vmap->val[ n ][ 1 ] )
											case 2
												Model->triangles(tri_count).t3coord(k) = nmathf.vec2f( vmap->val[ n ][ 0 ], vmap->val[ n ][ 1 ] )
											case 3
												Model->triangles(tri_count).t4coord(k) = nmathf.vec2f( vmap->val[ n ][ 0 ], vmap->val[ n ][ 1 ] )
										end select

									end if

									gcntlr+=1
									thistex = thistex->next
								loop

								if pol->surf->bump.tex>0 then
									if *pol->surf->bump.tex->imap.vmap_name = *vmap->name then
										Model->triangles(tri_count).bcoord(k) = nmathf.vec2f( vmap->val[ n ][ 0 ], vmap->val[ n ][ 1 ] )
									end if
								end if

							end if

							if vmap->type = ID_WGHT then
								'check the bone name against the name of this vmap
								'if it matches, we know this is the correct weight for this bone.
								if ubound(model->mBone)>0 then

									for b as integer = 0 to ubound(model->mBone)
										if model->mBone(b).id = *vmap->name then
											model->mBone(b).weight(n) = vmap->val[ n ][ 0 ]
										end if
									next

									'print #debugout, "weightmap has " & vmap->nverts & " vertex indices"

									'nverts as integer
									'perpoly as integer
									'vindex as integer ptr
									'pindex as integer ptr

								end if

							end if

							'if vmap->type = ID_MORF then
							'	print #debugout, *vmap->name & " found morph target under vmap..."
							'end if


						next j

						' vmads for this vertex
						for j as integer = 0 to pol->v[ k ].nvmaps - 1
							vmap = pol->v[ k ].vm[ j ].vmap
							n = pol->v[ k ].vm[ j ].index

							if vmap->type = ID_TXUV then
								if *pol->surf->color.tex->imap.vmap_name = *vmap->name then
									Model->triangles(tri_count).t1coord(k) = nmathf.vec2f( vmap->val[ n ][ 0 ], vmap->val[ n ][ 1 ] )
								end if
								dim as integer gcntlr
								dim as lwTexture ptr thistex = pol->surf->color.tex

								do while thistex

									if *thistex->imap.vmap_name = *vmap->name then

										select case as const gcntlr
											case 1
												Model->triangles(tri_count).t2coord(k) = nmathf.vec2f( vmap->val[ n ][ 0 ], vmap->val[ n ][ 1 ] )
											case 2
												Model->triangles(tri_count).t3coord(k) = nmathf.vec2f( vmap->val[ n ][ 0 ], vmap->val[ n ][ 1 ] )
											case 3
												Model->triangles(tri_count).t4coord(k) = nmathf.vec2f( vmap->val[ n ][ 0 ], vmap->val[ n ][ 1 ] )
										end select

									end if
									gcntlr+=1
									thistex = thistex->next
								loop

								if pol->surf->bump.tex>0 then
									if *pol->surf->bump.tex->imap.vmap_name = *vmap->name then
										Model->triangles(tri_count).bcoord(k) = nmathf.vec2f( vmap->val[ n ][ 0 ], vmap->val[ n ][ 1 ] )
									end if
								end if
							end if

						next j

					next k

					for g as integer = 0 to ubound(group)
						if *pol->surf->name = group(g).gName then
							Model->triangles(tri_count).Group = group(g).group
						end if
					next

					Model->triangles(tri_count).string_id = *pol->surf->name
					if tri_count>ubound(model->triangles) then
						#ifdef _debugout
						print #debugout, "triangle index exceeded!!!"
						#endif
					end if

					tri_count += 1


				case ID_BONE

					#if 0

					'print #debugout, " bone vertex count " & pol->nVerts
					'for k as integer = 0 to pol->nverts - 1

					'	n = pol->v[ k ].index
					'	'pt = @layer->point.pt[ n ]
					'
					'	'print #debugout, pt->pos(0)
					'
					'	print #debugout, "Number of vertex maps in this bone " & pol->v->nvMaps
					'
					'	'index as integer
					'	'norm(0 to 3-1) as single
					'	'nvmaps as integer
					'	'vm as lwVMapPt ptr
					'
					'next

					#endif

			end select

		next i


		for t as integer = 0 to ubound(model->triangles)

			swap Model->triangles(t).point_id(0), Model->triangles(t).point_id(2)
			swap Model->triangles(t).t1coord(0), Model->triangles(t).t1coord(2)
			swap Model->triangles(t).t2coord(0), Model->triangles(t).t2coord(2)
			swap Model->triangles(t).t3coord(0), Model->triangles(t).t3coord(2)
			swap Model->triangles(t).t4coord(0), Model->triangles(t).t4coord(2)
			swap Model->triangles(t).bcoord(0), Model->triangles(t).bcoord(2)
			swap Model->triangles(t).vnormals(0), Model->triangles(t).vnormals(2)

			h_table->add( Model->triangles(t).string_id, null )
		next

		material_sort( Model )

		for s as integer = 0 to ubound(model->surfaces)

			Model->Surfaces(s).Is_Bump = 0

			if group(s).bump_id > 0 then
				Model->Surfaces(s).Is_Bump = 1
				Model->Surfaces(s).Texb = texture(group(s).bump_id).img
			end if

			Model->Surfaces(s).Is_Multi = 0
			Model->Surfaces(s).Tex1 = texture(group(s).id1).img

			if group(s).id2 > 0 then
				Model->Surfaces(s).Is_Multi = 1
				Model->Surfaces(s).Tex2 = texture(group(s).id2).img
				Model->Surfaces(s).Tex3 = texture(group(s).id3).img
				Model->Surfaces(s).Tex4 = texture(group(s).id4).img
			end if

			Model->Surfaces(s).RGBAlpha  = allsurfs(s).RGBAlpha
			Model->Surfaces(s).Ambient   = allsurfs(s).Ambient
			Model->Surfaces(s).Diffuse   = allsurfs(s).Diffuse
			Model->Surfaces(s).Specular  = allsurfs(s).Specular
			Model->Surfaces(s).Shininess = allsurfs(s).Shininess

			for t as integer = Model->surfaces(s).Start_ID to Model->surfaces(s).End_ID

				Model->surfaces(s).surfname = Model->triangles(t).string_id

			next

		next

		if ubound(model->mBone)>0 then
			bone_sort( model, model->mBone() )
		end if
				
		memcpy( @model->tvertices(0).x, @model->vertices(0).x, sizeof(vec3f)*ubound(model->vertices) )

		return 0

	end function


	sub bone_sort( byref model as model_struct ptr, bone() as bone_struct )

		print #debugout, " "

		'sort out the hierarchy of the bones, since lightwave doesn't store it in an organized fashion
		
		for b1 as integer = 0 to ubound(bone)
			
			bone(b1).isRoot = true
			
		next
		
		for b1 as integer = 0 to ubound(bone)
			
			dim as vec3f p1 = bone(b1).position(1)

			for b2 as integer = 0 to ubound(bone)

				dim as vec3f p2 = bone(b2).position(0)

				if p1 = p2 then
					
					bone(b2).isRoot = false
					
				end if

			next
			
			if bone(b1).isRoot then
				
				dim as integer bCount = ubound(model->bone)+1
				redim preserve model->bone(bCount)
				model->bone(bCount) = @bone(b1)
				
			end if
			
		next
		
		
		if ubound(model->bone) > -1 then
		
			for b as integer = 0 to ubound(model->bone)
			
				bone_add_child( model->Bone(b), model->mBone() )
				
			next
		
		end if

	end sub


	sub bone_add_child( byref tBone as bone_struct ptr, bone() as bone_struct )
		
		dim as vec3f p1 = tBone->position(1)
		
		for b as integer = 0 to ubound(bone)
			
			dim as vec3f p2 = bone(b).position(0)
			
			if p1 = p2 then
				
				with *tBone
				
				dim as integer bCount = ubound(.child) + 1
				
					redim preserve .child(bCount)
				
					.child(bCount) = @bone(b)
					.child(bCount)->parent = tBone
					
					bone_add_child( .child(bCount), bone() )
					
				end with
					
			end if
			
		next

	end sub


	function load_model3d( byref filename as string, model() as model_struct, texture() as nimage.texture_struct, byref force_clamp as integer, byref static_shadows as integer ) as integer

		dim as lwObject ptr lwObject
		dim as uinteger ptr ErrID
		dim as integer ptr ErrPos

		if instr( filename, "skybox") then
			force_clamp = true
		end if

		if filename="" then
			#ifdef _debugout
			print #debugout, "No model specified"
			#endif

			return 0

		elseif lcase(right(filename,4)) = ".lwo" then

			lwObject = lwGetObject( filename, ErrID, ErrPos )

			if lwObject = 0 then
				#ifdef _debugout
				print #debugout, "There was an error of something..."
				'" & LWID_2String(*ErrID) & " at " & ErrPos
				#endif
				return 0
			else

				#ifdef _debugout
				print #debugout, "__________________________________"
				print #debugout, "loading " & filename
				#endif

				'max_models += 1'
				'model = reallocate( model, max_models * sizeof(model_struct) )
				'memset( @model[max_models - 1], 0, sizeof(model_struct) )
				redim preserve model(ubound(model)+1)
				'clear( @model(ubound(model)), 0, sizeof(model_struct) )

				lwobject_2model3d( @model(ubound(model)), lwObject, Texture(), force_clamp )
				lwFreeObject( lwObject )

				dim as integer rev = instrrev( filename, "/" )

				model(ubound(model)).mName = mid(filename,rev+1)
				calc_planes( @model(ubound(model)) )
				calc_normals( @model(ubound(model)) )
				'set_poly_neighbors( @model(ubound(model)) )
				resort_model_surfaces( @model(ubound(model)) )

				with model(ubound(model))

					'.dList = glGenLists(1)
					'glNewList( .dList, GL_COMPILE )
					'render_model( @model(ubound(model)), texture() )
					'glEndList()


					'for s as integer = 0 to ubound(.surfaces)'.max_surfaces-1
					'	.surfaces(s).dList = glGenLists(1)
					'	glNewList( .surfaces(s).dList, GL_COMPILE )
					'	render_model_surface( @model(ubound(model)), texture(), s )
					'	glEndList()
					'next

				end with

				#ifdef _debugout
				print #debugout, filename & " loaded"
				print #debugout, "__________________________________"
				#endif
			end if


		elseif lcase(right(filename,4)) = ".fo2" then

			#ifdef _debugout
			print #debugout, "__________________________________"
			print #debugout, "loading " & filename
			#endif

			'max_models += 1
			'model = reallocate( model, max_models * sizeof(model_struct) )
			'memset( @model[max_models - 1], 0, sizeof(model_struct) )
			redim preserve model(ubound(model)+1)
			'clear( @model(ubound(model)), 0, sizeof(model_struct) )

			'load_Binary( filename, model(ubound(model)), texture() )

			with model(ubound(model))

				.dList = glGenLists(1)
				glNewList( .dList, GL_COMPILE )
				'render_model( @model(ubound(model)), texture() )
				glEndList()


				for s as integer = 0 to ubound(.surfaces)'.max_surfaces-1
					.surfaces(s).dList = glGenLists(1)
					glNewList( .surfaces(s).dList, GL_COMPILE )
					'render_model_surface( @model(ubound(model)), texture(), s )
					glEndList()
				next

			end with

			#ifdef _debugout
			print #debugout, filename & " loaded"
			print #debugout, "__________________________________"
			#endif

		else
			#ifdef _debugout
			print #debugout, "unknown file format"
			#endif
			return 0
		endif


		with model(ubound(model))

			for t as integer = 0 to ubound(.triangles)
				for p as integer = 0 to 2
					.triangles(t).midpoint += .vertices(.triangles(t).point_id(p))
				next

				.triangles(t).midpoint/=3f
			next

			dim as nmathf.vec3f low  = nmathf.vec3f(2^32-1,2^32-1,2^32-1)
			dim as nmathf.vec3f high = nmathf.vec3f(-(2^32-1), -(2^32-1), -(2^32-1) )

			'get the bounding box for this model...


			for v as integer = 0 to ubound(.vertices)

				if .vertices(v).x < low.x then
					low.x = .vertices(v).x
				end if

				if .vertices(v).y < low.y then
					low.y = .vertices(v).y
				end if

				if .vertices(v).z < low.z then
					low.z = .vertices(v).z
				end if

				if .vertices(v).x > high.x then
					high.x = .vertices(v).x
				end if

				if .vertices(v).y > high.y then
					high.y = .vertices(v).y
				end if

				if .vertices(v).z > high.z then
					high.z = .vertices(v).z
				end if

			next

			with .bbox

				.centroid.x = (high.x+low.x) / 2.0
				.centroid.y = (high.y+low.y) / 2.0
				.centroid.z = (high.z+low.z) / 2.0

				.size.x = (high.x-low.x) / 2.0
				.size.y = (high.y-low.y) / 2.0
				.size.z = (high.z-low.z) / 2.0

				.vertices(0) = nmathf.vec3f(.centroid.x-.size.x, .centroid.y+.size.y, .centroid.z-.size.z )
				.vertices(1) = nmathf.vec3f(.centroid.x-.size.x, .centroid.y+.size.y, .centroid.z+.size.z )
				.vertices(2) = nmathf.vec3f(.centroid.x+.size.x, .centroid.y+.size.y, .centroid.z+.size.z )
				.vertices(3) = nmathf.vec3f(.centroid.x+.size.x, .centroid.y+.size.y, .centroid.z-.size.z )
				.vertices(4) = nmathf.vec3f(.centroid.x-.size.x, .centroid.y-.size.y, .centroid.z-.size.z )
				.vertices(5) = nmathf.vec3f(.centroid.x-.size.x, .centroid.y-.size.y, .centroid.z+.size.z )
				.vertices(6) = nmathf.vec3f(.centroid.x+.size.x, .centroid.y-.size.y, .centroid.z+.size.z )
				.vertices(7) = nmathf.vec3f(.centroid.x+.size.x, .centroid.y-.size.y, .centroid.z-.size.z )

				.triangles(0).point_id(0) = 4
				.triangles(0).point_id(1) = 0
				.triangles(0).point_id(2) = 3

				.triangles(1).point_id(0) = 4
				.triangles(1).point_id(1) = 3
				.triangles(1).point_id(2) = 7

				.triangles(2).point_id(0) = 0
				.triangles(2).point_id(1) = 1
				.triangles(2).point_id(2) = 2

				.triangles(3).point_id(0) = 0
				.triangles(3).point_id(1) = 2
				.triangles(3).point_id(2) = 3

				.triangles(4).point_id(0) = 6
				.triangles(4).point_id(1) = 2
				.triangles(4).point_id(2) = 1

				.triangles(5).point_id(0) = 6
				.triangles(5).point_id(1) = 1
				.triangles(5).point_id(2) = 5

				.triangles(6).point_id(0) = 7
				.triangles(6).point_id(1) = 6
				.triangles(6).point_id(2) = 5

				.triangles(7).point_id(0) = 7
				.triangles(7).point_id(1) = 5
				.triangles(7).point_id(2) = 4

				.triangles(8).point_id(0) = 5
				.triangles(8).point_id(1) = 1
				.triangles(8).point_id(2) = 0

				.triangles(9).point_id(0) = 5
				.triangles(9).point_id(1) = 0
				.triangles(9).point_id(2) = 4

				.triangles(10).point_id(0) = 7
				.triangles(10).point_id(1) = 3
				.triangles(10).point_id(2) = 2

				.triangles(11).point_id(0) = 7
				.triangles(11).point_id(1) = 2
				.triangles(11).point_id(2) = 6

			end with


			if patch_discontinuous_uvs( @model(ubound(model)) ) then
		 		print #debugout, "Discontinuous texture UV's patched..."
			end if


			'model render VBO creation
			redim preserve .uv1( ubound(.vertices) )
			redim preserve .uv2( ubound(.vertices) )
			redim preserve .uv3( ubound(.vertices) )
			redim preserve .uv4( ubound(.vertices) )
			redim preserve .uvb( ubound(.vertices) )

			for s as integer = 0 to ubound(.surfaces)

				for t as integer = .surfaces(s).Start_ID to .surfaces(s).End_ID
					for p as integer = 0 to 2

						with .surfaces(s)
							redim preserve .vertex_index( ubound(.vertex_index)+1 )
							.vertex_index( ubound(.vertex_index) ) = model(ubound(model)).triangles(t).point_id(p)
						end with

						.vnormals( model(ubound(model)).triangles(t).point_id(p) ) = model(ubound(model)).triangles(t).vnormals(p)
						.uv1( model(ubound(model)).triangles(t).point_id(p) ) = model(ubound(model)).triangles(t).t1coord(p)
						.uv2( model(ubound(model)).triangles(t).point_id(p) ) = model(ubound(model)).triangles(t).t2coord(p)
						.uv3( model(ubound(model)).triangles(t).point_id(p) ) = model(ubound(model)).triangles(t).t3coord(p)
						.uv4( model(ubound(model)).triangles(t).point_id(p) ) = model(ubound(model)).triangles(t).t4coord(p)
						.uvb( model(ubound(model)).triangles(t).point_id(p) ) = model(ubound(model)).triangles(t).bcoord(p)

					next
				next
			next

			glGenBuffers( 1, @.vbo )
			glBindBuffer( GL_ARRAY_BUFFER, .vbo )
			glBufferData( GL_ARRAY_BUFFER, (ubound(.tvertices)+1)*sizeof(nmathf.vec3f), @.tvertices(0).x, GL_DYNAMIC_DRAW )

			glGenBuffers( 1, @.normal_vbo )
			glBindBuffer( GL_ARRAY_BUFFER, .normal_vbo )
			glBufferData( GL_ARRAY_BUFFER, (ubound(.vnormals)+1)*sizeof(nmathf.vec3f), @.vnormals(0).x, GL_DYNAMIC_DRAW )

			glGenBuffers( 1, @.uv1_vbo )
			glBindBuffer( GL_ARRAY_BUFFER, .uv1_vbo )
			glBufferData( GL_ARRAY_BUFFER, (ubound(.uv1)+1)*sizeof(nmathf.vec2f), @.uv1(0).x, GL_DYNAMIC_DRAW )

			glGenBuffers( 1, @.uv2_vbo )
			glBindBuffer( GL_ARRAY_BUFFER, .uv2_vbo )
			glBufferData( GL_ARRAY_BUFFER, (ubound(.uv2)+1)*sizeof(nmathf.vec2f), @.uv2(0).x, GL_DYNAMIC_DRAW )

			glGenBuffers( 1, @.uv3_vbo )
			glBindBuffer( GL_ARRAY_BUFFER, .uv3_vbo )
			glBufferData( GL_ARRAY_BUFFER, (ubound(.uv3)+1)*sizeof(nmathf.vec2f), @.uv3(0).x, GL_DYNAMIC_DRAW )

			glGenBuffers( 1, @.uv4_vbo )
			glBindBuffer( GL_ARRAY_BUFFER, .uv4_vbo )
			glBufferData( GL_ARRAY_BUFFER, (ubound(.uv4)+1)*sizeof(nmathf.vec2f), @.uv4(0).x, GL_DYNAMIC_DRAW )

			glGenBuffers( 1, @.uvb_vbo )
			glBindBuffer( GL_ARRAY_BUFFER, .uvb_vbo )
			glBufferData( GL_ARRAY_BUFFER, (ubound(.uvb)+1)*sizeof(nmathf.vec2f), @.uvb(0).x, GL_DYNAMIC_DRAW )


			redim .vbo_index( ubound(.surfaces) )

			for s as integer = 0 to ubound(.surfaces)

				glGenBuffers( 1, @.vbo_index(s) )
				glBindBuffer( GL_ARRAY_BUFFER, .vbo_index(s) )
				glBufferData( GL_ARRAY_BUFFER, (ubound(.surfaces(s).vertex_index)+1) * sizeof(uinteger), @.surfaces(s).vertex_index(0), GL_DYNAMIC_DRAW )

			next

			glBindBuffer( GL_ARRAY_BUFFER, 0 )

			glDisable( GL_VERTEX_ARRAY )

		end with

		return 0

	end function


	sub unload_model3d( tModel() as Model_struct, byref max_models as integer, byref killid as integer )

		if killid > max_models-1 then exit sub
		if killid < 0 then exit sub

		if max_models > 0 then

			with tModel(killid)

				#ifdef _debugout
				print #debugout, "removing, " & .mName
				#endif

				if .dList<>0 then
					glDeleteLists( .dList, 1 )
				end if

			end with

			swap tModel(max_models-1), tModel(killid)

			max_models-=1

			if max_models>-1 then

				redim preserve tModel(max_models)

			else

				max_models = 0

			end if

		end if

	end sub

	function patch_discontinuous_uvs( byref model as model_struct ptr ) as integer

		dim as integer result

		with *model

			for t1 as integer = 0 to ubound(.triangles)

				for p1 as integer = 0 to 2

					for t2 as integer = t1+1 to ubound(.triangles)

						for p2 as integer = 0 to 2

							if .triangles(t1).point_id(p1) = .triangles(t2).point_id(p2) then

								if .triangles(t1).t1Coord(p1) = .triangles(t2).t1Coord(p2) then
									'do nothing
								else

									redim preserve .vertices( ubound(.vertices)+1 )
									.vertices( ubound(.vertices) ) = .vertices(.triangles(t1).point_id(p1))

									redim preserve .tvertices( ubound(.tvertices)+1 )
									.tvertices( ubound(.tvertices) ) = .tvertices(.triangles(t1).point_id(p1))

									redim preserve .vNormals( ubound(.vNormals)+1 )
									.vNormals( ubound(.vNormals) ) = .triangles(t1).vNormals(p1)

									.triangles(t2).point_id(p2) = ubound(.vertices)

									result = true

									exit for

								end if

							end if

						next
					next

				next

			next

		end with

		return result

	end function



	sub calc_normals(  byref Model as Model_struct ptr )

		dim  as nmathf.vec3f Normal, vTriangle(0 to 2)
		dim i as integer, ij as integer

		for i = 0 to ubound(model->triangles)
			
			for ij = 0 to 2
				vTriangle(ij).X = Model->tvertices(Model->triangles(i).point_Id(ij)).X
				vTriangle(ij).Y = Model->tvertices(Model->triangles(i).point_Id(ij)).Y
				vTriangle(ij).Z = Model->tvertices(Model->triangles(i).point_Id(ij)).Z
			next

			Model->triangles(i).Normal = nmathf.poly_normal( @vtriangle(0) )

		next

	end sub

	sub calc_vnormals(  byref Model as Model_struct ptr )

		for i as integer = 0 to ubound(model->triangles)

			for ij as integer = 0 to 2
				'Model->vNormals[Model->triangles[i].point_Id(ij)].X += Model->triangles[i].Normal.X
				'Model->vNormals[Model->triangles[i].point_Id(ij)].Y += Model->triangles[i].Normal.Y
				'Model->vNormals[Model->triangles[i].point_Id(ij)].Z += Model->triangles[i].Normal.Z
			next

		next

	end sub


	sub calc_planes(  byref Model as Model_struct ptr )

		dim as integer i
		dim as nmathf.vec3f v1, v2, v3

		for i = 0 to ubound(model->triangles)
			V1.X = Model->vertices(Model->triangles(i).point_Id(0)).X
			V1.y = Model->vertices(Model->triangles(i).point_Id(0)).Y
			V1.Z = Model->vertices(Model->triangles(i).point_Id(0)).Z

			V2.X = Model->vertices(Model->triangles(i).point_Id(1)).X
			V2.y = Model->vertices(Model->triangles(i).point_Id(1)).Y
			V2.Z = Model->vertices(Model->triangles(i).point_Id(1)).Z

			V3.X = Model->vertices(Model->triangles(i).point_Id(2)).X
			V3.y = Model->vertices(Model->triangles(i).point_Id(2)).Y
			V3.Z = Model->vertices(Model->triangles(i).point_Id(2)).Z

			Model->triangles(i).Plane.X = v1.y * (v2.z-v3.z) + v2.y * (v3.z-v1.z) + v3.y * (v1.z-v2.z)
			Model->triangles(i).Plane.Y = v1.z*(v2.x-v3.x) + v2.z*(v3.x-v1.x) + v3.z*(v1.x-v2.x)
			Model->triangles(i).Plane.Z = v1.x*(v2.y-v3.y) + v2.x*(v3.y-v1.y) + v3.x*(v1.y-v2.y)
			Model->triangles(i).Plane.W = -( v1.x*( v2.y*v3.z - v3.y*v2.z ) + v2.x*(v3.y*v1.z - v1.y*v3.z) + v3.x*(v1.y*v2.z - v2.y*v1.z) )
		next

	end sub


	sub set_poly_neighbors( byref Model as Model_struct ptr )

		dim as integer f, f2
		dim as integer i, ij
		dim as integer Hitcnt
		dim as integer P1a, P1b, P2a, P2b, C1a, C2a, C1b, C2b

		for f = 0 to ubound(model->triangles)
			for i = 0 to 2
				Model->triangles(f).Con(i) = -1
			next
		next

		for f = 0 to ubound(model->triangles)-1
			for f2 = f+1 to ubound(model->triangles)
				for i = 0 to 2
					if Model->triangles(f).Con(i) = -1 then
						for ij = 0 to 2

							P1a = i
							P1b = ij
							P2a = (P1a +1) mod 3
							P2b = (P1b +1) mod 3

							P1a = Model->triangles(f).Point_ID(P1a)
							P2a = Model->triangles(f).Point_ID(P2a)
							P1b = Model->triangles(f2).Point_ID(P1b)
							P2b = Model->triangles(f2).Point_ID(P2b)

							C1a=((P1a+P2a) - abs(P1a-P2a)) \ 2
							C2a=((P1a+P2a) + abs(P1a-P2a)) \ 2
							C1b=((P1b+P2b) - abs(P1b-P2b)) \ 2
							C2b=((P1b+P2b) + abs(P1b-P2b)) \ 2

							if (C1a = C1b) and (C2a = C2b) then
								Model->triangles(f).Con(i) = f2+1
								Model->triangles(f2).Con(ij) = f+1
							end if

						next
					end if
				next
			next
		next

		Model->shadow_caster = true

		for f = 0 to ubound(model->triangles)
			for i = 0 to 2
				if Model->triangles(f).Con(i) = -1 then
					Model->shadow_caster = false
					Model->triangles(f).flagged = 1
				end if
			next
		next

		if not Model->shadow_caster then

			#ifdef _debugout
			print #debugout, "MODEL NOT CONNECTED PROPERLY FOR SHADOW VOLUME RENDERING!"
			#endif

		end if


	end sub


	sub render_model_vbo( byref model as model_struct ptr, byref tMatrix as nmathf.matrix )


		glPushMatrix()
		glMultMatrixf( tMatrix )

		with *model

			glEnableClientState(GL_VERTEX_ARRAY)
			glEnableClientState(GL_NORMAL_ARRAY)

			glBindBuffer( GL_ARRAY_BUFFER, .vbo )
			glBufferSubData( GL_ARRAY_BUFFER, 0, (ubound(.tvertices)+1)*sizeof(nmathf.vec3f), @.tvertices(0).x )
			glvertexpointer( 3, GL_FLOAT,  0, 0 )

			glClientActiveTextureARB(GL_TEXTURE1_ARB)
			glActiveTextureARB(GL_TEXTURE1_ARB)
			glEnable(GL_TEXTURE_2D)
			glBindBuffer( GL_ARRAY_BUFFER, .uv1_vbo )
			glTexCoordPointer(2, GL_FLOAT, 0, 0)

			glBindBuffer( GL_ARRAY_BUFFER, .normal_vbo )
			glNormalPointer(GL_FLOAT, 0, 0)


			for s as integer = 0 to ubound(.surfaces)
				
				if model->surfaces(s).surfName = "invisible" then continue for
				
				dim as integer surfShade

				if Model->Surfaces(s).Is_Bump > 0 then surfShade = 2

				if Model->Surfaces(s).Is_Multi > 0 then

					if surfShade = 0 then surfShade = 1

				end if


				select case as const surfShade

					case 0
						glUseProgramObjectARB( Shader_PCF )
						glUniform1iARB(tType, 0)

						glClientActiveTextureARB(GL_TEXTURE1_ARB)
						glActiveTextureARB(GL_TEXTURE1_ARB)
						glEnable(GL_TEXTURE_2D)
						glEnableClientState(GL_TEXTURE_COORD_ARRAY)
						glBindBuffer( GL_ARRAY_BUFFER, .uv1_vbo )

						glTexCoordPointer(2, GL_FLOAT, 0, 0)
						glBindTexture ( GL_TEXTURE_2D, model->surfaces(s).tex1 )

					case 1

						glUseProgramObjectARB( Shader_PCF )
						glUniform1iARB(tType, 1)

						glClientActiveTextureARB(GL_TEXTURE1_ARB)
						glActiveTextureARB(GL_TEXTURE1_ARB)
						glEnable(GL_TEXTURE_2D)
						glEnableClientState(GL_TEXTURE_COORD_ARRAY)
						glBindBuffer( GL_ARRAY_BUFFER, .uv1_vbo )
						glTexCoordPointer(2, GL_FLOAT, 0, 0)
						glBindTexture ( GL_TEXTURE_2D, model->surfaces(s).tex1 )

						glClientActiveTextureARB(GL_TEXTURE2_ARB)
						glActiveTextureARB(GL_TEXTURE2_ARB)
						glEnable(GL_TEXTURE_2D)
						glEnableClientState(GL_TEXTURE_COORD_ARRAY)
						glBindBuffer( GL_ARRAY_BUFFER, .uv2_vbo )
						glTexCoordPointer(2, GL_FLOAT, 0, 0)
						glBindTexture ( GL_TEXTURE_2D, model->surfaces(s).tex2 )

						glClientActiveTextureARB(GL_TEXTURE3_ARB)
						glActiveTextureARB(GL_TEXTURE3_ARB)
						glEnable(GL_TEXTURE_2D)
						glEnableClientState(GL_TEXTURE_COORD_ARRAY)
						glBindBuffer( GL_ARRAY_BUFFER, .uv3_vbo )
						glTexCoordPointer(2, GL_FLOAT, 0, 0)
						glBindTexture ( GL_TEXTURE_2D, model->surfaces(s).tex3 )


					case 2

						glUseProgramObjectARB( Shader_PCF )
						glUniform1iARB(tType, 3)

						glClientActiveTextureARB(GL_TEXTURE1_ARB)
						glActiveTextureARB(GL_TEXTURE1_ARB)
						glEnable(GL_TEXTURE_2D)
						glEnableClientState(GL_TEXTURE_COORD_ARRAY)
						glBindBuffer( GL_ARRAY_BUFFER, .uv1_vbo )
						glTexCoordPointer(2, GL_FLOAT, 0, 0)
						glBindTexture ( GL_TEXTURE_2D, model->surfaces(s).tex1 )

						glClientActiveTextureARB(GL_TEXTURE2_ARB)
						glActiveTextureARB(GL_TEXTURE2_ARB)
						glEnable(GL_TEXTURE_2D)
						glEnableClientState(GL_TEXTURE_COORD_ARRAY)
						glBindBuffer( GL_ARRAY_BUFFER, .uvb_vbo )
						glTexCoordPointer(2, GL_FLOAT, 0, 0)
						glBindTexture ( GL_TEXTURE_2D, model->surfaces(s).texb )


				end select

				if Model->surfaces(s).surfName = "Leaves" then

					gltexparameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE )
					gltexparameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE )

					glDisable( GL_CULL_FACE )

					glEnable( GL_BLEND )
					glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA )


				elseif Model->surfaces(s).surfName = "water" then

					glEnable( GL_BLEND )
					glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA )

				end if

				glMaterialfv( GL_FRONT, GL_AMBIENT,   @Model->surfaces(s).Ambient.R )
				glMaterialfv( GL_FRONT, GL_DIFFUSE,   @Model->surfaces(s).Diffuse.R )
				glMaterialfv( GL_FRONT, GL_SPECULAR,  @Model->surfaces(s).Specular.R )
				glMaterialf ( GL_FRONT, GL_SHININESS, Model->surfaces(s).Shininess )
				glcolor4fv( @Model->surfaces(s).RGBAlpha.R )

				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, .vbo_index(s) )
				glDrawElements( GL_TRIANGLES, ubound(.surfaces(s).vertex_index)+1, GL_UNSIGNED_INT, 0 )


				glEnable( GL_CULL_FACE )
				glDisable( GL_BLEND )

				glClientActiveTextureARB(GL_TEXTURE1_ARB)
				glActiveTextureARB(GL_TEXTURE1_ARB)
				glDisable(GL_TEXTURE_2D)
				glDisableClientState(GL_TEXTURE_COORD_ARRAY)
				glBindBuffer( GL_ARRAY_BUFFER, 0 )

				glClientActiveTextureARB(GL_TEXTURE2_ARB)
				glActiveTextureARB(GL_TEXTURE2_ARB)
				glBindBuffer( GL_ARRAY_BUFFER, 0 )
				glDisableClientState(GL_TEXTURE_COORD_ARRAY)
				glDisable(GL_TEXTURE_2D)

				glClientActiveTextureARB(GL_TEXTURE3_ARB)
				glActiveTextureARB(GL_TEXTURE3_ARB)
				glBindBuffer( GL_ARRAY_BUFFER, 0 )
				glDisableClientState(GL_TEXTURE_COORD_ARRAY)
				glDisable(GL_TEXTURE_2D)


			next



			glDisableClientState(GL_NORMAL_ARRAY)
			glDisableClientState(GL_VERTEX_ARRAY)

		end with

		glPopMatrix()

	end sub


	sub render_model_tex_only( byref Model as Model_struct ptr, byref matrix as nMathf.Matrix, byref isDepthPass as integer )

		glPushMatrix()
		glMultMatrixF( matrix )

		for s as integer = 0 to ubound(model->surfaces)

			glMaterialfv( GL_FRONT, GL_AMBIENT,   @Model->surfaces(s).Ambient.R )
			glMaterialfv( GL_FRONT, GL_DIFFUSE,   @Model->surfaces(s).Diffuse.R )
			glMaterialfv( GL_FRONT, GL_SPECULAR,  @Model->surfaces(s).Specular.R )
			glMaterialf ( GL_FRONT, GL_SHININESS, Model->surfaces(s).Shininess )

			dim as integer surfShade

			if isDepthPass <> 0 then

				if Model->Surfaces(s).Is_Bump > 0 then surfShade = 2

				if Model->Surfaces(s).Is_Multi > 0 then

					if surfShade = 0 then surfShade = 1

				end if

			end if


			select case as const surfShade

				case 0

					glUseProgramObjectARB( Shader_PCF )
					glUniform1iARB(tType, 0)

					'if Model->surfaces(s).surfName = "water" then
					'	glUniform1iARB(tType, 3)
					'end if

					'glEnable(GL_BLEND)
					'glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ZERO, GL_ONE_MINUS_SRC_ALPHA)
					'glColor4fv( @Model->surfaces(s).rgbAlpha.R )
					'glColor4f(1,1,1,.1)

					glActiveTextureARB( GL_TEXTURE1 )
					glEnable(GL_TEXTURE_2D)
					glBindTexture ( GL_TEXTURE_2D, Model->surfaces(s).Tex1)'Texture(Model->surfaces(s).Tex1).img )


				case 1

					glUseProgramObjectARB( Shader_PCF )
					glUniform1iARB(tType, 1)

					glActiveTextureARB( GL_TEXTURE1 )
					glEnable(GL_TEXTURE_2D)
					glBindTexture ( GL_TEXTURE_2D, Model->surfaces(s).Tex1)

					glActiveTextureARB( GL_TEXTURE2 )
					glEnable(GL_TEXTURE_2D)
					glBindTexture ( GL_TEXTURE_2D, Model->surfaces(s).Tex2)

					glActiveTextureARB( GL_TEXTURE3 )
					glEnable(GL_TEXTURE_2D)
					glBindTexture ( GL_TEXTURE_2D, Model->surfaces(s).Tex3)

				case 2

					glUseProgramObjectARB( Shader_PCF )
					glUniform1iARB(tType, 3)

					glActiveTextureARB( GL_TEXTURE1 )
					glEnable(GL_TEXTURE_2D)
					glBindTexture ( GL_TEXTURE_2D, Model->surfaces(s).Tex1)

					glActiveTextureARB( GL_TEXTURE2 )
					glEnable(GL_TEXTURE_2D)
					glBindTexture ( GL_TEXTURE_2D, Model->surfaces(s).Texb)

			end select


			if Model->surfaces(s).surfName = "Leaves" then

				gltexparameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE )
				gltexparameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE )

				glDisable( GL_CULL_FACE )

			end if



			for t as integer = Model->surfaces(s).Start_ID to Model->surfaces(s).End_ID

				glBegin( GL_TRIANGLES )

				for p as integer = 0 to 2

					select case as const surfShade

						case 0

							glMultiTexCoord2fvARB(GL_TEXTURE1, @Model->triangles(t).t1coord(p).x)

						case 1

							glMultiTexCoord2fvARB(GL_TEXTURE1, @Model->triangles(t).t1coord(p).x )
							glMultiTexCoord2fvARB(GL_TEXTURE2, @Model->triangles(t).t2coord(p).x )
							glMultiTexCoord2fvARB(GL_TEXTURE3, @Model->triangles(t).t3coord(p).x )

						case 2

							glMultiTexCoord2fvARB(GL_TEXTURE1, @Model->triangles(t).t1coord(p).x )
							glMultiTexCoord2fvARB(GL_TEXTURE2, @Model->triangles(t).bcoord(p).x )

					end select


					glNormal3fv( @Model->Triangles(t).vNormals(p).x )
					glVertex3fv(@Model->vertices(Model->triangles(t).point_id(p)).x)


				next

				glEnd()

			next


			glDisable(GL_BLEND)
			glEnable( GL_CULL_FACE )

			if surfShade>0 then

				glUniform1iARB(tType, 0)

				glUseProgramObjectARB( 0 )

				glActiveTextureARB( GL_TEXTURE1 )
				glDisable(GL_TEXTURE_2D)

				glActiveTextureARB( GL_TEXTURE2 )
				glDisable(GL_TEXTURE_2D)

				glActiveTextureARB( GL_TEXTURE3 )
				glDisable(GL_TEXTURE_2D)

			end if


		next

		glActiveTextureARB( GL_TEXTURE1 )
		glDisable(GL_TEXTURE_2D)

		glActiveTextureARB( GL_TEXTURE2 )
		glDisable(GL_TEXTURE_2D)

		glActiveTextureARB( GL_TEXTURE3 )
		glDisable(GL_TEXTURE_2D)

		glPopMatrix()

	end sub


	sub RenderDepthPass( byref model as model_struct ptr, byref tMatrix as nmathf.matrix )

		glPushMatrix()
		glMultMatrixf( tMatrix )

		with *model

			glEnableClientState(GL_VERTEX_ARRAY)
			glBindBuffer( GL_ARRAY_BUFFER, .vbo )
			glvertexpointer( 3, GL_FLOAT,  0, 0 )

			glClientActiveTextureARB(GL_TEXTURE1_ARB)
			glActiveTextureARB(GL_TEXTURE1_ARB)
			glEnable(GL_TEXTURE_2D)
			glEnableClientState(GL_TEXTURE_COORD_ARRAY)
			glBindBuffer( GL_ARRAY_BUFFER, .uv1_vbo )
			glTexCoordPointer(2, GL_FLOAT, 0, 0)


			for s as integer = 0 to ubound(.surfaces)
				
				if model->surfaces(s).surfName = "invisible" then continue for
				
				glBindTexture ( GL_TEXTURE_2D, model->surfaces(s).tex1 )

				if Model->surfaces(s).surfName = "Leaves" then

					gltexparameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE )
					gltexparameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE )

					glDisable( GL_CULL_FACE )

				end if

				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, .vbo_index(s) )
				glDrawElements( GL_TRIANGLES, ubound(.surfaces(s).vertex_index)+1, GL_UNSIGNED_INT, 0 )

				glEnable( GL_CULL_FACE )

			next

			glDisableClientState(GL_TEXTURE_COORD_ARRAY)
			glDisableClientState(GL_VERTEX_ARRAY)

		end with

		glPopMatrix()



		'old immediate mode method________________________________________________________________________________________________
		exit sub

		glPushMatrix()
		glMultMatrixf( tMatrix )

		glActiveTextureARB( GL_TEXTURE1 )
		glEnable(GL_TEXTURE_2D)

		for s as integer = 0 to ubound(model->surfaces)

			glBindTexture ( GL_TEXTURE_2D, Model->surfaces(s).Tex1 )

			if Model->surfaces(s).surfName = "Leaves" then
				glDisable(GL_CULL_FACE)
			end if

			for t as integer = Model->surfaces(s).Start_ID to Model->surfaces(s).End_ID

				glBegin( GL_TRIANGLES )

				glNormal3fv( @Model->Triangles(t).Normal.x )

				for p as integer = 0 to 2

					glMultiTexCoord2fvARB(GL_TEXTURE1, @Model->triangles(t).t1coord(p).x)
					glVertex3fv(@Model->vertices(Model->triangles(t).point_id(p)).x)

				next

				glEnd()

			next

			glEnable(GL_CULL_FACE)

		next

		glPopMatrix()

	end sub

end namespace
