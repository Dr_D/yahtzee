#include once "gui.bi"

namespace ngui

	constructor gui_struct()

	end constructor

	destructor gui_struct()

	end destructor


	sub gui_struct.update_entire_god_damn_thing()

		if ubound(wind)<0 then exit sub

		for w as integer = 0 to ubound(wind)
			
			zOrder(w) = w
			
			with wind(w)

				.parent = @this

				if ubound(.button)>-1 then

					for b as integer = 0 to ubound(.button)

						with .button(b)

							.parent = @wind(w)

						end with

					next

				end if

			end with

		next

	end sub


	function gui_struct.GetWindowByTitle( byref wTitle as string ) as window_struct ptr

		for w as integer = 0 to ubound(wind)

			if wind(w).title = wTitle then

				return @wind(w)

			end if

		next

		return 0

	end function

	function gui_struct.AddWindow( byval posit as nmathf.vec2f, byval size as nmathf.vec2f, byval wColor as nimage.rgbalpha, byval fColor as nimage.rgbalpha ) as integer

		dim as integer max_winds=ubound(wind)+1
		redim preserve wind( max_winds )
		redim preserve zOrder( max_winds )

		with wind(max_winds)
			.parent = @this
			.posit = posit
			.size = size
			.wColor = wcolor
			.fColor = fColor
			.font = font
			.type_xor_delta = 5
		end with

		for w as integer = 0 to max_winds
			with wind(w)
				.is_focus = 0
			end with
		next

		wind(max_winds).is_focus = true

		zOrder(max_winds) = max_winds

		update_entire_god_damn_thing()

		return max_winds

	end function


	sub gui_struct.RemoveWindow( byval id as integer )

		if ubound(wind)<0 then exit sub

		if id<lbound(wind) then exit sub
		if id>ubound(wind) then exit sub

		with wind(id)

			if ubound(.button)>-1 	then 	erase .button
			if ubound(.tBox)>-1 		then 	erase .tBox
			if ubound(.label)-1 		then 	erase .label

		end with

		dim as integer max_winds = ubound(this.wind)

		if max_winds > 0 then

			swap wind(max_winds), wind(id)
			swap zOrder(max_winds), zOrder(id)

			redim preserve wind(max_winds-1)
			redim preserve zOrder(max_winds-1)

			update_entire_god_damn_thing()

			zSort(ubound(wind))
			wind(ubound(wind)).is_focus = true

		elseif max_winds = 0 then

			erase wind

			erase zOrder

			print #debugout, "All windows removed"

			exit sub

		end if

	end sub


	sub gui_struct.SetFont( byref font as ndisplay.font_struct ptr )

		this.font = font

	end sub


	sub gui_struct.zSort( byval w as integer )

		if ubound(zOrder)<1 then exit sub

		if w<lbound(zOrder) then exit sub
		if w>ubound(zOrder) then exit sub

		dim as integer original = zOrder(w)

		dim as integer temp(ubound(zOrder))

		for z as integer = 0 to ubound(zOrder)

			dim as integer nw = zOrder(z)

			if nw>ubound(wind) then

				update_entire_god_damn_thing()

			end if

		next

		for z as integer = 0 to ubound(zOrder)

			dim as integer w = zOrder(z)

			wind(w).is_focus = false

			temp(z) = zOrder(z)

		next


		for z as integer = ubound(zOrder)-1 to w step-1
			zOrder(z) = temp(z+1)
		next

		zOrder(ubound(zOrder)) = original

		wind(zOrder(ubound(zOrder))).is_focus = true

	end sub


	function gui_struct.PointInBox( byval pnt as nmathf.vec2f, byval posit as nmathf.vec2f, byval size as nmathf.vec2f ) as integer

		return ((pnt.x>=posit.x and pnt.x<= posit.x+size.x) and (pnt.y>=posit.y and pnt.y<= posit.y+size.y))

	end function


	sub window_struct.wPrint( byref tString as string, byref posit as nmathf.vec2f )


	end sub

	sub window_struct.BeginDraw()

		glDisable( GL_TEXTURE_2D )
		glDepthMask(GL_FALSE)
		glEnable( GL_BLEND )
		glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA )
		glPolygonMode( GL_FRONT, GL_FILL )
		glMatrixMode( GL_PROJECTION )
		glPushMatrix()
		glLoadIdentity()
		glMatrixMode( GL_MODELVIEW )
		glPushMatrix()
		glLoadIdentity()
		glOrtho( 0,Display.W, 0, Display.H, -1, 1 )
		glDisable( GL_LIGHTING )
		glDisable( GL_DEPTH_TEST )

	end sub

	sub window_struct.EndDraw()

		glMatrixMode( GL_PROJECTION )
		glPopMatrix()
		glmatrixmode( GL_MODELVIEW )
		glPopMatrix()
		glDepthMask(GL_TRUE)
		glEnable( GL_LIGHTING )
		glEnable( GL_DEPTH_TEST )
		glDisable( GL_BLEND )
		glEnable( GL_TEXTURE_2D)

	end sub

	sub DrawGuiBox( byval posit as nmathf.vec2f, byval size as nmathf.vec2f, byval fcolor as nimage.rgbalpha, byval is3d as integer, byval rev as integer, byval dimmed as single )

		static as nimage.rgbalpha white,lgray, black, dgray
		white = type(dimmed,dimmed,dimmed,dimmed)
		black = type(0,0,0,dimmed)
		lgray = type(.75*dimmed,.75*dimmed,.75*dimmed,dimmed)
		dgray = type(.25*dimmed,.25*dimmed,.25*dimmed,dimmed)

		if rev<>0 then
			swap white, black
			swap lgray, dgray
		end if

		'draw the "3D" effect of the window
		if is3d then
			glBegin(GL_QUADS)
			glColor4fv(@white.r)
			glVertex2f(posit.x,posit.y+size.y)
			glVertex2f(posit.x,posit.y)
			glColor4fv(@lgray.r)
			glVertex2f(posit.x+2,posit.y)
			glVertex2f(posit.x+2,posit.y+size.y)
			glEnd()

			glBegin(GL_QUADS)
			glColor4fv(@white.r)
			glVertex2f(posit.x+size.x,posit.y+size.y)
			glVertex2f(posit.x,posit.y+size.y)
			glColor4fv(@lgray.r)
			glVertex2f(posit.x,posit.y+size.y-2)
			glVertex2f(posit.x+size.x,posit.y+size.y-2)
			glEnd()


			glBegin(GL_QUADS)
			glColor4fv(@black.r)
			glVertex2f(posit.x,posit.y)
			glVertex2f(posit.x+size.x,posit.y)
			glColor4fv(@dgray.r)
			glVertex2f(posit.x+size.x,posit.y+2)
			glVertex2f(posit.x,posit.y+2)
			glEnd()

			glBegin(GL_QUADS)
			glColor4fv(@black.r)
			glVertex2f(posit.x+size.x,posit.y)
			glVertex2f(posit.x+size.x,posit.y+size.y)
			glColor4fv(@dgray.r)
			glVertex2f(posit.x+size.x-2,posit.y+size.y)
			glVertex2f(posit.x+size.x-2,posit.y)
			glEnd()
		end if

		'draw the foregeround area
		glColor4f( fcolor.r*dimmed, fcolor.g*dimmed, fcolor.b*dimmed, dimmed )
		glBegin( GL_QUADS )
		glVertex2f(posit.x+2,posit.y+size.y-2)
		glVertex2f(posit.x+2,posit.y+2)
		glVertex2f(posit.x+size.x-2,posit.y+2)
		glVertex2f(posit.x+size.x-2,posit.y+size.y-2)
		glEnd()

	end sub

	sub DrawMenuBar( byval posit as nmathf.vec2f, byval size as nmathf.vec2f, byval sColor as nimage.rgbalpha, byval eColor as nimage.rgbalpha, byval dimmed as single)

		glColor4f( scolor.r*dimmed, scolor.g*dimmed, scolor.b*dimmed, scolor.a*dimmed )
		glBegin( GL_QUADS )
		glColor4f(sColor.r,sColor.g,sColor.b, sColor.a*dimmed)
		glVertex2f(posit.x+2,posit.y+size.y-2)
		glVertex2f(posit.x+2,posit.y+size.y-20)
		glColor4f(eColor.r,eColor.g,eColor.b, eColor.a*dimmed)
		glVertex2f(posit.x+size.x-2,posit.y+size.y-20)
		glVertex2f(posit.x+size.x-2,posit.y+size.y-2)
		glEnd()

		glColor4f(0,0,0,dimmed)
		glBegin( GL_LINES )
		glVertex2f(posit.x+2,posit.y+size.y-21)
		glVertex2f(posit.x+size.x-2,posit.y+size.y-21)
		glEnd()

		glColor4f(1,1,1,dimmed)
		glBegin( GL_LINES )
		glVertex2f(posit.x+2,posit.y+size.y-22)
		glVertex2f(posit.x+size.x-2,posit.y+size.y-22)
		glEnd()

	end sub


	sub text_box_struct.TrimText( byval tpos as nmathf.vec2f )

		dim as integer arrindex, lenstr = len(tString), i


		dim as single startpos = tpos.x+posit.x
		dim as single endpos = tpos.x+posit.x+size-(font->kern)
		dim as single tx = startpos

		do

			i+=1
			aString+=mid(tString,i,1)

			tx += font->kern

		loop until i >= lenstr

	end sub


	sub window_struct.wDraw( trimmed() as string )

		'draw all the buttons & such
		this.BeginDraw()

		DrawGuiBox( posit, size, wColor, 1, 0, this.dimmer )
		DrawMenuBar( posit, size, wColor, type(1,1,1,.75), this.dimmer )

		if ubound(button)>-1 then
			for b as integer = 0 to ubound(button)
				with button(b)
					if .visible then
						DrawGuiBox( this.posit+.posit, .size, .wColor, 1, .clicked, this.dimmer )
					end if
				end with
			next
		end if

		if ubound(tBox)>-1 then
			for t as integer = 0 to ubound(tBox)
				with tbox(t)
					DrawGuiBox( this.posit+.posit, nmathf.vec2f(.size,.font->scale), .wColor, 1, 1, this.dimmer )
				end with
			next
		end if

		if ubound(label)>-1 then
			for l as integer = 0 to ubound(label)
				with label(l)
					DrawGuiBox( this.posit+.posit, .size, .wColor, 1, 1, this.dimmer )
				end with
			next
		end if


		this.EndDraw()

		'draw the text & such...
		font->oBegin()
		font->oPrint( title, posit.x+2, posit.y+size.y-22, fColor.r*this.dimmer, fColor.g*this.dimmer, fColor.b*this.dimmer, this.dimmer, font->scale )

		dim as string tString
		if ubound(button)>-1 then
			for b as integer = 0 to ubound(button)
				with button(b)
					if .visible then
						tString = ltrim(rtrim(.caption))
						font->oPrint( .caption, (this.posit.x+(.posit.x+.size.x/2)-len(tString)*(font->kern/2))-7, this.posit.y+(.posit.y+.size.y/2)-(font->scale/2)-2, .fColor.r*this.dimmer, .fColor.g*this.dimmer, .fColor.b*this.dimmer, this.dimmer, font->scale)
					end if
				end with
			next
		end if

		if ubound(label)>-1 then
			for l as integer = 0 to ubound(label)
				with label(l)
					'font->oPrint( .caption, this.posit.x+.posit.x+2, this.posit.y+.posit.y, .fColor.r*this.dimmer, .fColor.g*this.dimmer, .fColor.b*this.dimmer, this.dimmer, (.size.x/font->kern/2)*(len(.caption)-1), .size.y )
					font->oPrint( .caption, this.posit.x+.posit.x+2, this.posit.y+.posit.y, .fColor.r*this.dimmer, .fColor.g*this.dimmer, .fColor.b*this.dimmer, this.dimmer, font->scale )
				end with
			next
		end if

		if ubound(tBox)>-1 then
			for t as integer = 0 to ubound(tBox)
				dim as string temp
				with tbox(t)
					if len(.tString)>0 then

						dim as single tx = this.posit.x+.posit.x
						dim as single ty = this.posit.y+.posit.y-2

						for i as integer = 1 to len(.tString)

							temp = mid(.tString,i,1)

							font->oPrint( temp, tx, ty, .fColor.r*this.dimmer, .fColor.g*this.dimmer, .fColor.b*this.dimmer, this.dimmer, font->scale)

							tx+=font->kern

							if i*font->kern>=.size-font->kern then
								exit for
							end if

						next

					end if

					if .typing then
						if this.type_xor >=1 then
							font->oPrint( "|", this.posit.x+.posit.x+(.curspos*font->kern)-(font->kern/2), this.posit.y+.posit.y+2, .fColor.r*this.dimmer, .fColor.g*this.dimmer, .fColor.b*this.dimmer, this.dimmer, font->scale)
						end if
					end if

				end with
			next
		end if

		font->oEnd()

	end sub


	function window_struct.AddTextBox( byref posit as nmathf.vec2f, byref size as single, byref caption as string, byref wColor as nimage.rgbalpha, byref fColor as nimage.rgbalpha ) as integer

		dim as integer max_tboxes = ubound(tBox)+1
		redim preserve tbox( max_tboxes )

		with tbox(max_tboxes)
			.posit = posit
			.size = size
			.tString = caption
			.curspos = len(.tString)
			.OnClick = @OnClickDefault()
			.wColor = wColor
			.fColor = fColor
			.font = font
		end with

		return ubound(tBox)

	end function


	sub window_struct.RemoveTextBox( byval id as integer )

		if id<lbound(tBox) then exit sub
		if id>ubound(tBox) then exit sub

		dim as integer max_tBoxes = ubound(tBox)

		swap tBox(max_tBoxes), tBox(id)

		if max_tBoxes>0 then

			redim preserve tBox(max_tBoxes-1)

		else

			erase tBox

		end if

	end sub

	function window_struct.AddButton( byref posit as nmathf.vec2f, byref size as nmathf.vec2f, byref caption as string, byref wColor as nimage.rgbalpha = type(0,0,0,0), byval callback as any ptr ) as integer

		dim as integer max_buttons = ubound(button)+1

		redim preserve button(max_buttons)

		with button(max_buttons)

			.parent = @this
			.posit = posit
			.size = size
			.flag = guiCommand
			.caption = caption

			if wColor.r = 0 and wColor.g = 0 and wColor.b = 0 and wColor.a = 0 then
				.wColor = this.wColor
			else
				.wColor = wColor
			end if
			.fColor = this.fColor
			.font = font

			if callback = 0 then

				.OnClick = @OnClickDefault()

			else

				.OnClick = callback

			end if

		end with

		return ubound(button)

	end function

	sub window_struct.RemoveButton( byval id as integer )

		if id<lbound(button) then exit sub
		if id>ubound(button) then exit sub

		dim as integer max_buttons = ubound(button)

		swap button(max_buttons), button(id)

		if max_buttons>0 then

			redim preserve button(max_buttons-1)

		else

			erase button

		end if

	end sub

	sub window_struct.AddCloseButton()

		dim as integer max_buttons = ubound(button)+1

		redim preserve button( max_buttons )

		with button(max_buttons)
			.posit = size-nmathf.vec2f(25,21)
			.size = nmathf.vec2f(20,20)
			.flag = guiClose
			.caption = "X"
			.OnClick = @OnCloseDefault()
			.wColor = type(.75,.25,.25,1)
			.fColor = type(1,1,1,1)
			.font = font
		end with

	end sub

	sub window_struct.DeselectAllTboxes()

		typing = false
		
		if ubound(tBox)>-1 then
		
			for t as integer = 0 to ubound(tBox)
				with tbox(t)
					.typing = false
					.clicked = false
				end with
			next
			
		end if

	end sub


	function window_struct.AddLabel( byref posit as nmathf.vec2f, byref size as nmathf.vec2f, byref caption as string, byref wColor as nimage.rgbalpha = type(0,0,0,0), byref fColor as nimage.rgbalpha = type(0,0,0,0) ) as integer

		dim as integer max_labels = ubound(label)+1

		redim preserve label(max_labels)

		with label(max_labels)

			.posit = posit

			.size = size

			.caption = caption

			if wColor.r = 0 and wColor.g = 0 and wColor.b = 0 and wColor.a = 0 then
				.wColor = this.wColor
			else
				.wColor = wColor
			end if

			if fColor.r = 0 and fColor.g = 0 and fColor.b = 0 and fColor.a = 0 then
				.fColor = this.fColor
			else
				.fColor = fColor
			end if

			.font = font

			.parent = @this

		end with

		return ubound(label)

	end function

	sub window_struct.RemoveLabel( byval id as integer )

		if id<lbound(label) then exit sub
		if id>ubound(label) then exit sub

		dim as integer max_labels = ubound(label)

		swap label(max_labels), label(id)

		if max_labels>0 then

			redim preserve label(max_labels-1)

		else

			erase label

		end if

	end sub


	function valid_char( byval ascii as integer ) as integer
		dim as integer min_val = 32
		dim as integer max_val = 128

		if ascii>=min_val and ascii<=max_val then
			return ascii
		end if

		return -1
	end function


	function text_box_struct.ProcessKeyboardInput( byref event as FB.EVENT ) as integer

		dim as integer is_string = valid_char( event.ascii )
		dim as string eString = tString

		'print #debugout, event.scancode

		if is_string > -1 then
			tString+=chr(is_string)
			pString = tString
			curspos = len(tString)
		else
			'enter is pressed
			select case as const event.scancode

				case 14 'backspace

					curspos-=1
					if curspos<0 then
						curspos=0
					end if

					tString=mid(tString,1,curspos)

				case 28 'enter

					this.typing = false
					if len(eString)>0 then
						pString = eString
					else
						tString = pString
					end if

				case 75 'left arrow
					curspos-=1
					if curspos<0 then
						curspos=0
					end if

				case 77 'right arrow
					curspos+=1
					if curspos>len(tString) then
						curspos=len(tString)
					end if

			end select

		end if

		return 0

	end function


	sub gui_struct.update( byval time_step as single )

		static as integer mx, my, mdx, mdy


		if ubound(wind)<0 then

			exit sub

		end if

		dim as FB.EVENT event
		'some action means any action... any action sets this flag to true
		static as integer some_action
		dim as string trimmed()

		dim as integer minWin = 0
		dim as integer maxWin = ubound(wind)

		for z as integer = 0 to ubound(wind)

			dim as integer w = zOrder(z)

			if w>ubound(wind) then continue for

			if wind(w).isAlert then

				minWin = z
				maxWin = z

				exit for

			end if

		next

		if screenevent(@event) then

			for z as integer = maxWin to minWin step -1

				dim as integer w = zOrder(z)

				if w>ubound(wind) then continue for

				if wind(w).visible = false then continue for

				'check/set everything related to the text boxes associated with this window...
				if ubound(wind(w).tBox)>-1 then
					for t as integer = 0 to ubound(wind(w).tBox)
						with wind(w).tbox(t)
							dim as integer mouse_in = PointInBox( nmathf.vec2f(mx,my), wind(w).posit+.posit, nmathf.vec2f(.size, .font->scale))

							select case event.type
								
								
								Case FB.EVENT_MOUSE_BUTTON_PRESS
									
									if event.button = FB.BUTTON_LEFT then
										
										if mouse_in then
											
											.clicked = true
											
											some_action = true
											
											this.zSort(z)
											
										end if
										
									end if
									

								Case FB.EVENT_MOUSE_BUTTON_RELEASE

									if mouse_in and .clicked then
										
										for tb2 as integer = 0 to ubound(wind(w).tBox)
											
											wind(w).tBox(tb2).typing = false
											
										next
										
										wind(w).typing = true
										
										.typing = true
										
										.curspos = 0
										
										'.scrollpos = 0
										
										.tString = ""
										
									end if
									
									.clicked = false
									some_action = false
									

								case FB.EVENT_KEY_PRESS, FB.EVENT_KEY_REPEAT
									
									if wind(w).typing then
										
										.ProcessKeyboardInput( event )
										
										if .typing = false then
											
											wind(w).typing = false
											
										end if
										
									end if
									

							end select

							if some_action then

								exit for

							end if

						end with
					next
				end if


				'check/set the status of all the buttons associated with this window
				if ubound(wind(w).button)>-1 then
					for b as integer = 0 to ubound(wind(w).button)
						with wind(w).button(b)

							'if .visible then

								dim as integer mouse_in = PointInBox( nmathf.vec2f(mx,my), wind(w).posit+.posit, .size)

								select case event.type

									Case FB.EVENT_MOUSE_BUTTON_PRESS
										if event.button = FB.BUTTON_LEFT then
											if mouse_in then
												wind(w).DeselectAllTboxes()
												.clicked = true
												some_action = true
											end if
										end if

									Case FB.EVENT_MOUSE_BUTTON_RELEASE

										if mouse_in and .clicked then

											.OnClick( @wind(w).button(b) )

											if .set_close then
												wind(w).closing = true
											end if

										end if

										.clicked = false
										some_action = false

								end select

							'end if

						end with

						'no need to check more buttons now as one is already clicked
						if some_action then
							'print #debugout, "early exit 1"
							exit for
						end if

					next
				end if

				'do the rest of the generic window stuff...
				with wind(w)

					dim as integer mouse_in = PointInBox( nmathf.vec2f(mx,my), .posit, .size )
					dim as integer mouse_in_menu_bar = mouse_in and (my>=.posit.y+.size.y-20) 'shortcut...

					'some_action = false

					if not .canMove then
						'freeze everything until this window disappears
						'some_action = true

					end if

					select case event.type

						case FB.EVENT_MOUSE_BUTTON_PRESS
							If event.button = FB.BUTTON_LEFT then

								if mouse_in and not some_action then
									.DeselectAllTboxes()
									if mouse_in_menu_bar then
										.dragging = true
									end if
									some_action = true
									this.zSort(z)
								end if

							end if

						Case FB.EVENT_MOUSE_BUTTON_RELEASE
							.dragging = false

						case FB.EVENT_MOUSE_EXIT
							.dragging = false

						case FB.EVENT_MOUSE_MOVE

							if .canMove then

								mx = event.x
								my = display.h-event.y
								mdx = event.dx
								mdy = -event.dy

								if .dragging then
									.posit += nmathf.vec2f(mdx, mdy)
								end if

							end if

					end select

				end with

				if some_action then
					'print #debugout, "early exit 2"
					exit for
				end if

			next

		end if

		'draw windows and associated controls...
		dim as integer window_kill
		for z as integer = 0 to ubound(wind)

			dim as integer w = zOrder(z)

			if w>ubound(wind) then continue for

			with wind(w)

				if .isTimed then

					if timer>.killTime then

						.closing = true

					end if

				end if

				'for the flashing cursor
				.type_xor+=time_step*.type_xor_delta
				if .type_xor>2 then
					.type_xor = 0
				end if

				'keeping the windows in bounds of the screen
				if .closing then
					.dimmer-=time_step*5
					if .dimmer<=0 then
						.do_close = true
					end if
				else
					if .is_focus then
						.dimmer+=time_step*25
						if .dimmer>1 then .dimmer = 1
					else
						.dimmer-=time_step*.25
						if .dimmer<.75 then .dimmer=.75
					end if
				end if

				if .posit.x<0 then
					.posit.x=0
				elseif .posit.x>display.w-.size.x then
					.posit.x=display.w-.size.x
				end if

				if .posit.y<0 then
					.posit.y=0
				elseif .posit.y>display.h-.size.y then
					.posit.y=display.h-.size.y
				end if

				if .visible then
					.wDraw( trimmed() )
				end if

				if .do_close then

					RemoveWindow(w)
					some_action = false
					window_kill = true

					exit sub

				end if

			end with

		next

		'if window_kill then
		'	this.zSort(ubound(wind))
		'end if


	end sub


	'button_struct member funcs
	sub button_struct.SetUserCallback( byval callback as any ptr )

		if this.flag<>guiClose then

			this.OnClick = callback

		end if

	end sub

	sub button_struct.SetUserData( byval uData as any ptr )

		if this.flag<>guiClose then

			this.user_data = uData

		end if

	end sub

	'default callbacks
	function OnClickDefault(byval button as button_struct ptr) as integer

		print #debugout, "Running button On_Click default callback"

		return -1

	end function

	function OnCloseDefault(byval button as button_struct ptr) as integer

		print #debugout, "Running button On_Close default callback"

		button->set_close = true

		return -1

	end function




end namespace
 