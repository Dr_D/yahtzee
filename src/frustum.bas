#include once "frustum.bi"

'RelSoft

namespace nFrustum

	enum E_FRUSTUM_SIDE

	E_RIGHT   = 0        	'' The RIGHT side of the frustum
	E_LEFT    = 1        	'' The LEFT  side of the frustum
	E_BOTTOM  = 2        	'' The BOTTOM side of the frustum
	E_TOP     = 3        	'' The TOP side of the frustum
	E_BACK    = 4        	'' The BACK side of the frustum
	E_FRONT   = 5        	'' The FRONT side of the frustum

	end enum

	enum E_PLANE_DATA

	A = 0              		'' The X value of the plane's normal
	B = 1              		'' The Y value of the plane's normal
	C = 2              		'' The Z value of the plane's normal
	D = 3               	'' The distance the plane is from the origin

	end enum

	'' NORMALIZE PLANE
	'' This normalizes a plane (A side) from a given frustum.
	sub normalizeplane(frustum() as single, byval side as integer)

		'' Here we calculate the magnitude of the normal to the plane (point A B C)
		'' Remember that (A, B, C) is that same thing as the normal's (X, Y, Z).
		'' To calculate magnitude you use the equation:  magnitude = sqrt( x^2 + y^2 + z^2)
		dim as single magnitude = sqr( frustum(side,A) * frustum(side,A) +_
		frustum(side,B) * frustum(side,B) +_
		frustum(side,C) * frustum(side,C) )

		'' Then we divide the plane's values by it's magnitude.
		'' This makes it easier to work with.
		frustum(side,A) /= magnitude
		frustum(side,B) /= magnitude
		frustum(side,C) /= magnitude
		frustum(side,D) /= magnitude

	end sub


	'' CALCULATE FRUSTUM

	'' This extracts our frustum from the projection and modelview matrix.

	sub tfrustum.calculatefrustum()

		dim as single proj(0 to 15)     	  '' This will hold our projection matrix
		dim as single modl(0 to 15)          '' This will hold our modelview matrix
		dim as single clip(0 to 15)          '' This will hold the clipping planes

		'' glGetFloatv() is used to extract information about our OpenGL world.
		'' Below, we pass in GL_PROJECTION_MATRIX to abstract our projection matrix.
		'' It then stores the matrix into an array of [16].
		glGetFloatv( GL_PROJECTION_MATRIX, @proj(0) )

		'' By passing in GL_MODELVIEW_MATRIX, we can abstract our model view matrix.
		'' This also stores it in an array of [16].
		glGetFloatv( GL_MODELVIEW_MATRIX, @modl(0) )

		'' Now that we have our modelview and projection matrix, if we combine these 2 matrices,
		'' it will give us our clipping planes.  To combine 2 matrices, we multiply them.

		clip( 0) = modl( 0) * proj( 0) + modl( 1) * proj( 4) + modl( 2) * proj( 8) + modl( 3) * proj(12)
		clip( 1) = modl( 0) * proj( 1) + modl( 1) * proj( 5) + modl( 2) * proj( 9) + modl( 3) * proj(13)
		clip( 2) = modl( 0) * proj( 2) + modl( 1) * proj( 6) + modl( 2) * proj(10) + modl( 3) * proj(14)
		clip( 3) = modl( 0) * proj( 3) + modl( 1) * proj( 7) + modl( 2) * proj(11) + modl( 3) * proj(15)

		clip( 4) = modl( 4) * proj( 0) + modl( 5) * proj( 4) + modl( 6) * proj( 8) + modl( 7) * proj(12)
		clip( 5) = modl( 4) * proj( 1) + modl( 5) * proj( 5) + modl( 6) * proj( 9) + modl( 7) * proj(13)
		clip( 6) = modl( 4) * proj( 2) + modl( 5) * proj( 6) + modl( 6) * proj(10) + modl( 7) * proj(14)
		clip( 7) = modl( 4) * proj( 3) + modl( 5) * proj( 7) + modl( 6) * proj(11) + modl( 7) * proj(15)

		clip( 8) = modl( 8) * proj( 0) + modl( 9) * proj( 4) + modl(10) * proj( 8) + modl(11) * proj(12)
		clip( 9) = modl( 8) * proj( 1) + modl( 9) * proj( 5) + modl(10) * proj( 9) + modl(11) * proj(13)
		clip(10) = modl( 8) * proj( 2) + modl( 9) * proj( 6) + modl(10) * proj(10) + modl(11) * proj(14)
		clip(11) = modl( 8) * proj( 3) + modl( 9) * proj( 7) + modl(10) * proj(11) + modl(11) * proj(15)

		clip(12) = modl(12) * proj( 0) + modl(13) * proj( 4) + modl(14) * proj( 8) + modl(15) * proj(12)
		clip(13) = modl(12) * proj( 1) + modl(13) * proj( 5) + modl(14) * proj( 9) + modl(15) * proj(13)
		clip(14) = modl(12) * proj( 2) + modl(13) * proj( 6) + modl(14) * proj(10) + modl(15) * proj(14)
		clip(15) = modl(12) * proj( 3) + modl(13) * proj( 7) + modl(14) * proj(11) + modl(15) * proj(15)

		'' Now we actually want to get the sides of the frustum.  To do this we take
		'' the clipping planes we received above and extract the sides from them.

		'' This will extract the RIGHT side of the frustum
		m_Frustum(E_RIGHT,A) = clip( 3) - clip( 0)
		m_Frustum(E_RIGHT,B) = clip( 7) - clip( 4)
		m_Frustum(E_RIGHT,C) = clip(11) - clip( 8)
		m_Frustum(E_RIGHT,D) = clip(15) - clip(12)

		'' Now that we have a normal (A,B,C) and a distance (D) to the plane,
		'' we want to normalize that normal and distance.

		'' Normalize the RIGHT side
		normalizeplane(m_Frustum(), E_RIGHT)

		'' This will extract the LEFT side of the frustum
		m_Frustum(E_LEFT,A) = clip( 3) + clip( 0)
		m_Frustum(E_LEFT,B) = clip( 7) + clip( 4)
		m_Frustum(E_LEFT,C) = clip(11) + clip( 8)
		m_Frustum(E_LEFT,D) = clip(15) + clip(12)

		'' Normalize the LEFT side
		normalizeplane(m_Frustum(), E_LEFT)

		'' This will extract the BOTTOM side of the frustum
		m_Frustum(E_BOTTOM,A) = clip( 3) + clip( 1)
		m_Frustum(E_BOTTOM,B) = clip( 7) + clip( 5)
		m_Frustum(E_BOTTOM,C) = clip(11) + clip( 9)
		m_Frustum(E_BOTTOM,D) = clip(15) + clip(13)

		'' Normalize the BOTTOM side
		normalizeplane(m_Frustum(), E_BOTTOM)

		'' This will extract the TOP side of the frustum
		m_Frustum(E_TOP,A) = clip( 3) - clip( 1)
		m_Frustum(E_TOP,B) = clip( 7) - clip( 5)
		m_Frustum(E_TOP,C) = clip(11) - clip( 9)
		m_Frustum(E_TOP,D) = clip(15) - clip(13)

		'' Normalize the TOP side
		normalizeplane(m_Frustum(), E_TOP)

		'' This will extract the BACK side of the frustum
		m_Frustum(E_BACK,A) = clip( 3) - clip( 2)
		m_Frustum(E_BACK,B) = clip( 7) - clip( 6)
		m_Frustum(E_BACK,C) = clip(11) - clip(10)
		m_Frustum(E_BACK,D) = clip(15) - clip(14)

		'' Normalize the BACK side
		normalizeplane(m_Frustum(), E_BACK)

		'' This will extract the FRONT side of the frustum
		m_Frustum(E_FRONT,A) = clip( 3) + clip( 2)
		m_Frustum(E_FRONT,B) = clip( 7) + clip( 6)
		m_Frustum(E_FRONT,C) = clip(11) + clip(10)
		m_Frustum(E_FRONT,D) = clip(15) + clip(14)

		'' Normalize the FRONT side
		normalizeplane(m_Frustum(), E_FRONT)

	end sub

	'' The code below will allow us to make checks within the frustum.  For example,
	'' if we want to see if a point, a sphere, or a cube lies inside of the frustum.
	'' Because all of our planes point INWARDS (The normals are all pointing inside the frustum)
	'' we then can assume that if a point is in FRONT of all of the planes, it's inside.

	'' POINT IN FRUSTUM

	'' This determines if a point is inside of the frustum

	function tfrustum.pointinfrustum( byval x as single, byval y as single, byval z as single ) as integer

		'' Go through all the sides of the frustum

		for i as integer = 0 to 5
			'' Calculate the plane equation and check if the point is behind a side of the frustum
			if (m_Frustum(i,A) * x + m_Frustum(i,B) * y + m_Frustum(i,C) * z + m_Frustum(i,D) <= 0) then
				'' The point was behind a side, so it ISN'T in the frustum
				return false
			end if
		next i

		'' The point was inside of the frustum (In front of ALL the sides of the frustum)
		return true

	end function


	'' SPHERE IN FRUSTUM

	'' This determines if a sphere is inside of our frustum by it's center and radius.

	function tfrustum.sphereinfrustum( byval x as single, byval y as single, byval z as single , byval  radius  as single) as integer

		'' Go through all the sides of the frustum

		for i as integer = 0 to 5
			'' If the center of the sphere is farther away from the plane than the radius
			if (m_Frustum(i,A) * x + m_Frustum(i,B) * y + m_Frustum(i,C) * z + m_Frustum(i,D) <= -radius ) then
				'' The distance was greater than the radius so the sphere is outside of the frustum
				return false
			end if
		next i

		'' The sphere was inside of the frustum!
		return true

	end function


	'' CUBE IN FRUSTUM

	''  This determines if a cube is in or around our frustum by it's center and 1/2 it's length
	function tfrustum.cubeinfrustum( byref centroid as nmathf.vec3f, byval size as nmathf.vec3f ) as integer

		'' Basically, what is going on is, that we are given the center of the cube,
		'' and half the length.  Think of it like a radius.  Then we checking each point
		'' in the cube and seeing if it is inside the frustum.  If a point is found in front
		'' of a side, then we skip to the next side.  If we get to a plane that does NOT have
		'' a point in front of it, then it will return false.

		'' *Note* - This will sometimes say that a cube is inside the frustum when it isn't.
		'' This happens when all the corners of the bounding box are not behind any one plane.
		'' This is rare and shouldn't effect the overall rendering speed.

		' the "i<>4" check just skips the back plane, since we're using an infinite projection matrix.
		dim as single x = centroid.x
		dim as single y = centroid.y
		dim as single z = centroid.z
		dim as single sx = size.x
		dim as single sy = size.y
		dim as single sz = size.z


		for i as integer = 0 to 5

			'if i=4 then goto cont

			if(m_Frustum(i,A) * (x - size.x) + m_Frustum(i,B) * (y - size.y) + m_Frustum(i,C) * (z - size.z) + m_Frustum(i,D) > 0) then
				goto cont
			end if

			if(m_Frustum(i,A) * (x + size.x) + m_Frustum(i,B) * (y - size.y) + m_Frustum(i,C) * (z - size.z) + m_Frustum(i,D) > 0) then
				goto cont
			end if

			if(m_Frustum(i,A) * (x - size.x) + m_Frustum(i,B) * (y + size.y) + m_Frustum(i,C) * (z - size.z) + m_Frustum(i,D) > 0) then
				goto cont
			end if

			if(m_Frustum(i,A) * (x + size.x) + m_Frustum(i,B) * (y + size.y) + m_Frustum(i,C) * (z - size.z) + m_Frustum(i,D) > 0) then
				goto cont
			end if

			if(m_Frustum(i,A) * (x - size.x) + m_Frustum(i,B) * (y - size.y) + m_Frustum(i,C) * (z + size.z) + m_Frustum(i,D) > 0) then
				goto cont
			end if

			if(m_Frustum(i,A) * (x + size.x) + m_Frustum(i,B) * (y - size.y) + m_Frustum(i,C) * (z + size.z) + m_Frustum(i,D) > 0) then
				goto cont
			end if

			if(m_Frustum(i,A) * (x - size.x) + m_Frustum(i,B) * (y + size.y) + m_Frustum(i,C) * (z + size.z) + m_Frustum(i,D) > 0) then
				goto cont
			end if

			if(m_Frustum(i,A) * (x + size.x) + m_Frustum(i,B) * (y + size.y) + m_Frustum(i,C) * (z + size.z) + m_Frustum(i,D) > 0) then
				goto cont
			end if


			'' If we get here, it isn't in the frustum
			return false

			cont:

		next i

		return true

	end function

end namespace
