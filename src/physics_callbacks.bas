#include once "physics_callbacks.bi"

extern h_table	as nhash.ht_t ptr


namespace nPhysics


	function character_ray_filter cdecl(byval nBody as NewtonBody ptr, byval Normal as dFloat ptr, byval Collision_ID as integer, byval User_Data as any ptr, byval Intersect_Param as dFloat) as dFloat
		
		static as integer first_run
		static as uinteger INVISIBLE
		
		if not first_run then
			
			first_run = true
			
			dim as string temp = "invisible"
			INVISIBLE = h_table->lookup( strptr(temp) )->id
			
		end if		
		
		
		dim as dFloat Mass, Mx, My, Mz
		dim as Ray_Struct ptr Ray
		dim as Entity_Struct ptr Entity

		NewtonBodyGetMassMatrix( nBody, @Mass, @Mx, @My, @Mz)
		'Collision trees are always processed with 0 mass.
		'If the object that this ray hits has mass, then we know it's a rigid body model,
		'and not the terrain model.
		if Mass > 0 then

			Entity = NewtonBodyGetUserData( nBody )

			Ray              	= User_Data
			Ray->Param       	= Intersect_Param
			Ray->Normal.X    	= Normal[0]
			Ray->Normal.Y    	= Normal[1]
			Ray->Normal.Z    	= Normal[2]
			Ray->nBody 			= nBody

			return Ray->Param

		elseif Mass = 0 then
			
			Ray              	= User_Data
			Ray->Param       	= Intersect_Param
			Ray->Normal.X    	= Normal[0]
			Ray->Normal.Y    	= Normal[1]
			Ray->Normal.Z    	= Normal[2]
			Ray->Collision_ID = Collision_ID
			Ray->nBody 			= 0
			
			
			if Ray->Collision_ID = INVISIBLE then
				
				Ray = 0
				
				return 1.0
				
			else
				
				return Ray->Param
				
			end if

		else

			return 1.0

		end if

	end function


	function Vehicle_World_Contact_Begin cdecl( byval Material as NewtonMaterial ptr, byval rBody1 as NewtonBody ptr, byval rBody2 as NewtonBody ptr ) as integer

		'print #debugout, "Vehicle->Land collision callback called"

		return 1

	end function


	function Vehicle_World_Contact_Process cdecl( byval Material as NewtonMaterial ptr, byval Contact as NewtonContact ptr ) as integer

		static as integer first_run
		static as integer start_ID, road_ID, grass_ID, grass2road_ID, 	gravel_ID, guard_ID

		if not first_run then

			first_run = true

			dim as string temp

			temp = "road"
			road_ID = h_table->lookup( strptr(temp) )->id

			temp = "grass"
			grass_ID = h_table->lookup( strptr(temp) )->id

			temp = "grass2road"
			grass2road_ID = h_table->lookup( strptr(temp) )->id

			temp = "gravel"
			gravel_ID = h_table->lookup( strptr(temp) )->id

			temp = "guard"
			guard_ID = h_table->lookup( strptr(temp) )->id

		end if

		dim as dFloat ConTang1, ConTang2, Mag, NormSpeed1, NormSpeed2
		dim as nmathf.vec3f contact_position, contact_normal

		dim as Vehicle_Struct ptr Vehicle = NewtonMaterialGetMaterialPairUserData( Material )
		dim as integer Contact_Surface = NewtonMaterialGetContactFaceAttribute( Material )
		dim as integer TireID = NewtonMaterialGetBodyCollisionID( Material, Vehicle->Body )
		dim as integer tireContact = TireID>0 and TireID<=ubound(vehicle->tire)+1
		if tireContact then
			TireID-=1
		end if

		dim as single ImpactForce = NewtonMaterialGetContactNormalSpeed( Material, Contact )
		NewtonMaterialGetContactPositionAndNormal( Material, @contact_position.x, @contact_normal.x )

		'lookup the actual name of the surface we are in contact with from the global hash table
		'Vehicle->contact_string = *h_table->lookup( contact_surface )->s

		dim as integer contact_ID = h_table->lookup( contact_surface )->id

		with *vehicle

			.is_airborn = true

			'we should be able to process some scripts here...
			select case contact_ID'Vehicle->contact_string

				'case "start"


				'case "road", "grass2road"
				case road_ID, grass2road_ID

					.is_airborn = false
					.contact_surf = cROAD

					if not tireContact then

						NewtonMaterialSetContactFrictionState( material, 1, 0 )
						NewtonMaterialSetContactFrictionState( material, 1, 1 )

						NewtonMaterialSetContactStaticFrictionCoef( material, .97, 0 )
						NewtonMaterialSetContactStaticFrictionCoef( material, .968, 1 )

						NewtonMaterialSetContactKineticFrictionCoef( material, .94, 0 )
						NewtonMaterialSetContactKineticFrictionCoef( material, .93, 1 )

						NewtonMaterialSetContactSoftness( material, .1 )
						NewtonMaterialSetContactElasticity( material, .15 )

					else

						'old values
						'NewtonMaterialSetContactFrictionState( material, 1, 0 )
						'NewtonMaterialSetContactFrictionState( material, 1, 1 )

						'NewtonMaterialSetContactStaticFrictionCoef( material, .7, 0 )
						'NewtonMaterialSetContactStaticFrictionCoef( material, .68, 1 )

						'NewtonMaterialSetContactKineticFrictionCoef( material, .64, 0 )
						'NewtonMaterialSetContactKineticFrictionCoef( material, .63, 1 )

						'NewtonMaterialSetContactSoftness( material, .5 )
						'NewtonMaterialSetContactElasticity( material, .25 )

						'test values
						NewtonMaterialSetContactFrictionState( material, 1, 0 )
						NewtonMaterialSetContactFrictionState( material, 1, 1 )

						NewtonMaterialSetContactStaticFrictionCoef( material, .95, 0 )
						NewtonMaterialSetContactStaticFrictionCoef( material, .9, 1 )

						NewtonMaterialSetContactKineticFrictionCoef( material, .85, 0 )
						NewtonMaterialSetContactKineticFrictionCoef( material, .8, 1 )

						NewtonMaterialSetContactSoftness( material, .05 )
						NewtonMaterialSetContactElasticity( material, .1 )


						with .tire( TireID )
							.contact_surf = cROAD
							if .is_sliding then
								.add_particle = true
								.particle.position = contact_position+nMathf.vec3f(0f,.5+rnd*.5f,0f)
								.particle.prop = PARTICLE_SMOKE
							end if
						end with

					end if

					'textout += "road"


				case grass_ID
					'case "grass"

					.is_airborn = false
					.contact_surf = cGRASS

					if not tireContact then

						NewtonMaterialSetContactFrictionState( material, 1, 0 )
						NewtonMaterialSetContactFrictionState( material, 1, 1 )

						NewtonMaterialSetContactStaticFrictionCoef( material, .96, 0 )
						NewtonMaterialSetContactStaticFrictionCoef( material, .958, 1 )

						NewtonMaterialSetContactKineticFrictionCoef( material, .955, 0 )
						NewtonMaterialSetContactKineticFrictionCoef( material, .954, 1 )

						NewtonMaterialSetContactSoftness( material, .95 )
						NewtonMaterialSetContactElasticity( material, .45 )

					else

						'old values
						NewtonMaterialSetContactFrictionState( material, 1, 0 )
						NewtonMaterialSetContactFrictionState( material, 1, 1 )

						NewtonMaterialSetContactStaticFrictionCoef( material, .6, 0 )
						NewtonMaterialSetContactStaticFrictionCoef( material, .58, 1 )

						NewtonMaterialSetContactKineticFrictionCoef( material, .55, 0 )
						NewtonMaterialSetContactKineticFrictionCoef( material, .54, 1 )

						NewtonMaterialSetContactSoftness( material, .5 )
						NewtonMaterialSetContactElasticity( material, .15 )

						with .tire( TireID )
							.contact_surf = cGRASS
							if .is_sliding then
								.add_particle = true
								.particle.position = contact_position+nMathf.vec3f(0f,.5+rnd*.5f,0f)
								.particle.prop = PARTICLE_DUST
							end if
						end with

					end if

				case gravel_ID
					'case "gravel"

					.is_airborn = false
					.contact_surf = cGRAVEL

					if not tireContact then

						NewtonMaterialSetContactFrictionState( material, 1, 0 )
						NewtonMaterialSetContactFrictionState( material, 1, 1 )

						NewtonMaterialSetContactStaticFrictionCoef( material, .96, 0 )
						NewtonMaterialSetContactStaticFrictionCoef( material, .958, 1 )

						NewtonMaterialSetContactKineticFrictionCoef( material, .955, 0 )
						NewtonMaterialSetContactKineticFrictionCoef( material, .954, 1 )

						NewtonMaterialSetContactSoftness( material, .95 )
						NewtonMaterialSetContactElasticity( material, .45 )

					else

						'old values
						'NewtonMaterialSetContactFrictionState( material, 1, 0 )
						'NewtonMaterialSetContactFrictionState( material, 1, 1 )

						'NewtonMaterialSetContactStaticFrictionCoef( material, .4, 0 )
						'NewtonMaterialSetContactStaticFrictionCoef( material, .38, 1 )

						'NewtonMaterialSetContactKineticFrictionCoef( material, .45, 0 )
						'NewtonMaterialSetContactKineticFrictionCoef( material, .44, 1 )

						'NewtonMaterialSetContactSoftness( material, .75 )
						'NewtonMaterialSetContactElasticity( material, .1 )

						'test values
						NewtonMaterialSetContactFrictionState( material, 1, 0 )
						NewtonMaterialSetContactFrictionState( material, 1, 1 )

						NewtonMaterialSetContactStaticFrictionCoef( material, .7, 0 )
						NewtonMaterialSetContactStaticFrictionCoef( material, .68, 1 )

						NewtonMaterialSetContactKineticFrictionCoef( material, .64, 0 )
						NewtonMaterialSetContactKineticFrictionCoef( material, .63, 1 )

						NewtonMaterialSetContactSoftness( material, .5 )
						NewtonMaterialSetContactElasticity( material, .15 )

						with .tire( TireID )
							.contact_surf = cGRAVEL
							if .is_sliding then
								.add_particle = true
								.particle.position = contact_position+nMathf.vec3f(0f,.5+rnd*.5f,0f)
								.particle.prop = PARTICLE_GRAVEL
							end if
						end with

					end if

				case guard_ID
					'case "guard"

					.is_airborn = false
					.contact_surf = cROAD

					if not tireContact then

						NewtonMaterialSetContactFrictionState( material, 1, 0 )
						NewtonMaterialSetContactFrictionState( material, 1, 1 )

						NewtonMaterialSetContactStaticFrictionCoef( material, .97, 0 )
						NewtonMaterialSetContactStaticFrictionCoef( material, .968, 1 )

						NewtonMaterialSetContactKineticFrictionCoef( material, .94, 0 )
						NewtonMaterialSetContactKineticFrictionCoef( material, .93, 1 )

						NewtonMaterialSetContactSoftness( material, .1 )
						NewtonMaterialSetContactElasticity( material, .15 )

					else

						NewtonMaterialSetContactFrictionState( material, 0, 0 )
						NewtonMaterialSetContactFrictionState( material, 0, 1 )

						NewtonMaterialSetContactStaticFrictionCoef( material, .0007, 0 )
						NewtonMaterialSetContactStaticFrictionCoef( material, .0006, 1 )

						NewtonMaterialSetContactKineticFrictionCoef( material, .0005, 0 )
						NewtonMaterialSetContactKineticFrictionCoef( material, .0004, 1 )

						NewtonMaterialSetContactSoftness( material, .95 )
						NewtonMaterialSetContactElasticity( material, .05 )

						with .tire( TireID )
							.contact_surf = cROAD
							if .is_sliding then
								.add_particle = true
								.particle.position = contact_position+nMathf.vec3f(0f,.5+rnd*.5f,0f)
								.particle.prop = PARTICLE_SMOKE
							end if
						end with

					end if


				case else

					.is_airborn = false

					if not tireContact then

					else

						with .tire( TireID )
							.contact_surf = cROAD
							if .is_sliding then
								'.is_sliding = false
								.add_particle = true
								.particle.position = contact_position+nMathf.vec3f(0f,.5+rnd*.5f,0f)
								.particle.prop = PARTICLE_SMOKE
								'print #debugout, TireID
								'vehicle->test_string = "On the road! :)"
							end if
						end with

					end if


			end select

			if not tireContact then

				if ImpactForce>7 then
					.in_crash = true
				end if

			else

				if ImpactForce>15 then
					.in_crash = true
				end if

			end if


		end with

		return 1

	end function

	sub Vehicle_World_Contact_End cdecl( byval Material as NewtonMaterial ptr )

		'dim as vehicle_struct ptr Vehicle = NewtonMaterialGetMaterialPairUserData( Material )

	end sub


	'Vehicle to Vehicle Collision callbacks
	function Vehicle_Vehicle_Contact_Begin cdecl( byval Material as NewtonMaterial ptr, byval rBody1 as NewtonBody ptr, byval rBody2 as NewtonBody ptr ) as integer


		'print #debugout, "Vehicle->Land collision callback called"

		return 1

	end function

	function Vehicle_Vehicle_Contact_Process cdecl( byval Material as NewtonMaterial ptr, byval Contact as NewtonContact ptr ) as integer

		dim as vehicle_struct ptr Vehicle = NewtonMaterialGetMaterialPairUserData( Material )
		dim as integer TireID = NewtonMaterialGetBodyCollisionID( Material, Vehicle->Body )
		dim as integer tireContact = TireID>0 and TireID<=ubound(vehicle->tire)+1
		if tireContact then
			TireID-=1
		end if

		dim as single ImpactForce = NewtonMaterialGetContactNormalSpeed( Material, Contact )

		if ImpactForce>10 then
			vehicle->in_crash = true
		end if

		if tireContact then
			'if abs(ImpactForce)>3 then
			'print #debugout, "TIRE!"
			'print #debugout, ImpactForce
			'vehicle->in_crash = true
			'end if
		end if


		return 1

	end function

	sub Vehicle_Vehicle_Contact_End cdecl( byval Material as NewtonMaterial ptr )

		'dim as vehicle_struct ptr Vehicle = NewtonMaterialGetMaterialPairUserData( Material )

	end sub


	sub Tire_Callback cdecl( byval vJoint as NewtonJoint ptr )

		dim as any ptr Tire_ID = 0
		dim as integer CurEntry = any, Percent = any, tRPM = any
		dim as integer Is_Airborn, anySliding

		dim as single Steer_Angle = any, Load = any, Speed = any, Brake_Accel= any
		dim as single LongitudinalSpeed = any, LateralSpeed = any, TireSpeed = any

		dim as single TireOmega = any, tempOmega = any, driveRPM = any
		dim as Vehicle_Struct ptr Vehicle = NewtonJointGetUserData( vJoint )

		dim as single TransPreCalc = any
		dim as single OmegaPreCalc = any

		dim as integer airBorn = any, tirecnt = any, allairBorn = true


		Tire_ID = NewtonVehicleGetFirstTireID( vJoint )

		with *vehicle

			.is_sliding = false

			with .transmission
				TransPreCalc = .Gear_Ratio(.Gear) * .differential_Ratio * .efficiency
				OmegaPreCalc = .Gear_Ratio(.Gear) * .differential_Ratio * (60 / nMathf.Pi2)
			end with

			do

				Steer_Angle = .Steer_Angle
				'somehow this gets the wrong pointer...
				'we have to do the part after the commented code to  get the tire
				'dim as tire_struct ptr Otire = NewtonVehicleGetTireUserData( vJoint, Tire_ID )
				if tirecnt<0 or tirecnt>ubound(.tire) then exit do
				dim as tire_struct ptr tire = @.tire(tirecnt)
				if tire = 0 then exit sub

				airBorn = NewtonVehicleTireIsAirBorne( vJoint, Tire_ID )
				if airBorn = 0 then
					allAirBorn = false
				end if

				LongitudinalSpeed = abs(NewtonVehicleGetTireLongitudinalSpeed ( vJoint, Tire_Id))
				LateralSpeed = abs(NewtonVehicleGetTireLateralSpeed ( vJoint, Tire_Id))
				TireSpeed = LateralSpeed * LateralSpeed + LongitudinalSpeed * LongitudinalSpeed
				Load = NewtonVehicleGetTireNormalLoad( vJoint, Tire_ID )
				TireOmega = NewtonVehicleGetTireOmega( vJoint, Tire_ID )
				tempOmega = abs(TireOmega)

				NewtonVehicleSetTireMaxLongitudinalSlideSpeed( vJoint, Tire_ID, LongitudinalSpeed *  tire->longGrip )
				NewtonVehicleSetTireLongitudinalSlideCoeficient( vJoint, Tire_ID, (LongitudinalSpeed * Load) *  tire->longGripCo )

				NewtonVehicleGetTireMatrix( vJoint, Tire_ID, tire->tMatrix )
				tire->rotMatrix = tire->tMatrix
				tire->rotMatrix.position = nmathf.vec3f(0,0,0)

				if tire->Steer_Mode = 1 then

					select case as const tire->prop

						case TIRE_FR
							if Steer_Angle<0 then
								Steer_Angle*=1.25f
							end if
						case TIRE_FL
							if Steer_Angle>0 then
								Steer_Angle*=1.25f
							end if
					end select

					NewtonVehicleSetTireSteerAngle( vJoint, Tire_ID, Steer_Angle )

					if tempOmega>=1.0 then
						dim as single slipAngle = atan2 (lateralSpeed, longitudinalSpeed)
						slipAngle += NewtonVehicleGetTireSteerAngle (vJoint, Tire_ID)
						'NewtonVehicleSetTireMaxSideSleepSpeed ( vJoint, Tire_ID, TireSpeed * tire->latGrip )
						NewtonVehicleSetTireMaxSideSleepSpeed ( vJoint, Tire_ID, TireSpeed * tempOmega * tire->latGrip )
						NewtonVehicleSetTireSideSleepCoeficient( vJoint, Tire_ID, (load * lateralspeed *TireSpeed) * tire->latGripCo )
					end if

					tire->is_sliding = false

					if NewtonVehicleTireLostSideGrip( vJoint, Tire_ID) then
						if airBorn = 0 then
							tire->is_sliding = true
							.is_sliding = true
						end if
					end if

					if NewtonVehicleTireLostTraction( vJoint, Tire_ID ) then
						if airBorn = 0 then
							tire->is_sliding = true
						end if
					end if

				else

					if tempOmega>=1.0 then
						'if load>=1.0 then
						dim as single slipAngle = atan2 (lateralSpeed, longitudinalSpeed)
						'NewtonVehicleSetTireMaxSideSleepSpeed ( vJoint, Tire_ID, TireSpeed * tire->latGrip )
						NewtonVehicleSetTireMaxSideSleepSpeed ( vJoint, Tire_ID, TireSpeed * tempOmega * tire->latGrip )
						NewtonVehicleSetTireSideSleepCoeficient( vJoint, Tire_ID, (load * lateralspeed * TireSpeed) * tire->latGripCo )
						'end if
					end if


					dim as integer curEntry = 0
					dim as single rpm

					'if abs(tireOmega)>.1 Then
					rpm = tireOmega * OmegaPreCalc
					'else
					'rpm = 20f * Vehicle->transmission.gear_Ratio(Vehicle->transmission.Gear) * Vehicle->transmission.differential_Ratio * (60 / nMathf.Pi2)
					'end If

					If RPM>6999 Then RPM = 6999

					Select Case RPM
						Case 1 To 999
							CurEntry = 0
						case 1000 To 2999
							CurEntry = 1
						case 3000 To 4999
							CurEntry = 2
						case 5000 To 6999
							CurEntry = 3
							'Case 4000 To 4999
							'   CurEntry = 4
							'Case 5000 To 5999
							'CurEntry = 5
					End select

					driveRPM+=rpm

					dim as single Max_Torque = .transmission.torque_Curve( CurEntry )
					dim as single Engine_Torque = ((.Throttle * Max_Torque)/100f)
					dim as single Drive_Torque = Engine_Torque * TransPreCalc

					'wind resistance in the vehicle force/torque callback should take care of limiting speed
					'i haven't had a problem with excessive torque since implementing that,
					'so this next block seems to be redundant
					'if abs(tireOmega)>=80 then
					'	Drive_Torque = 0
					'end if

					If .transmission.reverse Then
						Drive_Torque = -Drive_Torque
					end if

					tire->is_sliding = false

					if airborn = 0 then

						if .throttle>0 then
							NewtonVehicleSetTireTorque( vJoint, Tire_ID, Drive_Torque )
						end if

						if NewtonVehicleTireLostSideGrip( vJoint, Tire_ID) then
							if airBorn = 0 then
								tire->is_sliding = true
								.is_sliding = true
							end if
						end if

					else

						NewtonVehicleSetTireTorque( vJoint, Tire_ID, 0 )

					end if

					if NewtonVehicleTireLostTraction( vJoint, Tire_ID ) then
						if airBorn = 0 then
							tire->is_sliding = true
						end if
					end if

				end if

				if .Brake then
					Brake_Accel = NewtonVehicleTireCalculateMaxBrakeAcceleration( vJoint, Tire_ID )
					NewtonVehicleTireSetBrakeAcceleration( vJoint, Tire_ID, Brake_Accel,  abs(Load*.25f) )
				end if


				if .critical_brake then
					Brake_Accel = NewtonVehicleTireCalculateMaxBrakeAcceleration( vJoint, Tire_ID )
					NewtonVehicleTireSetBrakeAcceleration( vJoint, Tire_ID, Brake_Accel,  .mass^2 )
				end if

				Tire_ID = NewtonVehicleGetNextTireID( vJoint, Tire_ID )
				tirecnt+=1

			loop while Tire_ID <> 0

			.engine_torque = driveRPM/2f


			if allAirBorn = true then

				if .play_celebrate = 0 then
					.play_celebrate = 1
				end if

			else

				.play_celebrate = 0

			end if

		end with

	end sub




	sub entity_Leave_World cdecl(byval rBody as NewtonBody ptr)
		'not used yet...
		dim as entity_struct ptr entity
		entity = NewtonBodyGetUserData( rBody )

		'#ifdef _debugout
		'print #debugout, entity->eName & " left the world!"
		'#endif

	end sub

	sub generic_physics_transform cdecl( byval rBody as NewtonBody ptr, byval mMatrix as dFloat ptr )

		dim as entity_struct ptr entity = NewtonBodyGetUserData( rBody )

		with *entity

			memcpy( .tMatrix, mMatrix, nmathf.MAT_COPY_SIZE )

			.bMatrix.position = .tMatrix.position
			.rotMatrix = .tMatrix
			.rotMatrix.position = nmathf.vec3f(0,0,0)

		end with

	end sub


	sub vehicle_physics_transform cdecl( byval rBody as NewtonBody ptr, byval mMatrix as dFloat ptr )

		dim as vehicle_struct ptr vehicle = NewtonBodyGetUserData( rBody )

		with *vehicle

			.last_position = .tMatrix.position
			memcpy( .tMatrix, mMatrix, nmathf.MAT_COPY_SIZE )
			.rotMatrix = .tMatrix
			.rotMatrix.position = nmathf.vec3f(0,0,0)

		end with

	end sub

	sub vehicle_force_torque cdecl( byval rBody as NewtonBody ptr )

		dim as Vehicle_Struct ptr Vehicle = NewtonBodyGetUserData( rBody )

		with *vehicle

			dim as dFloat Mass, Ixx, Iyy, Izz
			NewtonBodyGetMassMatrix( RBody, @Mass, @Ixx, @Iyy, @Izz )

			NewtonBodyGetVelocity( rBody, @.velocity.x )
			dim as single velMag = .velocity.magnitude*.Wind_Resistance
			dim as nmathf.vec3f resistance = .velocity*-velMag

			dim as nmathf.vec3f Omega
			NewtonBodyGetOmega( rBody, @Omega.x )
			Omega += .External_Omega

			NewtonBodySetOmega( rBody, @Omega.x )

			var Force = nmathf.vec3f(0, -Mass*Gravity, 0 )
			Force += .External_Force
			NewtonBodySetForce( rBody, @Force.x )

			NewtonBodyAddForce( rBody, @resistance.x )

			Vehicle->External_Force = nmathf.vec3f(0f,0f,0f)
			Vehicle->External_Omega = nmathf.vec3f(0f,0f,0f)


			select case vehicle->velocity.magnitude

				case 0 to 12
					.transmission.gear = 0
				case 13 to 19
					.transmission.gear = 1
				case 20 to 29
					.transmission.gear = 2
				case is > 29
					.transmission.gear = 3

			end select

			if .transmission.reverse then
				.transmission.gear = 0
			end if

		end with

	end sub



	sub character_controller cdecl( byval rBody as NewtonBody ptr )

		dim as dFloat Mass, Ixx, Iyy, Izz
		NewtonBodyGetMassMatrix( RBody, @Mass, @Ixx, @Iyy, @Izz )

		dim entity as entity_Struct ptr
		entity = NewtonBodyGetUserData( rBody )
		var velocity = nmathf.vec3f(0f,0f,0f)
		NewtonBodyGetVelocity( rBody, @velocity.x )
		'dim as single vel_mag = velocity.magnitude
		entity->velocity = velocity

		var Gravity_Force = nmathf.vec3f(0f, -Mass*Gravity, 0f)
		NewtonBodySetForce( rBody, @Gravity_Force.x )
		NewtonBodyAddForce( rBody, @entity->external_force.x )

		NewtonBodyAddTorque( rBody, @entity->external_omega.x )

		'We need to zero out the external forces here,
		'so it doesn't keep adding it on each update.
		entity->External_Force = nmathf.vec3f(0f,0f,0f)
		entity->External_Omega = nmathf.vec3f(0f,0f,0f)

		'if char->in_water = 1 then
		'	Gravity_Force*=-1.15
		'	if char->tMatrix.position.y<char->foul_pos.y then
		'		NewtonBodyAddForce( RBody, @Gravity_Force.x )
		'		NewtonBodyGetVelocity( rBody, @Gravity_Force.x)
		'		Gravity_Force.x*=.95
		'		Gravity_Force.z*=.95
		'		NewtonBodySetVelocity( rBody, @Gravity_Force.x)
		'	end if
		'end if

		'if char->reset_velocities <> 0 then
		'	char->reset_velocities = 0
		'	Gravity_Force*=0
		'	NewtonBodySetForce( rBody, @Gravity_Force.x )
		'	NewtonBodySetTorque( rBody, @Gravity_Force.x )
		'	NewtonBodySetOmega( rBody, @Gravity_Force.x )
		'	NewtonBodySetVelocity( rBody, @Gravity_Force.x )

		'end if

		'print #debugout, "character controller callback called"


	end sub


	sub camera_controller cdecl( byval rBody as NewtonBody ptr )

		dim as newtonWorld ptr nWorld = NewtonBodyGetWorld( rBody )

		dim as single tStep = NewtonGetTimeStep(nWorld)

		dim as dFloat Mass, Ixx, Iyy, Izz
		NewtonBodyGetMassMatrix( rBody, @Mass, @Ixx, @Iyy, @Izz )

		dim as camera_struct ptr camera
		camera = NewtonBodyGetUserData( rBody )

		dim as nmathf.Matrix tMatrix
		NewtonBodyGetMatrix( rBody, tMatrix )

		dim as nmathf.vec3f tPos = tMatrix.Position
		dim as nmathf.vec3f tVel
		tVel = (camera->cTarg-tPos) * (1000f * tStep)
		'tVel = (camera->cTarg-tPos)*(200f*tStep)

		NewtonBodySetVelocity( rBody, @tVel.x )
		'NewtonBodyAddForce( rBody, @tVel.x)


		'NewtonBodyGetVelocity( rBody, @camera->velocity.x )
		'NewtonBodyAddForce( rBody, @tvel.x )
		'print #debugout, "camera controller callback called"

	end sub

	sub camera_physics_transform cdecl( byval rBody as NewtonBody ptr, byval mMatrix as dFloat ptr )

		dim as camera_struct ptr camera
		camera = NewtonBodyGetUserData( rBody )
		memcpy( camera->tMatrix, mMatrix, nmathf.MAT_COPY_SIZE )

		'entity->bMatrix.position = entity->tMatrix.position
		'print #debugout, "generic transform callback called on, " & entity->eName

	end sub



	sub world_physics_transform cdecl( byval rBody as NewtonBody ptr, byval mMatrix as dFloat ptr )

		dim as world_struct ptr World
		World = NewtonBodyGetUserData( rBody )
		memcpy( world->wMatrix, mMatrix, nmathf.MAT_COPY_SIZE )

	end sub

	sub generic_force_torque cdecl( byval RBody as NewtonBody ptr )

		dim as dFloat Mass, Ixx, Iyy, Izz
		NewtonBodyGetMassMatrix( RBody, @Mass, @Ixx, @Iyy, @Izz )

		dim as entity_Struct ptr entity
		entity = NewtonBodyGetUserData( RBody )

		dim as nmathf.vec3f Omega
		NewtonBodyGetOmega( rBody, @Omega.x )
		Omega += entity->External_Omega

		NewtonBodySetOmega(RBody, @Omega.x)

		var Force = nmathf.vec3f(0, -Mass*Gravity, 0 )
		Force += entity->External_Force
		NewtonBodySetForce( rBody, @Force.x )
		NewtonBodyAddForce( rBody, @entity->external_force.x )


		'We need to zero out the external forces here,
		'so it doesn't keep adding it on each update.
		entity->External_Force = nmathf.vec3f(0f,0f,0f)
		entity->External_Omega = nmathf.vec3f(0f,0f,0f)

	end sub

	'_______________________________________________________________________________





	sub rolling_friction_callback cdecl( byval rjoint as NewtonJoint ptr )

		dim as entity_struct ptr entity = NewtonJointGetUserData( rJoint )

		dim as nmathf.vec3f Omega, Pin
		dim as dFloat OmegaMag, nTime, TorqueFriction, Mass, Ixx, Iyy, Izz

		dim as dFloat frictionTorque, frictionCoef

		NewtonBodyGetOmega(entity->Body, @Omega.X )
		OmegaMag = sqr( Omega.dot(Omega) )

		if OmegaMag>.001 then
			pin = omega / omegaMag

			NewtonUserJointAddAngularRow ( rjoint, 0.0f, @Pin.X )

			dim as NewtonWorld ptr nWorld = NewtonBodyGetWorld( entity->Body )
			nTime = NewtonGetTimeStep ( nworld )
			NewtonUserJointSetRowAcceleration ( rjoint, -omegaMag / nTime )

			NewtonBodyGetMassMatrix( entity->Body, @Mass, @Ixx, @Iyy, @Izz )
			frictionCoef = Mass*nTime
			frictionTorque = 0.4f*Ixx*entity->radius*entity->radius

			torqueFriction = frictionTorque * frictionCoef
			NewtonUserJointSetRowMinimumFriction ( rjoint, -torqueFriction )
			NewtonUserJointSetRowMaximumFriction ( rjoint,  torqueFriction )
		else

			Omega.X *=.2
			Omega.Y *=.2
			Omega.Z *=.2
			NewtonBodySetOmega( entity->Body, @Omega.X )

		end if

	end sub



	function Character_World_Contact_Begin cdecl( byval Material as NewtonMaterial ptr, byval rBody1 as NewtonBody ptr, byval rBody2 as NewtonBody ptr ) as integer

		return 1

	end function


	function Character_World_Contact_Process cdecl( byval Material as NewtonMaterial ptr, byval Contact as NewtonContact ptr ) as integer


		dim as integer Contact_Surface
		dim as dFloat ConTang1, ConTang2, Mag, NormSpeed1, NormSpeed2
		dim as entity_Struct ptr character
		dim as nmathf.vec3f contact_position, contact_normal

		character = NewtonMaterialGetMaterialPairUserData( Material )
		contact_surface = NewtonMaterialGetContactFaceAttribute( Material )

		NewtonMaterialGetContactPositionAndNormal( Material, @contact_position.x, @contact_normal.x )

		'check to see if the polygon is pretty flat... if so, we can jump
		dim as integer good_pos
		if contact_normal.y >=.995 then
			character->on_ground = 1
			if character->foul = 0 then
				good_pos = 1
			end if
		end if

		'lookup the actual name of the surface we are in contact with from the global hash table
		character->contact_string = *h_table->lookup( contact_surface )->s

		'we should be able to process some scripts here...
		select case character->contact_string

			case "fairway"
				NewtonMaterialSetContactStaticFrictionCoef( material, 1.75, 0 )
				NewtonMaterialSetContactStaticFrictionCoef( material, 1.5, 1 )
				NewtonMaterialSetContactSoftness( material, .65 )
				NewtonMaterialSetContactElasticity( material, .25 )
				if good_pos <> 0 then
					character->last_good_pos = contact_position
					character->last_good_pos.y+=1
				end if

			case "walls"
				NewtonMaterialSetContactStaticFrictionCoef( material, .15, 0 )
				NewtonMaterialSetContactStaticFrictionCoef( material, .145, 1 )
				NewtonMaterialSetContactSoftness( material, .15 )
				NewtonMaterialSetContactElasticity( material, .65 )

			case "water"
				if character->foul = 0 then
					character->foul = 1
				end if

				if character->in_water = 0 then
					character->in_water = 1
					character->foul_pos = contact_position
				end if

				return 0

			case "out of bounds"
				if character->foul = 0 then
					character->foul = 2
				end if

			case "goal bottom"
				if character->win = 0 then
					if character->hit_goal = 0 then
						character->hit_goal = -1
						character->goal_time = timer+1
					else
						if timer>character->goal_time then
							character->hit_goal = 0
							character->win = -1
							character->win_time = timer+2
						end if
					end if
				end if

			case else
				NewtonMaterialSetContactStaticFrictionCoef( material, .15, 0 )
				NewtonMaterialSetContactStaticFrictionCoef( material, .145, 1 )
				NewtonMaterialSetContactSoftness( material, .65 )
				NewtonMaterialSetContactElasticity( material, .25 )

		end select

		return 1

	end function

	sub character_World_Contact_End cdecl(byval Material as NewtonMaterial ptr)

	end sub



	function Camera_World_Contact_Begin cdecl( byval Material as NewtonMaterial ptr, byval rBody1 as NewtonBody ptr, byval rBody2 as NewtonBody ptr ) as integer

		return 0

	end function


	function Camera_World_Contact_Process cdecl( byval Material as NewtonMaterial ptr, byval Contact as NewtonContact ptr ) as integer

		return 0

		dim as integer Contact_Surface
		dim as dFloat ConTang1, ConTang2, Mag, NormSpeed1, NormSpeed2
		dim as entity_Struct ptr character
		dim as nmathf.vec3f contact_position, contact_normal

		character = NewtonMaterialGetMaterialPairUserData( Material )
		contact_surface = NewtonMaterialGetContactFaceAttribute( Material )

		NewtonMaterialGetContactPositionAndNormal( Material, @contact_position.x, @contact_normal.x )

		''check to see if the polygon is pretty flat... if so, we can jump
		'dim as integer good_pos
		'if contact_normal.y >=.995 then
		'	character->on_ground = 1
		'	if character->foul = 0 then
		'		good_pos = 1
		'	end if
		'end if

		''lookup the actual name of the surface we are in contact with from the global hash table
		'character->contact_string = *h_table->lookup( contact_surface )->s

		''we should be able to process some scripts here...
		'select case character->contact_string

		'	case "fairway"
		'		NewtonMaterialSetContactStaticFrictionCoef( material, 1.75, 0 )
		'		NewtonMaterialSetContactStaticFrictionCoef( material, 1.5, 1 )
		'		NewtonMaterialSetContactSoftness( material, .65 )
		'		NewtonMaterialSetContactElasticity( material, .25 )
		'		if good_pos <> 0 then
		'			character->last_good_pos = contact_position
		'			character->last_good_pos.y+=1
		'		end if

		'	case "walls"
		'		NewtonMaterialSetContactStaticFrictionCoef( material, .15, 0 )
		'		NewtonMaterialSetContactStaticFrictionCoef( material, .145, 1 )
		'		NewtonMaterialSetContactSoftness( material, .15 )
		'		NewtonMaterialSetContactElasticity( material, .65 )

		'	case "water"
		'		if character->foul = 0 then
		'			character->foul = 1
		'		end if

		'		if character->in_water = 0 then
		'			character->in_water = 1
		'			character->foul_pos = contact_position
		'		end if

		'		return 0

		'	case "out of bounds"
		'		if character->foul = 0 then
		'			character->foul = 2
		'		end if

		'	case "goal bottom"
		'		if character->win = 0 then
		'			if character->hit_goal = 0 then
		'				character->hit_goal = -1
		'				character->goal_time = timer+1
		'			else
		'				if timer>character->goal_time then
		'					character->hit_goal = 0
		'					character->win = -1
		'					character->win_time = timer+2
		'				end if
		'			end if
		'		end if

		'	case else
		'		NewtonMaterialSetContactStaticFrictionCoef( material, .15, 0 )
		'		NewtonMaterialSetContactStaticFrictionCoef( material, .145, 1 )
		'		NewtonMaterialSetContactSoftness( material, .65 )
		'		NewtonMaterialSetContactElasticity( material, .25 )

		'end select

		return 1

	end function

	sub Camera_World_Contact_End cdecl(byval Material as NewtonMaterial ptr)

	end sub

end namespace
